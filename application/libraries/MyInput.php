<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyInput{

	public function __Construct(){
		$this->CI  = get_instance();
		require_once APPPATH.'third_party/html_purifier/HTMLPurifier.auto.php';
	}

	public function Clean($dirty_html){

		$config = HTMLPurifier_Config::createDefault();
		$config->set('Attr.AllowedClasses',array('header'));
		$config->set('AutoFormat.AutoParagraph',true);
		$config->set('AutoFormat.RemoveEmpty',true);
		$config->set('HTML.Doctype','HTML 4.01 Strict');


		$purifier = new HTMLPurifier($config);
		$clean_html = $purifier->purify($dirty_html);
		return strip_tags($clean_html);
	}

}