<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App {

    private $title;
    private $layout;
    private $active;

    public function __Construct() {
        $this->CI = get_instance();
        $this->_class = $this->CI->router->fetch_class();
        $this->_method = $this->CI->router->fetch_method();
    }

    public function getLayout() {
        return $this->layout;
    }

    public function setLayout($var) {
        $this->layout = $var;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($var) {
        $this->title = $var;
    }

     public function getActive() {
        return $this->active;
    }

    public function setActive($var) {
        $this->active = $var;
    }


    public function auth($is_api = false){
       if($is_api){
           if($this->CI->session->userdata('IS_LOGIN')!=true){
               redirect('login');
           }
       }else{
           if($_POST){
                foreach ($_POST as $key => $value) {
                  if(!is_array($_POST[$key])){
                    $_POST[$key] = is_array($key) ? $_POST[$key]: $this->CI->myinput->Clean($_POST[$key]);
                  }
                }
            }
            if($this->CI->session->userdata('IS_LOGIN')==true){
                $CONTROLLER = $this->CI->session->userdata('CONTROLLER');
                // print_r($CONTROLLER);
                $i = 0;
                foreach($CONTROLLER as $c){
                    if($this->_class==$c->controller){
                        $i++;
                    }
                }
                if($i==0){
                    // echo $i;
                    // redirect('error');
                }
            }else{
               redirect('login');
            }
       }
    }

   

    public function render($title = "VIA",$render, $data = null) {
        $this->setTitle($title);
        $view = $this->CI->load->view('public/'.$render, $data, true);
        $this->setLayout($view);
        $this->CI->load->view('templates/content', null);
    }

    public function render2($title = "VIA",$render, $data = null) {
        $this->setTitle($title);
        $view = $this->CI->load->view('public/'.$render, $data, true);
        $this->setLayout($view);
        $this->CI->load->view('templates/content_laporan', null);
    }

    public function generateBreadcrumb() {
        $ci = &get_instance();
        $i = 1;
        $uri = $ci->uri->segment($i);
        $link = '
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="'.base_url().'"> Home</a></li>';

        while ($uri != '') {
            $prep_link = '';
            for ($j = 1; $j <= $i; $j++) {
                $prep_link .= $ci->uri->segment($j) . '/';
            }

            if ($ci->uri->segment($i + 1) == '') {
                $link .= '<li class="breadcrumb-item active">';
                $link .= ucwords($ci->uri->segment($i)) . '</li> ';
            } else {
                $link .= '<li class="breadcrumb-item"><a href="#">';
                $link .= ucwords($ci->uri->segment($i)) . '</a><span class="divider"></span></li> ';
            }

            $i++;
            $uri = $ci->uri->segment($i);
        }
        $link .= '</ol>';
        return $link;
    }

    public function script(){
        $current = $this->CI->router->fetch_class();
        if(file_exists('assets/ela/scripts/'.$current.'.js')){
            return "<script type='text/javascript' src='".base_url()."assets/ela/scripts/".$current.".js'></script>";
        }
        return false;
    }

    public function get_post($post){
        $data = array();
        foreach ($post as $key => $value) {
            $data[$key] = $value;
        }
        return $data;
    }

    public function reset_search($name){
        $this->CI->session->unset_userdata($name);
    } 

    public function search_handler($name,$searchterm){
        if($searchterm){
            $this->CI->session->set_userdata($name, $searchterm);
            return $searchterm;
        }
        else if($this->CI->session->userdata($name)){
            $searchterm = $this->CI->session->userdata($name);
            return $searchterm;
        }
        else{
            $searchterm ="";
            return $searchterm;
        }
    }

    public function pagination_config($url, $uri_segment, $totalRow, $per_page) {
        $config = array();
        $config["base_url"] = base_url($url);
        $config["total_rows"] = $totalRow;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $uri_segment;
        $config['full_tag_open'] = "<ul class='pagination pagination-sm no-margin pull-right'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        return $config;
    }

   
}
