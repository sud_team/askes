<div class="header">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- Logo -->
        <div class="navbar-header">

        <!-- <a class="navbar-brand hvr-bounce-in" href="http://www.tugumandiri.com" target="_blank"> -->

            <a class="navbar-brand" href="javascript:void(0)">
                <img src="<?php echo base_url() ?>assets/ela/images/tuguicon.png" alt=" " class="logo-small nav-toggler" />
             </a>

            <a class="navbar-brand hvr-bounce-out" href="http://www.tugumandiri.com" target="_blank">
                 <span><img src="<?php echo base_url() ?>assets/ela/images/tugutext.png" alt=" " class="logo-text hidden-sm-down" /></span>
            </a>
        </div>

        <!-- End Logo -->
        <div class="navbar-collapse">
            <!-- toggle and nav items -->
            <ul class="navbar-nav  mt-md-0">
                <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            </ul>
            <ul class="navbar-nav m-auto">
                <a href="http://djpsehat.com" class="hvr-bounce-in f-s-22 font-weight-bold"><span class="text-dark">DJP</span><span class="text-info">SEHAT</span></a>
            </ul>
            <!-- User profile and search -->
            <ul class="navbar-nav my-lg-0">

                <li class="nav-item  d-none d-sm-block">
                    <a class="nav-link text-center text-primary f-s-12"  href="<?php echo base_url('karyawan/karyawan_view') ?>">
                        <!-- <span name="namaPeserta" id="namaPeserta" value=""><?php echo substr(" $member->namaPeserta",0,2); ?></span> -->
                        <span name="namaPeserta" id="namaPeserta" value=""><?php echo $member->namaPeserta; ?></span>

                    </a>
                </li>
                <!-- Profile -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle hvr-radial-out"  href="<?php echo base_url('karyawan/karyawan_view') ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="text-white f-s-18"><?php echo substr(" $member->namaPeserta",0,2); ?></span></a>
                    <div  class="dropdown-menu dropdown-menu-right animated slideInRight bg-inverse mailbox ">
                        <ul class="dropdown-user ">
                            <li class=" hidden-sm-up ">
                            <a href="#"><i class="fa fa-user" aria-hidden="true"></i> &nbsp; <?php echo $member->namaPeserta; ?> </a>
                             </li>
                            <li><a href="#"><i class="fa fa-level-up" aria-hidden="true"></i> &nbsp;
                                    <?php
                                    if ($this->session->userdata("hak_akses") == 1) {
                                        echo "Administrator";
                                    } else if ($this->session->userdata("hak_akses") == 2) {
                                        echo "Admin";
                                    } else if ($this->session->userdata("hak_akses") == 3) {
                                        echo "Management";
                                    } else if ($this->session->userdata("hak_akses") == 4) {
                                        echo "SPV";
                                    } else if ($this->session->userdata("hak_akses") == 7) {
                                        echo "Produksi";
                                    } else if ($this->session->userdata("hak_akses") == 8) {
                                        echo "Keuangan";
                                    } else if ($this->session->userdata("hak_akses") == 8) {
                                        echo "Klaim";
                                    } else {
                                        echo "User";
                                    }
                                    ?>
                                </a></li>
                            <li><a href="#" id="changePassword"><i class="fa fa-key" aria-hidden="true"></i>
                                    </i> &nbspGanti Password <br></a></li>
                            <!--                                     <li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Setting</a></li> -->
                            <li><a href="<?php echo base_url() ?>login/logout"><i class="fa fa-power-off"></i> &nbsp;Logout</a></li>

                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- End header header -->

<script language="javascript">
    //jam();

    function jam() {
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds()
        var timeValue = "" + ((hours > 23) ? hours - 24 : hours)
        timeValue = ((hours < 10) ? "0" : "") + hours
        timeValue += ((minutes < 10) ? ":0" : ":") + minutes
        timeValue += ((seconds < 10) ? ":0" : ":") + seconds
        //document.getElementById("jam").innerHTML = " " + timeValue;
        //setTimeout("jam()", 1000);
    }
</script>