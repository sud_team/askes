<?php
// header("Cache-Control: private, max-age=10800, pre-check=10800");
// header("Pragma: private");
// header("Expires: " . date(DATE_RFC822,strtotime("+1 day")));
// 
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="djpsehat, asuransi, kesehatan, dirjen pajak" name="keywords">

    <!-- HTML Meta Tags -->
    <title><?php echo $this->app->getTitle(); ?> | DJP Sehat</title>

    <meta name="description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="TM Group Medicare Plan">
    <meta itemprop="description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">
    <meta itemprop="image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="http://djpsehat.com/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo $this->app->getTitle(); ?> | TM Group Medicare Plan">
    <meta property="og:description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">
    <meta property="og:image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo $this->app->getTitle(); ?> | TM Group Medicare Plan">
    <meta name="twitter:description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">
    <meta name="twitter:image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">

    <!-- Whatsapp thumb -->
    <meta property="og:site_name" content="http://djpsehat.com/">
    <meta property="og:title" content="<?php echo $this->app->getTitle(); ?> | TM Group Medicare Plan" />
    <meta property="og:description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia" />
    <meta property="og:image" itemprop="image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-180x180.png">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />
    <!-- Meta Tags Generated via http://heymeta.com -->
    <!-- Favicons -->

    <!-- <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon"> -->

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url() ?>assets/ela/icons/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/ela/icons/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/ela/icons/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/ela/icons/icon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/ela/icons/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/ela/icons/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap Core CSS -->

    <?php $this->load->view('templates/_css'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar login-gradient">
   
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <?php $this->load->view('templates/header'); ?>
        <!-- Left Sidebar  -->
        <?php $this->load->view('templates/sidebar'); ?>
         -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-primary"><?php echo strtoupper($this->router->fetch_class()); ?></h4>
                </div>
                <div class="col-md-7 align-self-center">
                    <?php echo $this->app->generateBreadcrumb(); ?>
                </div>
            </div> -->
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <?php $success = $this->session->flashdata('success'); ?>
                <?php if ($success) : ?>
                   <?php echo $this->session->flashdata('success'); ?>
                                  
                <?php endif ?>

                <?php $warning = $this->session->flashdata('warning'); ?>
                <?php if ($warning) : ?>
                  <?php echo $this->session->flashdata('warning'); ?></h5>
                                   

                <?php endif ?>

                <?php $danger = $this->session->flashdata('danger'); ?>
                <?php if ($danger) : ?>
                  
                                        <small><?php echo $this->session->flashdata('danger'); ?></small>
                                   
                <?php endif ?>
                
                <?php echo $this->app->GetLayout(); ?>
            </div>
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->

        <div class="modal fade" id="changepass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="<?php echo base_url() ?>dashboard/updatePass" method="post" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title font-weight-bold" id="exampleModalLabel"><i class="fa fa-key f-s-14" aria-hidden="true"></i> Ganti Password </h6>
                            
                        </div>
                        <div class="modal-body ">
                            <div class="col-sm-12" id="dok">

                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-secondary f-s-12 " data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-info f-s-12 " value="Update"  >
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TEMPLATE -->
        <div id="wgt-container-template" style="display: none">
            <div class="msg-wgt-container">
                <div class="msg-wgt-header">
                    <a href="javascript:;" class="online"></a>
                    <a href="javascript:;" class="name"></a>
                    <a href="javascript:;" class="close">x</a>
                </div>
                <div class="msg-wgt-message-container">
                    <table width="100%" class="msg-wgt-message-list">
                    </table>
                </div>
                <div class="msg-wgt-message-form">
                    <textarea name="message" placeholder="Ketik pesan anda. Tekan Shift + Enter untuk alinea baru"></textarea>
                </div>
            </div>
        </div>
        <?php $this->load->view('templates/footer'); ?>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
    <!-- End Wrapper -->
    <?php $this->load->view('templates/_js'); ?>
    <?php 
    ?>
</body>

</html>