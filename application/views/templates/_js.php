<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
	var current_active = '<?php echo $this->app->getActive(); ?>';
</script>

<?php if ($this->router->fetch_class() == "produksi") : ?>
	<script type="text/javascript">
		var file02 = '<?php echo $file02 ?>';
		var file03 = '<?php echo $file03 ?>';



		function submitForm() {
			swal({
				title: 'Apakah anda yakin?',
				text: 'Data peserta sudah benar',
				icon: 'info',
				showCancelButton: true,
				buttons: {
					cancel: {
						text: 'Tidak, batalkan!',
						value: null,
						visible: true,
						className: 'btn-default',
						closeModal: true
					},
					confirm: {
						text: 'Ya, kirim!',
						value: true,
						visible: true,
						className: 'btn-info',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					$.ajax({
						url: "<?php echo site_url('produksi/submitform') ?>",
						type: "post",
						success: function() {
							$('#upload-berhasil').show(function() {
								toastr.success('Data Peserta telah di upload', 'Upload Berhasil', {
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"positionClass": "toast-top-right",
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false

								})
							});
						},
						error: function() {
							swal('gagal');
						}

					});
				} else {
					swal('Batal', 'Submit data dibatalkan', 'error')
				}
			})
		}
	</script>
 

<?php endif ?>
<!-- All Jquery -->
<script src="<?php echo base_url() ?>assets/ela/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url() ?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url() ?>assets/ela/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url() ?>assets/ela/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?php echo base_url() ?>assets/ela/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>

<script src="<?php echo base_url() ?>assets/ela/js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/owl-carousel/owl.carousel-init.js"></script>

<script src="<?php echo base_url() ?>assets/ela/js/custom.min.js"></script>

<script src="<?php echo base_url() ?>assets/ela/js/lib/fastselect/fastselect.standalone.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/fastselect/fastselect.standalone.min.js"></script>

<script src="<?php echo base_url() ?>assets/ela/js/lib/dropzone/dropzone.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/custom-file-input/custom-file-input.js"></script>

<!--Custom JavaScript -->
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/datatables.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/select2/select2.full.min.js"></script>
 
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/ela/js/lib/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/input-mask/jquery.inputmask.extensions.js"></script>

<!-- Mask Money -->
<script src="<?php echo base_url(); ?>assets/ela/js/lib/MaskMoney/jquery.maskMoney.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/MaskMoney/numeral.min.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/html5-editor/wysihtml5-init.js"></script>
<!--Tracking Bar -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/ela/js/lib/filestyle/bootstrap-filestyle.min.js"> </script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/sweetalert/sweet-alert.min.js"></script>

<script src="<?php echo base_url(); ?>assets/ela//js/lib/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela//js/lib/toastr/toastr.init.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/calc/calc.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/highchart/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/highchart/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/highchart/export-data.js"></script>


<script type="text/x-template" id="msg-template" style="display: none">
	<tbody>
        <tr class="msg-wgt-message-list-header">
            <td rowspan="2"><img src="<?php echo  base_url('assets/chat/avatar.png') ?>"></td>
            <td class="name"></td>
            <td class="time"></td>
        </tr>
        <tr class="msg-wgt-message-list-body">
            <td colspan="2"></td>
        </tr>
        <tr class="msg-wgt-message-list-separator"><td colspan="3"></td></tr>
    </tbody>
</script>


<script type="text/javascript">
	Dropzone.options.dropzoneForm = {
		maxFilesize: 10, // Mb
		init: function() {
			// Set up any event handlers
			this.on('success', function() {
				if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
					location.reload();
				}
			});
		}
	};

	function clickModal(identifier, e) {
		e.preventDefault();
		var levelPlanLock = $("#levelPlanLock").val();
		if(levelPlanLock != ""){
			$("#levelPlan").val(levelPlanLock).attr("disabled",true);;
			$('#levelPlan').select2().trigger('change');
		}
		var id = $(identifier).attr('id');
		$("#myModal").modal('show');
	}

	function splitDateFormat(dateDefaultFormat) {
	
		var dateSplit = dateDefaultFormat.split("/");
		return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
	}

	function _calculateAge(birthday) { // birthday is a date
		var d = new Date(birthday);
		var ageDifMs = Date.now() - d.getTime();
		var ageDate = new Date(ageDifMs); // miliseconds from epoch
		return Math.abs(ageDate.getUTCFullYear() - 1970);
	}

	$('.save-btn').click(function(e) {
		e.preventDefault();
		var name = $("input[name='namaLengkap[]']").val();
		var hubungan = $("select[name='hubunganKeluarga[]']").val();

		var hubunganName = "";
		if (hubungan == "E") {
			var hubunganName = "Karyawan";
		} else if (hubungan == "S") {
			var hubunganName = "Istri";
		} else if (hubungan == "C") {
			var hubunganName = "Anak2";
		}

		var plan = $("select[name='levelPlan[]'] option:selected").val();
		var ttl = $("input[name='tglLahir[]']").val();
		var age = _calculateAge(splitDateFormat(ttl));
		var jeniskelamin = $("select[name='jenisKelamin[]'] option:selected").val();
		var nikah = $("select[name='statusPernikahan[]'] option:selected").val();
		var tlp = $("input[name='noHp[]']").val();
		var incrementPeserta = 1;
		var jmlPeserta = parseInt($("input[name='jmlPeserta']").val()) + incrementPeserta;
		$("input[name='jmlPeserta']").val(incrementPeserta)
		$("#jmlPeserta").html(jmlPeserta);

		var premi = 0;
		if (plan == '800') {
			if (age <= 23) {
				premi = 1552261;
			} else if (jeniskelamin == "M" && age > 23) {
				premi = 2186368;
			} else if (jeniskelamin == "F" && age > 23) {
				premi = 2535167;
			}
		} else if (plan == '1000') {
			if (age <= 23) {
				premi = 2255305;
			} else if (jeniskelamin == "M" && age > 23) {
				premi = 3183090;
			} else if (jeniskelamin == "F" && age > 23) {
				premi = 3679042;
			}

		} else if (plan == '1500') {
			if (age <= 23) {
				premi = 3266757;
			} else if (jeniskelamin == "M" && age > 23) {
				premi = 4620180;
			} else if (jeniskelamin == "F" && age > 23) {
				premi = 5328205;
			}
		}


		var tglMulaiAsuransi = $("input[name='tglMulaiAsuransi']").val();
		$("#tglAsuransi").html(tglMulaiAsuransi);

		var tglPesan = $("input[name='tglPesan']").val();
		$("#tglPemesanan").html(tglPesan);

		var jmlPremi = parseInt($("#jmlPremiHidden").val()) + premi;
		$("#jmlPremiHidden").val(jmlPremi);
		$("#jmlPremi").html(number_format(jmlPremi));
		
		/* Level Plan Lock */
		var levelPlanLock = $("#levelPlanLock");
		if(levelPlanLock.val() == ""){
			levelPlanLock.val(plan);
		}
		var $inputHidden = "<input type='hidden' name='namaLengkapHidden[]' value='" + name + "' /><input type='hidden' name='hubunganKeluargaHidden[]' value='" + hubungan + "' /><input type='hidden' name='levelPlanHidden[]' value='" + plan + "' /><input type='hidden' name='tglLahirHidden[]' value='" + ttl + "' /><input type='hidden' name='jenisKelaminHidden[]' value='" + jeniskelamin + "' /><input type='hidden' name='statusPernikahanHidden[]' value='" + nikah + "' /><input type='hidden' name='noHpHidden[]' value='" + tlp + "' />";
		$(".data-table ").append(
			"<tbody><tr class='bg-success text-light f-s-14 m-t-25'><td class='text-light'>" + $inputHidden + "<b>Data Peserta</b></td><td class='text-light'><b>" + name + "<b></td></tr><tr><td>Hubungan</td><td class='text-info'>" +
			hubunganName + "</td></tr><tr><td>Plan</td><td class='text-info'>" + plan + "</td></tr><tr><td>TTL</td><td class='text-info'>" + ttl +
			"</td></tr><tr><td>Jenis Kelamin</td><td class='text-info'>" + jeniskelamin + "</td></tr><tr><td>Nikah</td><td class='text-info'>" + nikah +
			"</td></tr><tr><td>Telpon</td><td class='text-info'>" + tlp +
			"</td></tr><tr><td></td><td><button class='btn btn-outline-danger btn-block deleterow f-s-14'> <i class='fa fa-trash f-s-14' aria-hidden='true'></i> Hapus </button></td></tr></tbody><br><br><br>"
		);

		$('#myModal').modal('toggle');
	});


	$("body").on("click", ".deleterow", function() {
		$(this).parents("table").remove();

	});

	$('.save-btn').click(function() {

		$('.ringkasan').removeClass('d-none')


	});
</script>

<?php echo $this->app->script(); ?>