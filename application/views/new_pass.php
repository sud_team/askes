<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="description" content="Virtual Insurance Application System">
    <meta name="keywords" content="Asuransi Kredit,Bank Jatim,Agra,Askred">
    <meta name="author" content="Agra Indonesia">
    <title>New Password</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/ela/favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/logincias/logincias.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/owl.theme.default.min.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/animate.css">

    <link href="<?php echo base_url()?>assets/ela/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>assets/ela/css/helper.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/ela/css/style.css" rel="stylesheet">
    <style>
                    body {
                        background: #12c2e9;
                        /* fallback for old browsers */
                        background: -webkit-linear-gradient(to right, #f64f59, #c471ed, #12c2e9);
                        /* Chrome 10-25, Safari 5.1-6 */
                        background: linear-gradient(to right, #f64f59, #c471ed, #12c2e9);
                        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                    }
                </style>
</head>

<body class="fix-header fix-sidebar">
    <div class="header" >
        <nav class="navbar navbar-light ">
            <!-- Logo -->
            <div class="navbar-header">
                <span>
                    <img src="<?php echo base_url()?>assets/ela/images/logo.png" alt="homepage" class="dark-logo" />

                    <img src="<?php echo base_url()?>assets/ela/images/logo-text.png" alt="homepage" class="dark-logo" />
                </span>
            </div>
            <div>
            <span class="text-dark f-s-14 m-r-20">
                    <font color="red">V</font>irtual
                    <font color="red">I</font>nsurance
                    <font color="red">A</font>pplication
            </div>
        </nav>
    </div>

    <!-- <div class="container"> -->
        <div class="row">
            <div class="m-auto p-30">

                <div id="dialog" class="dialog dialog-effect-in">
                    <div class="dialog-content card">
                        <form id="login_form" class="dialog-form" action="<?php echo base_url();?>login/updatePassword" method="post" name="loginform">
                            <div class="text-center">
                                <img src="<?php echo base_url()?>assets/ela/images/logobjtm.png" alt="homepage" class="dark-logo p-b-20" />

                            </div>
                            <hr>
                            <fieldset>
                                <?php $error_message = $this->session->flashdata('warning');?>
                                     <?php if ($error_message):?>
                                        <div class="alert alert-danger  alert-dismissable">
                                            <i class="fa fa-ban"></i>
                                            <small><?php echo $this->session->flashdata('warning'); ?></small>
                                        </div>
                                <?php endif?>
                                <legend>Buat password baru untuk pengguna baru</legend>
                                <div class="form-group">
                                    <label for="user_username" class="control-label ">Password baru :</label>
                                    <input type="hidden" class="form-control" name="idUser" value="<?php echo $this->session->userdata('idUserPass')?>" />
                                    <input type="password" id="pass" class="form-control" name="password" autocomplete="off" autofocus/>
                                </div>
                                <div class="form-group">
                                    <label for="user_password" class="control-label f-s-12">Ulangi password :</label>
                                    <input type="password" id="retry" class="form-control" name="retry" autocomplete="off" autofocus>
                                </div>

                                <div class="pad-top-10">
                                    <input name="button" type="submit" id="konfirmasi" disabled class="btn btn-default btn-block btn-lg" value="Konfirmasi">
                                    <a href="<?php echo base_url()?>login" class="btn btn-default btn-flat btn-lg" style="background-color: #8e2ee9">Kembali</a>
                                </div>
                                <div class="pad-top-20">
                                    <hr>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <font color="red">
                                        <span id="jam">
                                            <script language="javascript">
                                                jam();

                                                function jam() {
                                                    var now = new Date();
                                                    var hours = now.getHours();
                                                    var minutes = now.getMinutes();
                                                    var seconds = now.getSeconds()
                                                    var timeValue = "" + ((hours > 23) ? hours - 24 : hours)
                                                    timeValue = ((hours < 10) ? "0" : "") + hours
                                                    timeValue += ((minutes < 10) ? ":0" : ":") + minutes
                                                    timeValue += ((seconds < 10) ? ":0" : ":") + seconds
                                                    document.getElementById("jam").innerHTML = " " + timeValue;
                                                    setTimeout("jam()", 1000);
                                                }
                                            </script>
                                        </span>
                                    </font>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
    
    <div class="row bg-dark p-t-20 animated  slideInUp">
        <div class="col-md-3 p-l-30">
            <div class="card-content">
                <address>
                    <strong>Head Office
                    </strong>
                    <br> Wisma Bakrie 2, Lt.10
                    <br> Jl. HR Rasuna Said, Kav B-2
                    <br> Jakarta 12920, INDONESIA
                    <br>
                    <i class="fa fa-internet-explorer"> </i> www.agraindonesia.com
                    <br>
                </address>
            </div>
        </div>
        <div class="col-md-3  p-l-30">
            <div class="card-content">
                <!-- <h6 class="card-subtitle">Use <code>address</code> for get desire address.</h6> -->
                <address>
                    <strong>Customer Service
                    </strong>
                    <br>
                    <i class="fa fa-phone"></i> +62 (21) 5793 0433
                    <br>
                    <i class="fa fa-fax"></i> +62 812 1326 2259
                    <br>
                    <i class="fa fa-envelope"></i> cs.admin@agraindonesia.com
                    <br>
                </address>
            </div>
        </div>
    </div>
   <script src="<?php echo base_url()?>assets/ela/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url()?>assets/ela/js/jquery.slimscroll.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url()?>assets/ela/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>

    <script src="<?php echo base_url()?>assets/ela/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/ela/js/lib/owl-carousel/owl.carousel-init.js"></script>

    <script src="<?php echo base_url()?>assets/ela/js/custom.min.js"></script>

    <script type="text/javascript">
        $("#retry").keyup(function(){
            var pass = $("#pass").val();
            var retry = $("#retry").val();
            if(retry==pass){
                $("#konfirmasi").removeAttr("disabled","false");
            }
        });
    </script>


</body>

</html>