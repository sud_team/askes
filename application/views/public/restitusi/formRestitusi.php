<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card ">
            <!-- Data Debitur -->
            
            <div class="card-outline-fade2 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Data Debitur
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Pinjaman : <?php echo $NomorPinjaman?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                    <div class="form-body">
                        <div class="table-responsive data-toggle">
                            <table id="my-lg-0 " class="display narrow table-hover table-sm f-s-14" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">CIF :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->cif;?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Nama Debitur :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NamaDebitur;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkadKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Akhir Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkhirKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Lahir :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglLahir);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Usia :</td>
                                        <td class="text-dark w-25"><?php echo str_replace('.',' Tahun ',$debitur->UsiaDebitur)." Bulan";?></td>
                                        <td class="w-3"></td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Kategori :</td>
                                        <td class="text-dark w-25"><?php echo cekPekerjaan($debitur->KodePekerjaan);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Status :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->TcKet;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tenor :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->TenorTahun." Tahun (".$debitur->TenorBulan." Bulan)";?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Plafon :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->plafon);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <?php if ($debitur->KodePekerjaan=="BJ"): ?>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tahun Ke :</td>
                                        <td class="text-dark w-25"><?php echo restitusiBj($debitur->NomorRegistrasi)->TahunKe;?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Premi :</td>
                                        <td class="text-dark w-25"><?php echo price(restitusiBj($debitur->NomorRegistrasi)->ospremi);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>    
                                    <?php else: ?>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Premi :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->premi);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Total Premi :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->JumlahPremiTenor);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <?php endif ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Asuradur :</td>
                                        <td class="text-dark w-25"><?php echo cekNamaAsdur($debitur->kodeAsuransi)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Polis :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NomorPolis?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Bank :</td>
                                        <td class="text-dark w-25"><?php echo cekNamaBank($debitur->kodeBank)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Polis :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglPolis)?></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                
            </div>
            <!-- Data Debitur -->
            <!-- Form Restitusi -->
            <form class="form-horizontal  col-md-12" role="form" action="<?php echo base_url()?>restitusi/saveRestitusi" method="post" enctype="multipart/form-data">    
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Form Restitusi
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Restitusi : RS<?php echo $debitur->NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body" role="form">
                <div class="form-body">
                    <div class="table-responsive">
                        <input type="hidden" name="KodeRestitusi" value="RS<?php echo $debitur->NomorRegistrasi?>">
                       <input type="hidden" name="NomorRegistrasi" id="noreg" value="<?php echo $debitur->NomorRegistrasi?>">
                        <input type="hidden" name="TenorBulan" id="TenorBulan" value="<?php echo $debitur->TenorBulan?>">
                        <input type="hidden" name="TglAkadKredit" id="TglAkadKredit" value="<?php echo date_format(date_create($debitur->TglAkadKredit),'d-m-Y')?>">
                        <input type="hidden" name="JumlahPremiTenor" id="JumlahPremiTenor" value="<?php echo $debitur->JumlahPremiTenor?>">
                        <table id="my-lg-0" class="display narrow table-sm f-s-14 col-md-12" cellspacing="0" width="100%">
                            <tbody>

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Tanggal Pelunasan </td>
                                    <td class="w-30 ">
                                        <div class="col-xs-4 has-warning">
                                            <input type="text" class="form-control f-s-12 datepicker bg-white" name="tglpelunasan" id="tglpelunasan" value="<?php  if(!empty($pinjaman->TglPelunasanRestitusi)){ echo date_format(date_create($pinjaman->TglPelunasanRestitusi),'d-m-Y'); }?>" readonly>
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Pengembalian Premi </td>
                                    <td class="w-30 ">
                                        <div class="col-xs-4 has-warning">
                                            <span id="premi"><b class="price">Rp. <?php if(!empty($pinjaman->PengembalianPremi)){ echo price($pinjaman->PengembalianPremi);}?></b></span>
                                            <input type="hidden" name="premi" id="valPremi" value="<?php if(!empty($pinjaman->PengembalianPremi)){ echo $pinjaman->PengembalianPremi;}?>">
<!--                                             <input type="text" class="form-control f-s-12" name="tglresiko" id="tglresiko" value="<?php// if(!empty($pinjaman->TglResiko)){ echo date_format(date_create($pinjaman->TglResiko),'d/m/Y'); }?>">
                                            <small class="form-control-feedback"> Tangal Terjadinya Risiko </small> -->
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Periode pinjaman berjalan </td>
                                    <td class="w-30 ">
                                        <div class="col-xs-4 has-warning">
                                            <span id="periode"><b><?php if(!empty($pinjaman->BulanBerjalan)){ echo $pinjaman->BulanBerjalan;}?></b></span>
                                            <input type="hidden" name="periode" id="valPeriode" value="<?php if(!empty($pinjaman->BulanBerjalan)){ echo $pinjaman->BulanBerjalan;}?>">
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Sisa periode pinjaman </td>
                                    <td class="w-30">
                                        <div class="col-xs-4 has-warning">
                                            <span id="sisa"><b><?php if(!empty($pinjaman->SisaBulan)){ echo $pinjaman->SisaBulan;}?></b></span>
                                            <input type="hidden" name="sisa" id="valSisa" value="<?php if(!empty($pinjaman->SisaBulan)){ echo $pinjaman->SisaBulan;}?>">
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/span-->
                    </div>
                </div>

            </div>
            <!-- Form Restitusi -->
            <!-- Dokumen Restitusi -->
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Dokumen Restitusi
                        <span class="m-l-10 m-t-10 m-b-10 text-right">No. Restitusi RS<?php echo $debitur->NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <style type="text/css">
               /*  
                input[type="file"] {
                    display: none;
                } */
            </style>
            <div class="card-body">
                <!-- Dokumen Umum -->
                <h5 class="box-title text-center "> Upload Dokumen </h5>
                <div class="table-responsive">
                    <table id="" class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%">
                        <tbody>
                            <?php if (!empty($resdok->result())){ ?>
                                <?php foreach ($resdok->result() as $value): ?>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-50"><?php echo $value->dokumen;?></td>
                                    <td align="right">
                                        <div class="form-actions p-2">
                                            <label class="btn btn-outline-primary btn-sm">
                                                <a target="_blank" href="<?php echo base_url()?>upload/restitusi/<?php echo $value->NamaFile?>"> 
                                                <i class="fa fa-eye"></i> Lihat <?php echo $value->NamaFile?></a>
                                            </label>
                                        </div>
                                    </td>
                                </tr>    
                                <?php endforeach ?>
                            <?php } else { ?>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen1"></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="fileumum1" >
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen2"></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="fileumum2" >
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen3"></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="fileumum3" >
                                        </label>
                                    </div>
                                </td>
                            </tr> 
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
                <div class="col-lg-3">&nbsp;</div>
                <div class="form-actions p-2 pull-left">
                    <a href="<?php echo base_url()?>debitur/cari" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>
                </div>
                <div class="form-actions p-2 pull-right">
<!--                     <button type="submit" class="btn  btn-danger btn-sm">
                        <i class="fa fa-close"></i> Hapus </button> -->
                    <button type="submit" class="btn  btn-warning btn-sm">
                        <i class="fa fa-paper-plane "></i> Ajukan </button>
<!--                     <button type="submit" class="btn  btn-warning btn-sm">
                        <i class="fa fa-check"></i> Setujui </button> -->
                </div>
            </div>
            </form>
        </div>
    </div>

</div>