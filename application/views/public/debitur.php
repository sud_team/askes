<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Cari Data Pesera</h4>
              <h6 class="card-subtitle">Untuk menampilkan detail data Peserta termasuk proses pengajuan Klaim dan Restitusi</h6>
<!-- Menu Pencairan -->
            <form id="FormSubmit" method="post">
              <div class="col-md-7">
					<div class="row m-b-10">
						<div class="text-left text-info col-sm-5">
							<small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
							<select class="select2  custom-select f-s-12" id="cabang" name="cabang">
                               <option value=""></option>
                               <option value="semua">Semua</option>
							</select>
                        </div>
                        <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="select2  custom-select f-s-12" >
                                <option value=""></option>
                                <option value="semua">Semua</option>
                            </select>
                        </div>
                    </div>

                    <div class="row m-b-10">
                    <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Level Planning  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="select2  custom-select f-s-12" name="produk" id="produk">
								<option value=""></option>
								<option value="700">700</option>
								<option value="900">900</option>
								<option value="1400">1400</option>
                            </select>
                        </div>
                        <div class="text-left text-info col-sm-5">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">No. Peserta</small>
                            <input type="text" placeholder="No. Peserta" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Periode upload dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai">
                        </div>
                        <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir">
                        </div>
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cek" class="btn btn-info f-s-12 text-light">
                                <i class="fa fa-search"></i> Cari
                            </a>
                        </div>
                    </div>
              </div>
            </form>
<!-- Menu Pencairan Akhir -->

              <div class="table-responsive">
                  <table id="debitur" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
							<tr>
								 <th class="w-3">#</th>
								 <th>Tipe Transaksi</th>
								 <th class="w-6">No Peserta</th>
								 <th class="w-6">No. Pegawai Induk</th>
								 <th>Hubungan</th>
								 <th>Tanggal Mulai Asuransi</th>
								 <th>Level Plan</th>
								 <th>Nama Peserta</th>
								 <th class="w-6">Nama Karyawan</th>
								 <th>Jenis Kelamin</th>
								 <th>Nama Bank</th>
								 <th>No Rekening Bank</th>
								 <th>Cabang Bank</th>
								 <th class="w-6">Status Pernikahan</th>
								 <th>Email</th>
								 <th>HP</th>
								 <th>Keterangan</th>
							</tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="tracking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Tracking Debitur</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12 p-l-30 p-r-30">
              <div class="row text-left">
                  <lable class="col-sm-6 control-label text-info font-weight-bold">Data Debitur</lable>
              </div>
              <div class="col-xs-12" id="dok">
                          
              </div>
          </div>
          <div class="modal-footer ">
              <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
