<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Favicon icon -->
  <title>INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</title>
  <link href="<?php echo base_url()?>assets/ela/css/lib/invoice/invoice.css" rel="stylesheet">
</head>

<body class="body-invoice" onload="window.print()">

  <header class="clearfix">
    <div id="logo" ><img src="<?php echo base_url()?>assets/ela/images/Logo-Agra.png"></div><div id="alamat" >Wisma Bakrie 2, Lt.10 <br>Jl. HR Rasuna Said, Kav B-2 Karet 
        <br> Setiabudi Jakarta Selatan 12920
        <br> Telp. : +62 (21) 5793 0433
        <br> Fax : +62 (21) 5793 0432</div>
    </header>
      <h1 style="color: #fff">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</h1>
    <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>#</th>
            <th>ASURANSI</th>
            <th>DEBITUR</th>
            <th>TOTAL PREMI</th>
        </tr>
      </thead>
        <tbody>
        <?php 
        $totaldebitur2 = 0;
        $totalpremi = 0;
        $no=1 ; 
        foreach ($totaldebitur  as $value) { ?>
          <tr>
              <td class="no"><?php echo $no;?></td>
              <td class="pinjaman"><?php echo cekNamaAsdur($value->kodeAsuransi);?></td>
              <td class="cif" style="text-align: center;"><?php echo $value->deb;?></td>
              <td class="nama"  style="text-align: right;"><?php echo price($value->premi);?></td>
            </tr>
        <?php 
        $no++;
        $totaldebitur2 = $totaldebitur2+$value->deb;
        $totalpremi = $totalpremi+$value->premi;
        } ?>
        </tbody>
      </table>
      <div id="notices">
        <div><strong>NOTICE</strong>:</div>
        <div class="notice">Bukti pembayaran sah pendaftaran peserta Asuransi Kredit Bank Jatim</div>
      </div>
      <div id="total">
        <h1 style="color: #fff">Rincian Pembayaran</h1>
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>Tanggal Pembayaran</td>
            <td style="text-align:right;"><?php echo my_date_indo($tgl);?></td>
          </tr>
          <tr>
            <td>Jumlah Debitur</td>
            <td style="text-align:right;"><?php echo $totaldebitur2;?></td>
          </tr>
          <tr>
            <td>Total Premi</td>
            <td style="text-align:right;"><?php echo price($totalpremi);?></td>
          </tr>
        </table>
      </div>
      <br>
      <div id="paraf" class="" >
        <img style="width:150px;" src="<?php echo base_url()?>assets/ela/images/mtstagi.png">
        <div><strong>Sumitro Sitorus</strong></div>
        <div>Direktur Utama</div>
      </div>
    <footer style="margin-bottom: 80px;">
      <div style="text-align:center;">Invoice ini dicetak secara elektronik dan valid dengan tanda tangan dan stempel.</div>
    </footer>
  <div style="page-break-before:always;">
    <header class="clearfix">
    <div id="logo" ><img src="<?php echo base_url()?>assets/ela/images/Logo-Agra.png"></div><div id="alamat" >Wisma Bakrie 2, Lt.10 <br>Jl. HR Rasuna Said, Kav B-2 Karet 
        <br> Setiabudi Jakarta Selatan 12920
        <br> Telp. : +62 (21) 5793 0433
        <br> Fax : +62 (21) 5793 0432</div>
    </header>
      <h1 style="color: #fff">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</h1>
    <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>#</th>
          <th>NO. PINJAMAN</th>
          <th>DEBITUR</th>
          <th>PEKERJAAN</th>
          <th>TGL AKAD</th>
          <th>TENOR</th>
          <th>ASURANSI</th>
          <th>BANK</th>
          <th>PREMI</th>
        </tr>
      </thead>
      <tbody><?php 
        foreach ($debitur as $key){
        if($key->KodePekerjaan=="BJ"){
          $JumlahPremiTenor = $this->laporan->OsPremi($key->NomorPinjaman);
        }else{
          $JumlahPremiTenor = $key->JumlahPremiTenor;
        }?>
          <tr>
            <td class="no"><?php echo $key->rank;?></td>
            <td class="pinjaman"><?php echo $key->NomorPinjaman;?></td>
            <td class="cif"><?php echo $key->NamaDebitur;?></td>
            <td><?php echo cekPekerjaan($key->KodePekerjaan);?></td>
            <td class="tglk"><?php echo my_date_indo($key->TglAkadKredit);?></td>
            <td class="tenor"><?php echo $key->TenorTahun;?></td>
            <td><?php echo cekNamaAsdur($key->kodeAsuransi);?></td>
            <td><?php echo cekNamaBank($key->kodeBank);?></td>
            <td class="plafon"><?php echo price($JumlahPremiTenor);?></td>
          </tr>
          <?php }?>
        </tbody>
    </table>
  </div>
  </body>
</html>


       <!--  $mpdf->SetHTMLFooter('<div style="text-align:center;">Invoice ini dicetak secara elektronik dan valid dengan tanda tangan dan stempel.</div>');


       //generate the PDF from the given html
    $mpdf->WriteHTML($html);
    $mpdf->AddPage();
    $mpdf->WriteHTML($html2);

    $mpdf->Output('invoice_<?php //echo $tgl.<?php echo pdf','D');   -->