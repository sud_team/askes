<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">Invoice</h4>
              <div class="table-responsive">
                  <table id="invoice" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>#</th>
                              <th>TANGGAL UPLOAD</th>
                              <th>TOTAL DEBITUR</th>
                              <th>TOTAL PREMI</th>
                              <th>AKSI</th>
                          </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
