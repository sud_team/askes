<script>

 $('#listPeserta').DataTable({
		dom: 'Bfrtip',
        buttons: [
            'excel'
        ],	 
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"peserta/datatables",
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
    });
</script>