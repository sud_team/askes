<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body  m-l-20">
                    <h3 class="card-title m-b-0">Data Peserta</h3>
                    <p class="m-b-0">Detail Data peserta yang ikut</p>
                </div>
            </div>
            <div class="card-body">


                <!-- <h4 class="card-title">Cetak Data Pembayaran</h4>
                <h6 class="card-subtitle">Untuk mencetak Klaim dan Restitusi</h6> -->

                <!-- Menu Pencairan -->
                
                <div class="table-responsive">
                    <table id="listPeserta" class="display nowrap table-striped f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr  class="bg-primary text-light">
                                <th class="bg-primary text-light">No</th>
                                <th class="bg-primary text-light">No.Registrasi</th>
                                <th class="bg-primary text-light">Nama Peserta</th>
                                <th class="bg-primary text-light">Nama Karyawan</th>
                                <th class="bg-primary text-light">Status</th>
                                <th class="bg-primary text-light">Jenis Kelamin</th>
                                <th class="bg-primary text-light">Level Plan</th>
                                <th class="bg-primary text-light">Status Pernikahan</th>
                                <th class="bg-primary text-light">Tanggal Mulai Asuransi</th>
                                <th class="bg-primary text-light">Tanggal Lahir</th>
                                <th class="bg-primary text-light">Premi</th>
								<th class="bg-primary text-light">No.Peserta</th>                          
                            </tr>
                        </thead>
						<tbody>
							
						</tbody>
                    </table>
                </div>
                <!-- TABEL  (ditampilkan setelah di klik pencarian (tampilkan) -->

            </div>
        </div>
    </div>
</div>