<div class="row">
    <div class="col-12">
        <div class="card">
        <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa  fa-file-archive-o f-s-40 color-warning"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Klaim</h3>
                        <p>Histori pengajuan klaim meliputi detail klaim, progres klaim dan pembayaran klaim</p>
                    </div>
                </div>
            </div>

            <ul>
				<?php foreach($klaim AS $k=>$v){ ?>
                <li>
                    <div class="card">
                        <div class="card-header p-l-0 p-t-0">
                            No. <b class="text-warning"> <?php echo $v->noKlaim; ?></b><br>
                            Tanggal <b class="text-warning"><?php echo date("d/m/Y",strtotime($v->tglDibuat)); ?></b>
                        </div>
                        <div class="card-body m-t-10">
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-lg-3">
                                    <dl>

                                         <dt>Nama Peserta</dt>
                                        <dd><?php echo $v->namaPeserta; ?></dd>

                                        <dt>Status Klaim</dt>
                                        <dd>
                                            <div class="badge badge-warning f-s-12"><?php echo $v->status; ?></div>
                                        </dd>
                                        <dt>Keputusan Klaim</dt>
                                        <dd>
											<?php if($v->keputusanKlaim == 'disetujui'){ ?>
												<div class="badge badge-info f-s-12">Disetujui</div>
											<?php }elseif($v->keputusanKlaim == 'ditolak'){ ?>
												<div class="badge badge-danger f-s-12">Ditolak</div>												
											<?php }else{ ?>
												<div class="badge badge-danger f-s-12">-</div>
											<?php }?>
                                        </dd>
                                        <dt>Nominal Pembayaran</dt>
                                        <?php if(empty($v->nominalDisetujui)){ ?>
											<dd>-</dd>
										<?php }else{ ?>
											<dd><?php echo $v->nominalDisetujui ?></dd>									
										<?php } ?>
                                    </dl>
                                    <div class="button-list"><button class="btn btn-outline-warning  f-s-12">
                                            Detail
                                        </button>
                                        <a class="btn btn-warning f-s-12 pull-right" data-toggle="collapse" href="#KLM123123" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Tracking Klaim
                                        </a></div>
                                </div>

                                <div class="col-md-12 col-lg-9 ">
                                    <div class="collapse" id="KLM123123">
                                        <div class="form-group text-center p-t-20">
                                            <h6>Progres Status Klaim</h6>
                                        </div>
                                        <hr>
                                        <ul class="SteppedProgress">
                                            <li class="complete"><span> Diajukan <code>28/07/2019</code> </span>
                                            </li>
                                            <li class="complete"><span> Verifikasi Broker
                                                    <code>28/07/2019</code></span>
                                            </li>
                                            <li class="complete boxed"><span> Penangguhan
                                                    <code>28/07/2019</code></span>
                                            </li>
                                            <li class="complete"><span> Penilaian <code>28/07/2019</code></span>
                                            </li>
                                            <li class=""><span> Pembayaran <code>28/07/2019</code></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Keterangan class:
                                        1. complete proses sedang dilakukan belum proses/submit ke proses selanjutnya
                                        2. complete boxed proses sedang dalam proses
                                    -->
                            </div>
                        </div>
                    </div>

                </li>
				<?php } ?>
              
            </ul>


        </div>
    </div>
</div>
</div>