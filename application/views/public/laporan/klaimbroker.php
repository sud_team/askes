<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
            <h4 class="card-title">Klaim</h4>
            <h6 class="card-subtitle">Laporan Rincian Pengajuan Klaim per sesuai periode yang ditentukan</h6>
            <!-- Panel Utama -->
            <form id="FormSubmit" method="post">
            <div class="row">
                <!-- Menu Pencairan -->
                <div class="col-md-7">
                  <input type="hidden" id="link" value="filter_rekonsel">
                    
                    <div class="row m-b-10">
                    <div class="text-left text-info col-sm-5">
                            
                        </div>
                        <div class="text-left text-info col-sm-5">
                        </div>
                    </div>
                    
                    <div class="row m-b-20">
                    <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai" required>
                        </div>
                        <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir" required>
                        </div>
                        
                    </div>
					 <div class="row m-b-20">
                    <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Status</small>
                           <select class=" select2 f-s-12" style="width:100%" name="produk" id="produk">
								<option value="">Semua</option>
                                <option value="800">Pengajuan Klaim</option>
                                <option value="1000">Verifikasi</option>
                              </select>
                        </div>
                        
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cari" class="btn btn-info f-s-12 text-light">
                            <i class="fa fa-search"></i> Tampilkan
                            </a>
                        </div>
                    </div>
                </div>
              </form>
               
            </div>
            </form>
              <div class="table-responsive">
                  <!-- <a class="btn btn-xs btn-primary" id="validasi" style="color: #ffffff">Validasi</a> -->
                  <table id="laporan_klaim" class="display nowrap table-striped f-s-12" cellspacing="0" width="100%" cellspacing="0" width="100%">
                      <thead >
                          <tr class="bg-primary text-light">
                              <th class="bg-primary text-light">No</th>
                              <th class="bg-primary text-light">No.Klaim</th>
                              <th class="bg-primary text-light">Nama Rumah Sakit</th>
                              <th class="bg-primary text-light">Nomor Telepon</th>
                              <th class="bg-primary text-light">Tanggal Berobat</th>
                              <th class="bg-primary text-light">Biaya Pengobatan</th>
                              <th class="bg-primary text-light">Alamat RS</th>
                              <th class="bg-primary text-light">Kondisi Pasien</th>
                              <th class="bg-primary text-light">Diagnosa Sakit</th>
                              <th class="bg-primary text-light">Status</th>
                              <th class="bg-primary text-light">Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
                       
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>