<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	          	<div class="table-responsive" id="abc">                
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr >
	            				<th></th>
	            				<th>KANTOR CABANG</th>
	            				<th>KANTOR OPERASIONAL</th>
	            				<th>ASURADUR / PENJAMIN</th>
	            				<th>NO. AKAD KREDIT</th>
	            				<th>PEKERJAAN</th>
	            				<th>DEBITUR</th>
	            				<th>TGL. LAHIR</th>
	            				<th>MULAI ASURANSI</th>
	            				<th>TENOR</th>
	            				<th>AKHIR ASURANSI</th>
	            				<th>STATUS</th>
	            				<th>UANG PERTANGGUNGAN</th>
	            				<th>RATE PREMI &permil;</th>
	            				<th>PREMI</th>
	            				<?php if (($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==3) || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
	            				<th>FEE BANK</th>
	            				<?php endif ?>
	            				<?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
	            				<th>BREOKERAGE</th>
	            				<?php endif ?>
	            				<?php if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
	            				<th>NET PREMI</th>
	            				<?php endif ?>
	            			</tr>
	            		</thead>

	            			<tbody>
	            				<?php foreach ($debitur as $r): ?>
	            					<?php 
	            						$bangpem = bangpem($r->kodeBank);
								    	if($bangpem->capem==NULL){
								    		$bangpem->capem = $bangpem->cabang;
								    	}
								    ?>
							    	<tr>
							    	<td></td>
							    	<td><?php echo $bangpem->cabang?></td>
							    	<td><?php echo $bangpem->capem?></td>
							    	<td><?php echo cekNamaAsdur($r->kodeAsuransi)?></td>
							    	<td><?php echo $r->NomorPK?></td>
							    	<td><?php echo cekPekerjaan($r->KodePekerjaan)?></td>
							    	<td><?php echo $r->NamaDebitur?></td>
							    	<td><?php echo my_date_indo($r->TglLahir)?></td>
							    	<td><?php echo my_date_indo($r->TglAkadKredit)?></td>
							    	<td><?php echo $r->TenorBulan?></td>
							    	<td><?php echo my_date_indo($r->TglAkhirKredit)?></td>
							    	<td><?php echo $r->TcKet?></td>
							    	<td><?php echo price($r->plafon)?></td>
							    	<td><?php echo ($r->TarifPremi*1000)?></td>
							    	<td><?php echo price($r->JumlahPremiTenor)?></td>
							    	<?php if (($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==3) || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
							    	<td><?php echo price($r->FeePremiBank)?></td>
							    	<?php endif ?>
							    	<?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
							    	<td><?php echo price($r->FeePremiBroker)?></td>
							    	<?php endif ?>
							    	<?php if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
							    	<td><?php echo net_premi($r->premi,$r->FeePremiBank,$r->FeePremiBroker)?></td>
							    	<?php endif ?>
							    	</tr>
	            				<?php endforeach ?>
	            			</tbody>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>