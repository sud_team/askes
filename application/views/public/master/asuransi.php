<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Asuransi</h4>
                <div class="nav-item dropdown mega-dropdown">
                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-info btn-sm" data-toggle="dropdown" href="#">Tambah Asuransi
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="dropdown-menu animated zoomIn alert alert-info">
                        <div class="card-title text-center">
                            <h4>Entry data Asuransi</h4>
                        </div>
                        <form action="<?php echo base_url()?>master/saveAsuransi" class="form-valide" method="post">
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10">Kode Angka
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Kode Angka" name="angka" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Singkatan Asuransi
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Kode Asuransi" name="kode" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Asuransi
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Nama Asuransi" name="asuransi" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Fee Broker
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Fee Broker (0.x)" name="feebroker" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Fee Bank
                                    <span class="text-danger">*</span>
                                </label> 
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Fee Bank (0.x)" name="feebank" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 m-t-20">
                                    <button class="btn btn-info btn-sm sweet-success"  type="submit">Simpan
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--TABEL-->
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th>Kode</th>
                                <th>Singkatan</th>
                                <th>Asuransi</th>
                                <th>Fee Broker</th>
                                <th>Fee Bank</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($asuransi as $key): ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td style="text-align: center;"><?php echo $key->kodeAngka;?></td>
                                <td style="text-align: center;"><?php echo $key->kodeAsuransi;?></td>
                                <td style="text-align: center;"><?php echo $key->asuransi;?></td>
                                <td style="text-align: center;"><?php echo $key->FeeBroker;?></td>
                                <td style="text-align: center;"><?php echo $key->FeeBank;?></td>
                                <td><a class="btn btn-xs btn-info" style="color:#ffffff;" onclick="editAsuransi('<?php echo $key->kodeAngka;?>')">Edit</a></td>
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Rate Premi Pekerjaan</h4>
                <div class="nav-item dropdown mega-dropdown">
                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-info btn-sm" data-toggle="dropdown" href="#">Tambah Rate Premi
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="dropdown-menu animated zoomIn alert alert-info">
                        <div class="card-title text-center">
                            <h4>Entry rate premi</h4>
                        </div>
                        <form action="<?php echo base_url()?>master/saveRatePremi" class="form-valide" method="post">
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10">Kode Rate Premi
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Kode Rate Premi" name="kode" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Nama Rate Premi
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Nama Rate Premi" name="namaratepremi" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Rate Premi
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12 price" placeholder="Masukan Rate Premi" name="rate" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Produk
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <select class="js-example-basic-cabang col-sm-12 custom-select f-s-12" id="produk" name="produk">
                                        <option value="">Semua</option>
                                        <?php foreach ($produk as $as): ?>
                                        <option value="<?php echo $as->idProduk?>"><?php echo $as->NamaProduk?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 m-t-20">
                                    <button class="btn btn-info btn-sm sweet-success"  type="submit">Simpan
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--TABEL-->
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th>Kode Rate Premi</th>
                                <th>Premi</th>
                                <th>Rate Premi</th>
                                <!-- <th>Produk</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($rate as $key): ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td style="text-align: center;"><?php echo $key->kodePremi;?></td>
                                <td style="text-align: center;"><?php echo $key->NamaPremi;?></td>
                                <td style="text-align: center;"><?php echo $key->PremiTahun;?></td>
                                <?php /* <td style="text-align: center;"><?php echo $key->idProduk;?></td> */ ?>
                                <td><a class="btn btn-xs btn-info" style="color:#ffffff;" onclick="editRate('<?php echo $key->kodePremi;?>')">Edit</a></td>
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="asuransi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Asuransi</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <form method="post" action="<?php echo base_url()?>master/editAsuransiSave">
          <div class="modal-body f-s-12 p-l-30 p-r-30">
              <div class="row text-left">
                  <lable class="col-sm-6 control-label text-info font-weight-bold">Data Asuransi</lable>
              </div>
              <div class="col-xs-12" id="dok">
              </div>
          </div>
          <div class="modal-footer ">
                <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info btn-sm">Simpan</button>
          </div>
          </form>
      </div>
  </div>
</div>

<div class="modal fade" id="ratepremi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Rate Premi</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12 p-l-30 p-r-30">
              <div class="row text-left">
                  <lable class="col-sm-6 control-label text-info font-weight-bold">Rate Premi</lable>
              </div>
              <div class="col-xs-12" id="dok1">
                          
              </div>
          </div>
          <div class="modal-footer ">
              <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>