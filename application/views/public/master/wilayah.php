<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pengaturan User</h4>
                <h6 class="card-subtitle">Untuk menambahkan, merubah, menghapus dan aktifasi data user</h6>
                <div class="nav-item dropdown mega-dropdown">
                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-info btn-sm" data-toggle="dropdown" href="#">Buat User Baru
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="dropdown-menu animated zoomIn alert alert-info">
                        <div class="card-title text-center">
                            <h4>Entry data user</h4>
                        </div>
                        <form action="#" class="form-valide" method="post">
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-username">Username
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" id="val-username" name="val-username" placeholder="Masukan a username.." type="text">
                                </div>
                                <label class="col-form-label w-15 p-10" for="val-email">Email
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" id="val-email" name="val-email" placeholder="Masukan alamat email" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-password">N.I.K.
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" id="val-nik" name="val-nik" placeholder="Masukan N.I.K." type="text">
                                </div>
                                <label class="col-form-label w-15 p-10" for="val-phoneus">Kontak
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" id="val-phoneus" name="val-phoneus" placeholder="Masukan nomor telpon" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-suggestions">Wilayah
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <select class="form-control f-s-12" id="val-select2" name="val-select2">
                                        <option value="cab1">
                                            Surabaya
                                        </option>

                                        <option value="cab2">
                                            Malang
                                        </option>

                                        <option value="cab3">
                                            Jember
                                        </option>

                                    </select>
                                </div>
                                <label class="col-form-label w-15 p-10" for="val-select2">KCP
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <select class="form-control f-s-12">

                                        <optgroup label="Surabaya">

                                            <option value="cp1">
                                                KCP Juanda
                                            </option>
                                            <option value="cp2">
                                                KCP Pogot
                                            </option>
                                            <option value="cp3">
                                                KCP Darmo
                                            </option>
                                            <option value="cp3">
                                                Semua KCP Surabaya
                                            </option>
                                        </optgroup>
                                        <optgroup label="Malang">

                                            <option value="cp4">
                                                KCP Lawang
                                            </option>
                                            <option value="cp5">
                                                KCP Tumpang Malang
                                            </option>
                                            <option value="cp6">
                                                KCP Dampit
                                            </option>
                                            <option value="cp3">
                                                Semua KCP Malang
                                            </option>
                                        </optgroup>
                                        <optgroup label="Jember">

                                            <option value="cp7">
                                                KCP Kalisat
                                            </option>
                                            <option value="cp8">
                                                KCP Tanggul
                                            </option>
                                            <option value="cp9">
                                                KCP Kencong
                                            </option>
                                            <option value="cp3">
                                                Semua KCP Jember
                                            </option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10 " for="">Role</label>
                                <div class="col-s-12 has-success w-75">
                                    <select class="form-control f-s-12">
                                        <option value="Marketing"> Marketing</option>
                                        <option value="Staff"> Staff</option>
                                        <option value="Supervissor"> Supervissor</option>
                                        <option value="Kepala Cabang"> Kepala Cabang</option>
                                        <option value="Asuransi"> Asuransi</option>
                                        <option value="Broker"> Broker</option>
                                        <option value="Administrator"> Administrator</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-statususer">Status
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <select class="form-control f-s-12" id="val-statususer" name="val-statususer">
                                        <option value="cab1">
                                            Aktif
                                        </option>
                                        <option value="cab1">
                                            Non-aktif
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 m-t-20">
                                    <button class="btn btn-info btn-sm sweet-success"  type="submit">Simpan
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label class="col-form-label float-right p-r-15 ">Tanggal pembaharuan
                                        <span class="text-dark">dd/mm/yyyy</span>
                                    </label>
                                    <label class="col-form-label float-right  p-r-15">Tanggal pembuatan
                                        <span class="text-dark">dd/mm/yyyy</span>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--TABEL-->
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th class="w-15">Nama</th>
                                <th class="w-15">Username</th>
                                <th class="w-15">WIlayah</th>
                                <th class="w-15">KCP</th>
                                <th class="w-7">Tgl Pembaharuan</th>
                                <th class="w-15">Status</th>
                                <th>Role</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class=" text-center w-5" scope="row">1</td>
                                <td>Kolor Tea </td>
                                <td>
                                    <span class="">Shirt For Man</span>
                                </td>
                                <td class="color-primary">$21.56</td>
                                <td>Kantor Cabang</td>
                                <td class="color-primary">$21.56</td>
                                <td class="color-primary">$21.56</td>
                                <td>
                                    <span class="badge badge-warning">Aktif</span>
                                </td>
                                <td>$21.56</td>

                            </tr>
                            <tr>
                                <td class=" text-center w-5" scope="row">2</td>
                                <td>Kolor Tea Shirt For Women</td>
                                <td>
                                    <span class="">For Women</span>
                                </td>
                                <td class="color-primary">$21.56</td>
                                <td>Kantor Cabang</td>
                                <td class="color-primary">$21.56</td>
                                <td class="color-primary">$21.56</td>
                                <td>
                                    <span class="badge badge-default">Non-Aktif</span>
                                </td>
                                <td>$21.56</td>
                            </tr>
                            <tr>
                                <td class=" text-center w-5" scope="row">3</td>
                                <td>Blue Backpack For Baby</td>
                                <td>
                                    <span class="">For Baby</span>
                                </td>
                                <td class="color-primary">$21.56</td>
                                <td>Kantor Cabang</td>
                                <td class="color-primary">$21.56</td>
                                <td class="color-primary">$21.56</td>
                                <td>
                                    <span class="badge badge-warning">Aktif</span>
                                </td>
                                <td>$21.56</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>