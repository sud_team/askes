<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Term Condition</h4>
                <div class="nav-item dropdown mega-dropdown">
                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-info btn-sm" data-toggle="dropdown" href="#">Tambah TC
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="dropdown-menu animated zoomIn alert alert-info">
                        <div class="card-title text-center">
                            <h4>Entry data TC</h4>
                        </div>
                        <form action="<?php echo base_url()?>master/saveTc" class="form-valide" method="post">
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10">Usia Minimum
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Usia Minimum" name="usia_minimum" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Usia Maksimal
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Usia Maksimal" name="usia_maksimal" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10">Uang Minimum
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12 price" placeholder="Masukan Uang Minimum" name="uang_minimum" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Uang Maksimal
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12 price" placeholder="Masukan Uang Maksimal" name="uang_maksimal" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10">Batas Usia Pinjaman
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Batas Usia Pinjaman" name="batas_usia_maksimal" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Keterangan
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <select class="form-control f-s-12 js-example-basic-multiple" style="width: 100%;" name="keterangan[]" multiple="multiple">
                                        <option value="">Pilih beberapa Ctrl+Klik</option>
                                        <?php foreach ($keterangan as $key): ?>
                                        <option value="<?php echo $key->KeteranganTc?>"><?php echo $key->KeteranganTc?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-8 m-t-20">
                                    <button class="btn btn-info btn-sm sweet-success"  type="submit">Simpan
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--TABEL-->
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th>Kode Pekerjaan</th>
                                <th>Usia Min</th>
                                <th>Usia Max</th>
                                <th>Batas Umur Pinjaman</th>
                                <th>Uang Min</th>
                                <th>Uang Max</th>
                                <th>Keterangan</th>
                                <th><i class="fa fa-edit"></i>

</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($tc as $key): ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td style="text-align: center;"><?php if($key->KodePekerjaan=="BJ" || $key->KodePekerjaan=="45"){ echo cekPekerjaan($key->KodePekerjaan);}else{ echo "Pegawai";}?></td>
                                <td style="text-align: center;"><?php echo $key->umur1;?></td>
                                <td style="text-align: center;"><?php echo $key->umur2;?></td>
                                <td style="text-align: center;"><?php echo $key->umurMax;?></td>
                                <td><?php echo price($key->uang1);?></td>
                                <td><?php echo price($key->uang2);?></td>
                                <td><?php echo $key->keterangan;?></td>
                                <td><a class="btn btn-info btn-xs" style="color: #ffffff">Edit</a></td>
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>