<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Data Mutasi Debitur</h4>
              <div class="table-responsive">
                  <!-- <a class="btn btn-xs btn-primary" id="validasi" style="color: #ffffff">Validasi</a> -->
                  <table id="filter_DataMutasi" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th></th>
                              <th>Nomor Registrasi</th>
                              <th>No.Pinjaman</th>
                              <th>ID Debitur</th>
                              <th>Nama</th>
                              <th>Tgl Lahir</th>
                              <th>Pekerjaan</th>
                              <th>Tgl Kredit</th>
                              <th>Tgl Akhir</th>
                              <th>Tenor Bulan</th>
                              <th>Plafon</th>
                              <th>Status Pinjaman</th>
                              <th>Bank</th>
                              <th>User Hapus</th>
                              <th>Tgl Hapus</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>


