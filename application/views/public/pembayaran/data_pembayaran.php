<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-credit-card f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Pembayaran</h3>
                        <p>Histori pemesanan dan pembayaran Premi Asuransi (RP)</p>
                    </div>
                </div>
            </div>
            <div class="card-body m-t-20">
				<?php foreach($invoices AS $invoice){ ?>
                <ul>
                    <li>
                        <div class="card">
                            <div class="card-header p-l-0 p-t-0">
                                No. Inv <b class="text-info"><?php echo $invoice->noInvoice; ?></b><br> <span class="text-info"><?php echo date("d/m/Y",strtotime($invoice->created_at)); ?></span>
                            </div>
                            <div class="card-body m-t-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <dl>
                                           
                                            <dt>Jumlah Peserta</dt>
                                            <dd><?php echo $invoice->jmlPeserta; ?> Orang</dd>
                                            <dt>Premi</dt>
                                            <dd>Rp. <?php echo price($invoice->jmlPremi); ?></dd>
                                            
                                            <dt>Tanggal Mulai Asuransi</dt>
                                            <dd><?php echo date("d/m/Y",strtotime($invoice->tglMulaiAsuransi)); ?></dd>
                                        </dl>
                                    </div>
                                    <div class="col-md-6">
                                        <dl>

                                            <dt>Status Pembayaran</dt>
                                            <dd>
												<?php if($invoice->status_bayar == "belum_bayar"){ ?>
                                            		<div class="badge badge-danger animated fadeIn infinite f-s-12">Belum Bayar</div>												
                                                
											    <?php }elseif($invoice->status_bayar == "validasi"){ ?>
														<div class="badge badge-info f-s-12">Proses Validasi</div>
                                         		<?php }elseif($invoice->status_bayar == "sudah_lunas"){ ?>
													<div class="badge badge-warning animated fadeIn infinite f-s-12">Sudah Lunas</div>
												<?php } ?>
                                            </dd>
                                            <dt>Tanggal Pembayaran</dt>
                                            <dd><?php echo $invoice->tglBayar; ?>  </dd>
                                            <dt> <div class=" justify-content-end">
									<?php if($invoice->status_bayar == "sudah_lunas"){ ?>
                                        <div class="button-list"><a href="<?php echo base_url("pembayaran/detail_pembayaran/".$invoice->idInvoice); ?>" class="btn btn-outline-warning btn-block  f-s-12">
                                                Detail 
                                            </a>
                                        </div>
									<?php }else{ ?>
										<div class="button-list"><a href="<?php echo base_url("pembayaran/konfirmasi_pembayaran/".$invoice->idInvoice); ?>" class="btn btn-outline-danger btn-block  f-s-12">
                                                Konfirmasi 
                                            </a>
                                        </div>
									<?php } ?>
                                    </div></dt>
                                        </dl>
                                    </div>
                                    <!-- <div class="col-md-6 justify-content-end">
									<?php if($invoice->status_bayar == "sudah_lunas"){ ?>
                                        <div class="button-list"><a href="<?php echo base_url("pembayaran/detail_pembayaran/".$invoice->idInvoice); ?>" class="btn btn-outline-info btn-block  f-s-12">
                                                Detail 
                                            </a>
                                        </div>
									<?php }else{ ?>
										<div class="button-list"><a href="<?php echo base_url("pembayaran/konfirmasi_pembayaran/".$invoice->idInvoice); ?>" class="btn btn-outline-info btn-block  f-s-12">
                                                Konfirmasi 
                                            </a>
                                        </div>
									<?php } ?>
                                    </div> -->
                                </div>
                            </div>
                    </li>
                </ul>
				<?php } ?>
               
            </div>
            
        </div>
    </div>
</div>