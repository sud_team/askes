<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-credit-card f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Pembayaran</h3>
                        <p>Histori pemesanan dan pembayaran Premi Asuransi (RP)</p>
                    </div>
                </div>
            </div>

            <div class="card-body">

                <!-- <h4 class="card-title">Cetak Data Pembayaran</h4>
                <h6 class="card-subtitle">Untuk mencetak Klaim dan Restitusi</h6> -->

                <!-- Menu Pencairan -->
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="debitur" class="display nowrap table-striped" width="100%">
                                <thead>
                                    <tr class="bg-primary text-light">
                                        <th class="bg-primary text-light">No.Invoice</th>
                                        <th class="bg-primary text-light">Peserta</th>

                                        <th class="bg-primary text-light">Tgl. Asuransi</th>
                                        <th class="bg-primary text-light">Premi</th>
                                        <th class="bg-primary text-light">Status Pembayaran</th>
                                        <!-- <th class="bg-primary text-light">Detail</th> -->
                                    </tr>
                                </thead>
                                <tbody class="text-sm-center">


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- TABEL  (ditampilkan setelah di klik pencarian (tampilkan) -->

            </div>
        </div>
    </div>
</div>