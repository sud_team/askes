<div class="row justify-content-center">
    <div class="col-lg-6  ">

        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-credit-card f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3 class="m-b-0">Konfirmasi Pembayaran</h3>
                        <p class="m-b-0">Upload bukti transaksi pembayaran Premi</p>
                    </div>
                </div>
            </div>

            <!-- Data Debitur -->
            <!-- Form Klaim (bikin errro=align-self-center) -->
            <form class="form-horizontal " role="form" method="post" enctype="multipart/form-data">

                <div class="card-body" role="form">
                    <div class="col-12">
                        <p class="p-t-20 p-b-0">Ringkasan Pemesanan Anda</p>
                        <div class="form-body">
                            <input type="hidden" name="KodeKlaim" value="KLM<?php echo $NomorRegistrasi ?>">
                            <input type="hidden" name="NomorRegistrasi" value="<?php echo $NomorRegistrasi ?>">
                            <div class="row  p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Nama Akun</span>
                                <span class="col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo $member->namaPeserta; ?>
                                </span>
                            </div>
                            <div class="row p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Tanggal Pemesanan</span>
                                <span class=" col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo date("d/m/Y", strtotime($invoice->created_at)); ?>
                                </span>
                            </div>
                            <div class="row  p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> No Invoice</span>
                                <span class="col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo $invoice->noInvoice; ?>
                                </span>
                            </div>
                            <div class="row  p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Service Plan</span>
                                <span class=" col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo $order->levelPlan; ?>
                                </span>
                            </div>
                            <div class="row  p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Jumlah Peserta</span>
                                <span class="col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo $order->jml_peserta; ?>
                                </span>
                            </div>
                            <div class="row p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Tanggal Mulai Asuransi</span>
                                <span class=" col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo date("d/m/Y", strtotime($invoice->tglMulaiAsuransi)); ?>
                                </span>
                            </div>
                            <div class="row  p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Nominal Tagihan</span>
                                <span class="col-sm-6 col-xs-6 text-danger font-weight-bold"><?php echo number_format($invoice->jmlPremi); ?>
                                </span>
                            </div>
                            <div class="row p-b-5">
                                <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Invoice</span>
                                <span class="col-sm-6 col-xs-6 "><a class="btn btn-sm btn-outline-danger" href="<?php echo base_url('pembayaran/invoice/' . $invoice->filename_invoice) ?>"> <i class="fa fa-download"></i> Download </a>
                                </span>

                            </div>
                            <div class="row ">
                                <div class="col-sm-6">
                                    <label class="control-label">Tanggal Transfer / Transaksi</label>
                                    <input type="hidden" name="noInvoice" id="noInvoice" value="<?php echo $invoice->noInvoice; ?>" required>
                                    <input type="text" class="form-control datepicker-konfirm" placeholder="dd/mm/yyyy" name="tglBayar" id="tglBayar" required>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Upload Bukti Transaksi</label>
                                    <input type="file" name="filenameBuktiTransfer" id="file-1" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                    <label for="file-1" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file JPG, BMP atau PDF Max.2MB&hellip;</span></label>
                                </div>
                            </div>



                            <small class="text-danger hvr-grow"> *Status Pembayaran diupdate Maksimal 24 jam setelah melakukan konfirmasi pembayaran </small>
                            <hr>

                            <div class="row p-t-20">
                                <div class="col-sm-6 offset-sm-6">

                                    <button type="submit" name="submit" value="submit" class="btn btn-outline-danger btn-block f-s-12 pull-right">
                                        <i class="fa fa-paper-plane "></i> Kirim </button>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

        </div>
        </form>
    </div>
</div>

</div>