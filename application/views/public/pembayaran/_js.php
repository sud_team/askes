<script>
	Dropzone.options.dropzoneForm2 = {
		maxFilesize: 15, // Mb
		init: function () {
			myDropZone = this;
			
			$("#submit").click(function (e) {
				e.preventDefault();
				$("#LoadingImage").show();
				myDropZone.processQueue();
			});
			this.on('sending', function(file, xhr, formData){
				$("#LoadingImage").show();
				formData.append("tglBayar", $("#tglBayar").val());
				formData.append("noInvoice", $("#noInvoice").val());
			});	
			
			this.on("totaluploadprogress", function(progress) {
				console.log(progress + "%");
			});
			
			this.on("queuecomplete", function(progress) {
				console.log(progress + "%");
			});
			//Set up any event handlers
			this.on('success', function(){
				$("#LoadingImage").hide();
				if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
					location.reload();
				}
			});	
			
		}
	};
</script>