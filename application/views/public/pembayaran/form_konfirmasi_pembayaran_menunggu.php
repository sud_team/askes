<div class="row justify-content-center">
    <div class="col-lg-6">

        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-check-circle f-s-40 color-primary"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3 class="m-b-0">Detail Pemesanan</h3>
                        <p class="m-b-0">Rincian Transaksi Pemesanan </p>
                    </div>
                </div>
            </div>

            <!-- Data Debitur -->
            <!-- Form Klaim (bikin errro=align-self-center) -->
            <form class="form-horizontal " role="form" method="post">

                <div class="card-body" role="form">
                     <p class="p-t-20 p-b-0">Deatil Pemesanan Anda</p>
                     <div class="form-body">
                        <input type="hidden" name="KodeKlaim" value="KLM<?php echo $NomorRegistrasi ?>">
                        <input type="hidden" name="NomorRegistrasi" value="<?php echo $NomorRegistrasi ?>">
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Nama Akun</span>
                            <span class="col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo $member->namaPeserta; ?>
                            </span>
                        </div>
                        <div class="row p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Tanggal Pemesanan</span>
                            <span class=" col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo date("d/m/Y",strtotime($invoice->created_at)); ?>
                            </span>
                        </div>
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> No Invoice</span>
                            <span class="col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo $invoice->noInvoice; ?>
                            </span>
                        </div>
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Service Plan</span>
                            <span class=" col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo $order->levelPlan; ?>
                            </span>
                        </div>
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Jumlah Peserta</span>
                            <span class="col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo $order->jml_peserta; ?>
                            </span>
                        </div>
                        <div class="row p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Tanggal Mulai Asuransi</span>
                            <span class=" col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo date("d/m/Y",strtotime($invoice->tglMulaiAsuransi)); ?>
                            </span>
                        </div>
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Nominal Premi</span>
                            <span class="col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo number_format($invoice->jmlPremi); ?>
                            </span>
                        </div>
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Status</span>
                            <span class="col-sm-6 col-xs-6"><span class="badge badge-warning f-s-12 animated fadeIn infinite"> Rekonsel </span>
                            </span>
                        </div>
                        <div class="row  p-b-5">
                            <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Tanggal Pembayaran</span>
                            <span class="col-sm-6 col-xs-6 text-info font-weight-bold"><?php echo number_format($invoice->jmlPremi); ?>
                            </span>
                        </div>
                       
                        <div class="row p-b-15">
                        <span class=" text-sm-right p-r-0 col-sm-6 col-xs-6"> Bukti Transaksi</span>
                            <span class="col-sm-6 col-xs-6 "><a class="btn btn-sm btn-outline-info"> <i class="fa fa-download"></i> Download </a>
                            </span>
                           
                        </div>
                        <small class="text-danger hvr-grow"> *Pembayaran telah kami terima. Status pembayaran akan diupdate maksimal 24 jam</small>

                    </div>

                </div>



        </div>
        </form>
    </div>
</div>

</div>