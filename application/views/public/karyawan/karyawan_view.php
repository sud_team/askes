<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Data Karyawan</h3>
                        <p>Lengkapi data anda untuk menggunakan seluruh fasilitas aplikasi DJP Sehat </p>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <!-- <h4 class="card-title">Data Akun Peserta </h4>
                <p>Lengkapi data anda untuk menggunakan seluruh fasilitas aplikasi TM Group Medicare Plan</p>
                <br> -->
                <br>
                <form id="FormSubmitDataPeserta" method="post" class="form-horizontal form-bordered">
                    <div class="row">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">

                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">NIP</span>
                                <span class=" col-md-7 text-info"><?php echo $member->nip; ?></span>
                                <!--<small class="text-danger">Wajib diisi</small>-->
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Hubungan</span>
                                <span class=" col-md-7 text-info" name="hubungan">Employee / Karyawan
                                </span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Nama Lengkap</span>
                                <span class=" col-md-7 text-info" name="namaPeserta"><?php echo $member->namaPeserta; ?>
                                </span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Jenis Kelamin</span>
                                <span class=" col-md-7 text-info"><?php echo $member->jenisKelamin; ?></span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Tempat Lahir</span>
                                <span class=" col-md-7 text-info"><?php echo $member->tempatLahir; ?></span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Tanggal Lahir</span>
                                <span class=" col-md-7 text-info"><?php echo date("d/m/Y", strtotime($member->tglLahir)); ?></span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Status Pernikahan</span>
                                <span class=" col-md-7 text-info"><?php echo $member->statusPernikahan; ?></span>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Email</span>
                                <span class=" col-md-7 text-info"><?php echo $this->session->userdata("username"); ?></span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">No. Telpon</span>
                                <span class=" col-md-7 text-info"><?php echo $member->noHp; ?></span>
                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Alamat Lengkap</span>
                                <span class=" col-md-7 text-info"><?php echo $member->alamat; ?></span>

                            </div>
                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Nama Bank</span>
                                <span class=" col-md-7 text-info"><?php echo $member->namaBank; ?></span>
                            </div>

                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">No. Rekening Bank</span>
                                <span class=" col-md-7 text-info"><?php echo $member->noRekeningBank; ?></span>
                            </div>

                            <div class="form-group row">
                                <span class=" text-md-right col-md-4">Nama Pemilik Rekening</span>
                                <span class=" col-md-7 text-info"><?php echo $member->namaPemilikBank; ?></span>
                            </div>

                        </div>

                        <div class=" col-md-12">
                            <div class="button-list">
                                <a href="<?php echo base_url('karyawan/karyawan_form') ?>" class="btn btn-outline-danger f-s-12 edit-peserta pull-right"> <i class="fa fa-pencil f-s-14"> </i> Ubah Data </a>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
            <hr>
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-users f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Peserta DJP Sehat Terdaftar</h3>
                        <p>Detail peserta DJP Sehat yang telah didaftarkan untuk proses klaim dan lainya </p>
                    </div>
                </div>
            </div>

            <div class="card-body m-t-20">
                <ul>
                    <li>
                        <?php foreach ($peserta as $m) { ?>
                            <div class="card">
                                <div class="card-header p-l-0 p-t-0">
                                    <h5><b class="text-info"><?php echo $m->namaPeserta; ?></b></h5>
                        </div>
                                
                                <div class="card-body m-t-10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <dl>
                                               
                                                <dt>Jenis Kelamin</dt>
                                                <dd><?php echo $m->jenisKelamin; ?></dd>
                                                <dt>Hubungan</dt>
                                                <dd><?php echo $m->hubunganKeluarga; ?></dd>
                                                <dt>Status Peserta</dt>
                                                <dd>
                                                    <?php if ($m->status_bayar == 'sudah_lunas') { ?>
                                                        <div class="badge badge-info f-s-12">Aktif</div>
                                                    <?php } else { ?>
                                                        <div class="badge badge-danger f-s-12">Belum Aktif</div>
                                                    <?php } ?>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="col-md-6">
                                            <dl>
                                                <dt>No. Registrasi</dt>
                                                <dd><?php echo $m->noRegistrasi; ?></dd>
                                                <dt>No. Peserta</dt>
                                                <dd><?php echo (!empty($m->noPeserta) ? $m->noPeserta : "-"); ?></dd>
                                                <dt>Periode Asuransi</dt>
                                                <dd><?php echo date("d/m/Y", strtotime($m->tglMulaiAsuransi)); ?> s/d <?php echo date("d/m/Y", strtotime($m->tglAkhirAsuransi)); ?></dd>
                                            
                                            </dl>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="button-list"><a href="<?php echo base_url("peserta/listdetailpeserta/" . $m->idPeserta); ?>" class="btn btn-outline-info btn-block  f-s-12">
                                                    Detail
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>