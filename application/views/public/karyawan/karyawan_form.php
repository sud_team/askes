<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Data Karyawan</h3>
                        <p>Lengkapi data anda untuk menggunakan seluruh fasilitas aplikasi DJP Sehat </p>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <!-- <h4 class="card-title">Data Akun Peserta </h4>
                <p>Lengkapi data anda untuk menggunakan seluruh fasilitas aplikasi TM Group Medicare Plan</p>
                <br> -->
                <br>
                <form id="FormSubmitDataPeserta" method="post" class="form-horizontal form-bordered">
                    <div class="row">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">

                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">NIP</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="NIP" name="nip" id="nip" value="<?php echo $member->nip; ?>" readonly class="form-control">
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Hubungan</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="hubungan" name="hubungan" id="hubungan" value="Employee / Karyawan" readonly class="form-control">
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Nama Lengkap</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Sesuai Identitas" name="namaPeserta" id="namaPeserta" value="<?php echo $member->namaPeserta; ?>" class="form-control">
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Jenis Kelamin</label>
                                <div class="col-md-7">
                                    <select style="width: 100%" class="select2 form-control" id="jenisKelamin" name="jenisKelamin" <?php echo $member->jenisKelamin; ?> required>
                                        <option value="">-Pilih-</option>
                                        <option value="M" <?php echo ($member->jenisKelamin == "M" ? "selected" : ""); ?>>Laki-laki</option>
                                        <option value="F" <?php echo ($member->jenisKelamin == "F" ? "selected" : ""); ?>>Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Tempat Lahir</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Tempat Lahir" name="tempatLahir" id="tempatLahir" value="<?php echo $member->tempatLahir; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Tanggal Lahir</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker-ttl" placeholder="dd/mm/yyyy" value="<?php echo date("d/m/Y", strtotime($member->tglLahir)); ?>" name="tglLahir" id="tglLahir" required>
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Status Pernikahan</label>
                                <div class="col-md-7">
                                    <select style="width: 100%" class="select2 form-control" id="statusPernikahan" name="statusPernikahan">
                                        <option value="">-Pilih-</option>
                                        <option value="nikah" <?php echo ($member->statusPernikahan == "nikah" ? "selected" : ""); ?>>Nikah</option>
                                        <option value="belum_nikah" <?php echo ($member->statusPernikahan == "belum_nikah" ? "selected" : ""); ?>>Belum Nikah</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Email</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Surat Elektronik" readonly name="" id="" value="<?php echo $this->session->userdata("username"); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">No. Telpon</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="HP / Telepon" name="noHp" id="noHp" value="<?php echo $member->noHp; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Alamat Lengkap</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Sesuai identitas" name="alamat" id="" value="<?php echo $member->alamat; ?>" class="form-control">
                                </div>
                            </div>
                            <input type="hidden" id="link" value="filter_prapen">
                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Nama Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Bank" name="namaBank" id="namaBank" value="<?php echo $member->namaBank; ?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">No. Rekening Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="noRekeningBank" id="noRekeningBank" value="<?php echo $member->noRekeningBank; ?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Nama Pemilik Rekening</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="namaPemilikBank" id="namaPemilikBank" value="<?php echo $member->namaPemilikBank; ?>" class="form-control">
                                </div>
                            </div>




                            <!-- <div class="form-group row">
                                <label class="control-label text-md-right col-md-4">Level Plan</label>
                                <div class="col-md-7">
                                   <select style="width: 100%" class="select2 form-control" id="levelPlan" name="levelPlan">
                                        <option value="">-Pilih-</option>
                                        <option value="800" <?php echo ($member->levelPlan == "800" ? "selected" : ""); ?>>800</option>
                                        <option value="1000" <?php echo ($member->levelPlan == "1000" ? "selected" : ""); ?>>1000</option>
                                        <option value="1500" <?php echo ($member->levelPlan == "1500" ? "selected" : ""); ?>>1500</option>
                                    </select>
                                </div>
                            </div>
						 -->

                        </div>
                        <div class="col-md-12">
                            <div class="button-list">
                                <button type="submit" style="display:none" name="submit" value="submit" id="submitData">Simpan<i class="fa fa-save-o f-s-14"></i></button>
                                <button type="button" name="submit" value="submit" class="btn btn-outline-info f-s-12 simpan-peserta pull-right">Simpan<i class="fa fa-save-o f-s-14"></i></button></div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>