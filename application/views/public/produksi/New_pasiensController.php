<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Pasiens;
use App\Models\NewPasiens;
use App\Models\Propinsi;
use App\Models\Kabupaten;
use App\Models\Fasyankes;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Jenis_kelamin;
use App\Models\Kelompok_pasien;
use App\Models\Cara_bayar;
use App\Models\Agama;
use App\Models\Goldar;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use App\Models\Stmarital;
use App\Models\Wilayah_kerja;
use App\Models\Kunjungan;

use Yajra\Datatables\Datatables;
use Session;
use DB;
use Flash;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Crypto;
use FormatCustom;

class New_pasiensController extends Controller
{
    public function __construct(){
	   $this->middleware('auth');
       parent::__construct();
    }	 
	 
    public function index()
    {
		$check_duplicate_nik_hbsag = DB::table('mst_pasiens_temp')
			->select(array('nm_lengkap','nik',DB::raw('COUNT(*) AS jml'),DB::raw('"Tanggal Screening HBsAg" AS name_type')))
			->where('fasyankes_login',Auth::user()->mst_fasyankes_id)
			->whereRaw('(tgl_screening_hbsag IS NOT NULL AND tgl_screening_hbsag != "0000-00-00")')
			->groupBy('nik','tgl_screening_hbsag')
			->having(DB::raw('COUNT(CONCAT(nik,tgl_screening_hbsag))'), '>', 1)->get();
		
		$check_duplicate_nik_hiv = DB::table('mst_pasiens_temp')
			->select(array('nm_lengkap','nik',DB::raw('COUNT(*) AS jml'),DB::raw('"Tanggal Screening HIV" AS name_type')))
			->where('fasyankes_login',Auth::user()->mst_fasyankes_id)
			->whereRaw('(tgl_screening_hiv IS NOT NULL AND tgl_screening_hiv != "0000-00-00")')
			->groupBy('nik','tgl_screening_hbsag')
			->having(DB::raw('COUNT(CONCAT(nik,tgl_screening_hiv))'), '>', 1)->get();
		
		$check_duplicate_nik_sifilis = DB::table('mst_pasiens_temp')
			->select(array('nm_lengkap','nik',DB::raw('COUNT(*) AS jml'),DB::raw('"Tanggal Screening Sifilis" AS name_type')))
			->where('fasyankes_login',Auth::user()->mst_fasyankes_id)
			->whereRaw('(tgl_screening_sifilis IS NOT NULL AND tgl_screening_sifilis != "0000-00-00")')
			->groupBy('nik','tgl_screening_hbsag')
			->having(DB::raw('COUNT(CONCAT(nik,tgl_screening_sifilis))'), '>', 1)->get();
			
		
		$check_duplicate_nik = array_merge_recursive($check_duplicate_nik_hbsag,$check_duplicate_nik_hiv,$check_duplicate_nik_sifilis);
		
		$data['fasyankes'] = Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();		
		$title = 'Tambah Pasien Wilayah '.(isset($data['fasyankes']->nama_fasyankes) ? $data['fasyankes']->nama_fasyankes : '') ;
		
		$pasiens = NewPasiens::select(['local_id','mst_pasiens_id', 'nm_lengkap','alamat','nik','wilayah_kerja','tgl_screening_hbsag','nama_kelurahan'])
			->join('mst_wilayah_kerja','mst_wilayah_kerja.wilayah_kerja_id', '=', 'mst_pasiens_temp.wilayah_kerja_id')
			->join('mst_kelurahan','mst_kelurahan.mst_kelurahan_id', '=', 'mst_pasiens_temp.kelurahan_form')
			->where('fasyankes_login',Auth::user()->mst_fasyankes_id)
			->orderBy('mst_pasiens_temp.created_at','desc')->get();
		
		return View('newpasiens.index', compact('title','data','pasiens','check_duplicate_nik'));
    }
	
	/**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
	 
    public function listData()
    {		
		$Pasiens = NewPasiens::select(['local_id','mst_pasiens_id', 'nm_lengkap','alamat','nik','wilayah_kerja','tgl_screening_hbsag','nama_kelurahan'])
			->join('mst_wilayah_kerja','mst_wilayah_kerja.wilayah_kerja_id', '=', 'mst_pasiens_temp.wilayah_kerja_id')
			->join('mst_kelurahan','mst_kelurahan.mst_kelurahan_id', '=', 'mst_pasiens_temp.kelurahan_form')
			->where('fasyankes_login',Auth::user()->mst_fasyankes_id)
			->orderBy('mst_pasiens_temp.created_at','desc');
			
		return Datatables::of($Pasiens)
			->addColumn('action', function ($Pasiens) 
				{												
					return '<a href='.url("new_pasiens/edit/".$Pasiens->local_id).' class="btn btn-xs btn-primary ><i class="glyphicon glyphicon-edit"></i> Edit</a> |'
						   .'<a onClick="return confirmbutton(\'new_pasiens/delete/'.$Pasiens->local_id.'\')" href="javascript:;" class="btn btn-xs btn-danger "><i class="glyphicon glyphicon-trash"></i> Delete</a>'
						   ;
				})
		->make(true);
	}
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$data['fasyankes'] = Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();	
		$data['kelompok_risiko'] = DB::table('mst_kelompok_risiko')->orderBy('ordered','ASC')->get();
		
		$title = 'Tambah Pasien Wilayah '.$data['fasyankes']->nama_fasyankes;
		return View('newpasiens.create', compact('title','data'));
    }
	
	 public function export()
    {
		$data['fasyankes'] = Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();		
		$title = 'Tambah Pasien Wilayah '.$data['fasyankes']->nama_fasyankes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$input = $request->all();		
		$created_by = json_encode(['id'=>Auth::user()->id,'role_id'=>Auth::user()->role_id,'email'=>Auth::user()->email,'mst_propinsi_id'=>Auth::user()->mst_propinsi_id,'mst_propinsi_id'=>Auth::user()->mst_propinsi_id,'mst_fasyankes_id'=>Auth::user()->mst_fasyankes_id]);
		$now = Carbon::now();
		
		$input['created_by']= $created_by;
		$input['created_at']= $now;
		
		$input['status_gpa']= $input['status_gpa1'].'#'.$input['status_gpa2'].'#'.$input['status_gpa3'];
				
		$rules = array(
			'nik'	=> 'Required',
		);
		
		$pesan = array(
			'nik.required'	=> 'NIK Wajib diisi.',				
		);
		
		$valid = Validator::make($input, $rules, $pesan);
		
		if( $valid->passes()) 
		{			
			$last_id = NewPasiens::all()->last();
			$kode_fasyankes = $input['fasyankes_login'];
			$id_last_local = isset($last_id) ? $last_id->local_id + 1 : 1;
			$input_id=$kode_fasyankes.date('YmdHis').$id_last_local.$input['jk_id'];
			
			if($input['jml_bayi'] > 0){
				$input_bayi = array();
				for($i=1;$i<=$input['jml_bayi'];$i++){
					$input_bayi[] = array(
						'ordered' => $i,
						'pasien_register' => $input_id,
						'tgl_hb0' => (isset($input['tgl_hb0'][$i]) ? $input['tgl_hb0'][$i] : '') ,
						'tgl_hb1' => (isset($input['tgl_hb1'][$i]) ? $input['tgl_hb1'][$i] : ''),
						'tgl_hb2' => (isset($input['tgl_hb2'][$i]) ? $input['tgl_hb2'][$i] : ''),
						'tgl_hb3' => (isset($input['tgl_hb3'][$i]) ? $input['tgl_hb3'][$i] : ''),
						'tgl_hbig' =>(isset($input['tgl_hbig'][$i]) ? $input['tgl_hbig'][$i] : ''),
						'tgl_hbsag_anak' => (isset($input['tgl_hbsag_anak'][$i]) ? $input['tgl_hbsag_anak'][$i] : ''),
						'status_hbsag' => (isset($input['status_hbsag'][$i]) ? $input['status_hbsag'][$i] : ''),
						'tgl_antihbs' => (isset($input['tgl_antihbs'][$i]) ? $input['tgl_antihbs'][$i] : ''),
						'hasil_antihbs' => (isset($input['hasil_antihbs'][$i]) ? $input['hasil_antihbs'][$i] : ''),
					);	
				}
				
				DB::table('mst_pasiens_temp_baby')->insert($input_bayi);				
			}
			$Pasiens = NewPasiens::create(array_merge($input,array('mst_pasiens_id'=>$input_id)));
			
			Session::flash('message', 'Pasiens '.$Pasiens->nm_lengkap.' berhasil di tambah');
        	return redirect(route('new_pasiens'));
			
		}
		else 
		{ 			
			$input['from_change']=true;
			$pasiens = (object)$input;
			$lists['propinsi'] 			= Propinsi::where('mst_propinsi_id', '=', $pasiens->propinsi_id)->first();
			$lists['kota'] 				= Kabupaten::where('mst_kabupaten_id', '=', $pasiens->kota_id)->first();
			$lists['kecamatan']			= Kecamatan::where('mst_kecamatan_id',$pasiens->kecamatan_id)->first();
			$lists['kelurahan'] 		= Kelurahan::where('mst_kelurahan_id', $pasiens->kelurahan_id)->first();
			$lists['jenis_kelamin'] 	= Jenis_kelamin::where('jk_id', $pasiens->jk_id)->first();
			$lists['pekerjaan'] 		= Pekerjaan::where('pekerjaan_id', $pasiens->pekerjaan_id)->first();
			$lists['pendidikan'] 		= Pendidikan::where('pendidikan_id', $pasiens->pendidikan_id)->first();
			$lists['wilayah_kerja']		= Wilayah_kerja::where('wilayah_kerja_id', '=', $pasiens->wilayah_kerja_id)->first();
			$lists['fasyankes'] 		= Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();		
			$title = 'Add Pasiens Wilayah '.$lists['fasyankes']->nama_fasyankes;			
			return View('pasiens.edit', compact('pasiens','title','lists'))->withErrors($message);
		}
	} 
	
	public function download_data()
	{		
		# Rule export data
		/*
			1. $export['Reg_lroa'] = $export -> variable , ['Reg_lroa'] -> nama table ( disesuaikan table dan data yg akan di export dan upload ke server )
			2. data yg diambil tanpa id yg autoincrement ( ex : tanpa Reg_lroa_id / Reg_lroa_list_id )
			3. untuk file name di $datafile setelah nam harus menggunakan tanda '-' (ex : 'filename' => 'register_lroa-'.$rpt_no )
				karena nanti ketika di upload akan di split dan dibaca rpt_no dan path 
				( path yg dimaksud akan otomatis di tambahkan di controller dan mengikuti directory dan subdirectory pada $datafile)
			4. untuk data dalam reportat json dan di enkrip ( ex : Crypto::encrypt(json_encode($export)) )
			5. tambah button di list header & script di routes
		*/
		$type = "";	
		if(Auth::user()->mst_fasyankes_id)
		{ 
			$type = 'fasyankes';
		}
		else if(Auth::user()->mst_kabupaten_id)
		{
			$type = 'kabupaten';
		}
		else if(Auth::user()->mst_propinsi_id)
		{
			$type ='propinsi';
		}
		else{$type = 'pusat';}
		if(Auth::user()->mst_fasyankes_id)
		{
			$where = array();
			if(Auth::user()->mst_propinsi_id){ $where['propinsi_login'] = Auth::user()->mst_propinsi_id;}
			if(Auth::user()->mst_kabupaten_id){ $where['kabupaten_login'] = Auth::user()->mst_kabupaten_id;}
			if(Auth::user()->mst_fasyankes_id){ $where['fasyankes_login'] = Auth::user()->mst_fasyankes_id;}
			if($type){ $where['type'] = $type;}			
		}
		
		$export['filter'] = $where ;
		$export['absensi'] = array('data_individu');
		$export['filter_absensi'] = array(
			'laporan'=>'data_individu',
			'mst_fasyankes_id' => Auth::user()->mst_fasyankes_id
		);	
		
		$export['data_individu'] = newPasiens::select(
			'mst_pasiens_id',
			'nik',
			'nm_lengkap',
			'tmp_lahir',
			'tgl_lahir',
			'jk_id',
			'alamat',
			'tlp',
			'agama_id',
			'goldar_id',
			'pendidikan_id',
			'pekerjaan_id',
			'status_marital',
			'usia',
			'hasil_hiv',
			'hasil_sifilis',
			'status_kehamilan',
			'created_by',
			'modified_by',
			'created_at',
			'updated_at',
			'status_gpa',
			'propinsi_domisili',
			'kabupaten_domisili',
			'tgl_tafsiran_partus',
			'jml_bayi',
			'hasil_hbsag_ibu',
			'tgl_hbsag_ibu',
			'tgl_persalinan',
			'tmp_persalinan',
			'tgl_rujukan_rs',		
			'tgl_kunjungan',
			'propinsi_form',
			'kabupaten_form',
			'kecamatan_form',
			'kelurahan_form',
			'propinsi_login',
			'kabupaten_login',
			'fasyankes_login',
			'status_pasien',
			'no_resimen',
			'konseling_3e',
			'screening_triple_eliminasi',
			'screening_hepatitis_b',
			'screening_hepatitis_b_bumil',
			'screening_hepatitis_c',
			'wilayah_kerja_id',
			'tgl_screening_hbsag',
			'kode_spesimen_hbsag',
			'hasil_screening_hbsag',
			'tgl_screening_hiv',
			'kode_spesimen_hiv',
			'hasil_screening_hiv',
			'tgl_screening_sifilis',
			'kode_spesimen_sifilis',
			'hasil_screening_sifilis',
			'tgl_screening_antihcv',
			'kode_spesimen_antihcv',
			'hasil_screening_antihcv',
			'tgl_mulai_art',
			'tgl_bumil_masuk_pdp',
			'hepatitis',
			'sifilis_ditangani',
			'sifilis_diobati_adequat',
			'umur_kehamilan',
			'pasangan_diperiksa_sifilis',
			'pasangan_ketahui_status_hiv',
			'persalinan_status',
			'tgl_pemberian_arv',
			'tgl_dbseid_6_8',
			'hasil_dbseid_6_8',
			'tgl_konfirmasi_eid_12',
			'hasil_konfirmasi_eid_12',
			'tgl_balita_terdeteksi_hiv',
			'hasil_balita_terdeteksi_hiv',
			'tgl_balita_masuk_pdp',
			'tgl_balita_pengobatan_arv',
			'bayi_ibu_sifilis_rujuk',
			'kurang_2tahun_periksa_sifilis',
			'tgl_kurang_2tahun_periksa_sifilis',
			'hasil_kurang_2tahun_periksa_sifilis',
			'hepb_dalam_keluarga',
			'hepb_hubungan',
			'hepc_dalam_keluarga',
			'hepc_hubungan',
			'kelompok_risiko',
			'pemeriksaan_anti_hbs',
			'tgl_pemeriksaan_anti_hbs',
			'kode_spesimen_anti_hbs',
			'hasil_pemeriksaan_anti_hbs',
			'faskes_rujukan',
			'no_kk',
			'hepb_rujuk',
			'hepc_rujuk',
			'konseling_hepb',
			'konseling_hepc',
			'imunisasi_tindaklanjut_hepb1',
			'tgl_imunisasi_tindaklanjut_hepb1',
			'imunisasi_tindaklanjut_hepb2',
			'tgl_imunisasi_tindaklanjut_hepb2',
			'imunisasi_tindaklanjut_hepb3',
			'tgl_imunisasi_tindaklanjut_hepb3'
		)->where(array('fasyankes_login' => Auth::user()->mst_fasyankes_id))->get();
		
		$export['data_individu']['detail'] = DB::table('mst_pasiens_temp_baby')->select(
			'mst_pasiens_temp_baby.pasien_register', 
			'mst_pasiens_temp_baby.ordered',
			'mst_pasiens_temp_baby.tgl_hb0',
			'mst_pasiens_temp_baby.tgl_hb1', 
			'mst_pasiens_temp_baby.tgl_hb2', 
			'mst_pasiens_temp_baby.tgl_hb3', 
			'mst_pasiens_temp_baby.status_hbsag', 
			'mst_pasiens_temp_baby.tgl_hbsag_anak', 
			'mst_pasiens_temp_baby.tgl_hbig',
			'mst_pasiens_temp_baby.tgl_antihbs',
			'mst_pasiens_temp_baby.hasil_antihbs'
		)->join('mst_pasiens_temp', 'mst_pasiens_temp.mst_pasiens_id', '=', 'mst_pasiens_temp_baby.pasien_register')->where(array('mst_pasiens_temp.fasyankes_login' => Auth::user()->mst_fasyankes_id))->get();
		
		$datafile = array(
			'directory' => 'Individu',
			'subdirectory' => 'Data_individu',
			'filename' => 'Data_individu-'.date('Y-m-d').Auth::user()->mst_fasyankes_id,
			'data' => Crypto::encrypt(json_encode($export))
		);
		
		$generate = $this->_generateFileExport($datafile);
			
		return $generate;		
	}
   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($eid=null)
    {
        if($eid==''){
            Session::flash('message', 'Pasien tidak ditemukan');
            return redirect(route('Pasiens'));
        }
		
		//$id = Crypto::decrypt($eid);
		$id = $eid;
		$pasiens = (object)newPasiens::findOrFail($id)->toArray();
		
		$data['fasyankes'] = Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();		
		$title = 'Edit Pasien Wilayah '.$data['fasyankes']->nama_fasyankes;
		
		if (empty($pasiens)) {
            Session::flash('message', 'Pasien tidak ditemukan');
            return redirect(route('new_pasiens.index'));
        }
		
		$data['propinsi'] 			= Propinsi::where('mst_propinsi_id', '=', $pasiens->propinsi_form)->first();
		$data['kota'] 				= Kabupaten::where('mst_kabupaten_id', '=', $pasiens->kabupaten_form)->first();
		$data['list_kota']			= Kabupaten::where('mst_propinsi_id', '=', Auth::user()->mst_kabupaten_id)->get();
		$data['kecamatan']			= Kecamatan::where('mst_kecamatan_id',$pasiens->kecamatan_form)->first();
		$data['kelurahan'] 		= Kelurahan::where('mst_kelurahan_id', $pasiens->kelurahan_form)->first();
		$data['jenis_kelamin'] 	= Jenis_kelamin::where('jk_kode', $pasiens->jk_id)->first();
		$data['agama'] 			= Agama::where('agama_id', '=', $pasiens->agama_id)->first();
		$data['goldar'] 			= Goldar::where('goldar_id', '=', $pasiens->goldar_id)->first();
		$data['pekerjaan'] 		= Pekerjaan::where('pekerjaan_nama', $pasiens->pekerjaan_id)->first();
		$data['pendidikan'] 		= Pendidikan::where('pendidikan_nama', $pasiens->pendidikan_id)->first();
		$data['wilayah_kerja']		= Wilayah_kerja::where('wilayah_kerja_id', '=', $pasiens->wilayah_kerja_id)->first();
		$data['fasyankes'] 		= Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();		
		$data['faskes_rujukan'] 		= Fasyankes::where('mst_fasyankes_id',$pasiens->faskes_rujukan)->first();		
		$data['data'] 		= Fasyankes::where('mst_fasyankes_id',Auth::user()->mst_fasyankes_id)->first();		
		$data['status_gpa'] = (!empty($pasiens->status_gpa) ? explode('#',$pasiens->status_gpa) : '');
		$data['kelompok_risiko'] = DB::table('mst_kelompok_risiko')->orderBy('ordered','ASC')->get();
		$data['pasien_bayi'] = DB::table('mst_pasiens_temp_baby')->where('pasien_register',$pasiens->mst_pasiens_id)->limit(12)->get();
		
		return View('newpasiens.edit', compact('pasiens','title','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $eid)
    {
        $id = $eid;
		$Pasiens = NewPasiens::findOrFail($id);
		$input = $request->all();
		
		$rules = array(
			'nik'=> 'Required',
		);
		
		$pesan = array(
			'nik.required'=> 'NIK Wajib diisi.',				
		);
		
	
		$valid = Validator::make($input, $rules, $pesan);
		
		$input['status_gpa']= $input['status_gpa1'].'#'.$input['status_gpa2'].'#'.$input['status_gpa3'];
	
		if( $valid->passes() ) 
		{
			$Pasiens->fill($input)->save();
			NewPasiens::where('local_id', $id);
			DB::table('mst_pasiens_temp_baby')->where(array('pasien_register' => $input['mst_pasiens_id']))->delete();
			if($input['jml_bayi'] > 0){
				$input_bayi = array();
				for($i=1;$i<=$input['jml_bayi'];$i++){
					$input_bayi[] = array(
						'ordered' => $i,
						'pasien_register' => $input['mst_pasiens_id'],
						'tgl_hb0' => (isset($input['tgl_hb0'][$i]) ? $input['tgl_hb0'][$i] : '') ,
						'tgl_hb1' => (isset($input['tgl_hb1'][$i]) ? $input['tgl_hb1'][$i] : ''),
						'tgl_hb2' => (isset($input['tgl_hb2'][$i]) ? $input['tgl_hb2'][$i] : ''),
						'tgl_hb3' => (isset($input['tgl_hb3'][$i]) ? $input['tgl_hb3'][$i] : ''),
						'tgl_hbig' =>(isset($input['tgl_hbig'][$i]) ? $input['tgl_hbig'][$i] : ''),
						'tgl_hbsag_anak' => (isset($input['tgl_hbsag_anak'][$i]) ? $input['tgl_hbsag_anak'][$i] : ''),
						'status_hbsag' => (isset($input['status_hbsag'][$i]) ? $input['status_hbsag'][$i] : ''),
						'tgl_antihbs' => (isset($input['tgl_antihbs'][$i]) ? $input['tgl_antihbs'][$i] : ''),
						'hasil_antihbs' => (isset($input['hasil_antihbs'][$i]) ? $input['hasil_antihbs'][$i] : ''),
					);	
				}
				DB::table('mst_pasiens_temp_baby')->insert($input_bayi);			
			}
			Session::flash('message', 'Pasiens '.$Pasiens->nm_lengkap.' berhasil di update!');
			
			return redirect(route('new_pasiens'));
		} 
		else 
		{ 			
			$messages = $valid->messages();			
			return $this->edit($eid)
						->withErrors($valid);
		}	  
    }	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($eid)
    {
       $Pasiens = newPasiens::findOrFail($eid);

        if (empty($Pasiens)) {
            Session::flash('message', 'Pasiens tidak ditemukan');
			return redirect(route('Pasiens'));
        }
		
		
		$del_pasien = DB::table('mst_pasiens_temp')->where('local_id', '=', $eid)->delete();
       Session::flash('message', 'Pasiens '.$Pasiens->nm_lengkap.' berhasil di hapus');
        return redirect(route('new_pasiens'));
    } 
	
	
}
