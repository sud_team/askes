<!doctype html>
<html>

<head>
	<meta name="viewport" content="width=device-width">

	<meta charset="utf-8">
	<title>Invoice DJPSEHAT <?php echo base_url(''); ?></title>

 	<style>
		@font-face {
			font-family: 'Source Sans Pro';
 			src: url("<?php echo base_url('assets/font/source-sans-pro.regular.ttf'); ?>") 
			format("truetype");
		}

		body {
			font-family: 'Source Sans Pro', sans-serif;

		}

		.invoice-box {
			padding: 30px;
			font-size: 12px;
			line-height: 14px;
			font-family: 'Source Sans Pro', sans-serif;

		}

	 
		.watermark {
			height: 95%;
			width: 90%;
			position: absolute;
			top: 120px;
			bottom: 0px;
			right: 30%;
			left: 10%;
			background-repeat: no-repeat;
			background-size: cover;
			z-index: 9999;
			opacity: 0.15;
			-ms-transform: rotate(-30deg);
			/* IE 9 */
			-webkit-transform: rotate(-30deg);
			/* Safari 3-8 */
			transform: rotate(-30deg);
		}

		.invoice-box .t-invoice {
			font-size: 18px;
			font-weight: 400;
			color: #ff1846;
			 
		}

		.invoice-box .t-brand {
			font-size: 16px;
			font-weight: 600;
			color: #666666;
 		}

		.t-brand .t-brand1 {
			color: #0091EA;
		}

		.invoice-box .t-invoice-created {
			font-weight: 600;
			color: #666666;
		}

		.invoice-box table {
			width: 100%;
			text-align: left;

		}

		.invoice-box table td {
			padding: 5px;
			vertical-align: top;
		}

		.invoice-box table tr td:nth-child(2) {
			text-align: right;
		}

		.invoice-box table tr.top table td {
			padding-bottom: 20px;
		}

		.invoice-box table tr.top table td.title {
			font-size: 45px;
			line-height: 45px;
			color: #333333;
			text-transform: uppercase;
			font-weight: 300;
			font-family: 'Source Sans Pro', sans-serif;
		}

		.invoice-box table tr.top table td.title img {
			width: 220px
		}

		.invoice-box table tr.information table td {
			padding-bottom: 25px;
		}

		.invoice-box table tr.information span:nth-child(1) {
			font-weight: bold;
			font-size: 8pt;
			color: #666666;
			font-family: 'Source Sans Pro', sans-serif;

		}

		.invoice-box table tr.heading td {
			background: #eee;
			border-bottom: 1px solid #ddd;
			font-weight: bold;
			color: #ff1846;
			font-family: 'Source Sans Pro', sans-serif;

		}

		.invoice-box table tr.details td {
			padding-bottom: 10px;

		}

		.invoice-box table tr.item td {
			border-bottom: 1px solid #eee;
		}

		.invoice-box table tr.item:last-child td {
			border-bottom: none;
		}

		.invoice-box .invoice-summary {
			float: right;
			width: 250px;
			border: 1px solid #eee;
			text-align: right;
			padding: 10px;
			background: #eee;
			margin-top: 25px;
			border-radius: 4px;
			height: 245px
		}

		.invoice-box .invoice-summary-text {
			font-family: 'Source Sans Pro', sans-serif;

			font-weight: 400;
			text-align: center;
			font-size: 12px;
			color: #ff1846;
			border-bottom: 0.125pt solid #333;
			padding-bottom: 15px;
			margin-bottom: 5px
		}

		.invoice-box .invoice-summary .invoice-total {
			font-weight: bold;
			color: #ff1846;
			font-family: 'Source Sans Pro', sans-serif;

		}

		.invoice-box .invoice-summary .invoice-final {
			font-weight: 300;
			padding-top: 6pt;
			font-family: 'Source Sans Pro', sans-serif;

		}

		.invoice-box .invoice-summary .invoice-exchange {
			font-weight: 300;
			font-size: 12px;
		}

		.invoice-box .information .information-client td {
			width: 10px
		}

		.details .payment-price {
			float: right;
		}

		.details .payment-method {
			line-height: normal
		}

		.bank-info {
			width: 320px;
			text-align: center;
			margin-top: 0px;
			color: #333333;

		}

		.bank-info .info-payment {
			text-align: left;
			padding-bottom: 25px;
			color: #333333;
			text-align: justify;
			text-justify: inter-word;
		}

		.bank-info img {
			position: static;
			width: 120px;
		}

		.footer img {

			position: absolute;
			width: 75px;
			padding-left: 95px
		}

		.footer .footer-text {
			color: #333;
			text-align: left;
			padding-left: 180px
		}

		.footer {
			position: fixed;
			left: 0;
			bottom: 20;
			width: 100%;
			font-size: 10px;
			border-top: 0.05pt solid #333;
			padding-top: 10px;
						font-family: 'Source Sans Pro', sans-serif;

		}
	</style>

</head>

<body>
	<div class="watermark" style="background: url('<?php echo base_url() ?>assets/ela/images/unpaid.png');"></div>

	<div class="invoice-box">

		<table cellpadding="0" cellspacing="0">
			<tr class="top">
				<td colspan="2">
					<table>
						<tr>
							<td class="title">
								<img src="<?php echo base_url() ?>assets/ela/images/tugulogo.png" alt="tugu_logo">
							</td>

							<td>
								<span class="t-brand">DJP <span class="t-brand1">SEHAT</span></span>
								<br><br>
								<span class="t-invoice">INVOICE TAGIHAN</span>
								<br>

								<span class="t-invoice-created">NO. <?php echo $invoice->noInvoice; ?></span>
								<br>

								<span class="invoice-created">Tanggal Pemesanan</span>:
								<span class="t-invoice-created"><?php echo date("d/m/Y", strtotime($invoice->created_at)); ?></span>
								<br>
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr class="information">
				<td colspan="2">
					<table>
						<tr>
							<td class="information-company" width="40%">
								<!-- <span id="company-name">Agra Indonesia</span><br>
									<span id="company-address">Menara Palma, 9th Floor</span><br>
									<span id="company-town"> Jl. HR. Rasuna Said, Blok X2, Kav.6, Jakarta Selatan</span><br>
									<span id="company-country"> Indonesia 12950</span><br> -->
							</td>

							<td class="information-client" width="30%">
								<span class="t-invoice-to">Akun Pemesan</span> <br>
								<span id="client-name">Nama : </span> <?php echo $data_karyawan->namaPeserta; ?><br>
								<span id="client-address">Email : </span> <?php echo $this->session->userdata("username") ?></span><br>
								<span id="client-town">Telp. : </span> <?php echo $data_karyawan->noHp; ?><br>
								<span id="client-country"> <?php echo $data_karyawan->alamat; ?> </span><br>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<table class="invoice-payment" cellpadding="0" cellspacing="0">
			<tr class="heading">
				<td colspan="6">
					<span class="t-payment-method">Nama Peserta</span>
				</td>

			</tr>
			<?php foreach ($data_peserta as $v) { ?>
				<tr class="details">
					<td><span class="payment-method"><?php echo $v["namaPeserta"]; ?></span> </td>
					<td><span class="payment-method"><?php echo $v["jenisKelamin"]; ?></span> </td>
					<td><span class="payment-method"><?php echo $v["hubunganKeluarga"]; ?></span> </td>
					<td><span class="payment-method"><?php echo $data_karyawan->tempatLahir . ', ' . date("d/m/Y", strtotime($v["tglLahir"])); ?></span></td>
					<td><span class="payment-method"><?php echo $v["levelPlan"]; ?></span></td>
					<td> <span class="payment-price">Rp. <?php echo price($v["premi"]); ?></span></td>
				</tr>
			<?php } ?>
		</table>
		<table class="invoice-items" cellpadding="0" cellspacing="0">
			<tr class="heading">
				<td>Sub Total</td>
				<td><span class="t-price">Rp. <?php echo price($invoice->jmlPremi); ?></span></td>
			</tr>
		</table>
		<div class="invoice-summary">
			<div class="invoice-summary-text">Ringkasan Pemesanan
			</div>
			<div class="invoice-final">Service Plan</div>
			<div class="invoice-total"><span class="t-price"><?php echo price($invoice->levelPlan); ?></span></div>
			<div class="invoice-final">Jumlah Peserta</div>
			<div class="invoice-total"><span class="t-price"><?php echo $jml_peserta; ?> Orang</span></div>
			<div class="invoice-final">Tanggal Asuransi</div>
			<div class="invoice-total"><span class="t-price"><?php echo date("d/m/Y", strtotime($invoice->tglMulaiAsuransi)); ?></span></div>
			<div class="invoice-final">Jumlah Premi yang harus dibayar</div>
			<div class="invoice-total"><span class="t-price">Rp. <?php echo price($invoice->jmlPremi); ?></span></div>
			<br>
		</div>

		<div class="bank-info">
			<img src="<?php echo base_url() ?>assets/ela/images/mandiri.png" alt="Bank_Mandiri_logo"> <br>
			<span><b>Bank Mandiri Cab. Depkeu RI</b></span> <br>Nomor Rekening<br><span><b>130.000.9090800</b></span><br>Atas Nama<br><span><b>PT. Anugerah Bersama Berkah Abadi</b></span>
			<br><br>
			<div class="info-payment">Pembayaran dapat dilakukan melalui ATM, Internet Banking, Mobile Banking atau Transfer Bank dengan cara memasukan nomor Invoice <b>02342342</b> pada kolom berita transaksi</div>
		</div>
	</div>


	<div class="footer"><img src="<?php echo base_url() ?>assets/ela/images/agralogo.png" alt="tugu_logo">
		<div class="footer-text"> Menara Palma, 9th FloorJl. HR. Rasuna Said, Blok X2, Kav.6, Jakarta Selatan Indonesia 12950<br>Telp. : +62 (21) 2598 3210 | Fax : +62 (21) 2598 3211 | Email: cs.admin@agraindonesia.com | www.agraindonesia.com
		</div>
	</div>
	<!-- Dependencies -->
	<!-- Invoice -->
</body>

</html>