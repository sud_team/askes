<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pendaftaran Asuransi Perorangan</h4>
                <h6 class="card-subtitle">Formulir untuk mendaftarkan peserta baru secara individu
                </h6>
                <br>
                <form id="FormSubmit" method="post" class="form-horizontal form-bordered">
                    <div class="row">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">
                           <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Peserta</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="nama" id="nama" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>
							
							<div class="form-group row">
                                <label class="control-label text-right  col-md-4">Email</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="email" name="email" id="email" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>
							
							<div class="form-group row">
                                <label class="control-label text-right  col-md-4">Password</label>
                                <div class="col-md-7">
                                    <input type="password" placeholder="Password" name="password" id="password" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Jenis Kelamin</label>
                                <div class="col-md-7" >
                                    <select class=" select2 form-control" id="jenisKelamin" name="jenisKelamin" required>
                                        <option value="">-Pilih-</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Mulai Asuransi</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglMulaiAsuransi" id="tglMulaiAsuransi" required >
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Level Planning </label>
                                <div class="col-md-7">
                                    <select class="select2 form-control" id="levelPlanning" name="levelPlanning">
                                        <option value="">-Pilih-</option>
                                        <option value="700">700</option>
                                        <option value="900">900</option>
                                        <option value="1400">1400</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">


                            
                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Lahir</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglLahir" id="tglLahir" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Bank" name="namaBank" id="namaBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">No. Rekening Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="noRekeningBank" id="noRekeningBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Pemilik Rekening</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="namaPemilikRekening" id="namaPemilikRekening" class="form-control">
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Status Pernikahan</label>
                                <div class="col-md-7">

                                    <select class="select2 form-control" id="statusPernikahan" name="statusPernikahan">
                                        <option value="">-Pilih-</option>
                                        <option value="nikah">Nikah</option>
                                        <option value="belum_nikah">Belum Nikah</option>
                                    </select></div>
                            </div>

                            <div class="form-group row">

                                <div class="offset-sm-7 col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm pull-right">Submit <i class="fa fa-send-o f-s-14"></i>
                                    </button>

                                </div>
                            </div>
                        </div>



                    </div>

                </form>
            </div>
        </div>
    </div>
</div>