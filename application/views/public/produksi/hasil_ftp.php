<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Hasil Upload FTP</h4>
             <h6 class="card-subtitle">Untuk men-download arsip file .csv PUT-02, PUT-03 dan Detail Upload yang telah dilakukan Upload Produksi</h6>
            <!-- Panel Utama -->
            <form id="FormSubmit" method="post">
            <div class="row">
                <!-- Menu Pencairan -->
                <div class="col-md-7">
                  <input type="hidden" id="link" value="filter_hasil_ftp">
                    <div class="row m-b-10">
                      <div class="text-left text-info col-sm-5">
                          <small class="form-control-feedback f-s-10">Jenis file .csv</small>
                          <select class="js-example-basic-cabang  custom-select f-s-12" id="jenisftp" name="jenisftp">
                              <option value="">Pilih</option>
                              <option value="1">PUT-02</option>
                              <option value="2">PUT-03</option>
                              <option value="5">Detail Upload</option>
                              <!-- <option value="3">FTP 04</option>
                              <option value="4">FTP 05</option> -->
                          </select>
                      </div>
                    </div>
                    <div class="row m-b-20">
                      <div class="text-left text-info col-sm-5">
                        <small class="form-control-feedback f-s-10 text-info">Tanggal Upload Produksi</small>
                        <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai" required>
                      </div>
                      <div class="col-sm-2 m-t-20 text-right">
                        <a id="cariUpload" class="btn btn-info f-s-12 text-light">
                          <i class="fa fa-search"></i> Tampilkan
                        </a>
                      </div>
                    </div>
                </div>
              </form>
                    <div class="col-md-4 bg-light-danger radius p-10 m-b-30 animated animated bounceIn">
                      <div class="box-title text-center text-red">
                          <h6>
                              <b>Info!</b> 
                          </h6>
                      </div>
                      <hr>
                      <div class=" m-l-10 m-b-0 text-red f-s-12">
                        Arsip file .csv PUT-02, PUT-03 dan Detail Upload dapat di download berdasarkan tanggal upload produksi<br><hr>Apabila Upload Produksi lebih dari 1 (satu) kali dalam sehari karena adanya data Debitur tambahan, maka data Debitur yang baru akan digabung kedalam file .csv PUT-02, PUT-03 dan Detail Upload ditanggal yang sama </div>
                      <div class="row m-l-10 m-b-20"></div>
                  </div>
                <!-- Menu Pencairan Akhir -->
                <!-- Display Konfirmasi Akhir "VISIBLE SETELAH PENCARIAN"-->
            </div>
            </form>
              <div class="table-responsive m-b-15">
              <h4 class="card-header">Export PUT-02</h4>

                  <table id="hasil02" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>Nomor Registrasi</th>
                      </tr>
                    </thead>
                  </table>
              </div>
              <div class="table-responsive m-b-15">
              <h4 class="card-header">Export PUT-03</h4>

                  <table id="hasil03" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>Nomor Registrasi</th>
                      </tr>
                    </thead>
                  </table>
              </div>
              <div class="table-responsive m-b-15">
              <h4 class="card-header">Export Detail Upload</h4>

                  <table id="hasilupload" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th class="w-3">#</th>
                              <th>No. Registrasi</th>
                              <th class="w-6">CIF</th>
                              <th class="w-6">No. Pinjaman</th>
                              <th>Kode Bank</th>
                              <th>Nama</th>
                              <th>Tipe Loan</th>
                              <th>Jenis Kelamin</th>
                              <th class="w-6">KTP</th>
                              <th>Tempat Lahir</th>
                              <th>Tgl. Lahir</th>
                              <th>Alamat</th>
                              <th>Pekerjaan</th>
                              <th class="w-6">No. PK</th>
                              <th>Tgl. Akad</th>
                              <th>Tgl. Akhir</th>
                              <th>Plafon</th>
                              <th>Premi</th>
                              <th class="w-6">No. Referensi <br>Premi</th>
                              <th>New/Topup<br></th>
                              <th>Kode Asuransi<br></th>
                              <th>Status<br></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>


