<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Penyesuaian Interest Pegawai Bank Jatim</h4>
             <h6 class="card-subtitle">Untuk menetapkan premi debitur sesuai bunga yang ditetapkan Bank Jatim pada kategori Debitur Pegawai Bank Jatim</h6>
            <!-- Panel Utama -->
            <form id="FormSubmit" method="post">
            <div class="row">
                <!-- Menu Pencairan -->
                <div class="col-md-7">
                  <input type="hidden" id="link" value="filter_prapen">
                    <div class="row m-b-10">
                    <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-b-10">
                    <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" name="produk" id="produk">
                                    <option value="">Semua</option>
                                    <?php foreach ($produk as $pd): ?>
                                    <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                    <?php endforeach ?>
                                      
                                </select>
                        </div>
                        <div class="text-left text-info col-sm-5">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-info col-sm-5">
                            <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                            <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Periode upload dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai" required>
                        </div>
                        <div class="text-left text-info col-sm-4">
                            <small class="form-control-feedback f-s-10 text-info">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir" required>
                        </div>
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cariBunga" class="btn btn-info f-s-12 text-light">
                            <i class="fa fa-search"></i> Tampilkan
                            </a>
                        </div>
                    </div>

                </div>
              </form>
                    <div class="col-md-4 bg-light-danger radius p-10 m-b-30 animated animated bounceIn">
                      <div class="box-title text-center text-red">
                          <h6>
                              <b>Info!</b> 
                          </h6>
                      </div>
                      <hr>
                      <div class=" m-l-10 m-b-0 text-red f-s-12">
                      
                      Seluruh Debitur dengan kategori pekerjaan <b>Pegawai Bank Jatim</b> akan dilakukan penyesuaian interest untuk menetapkan Premi dengan formula perhitungan anuitas sebelum dilakukan Rekonsel.
                       <hr>
                              Selalu lakukan pengecekan data Penyesuaian Interest setiap melakukan produksi<hr>
                      </div>
                      <div class="row m-l-10 m-b-20"></div>
                  </div>
                <!-- Menu Pencairan Akhir -->
                <!-- Display Konfirmasi Akhir "VISIBLE SETELAH PENCARIAN"-->
            </div>
            </form>
              <div class="table-responsive">
                  <!-- <a class="btn btn-xs btn-primary" id="validasi" style="color: #ffffff">Validasi</a> -->
                  <table id="DataBunga" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>No.Pinjaman</th>
                              <th>Nama</th>
                              <th>Bank Operasional</th>
                              <th>Tgl Kredit</th>
                              <th>Tgl Akhir</th>
                              <th>Tenor Bulan</th>
                              <th>Plafon</th>
                              <th>Status Pinjaman</th>
                              <th>Bunga</th>
                              <th>Adjust</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>


