<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pendaftaran Asuransi Perorangan</h4>
                <h6 class="card-subtitle">Formulir untuk mendaftarkan peserta baru secara individu
                </h6>
                <br>
                <form id="FormSubmit" method="post" class="form-horizontal form-bordered">
                    <div class="row">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tipe Transaksi</label>
                                <div class="col-md-7">
                                    <select class="select2 form-control" id="cabang" name="jenisPerubahan">
                                        <option value="">-Pilih-</option>
                                        <option value="A">A</option>
                                        <option value="T">T</option>
                                        <option value="C">C</option>
                                    </select>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nomor Peserta</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="noPeserta" id="nama" class="form-control ">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nomor Pinjaman</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="nopin" id="nopin" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Mulai Asuransi</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglMulaiAsuransi" id="tglMulaiAsuransi" required >
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Level Planning </label>
                                <div class="col-md-7">
                                    <select class="select2 form-control" id="cabang" name="jenisPerubahan">
                                        <option value="">-Pilih-</option>
                                        <option value="700">700</option>
                                        <option value="900">900</option>
                                        <option value="1400">1400</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">


                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Peserta</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="nama" id="nama" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Jenis Kelamin</label>
                                <div class="col-md-7" >
                                    <select class=" select2 form-control" id="cabang" name="jenisPerubahan" required>
                                        <option value="">-Pilih-</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Lahir</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control ttl" placeholder="dd/mm/yyyy" name="tglMulaiAsuransi" id="tglMulaiAsuransi" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Bank" name="nama" id="nama" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">No. Rekening Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="nopin" id="nopin" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Pemilik Rekening</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="nama" id="nama" class="form-control">
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Status Pernikahan</label>
                                <div class="col-md-7">

                                    <select class="select2 form-control" id="cabang" name="jenisPerubahan">
                                        <option value="">-Pilih-</option>
                                        <option value="700">700</option>
                                        <option value="900">900</option>
                                        <option value="1400">1400</option>
                                    </select></div>
                            </div>

                            <div class="form-group row">

                                <div class="offset-sm-7 col-md-4">
                                    <button class="btn btn-outline-info btn-sm ">Batalkan
                                    </button>
                                    <button class="btn btn-info btn-sm pull-right">Submit <i class="fa fa-send-o f-s-14"></i>
                                    </button>

                                </div>
                            </div>
                        </div>



                    </div>

                </form>


                <div class="table-responsive">
                    <!-- <a class="btn btn-xs btn-primary" id="validasi" style="color: #ffffff">Validasi</a> -->
                    <form method="post" action="<?php echo base_url() ?>produksi/saveBatasPensiun">
                        <table id="DataPrapen" class="display nowrap table-hover table-bordered" cellspacing="0" width="100%">
                            < <table id="upload" class="display table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-3">#</th>
                                <th>Tipe Transaksi</th>
                                <th class="w-6">No Peserta</th>
                                <th class="w-6">No. Pegawai Induk</th>
                                <th>Hubungan</th>
                                <th>Tanggal Mulai Asuransi</th>
                                <th>Level Plan</th>
                                <th>Nama Peserta</th>
                                <th class="w-6">Nama Karyawan</th>
                                <th>Jenis Kelamin</th>
                                <th>Nama Bank</th>
                                <th>No Rekening Bank</th>
                                <th>Cabang Bank</th>
                                <th class="w-6">Status Pernikahan</th>
                                <th>Email</th>
                                <th>HP</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data_peserta as $k => $v) { ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $v['jenisPerubahan']; ?></td>
                                    <td><?php echo $v['noPeserta']; ?></td>
                                    <td><?php echo $v['noIndukPegawai']; ?></td>
                                    <td><?php echo $v['hubunganKeluarga']; ?></td>
                                    <td><?php echo $v['tglMulaiAsuransi']; ?></td>
                                    <td><?php echo $v['levelPlan']; ?></td>
                                    <td><?php echo $v['namaPeserta']; ?></td>
                                    <td><?php echo $v['namaKaryawan']; ?></td>
                                    <td><?php echo $v['jenisKelamin']; ?></td>
                                    <td><?php echo $v['namaBank']; ?></td>
                                    <td><?php echo $v['noRekeningBank']; ?></td>
                                    <td><?php echo $v['cabangBank']; ?></td>
                                    <td><?php echo $v['statusPernikahan']; ?></td>
                                    <td><?php echo $v['email']; ?></td>
                                    <td><?php echo $v['noHp']; ?></td>
                                    <td><?php echo $v['keterangan']; ?></td>
                                    <td><a href="<?php echo base_url() ?>produksi/klaim" class="btn btn-success" >Klaim</a><br/><a href="#" class="btn btn-info" >Invoice</a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        </table>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>