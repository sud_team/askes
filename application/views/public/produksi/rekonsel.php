<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header p-l-0 p-b-0">
                <h4 class="card-title">Rekonsel</h4>
                <h6 class="card-subtitle">Pencocokan data peserta dengan Premi yang diterima, Pegawai Dirjen Pajak</h6>
            </div>
            <div class="card-body m-t-20">
                <form id="FormSubmit" method="post">
                    <div class="row p-b-25 justify-content-between">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6 col-lg-5">
                            <input type="hidden" id="link" value="filter_rekonsel">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Service Plan</label>
                                    <select class="form-control select2" style="width:100%" name="produk" id="produk">
                                        <option value="">Semua</option>
                                        <option value="800">800</option>
                                        <option value="1000">1000</option>
                                        <option value="1500">1500</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>No. Invoice</label>
                                    <input type="text" placeholder="No. Invoice" name="noInvoice" id="noInvoice" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Tanggal Pendaftaran dari</label>
                                    <input type="text" class="form-control f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Sampai dengan</label>
                                    <input type="text" class="form-control f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                </div>
                                <div class="col-md-6 form-group">
                                    <a id="cari" class="btn btn-outline-info f-s-12 pull-right">
                                        <i class="fa fa-search"></i> Tampilkan
                                    </a></div>
                            </div>
                        </div>
                      
                        <div class="col-md-6 col-lg-4  ">
                            <div class="box bg-light-info">
                                <h6 class="box-title text-center">
                                    Summary Data Yang akan di Rekonsel
                                </h6>
                                <hr>
                                <span class="text-justify"> Pilih peserta yang akan direkonsel pada kolom sebelah kiri tabel debitur dibawah </span><br>
                                <div class="row p-t-8">
                                    <span class="text-md-right col-sm-6">Tanggal Rekonsel</span>
                                    <span class=" col-md-6 text-info f-s-14"> <b><?php echo my_date_indo(date("Y-m-d")) ?></b></span>
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>

                                <div class="row p-t-8">
                                    <span class="text-md-right col-sm-6">Jumlah Invoice</span>
                                    <span class="col-md-6 text-info font-weight-bold f-s-14" id="jml-invoice">  </span>
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>
                                <div class="row p-t-8">
                                    <span class="text-md-right col-sm-6">Total Premi</span>
                                    <span class=" col-md-6 text-info font-weight-bold f-s-14" id="total-premi">  </span>
                                    <!--<small class="text-danger">Wajib diisi</small>-->
                                </div>

                                <div class="row p-t-15 justify-content-center">
                                    <div class="col-lg-6">
                                    <a class="btn btn-block btn-primary pull-right text-light f-s-12 price" id="validasi">Konfirmasi
										<input type="hidden" name="jmlPremiHidden" id="jmlPremiHidden" value=0>
										<i class="fa fa-check-square-o"></i>
                                    </a></div>
                                </div>
                            </div>
                        </div>

                    </div>
               


                <div class="table-responsive">
                    <!-- <a class="btn btn-xs btn-primary" id="validasi" style="color: #ffffff">Validasi</a> -->
                    <table id="DataPeserta" class="display nowrap table-hover table-striped table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.Invoice</th>
                                <th>Nama Pegawai</th>
                                <th>Tgl Mulai Asuransi</th>
                                <th>Jumlah Peserta</th>
                                <th>Download Bukti Transfer</th>
                                <th>Jumlah Premi</th>
                                <th><input type="checkbox" id="chkSelectAll"></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">

                        </tbody>
                    </table>
                </div>
				 </form>
            </div>
        </div>
    </div>
</div>