<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Upload No Peserta</h4>
                <h6 class="card-subtitle">Upload file No Peserta <code>ASKES</code> </h6>
                <form action="<?php echo base_url() ?>produksi/upload_process" method="post" enctype="multipart/form-data" class="dropzone" id="dropzoneForm">
                    <div class="fallback">
                        <input name="file" type="file" multiple required />
                    </div>


                </form>
                <div id="upload-berhasil">
                    </div>
                <?php if ($success) { ?>
                    <!-- toastr.init.js -->
                    <div id="upload-berhasil">
                    </div>
                <?php } ?>
				<!-- <a href="<?php echo base_url("upload/produksi/template.xlsx"); ?>" class="btn btn-success">Download Template</a> -->
          
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
