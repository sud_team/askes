<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pendaftaran Asuransi Perorangan</h4>
                <h6 class="card-subtitle">Formulir untuk mendaftarkan peserta baru secara individu</h6>
                <br>
				<?php $success_message = $this->session->flashdata('success'); ?>
                <?php if ($success_message) : ?>
                    <div class="text-danger text-center animated flash">
                        <i class="fa fa-check"></i>
                        <span><?php echo $this->session->flashdata('success'); ?></span>
                    </div>
                <?php endif ?>
                <form id="FormSubmit" method="post" class="form-horizontal form-bordered">
                    <div class="row">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">
                           <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Peserta</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="namaPeserta" id="nama" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>
							
							<div class="form-group row">
                                <label class="control-label text-right  col-md-4">Email</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="email" name="email" id="email" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>
							
							<div class="form-group row">
                                <label class="control-label text-right  col-md-4">Username</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="username" name="username" id="username" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>
							
							<div class="form-group row">
                                <label class="control-label text-right  col-md-4">Password</label>
                                <div class="col-md-7">
                                    <input type="password" placeholder="Password" name="password" id="password" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Jenis Kelamin</label>
                                <div class="col-md-7" >
                                    <select class=" select2 form-control" id="jenisKelamin" name="jenisKelamin" required>
                                        <option value="">-Pilih-</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Mulai Asuransi</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglMulaiAsuransi" id="tglMulaiAsuransi" required >
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Level Plan </label>
                                <div class="col-md-7">
                                    <select class="select2 form-control" id="levelPlan" name="levelPlan">
                                        <option value="">-Pilih-</option>
                                        <option value="700">700</option>
                                        <option value="900">900</option>
                                        <option value="1400">1400</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">
                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Lahir</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglLahir" id="tglLahir" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Bank" name="namaBank" id="namaBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">No. Rekening Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="noRekeningBank" id="noRekeningBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Pemilik Rekening</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="namaPemilikBank" id="namaPemilikBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Status Pernikahan</label>
                                <div class="col-md-7">
                                    <select class="select2 form-control" id="statusPernikahan" name="statusPernikahan">
                                        <option value="">-Pilih-</option>
                                        <option value="nikah">Nikah</option>
                                        <option value="belum_nikah">Belum Nikah</option>
                                    </select>
								</div>
                            </div>

                            <div class="form-group row">
                                <div class="offset-sm-7 col-md-4">
                                    <button type="submit" name="submit" value="submit" class="btn btn-info btn-sm pull-right">Submit <i class="fa fa-send-o f-s-14"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
					<button class="btn btn-success" onClick="return addForm(this,event)">Add</button>
				   <div class="table-responsive">
                    <form method="post" action="<?php echo base_url() ?>produksi/saveBatasPensiun">
                        <table id="DataPrapen" class="display nowrap table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-3">No</th>
                                <th>Hubungan</th>
                                <th>Level Plan</th>
                                <th>Nama Peserta</th>
                                <th class="w-6">Nama Karyawan</th>
                                <th>Jenis Kelamin</th>
                                <th>Nama Bank</th>
                                <th>No Rekening Bank</th>
                                <th>Cabang Bank</th>
                                <th class="w-6">Status Pernikahan</th>
                                <th>Email</th>
                                <th>HP</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody class="test">
							<tr class="duplicate">
								<td class="number">1</td>
								<td>
									<select class="select2 form-control" id="statusPernikahan[]" name="statusPernikahan">
                                        <option value="">-Pilih-</option>
                                        <option value="E">Karyawan</option>
                                        <option value="S">Pasangan</option>
                                        <option value="C">Anak</option>
                                    </select>
								</td>
								<td>
									<select class="select2 form-control" id="levelPlan[]" name="levelPlan[]">
                                        <option value="">-Pilih-</option>
                                        <option value="700">700</option>
                                        <option value="900">900</option>
                                        <option value="1400">1400</option>
                                    </select>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Nama Peserta" name="namaPeserta[]" id="namaPeserta" required>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Nama Karyawan" name="namaKaryawan[]" id="namaKaryawan" required>
								</td>
								<td>
									<select class="select2 form-control" id="jenisKelamin" name="jenisKelamin[]" required>
                                        <option value="">-Pilih-</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Nama Bank" name="namaBank[]" id="namaBank" required>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="No Rekening Bank" name="noRekeningBank[]" id="noRekeningBank" required>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Cabank Bank" name="cabangBank[]" id="cabangBank" required>
								</td>
								
								<td>
									<select class="select2 form-control" id="statusPernikahan" name="statusPernikaha[]n">
                                        <option value="">-Pilih-</option>
                                        <option value="nikah">Nikah</option>
                                        <option value="belum_nikah">Belum Nikah</option>
                                    </select>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Email Peserta" name="emailPeserta[]" id="emailPeserta[]" required>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="HP" name="hp[]" id="hp[]" required>
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Keterangan" name="keterangan[]" id="keterangan[]" required>
								</td>
							</tr>
							
						</tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
