 <form method="post">
	<div class="row justify-content-center">
		<div class="col-lg-7 col-md-8">
			<div class="card">
				<div class="card-header p-l-0 p-t-0">
					<div class="media">
						<div class="media-left meida media-middle">
							<span><i class="fa fa-shopping-cart  f-s-40 color-success"></i></span>
						</div>
						<div class="media-body m-l-20">
							<h3 class="m-b-0">Pendaftaran Peserta Asuransi</h3>
							<p>Pendaftaran dan Pembelian Premi Asuransi </p>
						</div>
					</div>
				</div>

				<div class="card-body">
					<br>
					<div class="row">
						<div class="col-md-8 col-lg-8 col-xl-6">
							<label>Tentukan tanggal Aktif Asuransi</label>
							<input type="text" name="tglMulaiAsuransi" class="form-control datepicker-pesan input-focus" autocomplete="off" placeholder="dd/mm/yyyy" required > <br>
							<input type="hidden" name="tglPesan" value="<?php echo date("d/m/Y"); ?>" >
							<input type="hidden" name="jmlPeserta" value=0>
							<input type="hidden" name="levelPlanLock" id="levelPlanLock" value="">
						</div>

						<div class="col-md-8 col-lg-8 col-xl-6">
							<label>Tentukan peserta yang akan didaftarkan</label>
							<button type="button" class="btn btn-outline-info btn-block f-s-14" data-toggle="modal" data-target="#myModal" id="tambahPeserta" onClick="clickModal(this,event)">Tambah Peserta</button>
						</div>
					</div>
				</div>
				<!-- Modal -->
				<div id="myModal" class="modal fade" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<div class="modal-title pull-left"> <img src="<?php echo base_url() ?>assets/ela/images/tugulogo.png" class="logo-text modal-title m-0 p-0" />
								</div>
								<h5 class="modal-title pull-right">Form data peserta<br>
									<p class=" text-info"> DJP Sehat</p>
								</h5>
							</div>


							<div class="modal-body">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Level Plan:</label>
											<?php
												$disabled = "";
												if($invoice->levelPlan != ''){
													$disabled = "disabled";
												}
											?>
											<select class="select2 form-control" style="width:100%" id="levelPlan" name="levelPlan[]" <?php echo $disabled; ?>>
												<option value="">-Pilih-</option>
												<option value="800" <?php echo ($invoice->levelPlan == "800" ? "selected" : ""); ?>>800</option>
												<option value="1000" <?php echo ($invoice->levelPlan == "1000" ? "selected" : ""); ?>>1000</option>
												<option value="1500" <?php echo ($invoice->levelPlan == "1500" ? "selected" : ""); ?>>1500</option>
											</select>
										</div>
										<div class="form-group">
											<label>Hubungan:</label>
											<select class="select2 form-control" style="width:100%" onChange="setDataPeserta(this,event)" id="hubunganKeluarga" name="hubunganKeluarga[]">
												<option value="">-Pilih-</option>
												<option value="E">Karyawan</option>
												<option value="S">Pasangan</option>
												<option value="C">Anak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Nama:</label>
											<input type="text" name="namaLengkap[]" id="namaLengkap" class="form-control" autocomplete="off" placeholder="Nama Lengkap" required>
										</div>

										<div class="form-group">
											<label>Tanggal Lahir:</label>
											<input type="text" name="tglLahir[]" id="tglLahir" class="form-control datepicker-modal" autocomplete="off" placeholder="dd/mm/yyyy" required="">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Jenis Kelamin:</label>
											<select class="select2 form-control" style="width:100%" id="jenisKelamin" name="jenisKelamin[]">
												<option value="">-Pilih-</option>
												<option value="M">Laki-laki</option>
												<option value="F">Perempuan</option>
											</select>
										</div>
										<div class="form-group">
											<label>Status Pernikahan:</label>
											<select class="select2 form-control" style="width:100%" id="statusPernikahan" name="statusPernikahan[]">
												<option value="">-Pilih-</option>
												<option value="nikah">Nikah</option>
												<option value="belum_nikah">Belum Nikah</option>
											</select>
										</div>
										<div class="form-group">
											<label>Telpon / HP:</label>
											<input type="text" name="noHp[]" id="noHp" class="form-control" autocomplete="off" placeholder="Telpon / HP" >
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="f-s-12 btn btn-dark pull-right" data-dismiss="modal">Close</button>
									<button type="button" class="f-s-12 btn btn-outline-info save-btn">Simpan</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="col-lg-7 col-md-8">
			<div class="card d-none ringkasan animated fadeInUp">
				<div class="card-header">
				<h4 class="card-title">Detail Pendaftaran Peserta <p class=" text-info"> DJP Sehat</p>
				</h4>
				</div>				<div class="card-body">
					<div class="ringkasan d-none">
					<br>

						<p>Ringkasan Pemesanan Anda</p>
						<div class="row form-horizontal form-bordered">
							<div class="col-lg-12">
								<div class="row p-b-5">
									<div class="col-sm-6 col-xs-6 text-sm-right ">Tanggal Asuransi</div>
									<div class="col-sm-6 col-xs-6 text-info font-weight-bold" id="tglAsuransi"> </div>

								</div>
								<div class="row  p-b-5">
									<div class="col-sm-6 col-xs-6 text-sm-right ">Tanggal Pemesanan</div>
									<div class="col-sm-6 col-xs-6 text-info font-weight-bold" id="tglPemesanan"> </div>

								</div>
								<div class="row p-b-5">
									<div class="col-sm-6 col-xs-6 text-sm-right ">Jumlah Peserta</div>
									<div class="col-sm-6 col-xs-6 text-info font-weight-bold" id="jmlPeserta">0</div>

								</div>
								<div class="row p-b-5">
									<div class="col-sm-6 col-xs-6 text-sm-right ">Jumlah Premi</div>
									<div class="col-sm-6 col-xs-6 text-info font-weight-bold" id="jmlPremi">0</div>
									<input type="hidden" name="jmlPremiHidden" id="jmlPremiHidden" value=0>

								</div>
							</div>

						</div>
						<br>
						<div class="row">

							<div class="col-sm-6 p-b-10"> <a href="javascript:location.reload(true)" class="btn btn-secondary btn-block f-s-14"><i class="fa fa-refresh"></i> Batal </a>
							</div>
							<div class="col-sm-6 p-b-10"> <button type="button" name="order" value="order" class="btn btn-info btn-block f-s-14 pemesanan">Konfirmasi</button>
							</div>

							<button type="submit" name="order" value="order" style="display:none" id="pemesanan">Pesan</button>
						</div>

					</div>

					<div class="row p-t-20">
						<div class="col-12">
						<table class="table  table-striped data-table">



						</table>
					</div></div>


				</div>

			</div>
		</div>
 	</div>
</form>