<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pendaftaran Asuransi Perorangan</h4>
                <h6 class="card-subtitle">Formulir untuk mendaftarkan peserta baru secara individu
                </h6>
                <br>
                <form id="FormSubmit" method="post" class="form-horizontal form-bordered">
                    <div class="row">
                        <!-- Menu Pencairan -->
                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">
                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Peserta</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="nama" id="nama" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Email</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="email" name="email" id="email" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Password</label>
                                <div class="col-md-7">
                                    <input type="password" placeholder="Password" name="password" id="password" class="form-control" required>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Jenis Kelamin</label>
                                <div class="col-md-7">
                                    <select class=" select2 form-control" id="jenisKelamin" name="jenisKelamin" required>
                                        <option value="">-Pilih-</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                    <small class="text-danger">Wajib diisi</small>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Mulai Asuransi</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglMulaiAsuransi" id="tglMulaiAsuransi" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Level Planning </label>
                                <div class="col-md-7">
                                    <select class="select2 form-control" id="levelPlanning" name="levelPlanning">
                                        <option value="">-Pilih-</option>
                                        <option value="700">700</option>
                                        <option value="900">900</option>
                                        <option value="1400">1400</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <input type="hidden" id="link" value="filter_prapen">



                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Tanggal Lahir</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" name="tglLahir" id="tglLahir" required>
                                    <small class="text-danger">Wajib diisi</small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Bank" name="namaBank" id="namaBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">No. Rekening Bank</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nomor" name="noRekeningBank" id="noRekeningBank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Nama Pemilik Rekening</label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Nama" name="namaPemilikRekening" id="namaPemilikRekening" class="form-control">
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right  col-md-4">Status Pernikahan</label>
                                <div class="col-md-7">

                                    <select class="select2 form-control" id="statusPernikahan" name="statusPernikahan">
                                        <option value="">-Pilih-</option>
                                        <option value="nikah">Nikah</option>
                                        <option value="belum_nikah">Belum Nikah</option>
                                    </select></div>
                            </div>

                            <div class="form-group row">

                                <div class="offset-sm-7 col-md-4">
                                    <button type="submit" class="btn btn-info btn-sm pull-right">Submit <i class="fa fa-send-o f-s-14"></i>
                                    </button>

                                </div>
                            </div>
                        </div>



                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-body">

        <section class="panel form-wizard" id="w4">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Form Wizard</h2>
            </header>
            <div class="panel-body">
                <div class="wizard-progress wizard-progress-lg">
                    <div class="steps-progress">
                        <div class="progress-indicator" style="width: 0%;"></div>
                    </div>
                    <ul class="wizard-steps">
                        <li class="active">
                            <a href="#w4-account" data-toggle="tab"><span>1</span>Account Info</a>
                        </li>
                        <li>
                            <a href="#w4-profile" data-toggle="tab"><span>2</span>Profile Info</a>
                        </li>
                        <li>
                            <a href="#w4-billing" data-toggle="tab"><span>3</span>Billing Info</a>
                        </li>
                        <li>
                            <a href="#w4-confirm" data-toggle="tab"><span>4</span>Confirmation</a>
                        </li>
                    </ul>
                </div>

                <form class="form-horizontal" novalidate="novalidate">
                    <div class="tab-content">
                        <div id="w4-account" class="tab-pane active">
                            <div class="form-group has-error">
                                <label class="col-sm-3 control-label" for="w4-username">Username</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="username" id="w4-username" required="">
                                    <label for="w4-username" class="error">This field is required.</label></div>
                            </div>
                            <div class="form-group has-error">
                                <label class="col-sm-3 control-label" for="w4-password">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" id="w4-password" required="" minlength="6">
                                    <label for="w4-password" class="error">This field is required.</label></div>
                            </div>
                        </div>
                        <div id="w4-profile" class="tab-pane">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="w4-first-name">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="first-name" id="w4-first-name" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="w4-last-name">Last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="last-name" id="w4-last-name" required="">
                                </div>
                            </div>
                        </div>
                        <div id="w4-billing" class="tab-pane">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="w4-cc">Card Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="cc-number" id="w4-cc" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputSuccess">Expiration</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="exp-month" required="">
                                        <option>January</option>
                                        <option>February</option>
                                        <option>March</option>
                                        <option>April</option>
                                        <option>May</option>
                                        <option>June</option>
                                        <option>July</option>
                                        <option>August</option>
                                        <option>September</option>
                                        <option>October</option>
                                        <option>November</option>
                                        <option>December</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" name="exp-year" required="">
                                        <option>2014</option>
                                        <option>2015</option>
                                        <option>2016</option>
                                        <option>2017</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                        <option>2022</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="w4-confirm" class="tab-pane">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="w4-email">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="email" id="w4-email" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9">
                                    <div class="checkbox-custom">
                                        <input type="checkbox" name="terms" id="w4-terms" required="">
                                        <label for="w4-terms">I agree to the terms of service</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <ul class="pager">
                    <li class="previous disabled">
                        <a><i class="fa fa-angle-left"></i> Previous</a>
                    </li>
                    <li class="finish hidden pull-right">
                        <a>Finish</a>
                    </li>
                    <li class="next">
                        <a>Next <i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>
            </div>
        </section>
        </div>
        </div>    
    </div>
</div>