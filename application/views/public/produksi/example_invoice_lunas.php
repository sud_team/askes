<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width">

    <meta charset="utf-8">
    <title>Kwitansi DJPSEHAT</title>

     <style>
       	@font-face {
			font-family: 'Source Sans Pro';
 			src: url("<?php echo base_url('assets/font/source-sans-pro.refuler.ttf'); ?>") 
			format("truetype");
		}

		body {
			font-family: 'Source Sans Pro', sans-serif;

		}

        .invoice-box {
            padding: 30px;
            font-size: 12px;
            line-height: 14px;
            font-family: 'Source Sans Pro', sans-serif;

        }

        /* .watermark {
			background-image: url(https://image.shutterstock.com/image-vector/unpaid-rubber-stamp-260nw-796984333.jpg);
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
			opacity: 0.2;
			position: relative;
  		} */
        .watermark {
            height: 95%;
            width: 90%;
            position: absolute;
            top: 120px;
            bottom: 0px;
            right: 30%;
            left: 10%;
            background-repeat: no-repeat;
            background-size: cover;
            z-index: 9999;
            opacity: 0.15;
            -ms-transform: rotate(-30deg);
            /* IE 9 */
            -webkit-transform: rotate(-30deg);
            /* Safari 3-8 */
            transform: rotate(-30deg);
        }

        .invoice-box .t-invoice {
            font-size: 18px;
            font-weight: 400;
            color: #2ecc71;
            padding-bottom: 15px
        }

        .invoice-box .t-brand {
            font-size: 16px;
            font-weight: 600;
            color: #666666;
        }

        .t-brand .t-brand1 {
            color: #0091EA;
        }

        .invoice-box .t-invoice-created {
            font-weight: 600;
            color: #666666;
        }

        .invoice-box table {
            width: 100%;
            text-align: left;

        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333333;
            text-transform: uppercase;
            font-weight: 300;
            font-family: 'Source Sans Pro', sans-serif;
        }

        .invoice-box table tr.top table td.title img {
            width: 220px
        }

        .invoice-box table tr.information table td {
            padding-bottom: 25px;
        }

        .invoice-box table tr.information span:nth-child(1) {
            font-weight: bold;
            font-size: 8pt;
            color: #666666
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            color: #2ecc71
        }

        .invoice-box table tr.details td {
            padding-bottom: 10px;

        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item:last-child td {
            border-bottom: none;
        }

        .invoice-box .invoice-summary {
            float: right;
            width: 250px;
            border: 1px solid #eee;
            text-align: right;
            padding: 10px;
            background: #eee;
            margin-top: 25px;
            border-radius: 4px;
            height: 265px
        }

        .invoice-box .invoice-summary-text {
            font-weight: 400;
            text-align: center;
            font-size: 12px;
            color: #2ecc71;
            border-bottom: 0.125pt solid #333;
            padding-bottom: 15px;
            margin-bottom: 5px
        }

        .invoice-box .invoice-summary .invoice-total {
            font-weight: bold;
            color: #2ecc71;
        }

        .invoice-box .invoice-summary .invoice-final {
            font-weight: 300;
            padding-top: 6pt;
        }

        .invoice-box .invoice-summary .invoice-exchange {
            font-weight: 300;
            font-size: 12px;
        }

        .invoice-box .information .information-client td {
            width: 10px
        }

        .details .payment-price {
            float: right;
        }

        .details .payment-method {
            line-height: normal
        }

        .details .payment-method .left {
            float: left;
        }

        .bank-info {
            width: 320px;
            text-align: center;
            margin-top: 0px;
            color: #333333;

        }

        .bank-info .info-payment {
            text-align: left;
            padding-bottom: 25px;
            color: #333333;
            text-align: justify;
            text-justify: inter-word;
        }

        .bank-info .info-payment ol li {
            padding-bottom: 5px;
        }

        .bank-info img {
            position: static;
            width: 120px;
        }

        .footer img {
            position: absolute;
            width: 75px;
            padding-left: 95px
        }

        .footer .footer-text {
            color: #333;
            text-align: left;
            padding-left: 180px
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 20;
            width: 100%;
            font-size: 10px;
            border-top: 0.05pt solid #333;
            padding-top: 10px;
        }
    </style>

</head>

<body>
    <div class="watermark" style="background: url('<?php echo base_url() ?>assets/ela/images/paid.png');"></div>

    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="<?php echo base_url() ?>assets/ela/images/tugulogo.png" alt="tugu_logo">
                            </td>

                            <td>
                                <span class="t-brand">DJP <span class="t-brand1">SEHAT</span></span>
                                <br><br>
                                <span class="t-invoice">KWITANSI PEMBAYARAN</span>
                                <br>

                                <span class="t-invoice-created">NO. INV. 34534534</span>
                                <br>

                                <span class="invoice-created">Tanggal Cetak Kwitansi</span>:
                                <span class="t-invoice-created">29/0/2016</span>
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="information-company" width="40%">
                                <!-- <span id="company-name">Agra Indonesia</span><br>
									<span id="company-address">Menara Palma, 9th Floor</span><br>
									<span id="company-town"> Jl. HR. Rasuna Said, Blok X2, Kav.6, Jakarta Selatan</span><br>
									<span id="company-country"> Indonesia 12950</span><br> -->
                            </td>

                            <td class="information-client" width="30%">
                                <span class="t-invoice-to">Akun Pemesan</span> <br>
                                <span id="client-name">Nama : </span> Eko Setiawan<br>
                                <span id="client-address">Email : </span> eko_mojo@yahoo.com</span><br>
                                <span id="client-town">Telp. : </span> 08123239499<br>
                                <span id="client-country"> Jl. Tebet Barat Dalam VIII B No.02, Tebet, Kota Jakarta Selatan, DKI Jakarta, 12810 </span><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table class="invoice-payment" cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td colspan="6">
                    <span class="t-payment-method">Nama Peserta</span>
                </td>

            </tr>

            <tr class="details">
                <td><span class="payment-method">Michelle Ma Belle</span> </td>
                <td class="left"><span class="payment-method">Perempuan</span> </td>
                <td><span class="payment-method">Istri</span> </td>
                <td><span class="payment-method">Bekasi, 02/04/1990</span></td>
                <td><span class="payment-method">Plan Kamar 800</span></td>
                <td> <span class="payment-price">Rp. 800.000</span></td>
            </tr>
            <tr class="details">
                <td><span class="payment-method">Zia</span> </td>
                <td><span class="payment-method">Laki-laki</span> </td>
                <td><span class="payment-method">Anak</span> </td>
                <td><span class="payment-method">Bekasi, 29/10/2018</span></td>
                <td><span class="payment-method">Plan Kamar 800</span></td>
                <td> <span class="payment-price">Rp. 800.000</span></td>
            </tr>
        </table>
        <table class="invoice-items" cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>Sub Total</td>
                <td><span class="t-price">Rp.16.000.000</span></td>
            </tr>
        </table>
        <div class="invoice-summary">
            <div class="invoice-summary-text">Detail Pembayaran
            </div>
            <div class="invoice-final">Service Plan</div>
            <div class="invoice-total"><span class="t-price">800</span></div>
            <div class="invoice-final">Jumlah Peserta</div>
            <div class="invoice-total"><span class="t-price">2 Orang</span></div>
            <div class="invoice-final">Tanggal Asuransi</div>
            <div class="invoice-total"><span class="t-price">05/08/2019</span></div>
            <div class="invoice-final">Jumlah Premi yang telah dibayar</div>
            <div class="invoice-total"><span class="t-price">Rp.16.000.000</span></div>
            <div class="invoice-final">Tanggal Pembayaran</div>
            <div class="invoice-total"><span class="t-price">05/08/2019</span></div>
        </div>

        <div class="bank-info">
            <div class="info-payment">
                <ol>
                    <li>Kwitansi <cite>elektronik</cite> ini adalah bukti pembayaran sah pendaftaram peserta Program DJPSEHAT.</li>
                    <li>Untuk informasi lebih lanjut mengenai produk dan klaim, kunjungi website kami di www.djpsehat.com atau hubungi call center kami melalui nomor telpon 0811-1400-106, 0811-911-0884, 0804-1168-168 dan email costumerservice@djpsehat.com.</li>
                    <li>Terimakasih kami sampaikan atas kepercayaan anda untuk terus menggunakan Program Perlindungan Kesehatan DJPSEHAT.</li>
                    <li>Kartu peserta akan dikirmkan maksimal 14 hari kerja sejak tanggal kwitansi.</li>
                </ol>
            </div>


        </div>
    </div>


    <div class="footer"><img src="<?php echo base_url() ?>assets/ela/images/agralogo.png" alt="tugu_logo">
		<div class="footer-text"> Menara Palma, 9th FloorJl. HR. Rasuna Said, Blok X2, Kav.6, Jakarta Selatan Indonesia 12950<br>Telp. : +62 (21) 2598 3210 | Fax : +62 (21) 2598 3211 | Email: cs.admin@agraindonesia.com | www.agraindonesia.com
		</div>
	</div>
</body>

</html>