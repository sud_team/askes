<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Invoice Tagihan DJP Sehat</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <style>
        /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
        /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
        
        @media only screen and (max-width: 720px) {
            table[class=body] h1 {
                font-size: 18px !important;
                margin-bottom: 0px !important;
            }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 14px !important;
            }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }
            table[class=body] .content {
                padding: 0 !important;
            }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            table[class=body] .btn table {
                width: 100% !important;
            }
            table[class=body] .btn a {
                width: 100% !important;
            }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }
        /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
        
        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }
    </style>

</head>

<body style="color:#777777 !important; background: radial-gradient(circle, #ffe3ef 0%, #cfe0f5 100%); font-family: source sans pro; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
     <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; padding: 6px;  ">
        <tr>
            <td style="font-family: source sans pro; font-size: 14px; vertical-align: top;">&nbsp;</td>
            <td class="container" style="font-family: source sans pro; font-size: 14px; vertical-align: top; display: block; Margin: 10px auto;  width: 480px; padding: 5px; max-width: 580px;">
                <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; min-width: 310px; max-width: 580px; padding: 5px;">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Nomor <?php echo $invoice->noInvoice; ?> Periksa email anda</span>
                    <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px; -moz-box-shadow: 0 10px 40px 0 #3e396b12, 0 2px 9px 0 #3e396b0f;
                    -webkit-box-shadow: 0 10px 40px 0 #3e396b12, 0 2px 9px 0 #3e396b0f;
                    box-shadow: 0 10px 40px 0 #3e396b12, 0 2px 9px 0 #3e396b0f;">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper" style="font-family: source sans pro; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                    <tr>
                                        <td style="width: 50%"><img src="http://dev-askes.saktiungguldata.co.id/assets/ela/images/tuguinvoice.jpg" alt="tugu_logo" style="width: 160px;">
                                            <h1 style="font-weight: normal;
                                           font-size: 18px;
                                      
                                           text-align: right !important; margin-bottom: 0; ">INVOICE TAGIHAN</h1>
                                            <p style="font-weight: 600; font-size: 18px; margin-top: 0px; text-align: right !important;">DJP Sehat</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: source sans pro; font-size: 14px; vertical-align: top;">
                                            <p style="font-family: source sans pro; font-size: 14px; font-weight: normal; margin: 0;">Kepada Yth,</p>
                                            <p style="font-family: source sans pro; font-size: 16px; font-weight: bold; margin: 0; Margin-bottom: 15px;">Adam Mewengkang</p>

                                            <p style="font-family: source sans pro; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Terimakasih telah melakukan pemesanan Asuransi Kesehatan Tugu Mandiri DJP Sehat pada Selasa, 13 Juni 2019 dengan rincian </p>
                                            <table align="center" cellpadding="2" style="width: 80%;
                                            max-width: 80%;  padding: 0 0px 20px 0px; align-content: center; float: center" cellspacing="0">
                                                <tbody>
                                                    <tr style=" color:#666 !important; font-size:16px;font-weight: bold ">
                                                        <td><b>Nama Peserta</b></td>
                                                        <td><b>Status</b>
                                                        </td>
                                                    </tr>
													<?php foreach($data_peserta AS $v){ ?>
														<tr>
															<td><?php echo $v["namaPeserta"]; ?></td>
															<td>
																<div style="background: #ff1846; border-radius: 4px; text-align: center; color: #ffffff;">Belum Aktif</div>
															</td>
														</tr>
													   
													<?php } ?>
                                                </tbody>
                                            </table>

                                            <div style="background-color: #fedee5; border-color: #ffc9d5; color: #ff1846; border-radius: 4px; padding: 10px 0;  margin: 0 0px 20px 0px; ">
                                                <table align="center" style="float:center" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: right; padding-right: 8px; font-weight: bold">Level Plan</td>
                                                            <td style="text-align: left"><?php echo $invoice->levelPlan; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-right: 8px; font-weight: bold">Nomor Invoice</td>
                                                            <td style="text-align: left"><?php echo $invoice->noInvoice; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-right: 8px; font-weight: bold">Tanggal Asuransi</td>
                                                            <td style="text-align: left"><?php echo $invoice->tglMulaiAsuransi; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-right: 8px; font-weight: bold">Jumlah Peserta</td>
                                                            <td style="text-align: left"><?php echo $jml_peserta; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-right: 8px; font-weight: bold">Total Premi</td>
                                                            <td style="text-align: left"><?php echo $invoice->jmlPremi; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <p style="font-family: source sans pro; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; ">Segera lakukan pembayaran selambat - lambatnya pada tanggal <span style="font-weight: bold; color:#ff1846 "> 29 Februari 2019</span> ke nomor rekening sebagai berikut.</p>
                                            <table align="center" width="100%" style="padding-bottom: 20px;float:center">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center"><img src="http://dev-askes.saktiungguldata.co.id/assets/ela/images/mandiriinvoice.jpg" alt="Bank_Mandiri_logo"  style="width: 140px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;font-family: source sans pro; font-size: 14px; "><span style="font-weight: bold; color:#333">Bank Mandiri Cab. Depkeu RI</span> <br>Nomor Rekening<br><span style="font-weight: bold; color:#333">130.000.9090800</span><br>Atas Nama<br><span style="font-weight: bold; color:#333">PT. Anugerah Bersama Berkah Abadi</span></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                            <p style="font-family: source sans pro; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; ">Apabila telah melakukan pembayaran, lakukan konfirmasi dengan mengirim foto bukti transfer melalui tombol dibawah ini</p>

                                            <table border="0 " cellpadding="0 " cellspacing="0 " class="btn btn-primary " style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; ">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" style="font-family: source sans pro; float: center; font-size: 14px; vertical-align: top; padding-bottom: 15px; ">
                                                            <table border="0 " cellpadding="0 " cellspacing="0 " style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto; ">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-family: source sans pro; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center; "> <a href="#" target="_blank " style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size:
                                                            14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db; ">Konfirmasi</a> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p style="font-family: source sans pro; font-size: 14px; font-weight: normal;  ">Informasi dan bantuan hubungi kami di <a style="color:#3498db " href="tel://08041168168">08041168168</a> <br>atau melalui Whatsapp <a style="color:#3498db " href="https://wa.me/628119110884" target="_blank">08119110884</a></p>
                                            <p style="font-family: source sans pro; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; "></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>

                    <!-- START FOOTER -->
                    <div class="footer " style="clear: both; Margin-top: 10px; text-align: center; width: 100%; ">
                        <table border="0 " cellpadding="0 " cellspacing="0 " style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; ">
                            <tr>
                                <td class="content-block " style="font-family: source sans pro; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #666666; text-align: center; ">
                                    <span class="apple-link " style="color: #999999; font-size: 12px; text-align: center; ">AGRA INDONESIA, Jl. HR. Rasuna Said, Blok X2, Kav.6, Jakarta Selatan Indonesia 12950</span>
                                    <!-- <br> Telp. : +62 (21) 2598 3210 Fax : +62 (21) 2598 3211 Email : claims@agraindonesia.com cs.admin@agraindonesia.com marketing@agraindonesia.com . -->
                                </td>
                            </tr>

                        </table>
                    </div>
                    <!-- END FOOTER -->

                    <!-- END CENTERED WHITE CONTAINER -->
                </div>
            </td>
            <td style="font-family: source sans pro; font-size: 14px; vertical-align: top; ">&nbsp;</td>
        </tr>
    </table>
</body>

</html>