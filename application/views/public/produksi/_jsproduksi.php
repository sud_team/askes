<script>
	function setDataPeserta(identifier,e){
		
		var value= $(identifier).val();
		var namaPeserta = "<?php echo $member->namaPeserta; ?>";
		var tglLahir = "<?php echo date("d/m/Y",strtotime($member->tglLahir)); ?>";
		var namaPeserta = "<?php echo $member->namaPeserta; ?>";
		var jenisKelamin = "<?php echo $member->jenisKelamin; ?>";
		var statusPernikahan = "<?php echo $member->statusPernikahan; ?>";
		var noHp = "<?php echo $member->noHp; ?>";
		
		
		if(value == "E"){
			$("#tglLahir").val(tglLahir).attr("readonly",true);
			$("#namaLengkap").val(namaPeserta).attr("readonly",true);
			$("#noHp").val(noHp).attr("readonly",true);
			$("#jenisKelamin").val(jenisKelamin).attr("readonly",true);;
			$('#jenisKelamin').select2().trigger('change');
			
			$("#statusPernikahan").val(statusPernikahan).attr("readonly",true);
			$('#statusPernikahan').select2().trigger('change');
		}else{
			$("#tglLahir").val("").attr("readonly",false);
			$("#namaLengkap").val("").attr("readonly",false);
			$("#noHp").val("").attr("readonly",false);
			$("#jenisKelamin").val("").attr("readonly",true);;
			$('#jenisKelamin').select2().trigger('change');
			
			$("#statusPernikahan").val("").attr("readonly",true);
			$('#statusPernikahan').select2().trigger('change');
		}
		
	}
	

	function pickDataRekonsel(identifier,e){
		var no = $('input[name="checkbox[]"]:checked').length;;
		var data = $(identifier).val();
		var dataPremi = $(identifier).data("premi");
		var jmlPremi = 0;
		$("#jml-invoice").html(no);
		
		if ($(identifier).is(':checked')) {
			jmlPremi = parseInt($("#jmlPremiHidden").val()) + dataPremi;
		}else{
			jmlPremi = parseInt($("#jmlPremiHidden").val()) - dataPremi;
		}
		
		$("#jmlPremiHidden").val(jmlPremi);
		$("#total-premi").html(number_format(jmlPremi));
	}
</script>