<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-stethoscope f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3 class="m-b-0">Form Klaim</h3>
                        <p> Formulir untuk pengajuan klaim DJP Sehat </p>
                    </div>
                </div>
            </div>
            <div class="card-body m-t-20">
                <div class="row">
                    <input type="hidden" name="noKlaim" value="KLM<?php echo $noKlaim ?>">
                    <input type="hidden" name="idPeserta" value="<?php echo $idPeserta ?>">
                    <div class="col-sm-6">

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Nama Peserta</span>
                            <span class=" col-md-7 text-info" name="namaPeserta"><?php echo $member->namaPeserta; ?>
                            </span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Jenis Kelamin</span>
                            <span class=" col-md-7 text-info"><?php echo $member->jenisKelamin; ?> </span>
                        </div>


                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tempat Tanggal Lahir</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->tempatLahir; ?>,
                                <?php echo $member->tglLahir; ?></span>
                        </div>

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Periode Asuransi</span>
                            <span class=" col-md-7 text-info"><?php echo date("d/m/Y", strtotime($member->tglMulaiAsuransi)); ?>
                                <span class=text-muted>s/d</span>
                                <?php echo date("d/m/Y", strtotime($member->tglAkhirAsuransi)); ?></span>
                        </div>


                    </div>

                    <div class="col-sm-6">

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Klaim</span>
                            <span class=" col-md-7 text-info"> <?php echo $noKlaim; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tanggal Klaim</span>
                            <span class=" col-md-7 text-info"> </span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Diajukan oleh</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->namaKaryawan; ?> </span>
                        </div>

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Registrasi</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noRegistrasi; ?></span>
                        </div>
                    </div>
                </div>


            </div>
            <hr>

            <form class="form-horizontal " role="form" method="post" action="<?php echo base_url("klaim/saveKlaim"); ?>" enctype="multipart/form-data">

                <input type="hidden" name="noKlaim" value="KLM<?php echo $noKlaim ?>">
                <input type="hidden" name="idPeserta" value="<?php echo $idPeserta ?>">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Data Rumah Sakit/Klinik</h6>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Rumah Sakit/Klinik</label>
                            <div class="col-md-7">
                                <input type="text" name="namaRumahSakit" required id="namaRumahSakit" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">No. Telpon</label>
                            <div class="col-md-7">
                                <input type="text" placeholder="No Telepon" required name="telpRumahSakit" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Alamat</label>
                            <div class="col-md-7">
                                <input type="text" name="alamatRumahSakit" required id="alamatRumahSakit" class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Anamnesa atau Riwayat Medis</h6>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Tanggal Pengobatan</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control datepicker" name="tglBerobat" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Kondisi Pasien</label>
                            <div class="col-md-7">
                                <input type="text" placeholder="Kondisi Pasien" name="kondisiPasien" required class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Diagnosa Penyakit</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="diagnosaSakit" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Total Biaya Pengobatan</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="biayaPengobatan" required>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row justify-content-center">
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Upload Dokumen Klaim</h6>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Resume Medis / Diagnosa</label>
                            <div class="col-md-7">
                                <div class="form-control">
                                    <input type="file" name="resumeMedis">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Kwitansi Pembayaran RS/Klinik</label>
                            <div class="col-md-7">
                                <div class="form-control">
                                    <input type="file" name="kwitansiPembayaran">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Rincian Pengobatan/Pembayaran</label>
                            <div class="col-md-7">
                                <div class="form-control">
                                    <input type="file" name="rincianPengobatan">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Dokumen Tambahan</label>
                            <div class="col-md-7">
                                <div class="form-control">
                                    <input type="file" name="dokumenTambahan">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Dokumen Tambahan</label>
                            <div class="col-md-7">
                                <div class="form-control">
                                    <input type="file" name="dokumenTambahan">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="form-actions row">
                    <a href="<?php echo base_url() ?>debitur/cari" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>

                    <button type="submit" class="btn  btn-warning btn-sm" value="submit" name="submit">
                        <i class="fa fa-paper-plane "></i> Ajukan </button>
                </div>
            </form>

            <!-- Data Debitur -->
            <!-- Form Klaim (bikin errro=align-self-center) -->
            <form class="form-horizontal " role="form" method="post" action="<?php echo base_url("klaim/saveKlaim"); ?>" enctype="multipart/form-data">
                <div class="card-outline-fade3 p-t-30 p-b-20">
                    <div class="card-header">
                        <h5 class="m-b-0 text-white">ANAMNESA/RIWAYAT PERAWATAN
                            <span class="m-l-40 m-t-10 m-b-10 text-right">No. Klaim : <?php echo $noKlaim; ?></span>
                        </h5>
                    </div>
                </div>
                <div class="card-body" role="form">
                    <div class="form-body">
                        <div class="table-responsive">
                            <input type="hidden" name="noKlaim" value="KLM<?php echo $noKlaim ?>">
                            <input type="hidden" name="idPeserta" value="<?php echo $idPeserta ?>">
                            <table id="my-lg-0" class="display narrow table-sm f-s-14" cellspacing="0" width="100%">
                                <tbody>

                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15"> Nama Rumah Sakit</td>
                                        <td class="w-20 ">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14" name="namaRumahSakit" required id="namaRumahSakit">
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15"> Alamat</td>
                                        <td class="w-20 ">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14" name="alamatRumahSakit" required id="alamatRumahSakit">
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15">Nomor Telepon </td>
                                        <td class="w-20 ">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14" placeholder="No Telepon" required name="telpRumahSakit">
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15"> Kondisi Pasien </td>
                                        <td class="w-20">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14" placeholder="Kondisi Pasien" name="kondisiPasien" required>
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15">Tanggal Berobat</td>
                                        <td class="w-20 ">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14 datepicker" placeholder="D/M/Y" name="tglBerobat" required>
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15"> Diagnosa Sakit</td>
                                        <td class="w-20">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14" placeholder="Diagnosa Sakit" name="diagnosaSakit" required>
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-15">Nominal Biaya Pengobatan</td>
                                        <td class="w-20 ">
                                            <div class="col-xs-4 has-warning">
                                                <input type="text" class="form-control f-s-14 price" placeholder="Rp." name="biayaPengobatan" required>
                                            </div>
                                        </td>
                                        <td class="w-3">&nbsp;</td>

                                    </tr>


                                </tbody>
                            </table>
                            <!--/span-->
                        </div>
                    </div>

                </div>
                <!-- Form Klaim -->
                <!-- Dokumen Klaim -->
                <div class="card-outline-fade3 p-t-30 p-b-20">
                    <div class="card-header">
                        <h5 class="m-b-0 text-white">Dokumen Klaim Upload
                            <span class="m-l-10 m-t-10 m-b-10 text-right">No. Klaim
                                KLM<?php echo $NomorRegistrasi ?></span>
                        </h5>
                    </div>
                </div>

                <div class="card-body">
                    <!-- Dokumen Umum -->
                    <!-- Dokumen Khusus -->
                    <h5 class="box-title text-center p-t-20  "> Resume Medis/Diagnosa </h5>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                        <tbody>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50">Resume Medis</td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="resumeMedis">
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Dokumen Khusus -->
                    <h5 class="box-title text-center p-t-20  "> Kwitansi Pembayaran </h5>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                        <tbody>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50">Kwitansi Pembayaran</td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="kwitansiPembayaran">
                                        </label>

                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Dokumen Khusus -->
                    <h5 class="box-title text-center p-t-20  "> Rincian Pengobatan </h5>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                        <tbody>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50">Dokumen Rincian Pengobatan</td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="rincianPengobatan">
                                        </label>

                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Dokumen Khusus -->
                    <h5 class="box-title text-center p-t-20  "> Dokumen Tambahan </h5>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                        <tbody>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50">Dokumen Tambahan</td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="dokumenTambahan">
                                        </label>

                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="col-lg-3">&nbsp;</div>
                <div class="form-actions p-2 pull-left">
                    <a href="<?php echo base_url() ?>debitur/cari" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>
                </div>
                <div class="form-actions p-2 pull-right">
                    <button type="submit" class="btn  btn-warning btn-sm" value="submit" name="submit">
                        <i class="fa fa-paper-plane "></i> Ajukan </button>
                </div>
            </form>
        </div>
    </div>

</div>