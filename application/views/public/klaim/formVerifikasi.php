<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa  fa-file-archive-o f-s-40 color-warning "></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3 class="m-b-0">Form Klaim</h3>
                        <p> Formulir untuk Verifikasi klaim DJP Sehat </p>
                    </div>
                </div>
            </div>
            <div class="card-body m-t-20">
                <div class="row">
                    <input type="hidden" name="noKlaim" value="KLM<?php echo $noKlaim ?>">
                    <input type="hidden" name="idPeserta" value="<?php echo $idPeserta ?>">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Klaim</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noKlaim; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Registrasi</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noRegistrasi; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Nama Peserta</span>
                            <span class=" col-md-7 text-info" name="namaPeserta"><?php echo $member->namaPeserta; ?>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Jenis Kelamin</span>
                            <span class=" col-md-7 text-info"><?php echo $member->jenisKelamin; ?> </span>
                        </div>


                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tempat Tanggal Lahir</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->tempatLahir; ?>,
                                <?php echo $member->tglLahir; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Periode Asuransi</span>
                            <span class=" col-md-7 text-info"><?php echo date("d/m/Y", strtotime($member->tglMulaiAsuransi)); ?>
                                <span class="text-secondary">s/d</span>
                                <?php echo date("d/m/Y", strtotime($member->tglAkhirAsuransi)); ?></span>
                        </div>

                    </div>
                </div>


            </div>
            <hr>
             <div class="row">
                 <div class="col-sm-6">
                     <div class="form-group text-center">
                         <h6>Data Rumah Sakit/Klinik</h6>
                     </div>
                    <div class="form-group row">
                         <span class=" text-md-right col-md-4">Nama Rumah Sakit</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->namaRumahSakit; ?></span>
                     </div>
                    
                    <div class="form-group row">
                         <span class=" text-md-right col-md-4">No. Telepon Rumah Sakit</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->telpRumahSakit; ?></span>
                     </div>
                     <div class="form-group row">
                         <span class=" text-md-right col-md-4">Alamat Rumah Sakit</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->alamatRumahSakit; ?></span>
                     </div>

                 </div>
                 <div class="col-sm-6">
                     <div class="form-group text-center">
                         <h6>Anamnesa atau Riwayat Medis</h6>
                     </div>

                     <div class="form-group row">
                         <span class=" text-md-right col-md-4">Tanggal Berobat</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->tglBerobat; ?></span>
                     </div>
                     <div class="form-group row">
                         <span class=" text-md-right col-md-4">Kondisi Pasien</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->kondisiPasien; ?></span>
                     </div>
                    <div class="form-group row">
                         <span class=" text-md-right col-md-4">Diagnosa Penyakit</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->diagnosaSakit; ?></span>
                     </div>
                     <div class="form-group row">
                         <span class=" text-md-right col-md-4">Biaya Pengobatan</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->biayaPengobatan; ?></span>
                     </div>
                 </div>
             </div>
             <hr>
             <div class="row justify-content-center">
                 <div class="col-sm-6">
                     <div class="form-group text-center">
                         <h6>Upload Dokumen Klaim</h6>
                     </div>
                     <div class="form-group row">
                         <span class=" text-md-right col-md-4">Resume Medis / Diagnosa</span> 		   
				<span class=" col-md-7 text-info"><a href="<?php echo base_url('klaim/download_dokumen/resumeMedis/'.$member->filename_resumeMedis); ?>" class="btn btn-outline-dark btn-sm"> <i class="fa fa-download" aria-hidden="true"></i></a></span>
                    
                     </div>
                     <div class="form-group row">
				<span class=" text-md-right col-md-4">Kwitansi Pembayaran RS/Klinik</span> 		   
				<span class=" col-md-7 text-info"><a href="<?php echo base_url('klaim/download_dokumen/kwitansiPembayaran/'.$member->filename_kwitansiPembayaran); ?>" class="btn btn-outline-dark btn-sm"> <i class="fa fa-download" aria-hidden="true"></i></a></span>
                     </div>
                     <div class="form-group row">                        
				<span class=" text-md-right col-md-4">Rincian Pengobatan/Pembayaran</span> 		    
				<span class=" col-md-7 text-info"><a href="<?php echo base_url('klaim/download_dokumen/kwitansiPembayaran/'.$member->filename_kwitansiPembayaran); ?>" class="btn btn-outline-dark btn-sm"> <i class="fa fa-download" aria-hidden="true"></i></a></span>
                     </div>
                      <div class="form-group row">                        
				<span class=" text-md-right col-md-4">Dokumen Tambahan</span> 		  
				<span class=" col-md-7 text-info"><a href="<?php echo base_url('klaim/download_dokumen/dokumenTambahan/'.$member->filename_dokumenTambahan); ?>" class="btn btn-outline-dark btn-sm"> <i class="fa fa-download" aria-hidden="true"></i></a></span>
                     </div>
                </div>

             </div>
             <hr>
			
			 <form method="post">
				<input type="hidden" value="<?php echo $member->idKlaim; ?>" name="idKlaim">
				 <?php if($member->status == "pengajuan"){ ?>
				<div class="row">
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Verifikasi Klaim</h6>
                        </div>
                       <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Tanggal Terima dokumen Asli</label>
                            <div class="col-md-7">
                                <input type="text" name="tglTerimaDokumenAsli" required id="tglTerimaDokumenAsli" class="form-control datepicker">
                            </div>
                        </div>
                       
                       <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Tanggal kirim Dokumen ke Asuransi</label>
                            <div class="col-md-7">
                                <input type="text" name="tglKirimKeAsuransi" required id="tglKirimKeAsuransi" class="form-control datepicker">
                            </div>
                        </div>
						 <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Catatan Verifikasi</label>
                            <div class="col-md-7">
                                <textarea class="form-control" rows="5" name="catatanVerifikasi"></textarea>
                            </div>
                        </div>

                    </div>
                   
                </div>
				<div class="form-actions ">

					<button type="submit" class="btn btn-secondary f-s-12" value="submit" name="notvalid">
                        <i class="fa fa-undo"></i> Tidak Valid </button>
                    <button type="submit" class="btn btn-warning f-s-12 pull-right" value="submit" name="valid">
                        <i class="fa fa-paper-plane-o "></i> Verifikasi </button>
                </div>
				 <?php }elseif($member->status == "verified"){ ?>
				 
             <div class="row">
                 <div class="col-sm-6">
                     <div class="form-group text-center">
                         <h6>Data Verifikasi</h6>
                     </div>
                    <div class="form-group row">
                         <span class=" text-md-right col-md-4">Tanggal Terima Dokumen Asli</span>
                         <span class=" col-md-7 text-info"> <?php echo date("d/m/Y",strtotime($member->tglTerimaDokumenAsli)); ?></span>
                     </div>
                    
                    <div class="form-group row">
                         <span class=" text-md-right col-md-4">Tanggal Kirim ke Asuransi</span>
                         <span class=" col-md-7 text-info"> <?php echo date("d/m/Y",strtotime($member->tglKirimKeAsuransi)); ?></span>
                     </div>
                     <div class="form-group row">
                         <span class=" text-md-right col-md-4">Catatan Verifikasi</span>
                         <span class=" col-md-7 text-info"> <?php echo $member->catatanVerifikasi; ?></span>
                     </div>

                 </div>                 
             </div>
             <hr>
				 <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Penilaian Klaim</h6>
                        </div>
                       <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Tanggal Keputusan Klaim</label>
                            <div class="col-md-7">
                                <input type="text" name="tglKeputusanKlaim" required id="tglKeputusanKlaim" class="form-control datepicker">
                            </div>
                        </div>
                       
                       <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Keputusan Klaim</label>
                            <div class="col-md-7">
								<select class="select2" style="width:100%" name="keputusanKlaim" required>
									<option value="">-Silahkan Pilih-</option>
									<option value="ditolak">Ditolak</option>
									<option value="disetujui">Disetujui</option>
									<option value="ditangguhkan">Ditangguhkan</option>
								</select>
                            </div>
                        </div>
                    </div>
					
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6></h6>
                        </div>
                       <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Alasan Kesimpulan Klaim</label>
                            <div class="col-md-7">
                                <input type="text" name="alasanKlaim" required id="alasanKlaim" class="form-control">
                            </div>
                        </div>
                       
                       <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Nominal Klaim Disetujui</label>
                            <div class="col-md-7">
                                <input type="text" name="tglKirimKeAsuransi" required id="nominalKlaimDisetujui" class="form-control">
                            </div>
                        </div>
                    </div>
                   
                </div>
				<div class="form-actions ">

					
                    <button type="submit" class="btn btn-warning f-s-12 pull-right" value="submit" name="penilaian">
                        <i class="fa fa-paper-plane-o "></i> Nilai </button>
                </div>				
				 <?php }elseif($member->status == "approved"){ ?>
				 
				 <div class="row">
					 <div class="col-sm-6">
						 <div class="form-group text-center">
							 <h6>Data Verifikasi</h6>
						 </div>
						<div class="form-group row">
							 <span class=" text-md-right col-md-4">Tanggal Terima Dokumen Asli</span>
							 <span class=" col-md-7 text-info"> <?php echo date("d/m/Y",strtotime($member->tglTerimaDokumenAsli)); ?></span>
						 </div>
						
						<div class="form-group row">
							 <span class=" text-md-right col-md-4">Tanggal Kirim ke Asuransi</span>
							 <span class=" col-md-7 text-info"> <?php echo date("d/m/Y",strtotime($member->tglKirimKeAsuransi)); ?></span>
						 </div>
						 <div class="form-group row">
							 <span class=" text-md-right col-md-4">Catatan Verifikasi</span>
							 <span class=" col-md-7 text-info"> <?php echo $member->catatanVerifikasi; ?></span>
						 </div>

					 </div>                 
				 </div>
				 <hr/>
				 <div class="row">
					 <div class="col-sm-6">
						 <div class="form-group text-center">
							 <h6>Penilaian Klaim</h6>
						 </div>
						<div class="form-group row">
							 <span class=" text-md-right col-md-4">Tanggal Keputusan Klaim</span>
							 <span class=" col-md-7 text-info"> <?php echo $member->tglKeputusanKlaim; ?></span>
						 </div>
						
						<div class="form-group row">
							 <span class=" text-md-right col-md-4">Keputusan Klaim</span>
							 <span class=" col-md-7 text-info"> <?php echo $member->keputusanKlaim; ?></span>
						 </div>
					 </div>          
					 <div class="col-sm-6">
						 <div class="form-group text-center">
							 <h6></h6>
						 </div>
						 <div class="form-group row">
							 <span class=" text-md-right col-md-4">Alasan Klaim</span>
							 <span class=" col-md-7 text-info"> <?php echo $member->alasanKlaim; ?></span>
						 </div>
						 <div class="form-group row">
							 <span class=" text-md-right col-md-4">Nominal Klaim Disetujui</span>
							 <span class=" col-md-7 text-info"> <?php echo price($member->nominalKlaimDisetujui); ?></span>
						 </div>

					 </div>      					 
				 </div>
				 <hr>
					 <div class="row">
						<div class="col-sm-6">
							<div class="form-group text-center">
								<h6>Penilaian Klaim</h6>
							</div>
						   <div class="form-group row">
								<label class="control-label text-md-right col-md-4">Tanggal Keputusan Klaim</label>
								<div class="col-md-7">
									<input type="text" name="tglKeputusanKlaim" required id="tglKeputusanKlaim" class="form-control datepicker">
								</div>
							</div>
						   
						   <div class="form-group row">
								<label class="control-label text-md-right col-md-4">Keputusan Klaim</label>
								<div class="col-md-7">
									<select class="select2" style="width:100%" name="keputusanKlaim" required>
										<option value="">-Silahkan Pilih-</option>
										<option value="ditolak">Ditolak</option>
										<option value="disetujui">Disetujui</option>
										<option value="ditangguhkan">Ditangguhkan</option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group text-center">
								<h6></h6>
							</div>
						   <div class="form-group row">
								<label class="control-label text-md-right col-md-4">Alasan Kesimpulan Klaim</label>
								<div class="col-md-7">
									<input type="text" name="alasanKlaim" required id="alasanKlaim" class="form-control">
								</div>
							</div>
						   
						   <div class="form-group row">
								<label class="control-label text-md-right col-md-4">Nominal Klaim Disetujui</label>
								<div class="col-md-7">
									<input type="text" name="tglKirimKeAsuransi" required id="nominalKlaimDisetujui" class="form-control">
								</div>
							</div>
						</div>
					   
					</div>
					<div class="form-actions ">

						
						<button type="submit" class="btn btn-warning f-s-12 pull-right" value="submit" name="penilaian">
							<i class="fa fa-paper-plane-o "></i> Nilai </button>
					</div>				
					<?php } ?>
					
					
            </form>
			

        </div>
    </div>

</div>