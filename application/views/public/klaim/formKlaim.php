<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa  fa-file-archive-o f-s-40 color-warning "></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3 class="m-b-0">Form Klaim</h3>
                        <p> Formulir untuk pengajuan klaim DJP Sehat </p>
                    </div>
                </div>
            </div>
            <div class="card-body m-t-20">
                <div class="row">
                    <input type="hidden" name="noKlaim" value="KLM<?php echo $noKlaim ?>">
                    <input type="hidden" name="idPeserta" value="<?php echo $idPeserta ?>">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Klaim</span>
                            <span class=" col-md-7 text-info"> <?php echo $noKlaim; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Registrasi</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noRegistrasi; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Nama Peserta</span>
                            <span class=" col-md-7 text-info" name="namaPeserta"><?php echo $member->namaPeserta; ?>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Jenis Kelamin</span>
                            <span class=" col-md-7 text-info"><?php echo $member->jenisKelamin; ?> </span>
                        </div>


                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tempat Tanggal Lahir</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->tempatLahir; ?> <span class="text-secondary">, </span>
                            <?php echo date("d/m/Y", strtotime($member->tglLahir)); ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Periode Asuransi</span>
                            <span class=" col-md-7 text-info"><?php echo date("d/m/Y", strtotime($member->tglMulaiAsuransi)); ?>
                                <span class="text-secondary">s/d</span>
                                <?php echo date("d/m/Y", strtotime($member->tglAkhirAsuransi)); ?></span>
                        </div>

                    </div>
                </div>


            </div>
            <hr>

            <form class="form-horizontal " role="form" method="post" action="<?php echo base_url("klaim/saveKlaim"); ?>" enctype="multipart/form-data">

                <input type="hidden" name="noKlaim" value="KLM<?php echo $noKlaim ?>">
                <input type="hidden" name="idPeserta" value="<?php echo $idPeserta ?>">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Data Rumah Sakit/Klinik</h6>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Rumah Sakit/Klinik</label>
                            <div class="col-md-7">
                                <input type="text" name="namaRumahSakit" required id="namaRumahSakit" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">No. Telpon</label>
                            <div class="col-md-7">
                                <input type="text" placeholder="" required name="telpRumahSakit" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Alamat</label>
                            <div class="col-md-7">
                                <input type="text" name="alamatRumahSakit" required id="alamatRumahSakit" class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Anamnesa atau Riwayat Medis</h6>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Tanggal Pengobatan</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control datepicker" name="tglBerobat" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Kondisi Pasien</label>
                            <div class="col-md-7">
                                <input type="text" placeholder="Kondisi Pasien" name="kondisiPasien" required class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Diagnosa Penyakit</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="diagnosaSakit" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-4">Total Biaya Pengobatan</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="biayaPengobatan" required>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row justify-content-center">
                    <div class="col-sm-6">
                        <div class="form-group text-center">
                            <h6>Upload Dokumen Klaim</h6>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Resume Medis / Diagnosa</label>
                            <div class="col-md-7">
                                <input type="file" name="resumeMedis" id="file-1" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-1" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file JPG, BMP atau PDF Max.2MB&hellip;</span></label>
                                <!-- <input type="file" name="resumeMedis"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Kwitansi Pembayaran RS/Klinik</label>
                            <div class="col-md-7">
                                <input type="file" name="kwitansiPembayaran" id="file-2" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-2" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file JPG, BMP atau PDF Max.2MB&hellip;</span></label>
                                <!-- <input type="file" name="kwitansiPembayaran"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Rincian Pengobatan/Pembayaran</label>
                            <div class="col-md-7">
                                <input type="file" name="rincianPengobatan" id="file-3" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-3" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file JPG, BMP atau PDF Max.2MB&hellip;</span></label>

                                <!-- <input type="file" name="rincianPengobatan"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Dokumen Tambahan</label>
                            <div class="col-md-7">
                                <input type="file" name="dokumenTambahan" id="file-4" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-4" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file JPG, BMP atau PDF Max.2MB&hellip;</span></label>

                                <!-- <input type="file" name="dokumenTambahan"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-md-right col-md-5">Dokumen Tambahan</label>
                            <div class="col-md-7">
                                <input type="file" name="dokumenTambahan" id="file-5" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-5" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file JPG, BMP atau PDF Max.2MB&hellip;</span></label>
                                <!-- <input type="file" name="dokumenTambahan"> -->
                            </div>
                        </div>

                    </div>

                </div>
                <hr>


                <dl class="text-right">
                    <dt>Diajukan oleh</dt>
                    <dd><?php echo $member->namaKaryawan; ?></dd>

                    <dt>Pada Tanggal</dt>
                    <dd><?php echo date("Y/m/d"); ?></dd>
                </dl>
                <hr>

                <div class="form-actions ">


                    <button href="<?php echo base_url() ?>peserta/listpesertaklien" class="btn btn-secondary f-s-12"><i class="fa fa-undo"></i> Batal</button>

                    <a type="submit" class="btn btn-warning f-s-12 pull-right ajukanklaim" value="submit" name="submit">
                        <i class="fa fa-paper-plane-o "></i> Ajukan </a>
                </div>
            </form>


        </div>
    </div>

</div>