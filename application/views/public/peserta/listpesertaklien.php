<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3>Daftar Peserta</h3>
                        <p>Detail peserta DJP Sehat untuk proses klaim dan lainya </p>
                    </div>
                </div>
            </div>

            <div class="card-body m-t-20">
                <ul>
                    <li>
						<?php foreach($members AS $m){ ?>
                        <div class="card">
                            <div class="card-header p-l-0 p-t-0">
                                No. Reg <b class="text-info"><?php echo $m->noRegistrasi; ?></b><br>
                            </div>
                            <div class="card-body m-t-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <dl>
                                            <dt>Nama Peserta</dt>
                                            <dd><?php echo $m->namaPeserta; ?></dd>
                                            <dt>Jenis Kelamin</dt>
                                            <dd><?php echo $m->jenisKelamin; ?></dd>
                                            <dt>Hubungan</dt>
                                            <dd><?php echo $m->hubunganKeluarga; ?></dd>
                                        </dl>
                                    </div>
                                    <div class="col-md-6">
                                        <dl>
                                            <dt>No. Peserta</dt>
                                            <dd><?php echo (!empty($m->noPeserta) ? $m->noPeserta : "-"); ?></dd>
                                            <dt>Periode Asuransi</dt>
                                            <dd><?php echo date("d/m/Y",strtotime($m->tglMulaiAsuransi)); ?> s/d <?php echo date("d/m/Y",strtotime($m->tglAkhirAsuransi)); ?></dd>
                                            <dt>Status Peserta</dt>
                                            <dd>
												<?php if($m->status_bayar == 'sudah_lunas'){ ?>
													<div class="badge badge-info f-s-12">Aktif</div>
												<?php }else{ ?>
													<div class="badge badge-danger f-s-12">Belum Aktif</div>
												<?php } ?>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="button-list"><a href="<?php echo base_url("peserta/listdetailpeserta/".$m->idPeserta); ?>" class="btn btn-outline-info btn-block  f-s-12">
                                                Detail
											</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<?php } ?>
                    </li>                 
                </ul>
            </div>
        </div>
    </div>
</div>