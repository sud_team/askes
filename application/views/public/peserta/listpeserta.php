<div class="row">
    <div class="col-12">
        <div class="card">
        <div class="card-header p-l-0 p-b-0">
                <h4 class="card-title">Data Peserta</h4>
                <h6 class="card-subtitle">Seluruh peserta Asuransi DJP Sehat yang telah terdaftar secara sah </h6>
            </div>
            <div class="card-body m-t-20">


                <!-- <h4 class="card-title">Cetak Data Pembayaran</h4>
                <h6 class="card-subtitle">Untuk mencetak Klaim dan Restitusi</h6> -->

                <!-- Menu Pencairan -->
                
                <div class="table-responsive">
                    <table id="listPeserta" class="display nowrap table-striped f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr  class="bg-primary text-light">
                                <th class="bg-primary text-light">No</th>
                                <th class="bg-primary text-light">No.Registrasi</th>
                                <th class="bg-primary text-light">Nama Peserta</th>
                                <th class="bg-primary text-light">Nama Karyawan</th>
                                <th class="bg-primary text-light">Status</th>
                                <th class="bg-primary text-light">Jenis Kelamin</th>
                                <th class="bg-primary text-light">Level Plan</th>
                                <th class="bg-primary text-light">Status Pernikahan</th>
                                <th class="bg-primary text-light">Tanggal Mulai Asuransi</th>
                                <th class="bg-primary text-light">Tanggal Lahir</th>
                                <th class="bg-primary text-light">Premi</th>
								<th class="bg-primary text-light">No.Peserta</th>                          
								<th class="bg-primary text-light">Status Bayar</th>                          
                            </tr>
                        </thead>
						<tbody>
							
						</tbody>
                    </table>
                </div>
                <!-- TABEL  (ditampilkan setelah di klik pencarian (tampilkan) -->

            </div>
        </div>
    </div>
</div>