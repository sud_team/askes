<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                    </div>
                    <div class="media-body m-l-20">
                        <h3 class="m-b-0">Data Peserta</h3>
                        <p> Terdaftar sebagai peserta Asuransi DJP Sehat </p>
                    </div>
                </div>
            </div>
            <div class="card-body m-t-20">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Registrasi</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noRegistrasi; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Level Plan</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->levelPlan; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Nama Peserta</span>
                            <span class=" col-md-7 text-info" name="namaPeserta"><?php echo $member->namaPeserta; ?>
                            </span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Hubungan</span>
                            <span class=" col-md-7 text-info" name="hubungan"><?php echo $member->hubunganKeluarga; ?>
                            </span>
                        </div>

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Jenis Kelamin</span>
                            <span class=" col-md-7 text-info"><?php echo $member->jenisKelamin; ?> </span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Telpon</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noHp; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tempat Lahir</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->tempatLahir; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tanggal Lahir</span>
                            <span class=" col-md-7 text-info"> <?php echo date("d/m/Y", strtotime($member->tglLahir)); ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Status Pernikahan</span>
                            <span class=" col-md-7 text-info"><?php echo $member->statusPernikahan; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tanggal Mulai Asuransi</span>
                            <span class=" col-md-7 text-info"><?php echo date("d/m/Y",strtotime($member->tglMulaiAsuransi)); ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tanggal Akhir Asuransi</span>
                            <span class=" col-md-7 text-info"><?php echo date("d/m/Y",strtotime($member->tglAkhirAsuransi)); ?> </span>
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Didaftarkan oleh</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->namaKaryawan; ?> </span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Email</span>
                            <span class=" col-md-7 text-info"><?php echo $this->session->userdata("username"); ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Invoice</span>
                            <span class=" col-md-7 text-info"> <?php echo $member->noInvoice; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Tanggal Aktif Asuransi</span>
                            <span class=" col-md-7 text-info"> <?php echo date("d/m/Y",strtotime($member->tglMulaiAsuransi)); ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Telpon</span>
                            <span class=" col-md-7 text-info"><?php echo $member->noHp; ?></span>
                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Alamat Lengkap</span>
                            <span class=" col-md-7 text-info"><?php echo $member->alamat; ?></span>

                        </div>
                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Nama Bank</span>
                            <span class=" col-md-7 text-info"><?php echo $member->namaBank; ?></span>
                        </div>

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">No. Rekening Bank</span>
                            <span class=" col-md-7 text-info"><?php echo $member->noRekeningBank; ?></span>
                        </div>

                        <div class="form-group row">
                            <span class=" text-md-right col-md-4">Nama Pemilik Rekening</span>
                            <span class=" col-md-7 text-info"><?php echo $member->namaPemilikBank; ?></span>
                        </div>

                    </div>
                </div>
                <div class="row justify-content-end">

                    <div class="col-sm-4 col-md-3 p-b-5">
                        <a class="btn btn-block btn-outline-warning f-s-12" href="<?php echo base_url('klaim/pengajuan/'.$idPeserta) ?>">Form Pengajuan Klaim</a>
                    </div>
                    <div class="col-sm-4 col-md-3 p-b-5">
                        <button class="btn btn-block btn-outline-danger f-s-12" data-toggle="collapse" href="<?php echo base_url('terminate/pengajuan'.$idPeserta) ?>" aria-expanded="true" aria-controls="ajukanterminate">Form Pengajuan Terminate</button>
                    </div>
                    <div class="col-sm-4 col-md-3 p-b-5">
                        <button class="btn btn-block btn-outline-info f-s-12" data-toggle="collapse" href="<?php echo base_url('revisi/pengajuan'.$idPeserta) ?>" aria-expanded="true" aria-controls="ajukanrevisi" aria-expanded="true" aria-controls="ajukanrevisi">Revisi Data</button>
                    </div>
                </div>


                <div class="accordion" id="accordionExample">

                    <!-- KLAIM FORM -->
                    <div class="collapse multi-collapse" id="ajukanklaim" data-parent="#accordionExample">
                        <div class="card">
                            <div class="card-header">
                                <h4>Form Pengajuan Klaim</h4>
                            </div>
                            <div class="card-body p-t-20">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group text-center">
                                            <h6>Data Rumah Sakit/Klinik</h6>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Rumah Sakit/Klinik</label>
                                            <div class="col-md-7">
                                                <input type="text" name=" " id=" " value=" " class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">No. Telpon</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder=" " name=" " id=" " value=" " class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Alamat</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder=" " name=" " id=" " value=" " class="form-control">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group text-center">
                                            <h6>Data Klaim Peserta</h6>
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Tanggal Pengobatan</label>
                                            <div class="col-md-7">
                                                <input type="text" name=" " id=" " value=" " class="form-control datepicker">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Kondisi Pasien</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder=" " name=" " id=" " value=" " class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Diagnosa Penyakit</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder=" " name=" " id=" " value=" " class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Total Biaya Pengobatan</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder=" " name=" " id=" " value=" " class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row justify-content-center">
                                    <div class="col-sm-6">
                                        <div class="form-group text-center">
                                            <h6>Upload Dokumen Klaim</h6>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-5">Resume Medis / Diagnosa</label>
                                            <div class="col-md-7">
                                                <div class="form-control">
                                                    <input type="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-5">Kwitansi Pembayaran RS/Klinik</label>
                                            <div class="col-md-7">
                                                <div class="form-control">
                                                    <input type="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-5">Rincian Pengobatan/Pembayaran</label>
                                            <div class="col-md-7">
                                                <div class="form-control">
                                                    <input type="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-5">Dokumen Tambahan</label>
                                            <div class="col-md-7">
                                                <div class="form-control">
                                                    <input type="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-5">Dokumen Tambahan</label>
                                            <div class="col-md-7">
                                                <div class="form-control">
                                                    <input type="file">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning f-s-12 pull-right"> Ajukan Klaim <i class="fa fa-paper-plane-o" style="font-size:16px"></i></button>
                                <button class="btn btn-secondary f-s-12" data-toggle="collapse" href="#ajukanklaim" role="button" aria-expanded="false" aria-controls="ajukanklaim">Tutup</button>
                            </div>
                        </div>
                    </div>

                    <!-- TERMINASI -->
                    <div class="collapse multi-collapse" id="ajukanterminate" data-parent="#accordionExample">
                        <div class="card">
                            <div class="card-header">
                                <h4>Form Pengajuan Terminate</h4>
                            </div>
                            <div class="card-body p-t-20">
                                <div class="row justify-content-center">
                                    <div class="col-sm-6">
                                        <div class="form-group text-center">
                                            <h6>Isi alasan terminasi</h6>
                                        </div>
                                        <div class="form-group row">
                                            <span class=" text-md-right col-md-4">Tanggal Pengajuan</span>
                                            <span class=" col-md-7 text-info"><?php echo date("d/m/Y");?></span>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Alasan </label>
                                            <div class="col-md-7">
                                                <textarea class="form-control h-100"></textarea> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-danger f-s-12 pull-right">Ajukan Terminasi <i class="fa fa-paper-plane-o" style="font-size:16px"></i></button>
                                <button class="btn btn-secondary f-s-12" data-toggle="collapse" href="#ajukanterminate" role="button" aria-expanded="false" aria-controls="ajukanklaim">Tutup</button>
                            </div>
                        </div>
                    </div>

                    <!-- REVISI -->
                    <div class="collapse multi-collapse" id="ajukanrevisi" data-parent="#accordionExample">
                        <div class="card">
                            <div class="card-header">
                                <h4>Form Refisi Data</h4>
                            </div>
                            <div class="card-body p-t-20">
                                <div class="row justify-content-center">
                                    <div class="col-sm-6">
                                        <div class="form-group text-center">
                                            <h6>Tentukan data yang akan di revisi </h6>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Nama Lengkap</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder="Sesuai Identitas" name="namaPeserta" id="namaPeserta" value="<?php echo $member->namaPeserta; ?>" class="form-control">
                                                <!--<small class="text-danger">Wajib diisi</small>-->
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Jenis Kelamin</label>
                                            <div class="col-md-7">
                                                <select style="width: 100%" class="select2 form-control" id="jenisKelamin" name="jenisKelamin" <?php echo $member->jenisKelamin; ?> required>
                                                    <option value="">-Pilih-</option>
                                                    <option value="M" <?php echo ($member->jenisKelamin == "M" ? "selected" : ""); ?>>Laki-laki</option>
                                                    <option value="F" <?php echo ($member->jenisKelamin == "F" ? "selected" : ""); ?>>Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Tempat Lahir</label>
                                            <div class="col-md-7">
                                                <input type="text" placeholder="Tempat Lahir" name="tempatLahir" id="tempatLahir" value="<?php echo $member->tempatLahir; ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-md-right col-md-4">Tanggal Lahir</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control datepickerttl" placeholder="dd/mm/yyyy" value="<?php echo date("d/m/Y", strtotime($member->tglLahir)); ?>" name="tglLahir" id="tglLahir" required>
                                                <!--<small class="text-danger">Wajib diisi</small>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-info f-s-12 pull-right">Ajukan Revisi <i class="fa fa-paper-plane-o" style="font-size:16px"></i></button>
                                <button class="btn btn-secondary f-s-12" data-toggle="collapse" href="#ajukanrevisi" role="button" aria-expanded="false" aria-controls="ajukanrevisi">Tutup</button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>