<?php if($this->session->userdata("roles") == 5){ ?>
	<div class="row">
	<div class="col-md-6 col-lg-6 col-sm-6 hvr-wobble-horizontal">
	<a href="<?php echo base_url('karyawan/karyawan_view') ?>"><div class="card p-30">
			<div class="media">
				<div class="media-left meida media-middle">
					<span><i class="fa fa-user f-s-40 color-danger"></i></span>
				</div>
				<div class="media-body m-l-20">
					<h3>Data Karyawan</h3>
					<p class="m-b-0">Informasi dan kelengkapan data pengguna </p>
				</div>
			</div>
		</div></a>
	</div>
	<div class="col-md-6 col-lg-6 col-sm-6 hvr-wobble-horizontal">
	<a href="<?php echo base_url('order/tambah_peserta') ?>">
		<div class="card p-30">
			<div class="media">
				<div class="media-left meida media-middle">
					<span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
				</div>
				<div class="media-body m-l-20">
					<h3>Pendaftaran Peserta Asuransi</h3>
					<!-- <p class="m-b-0">Pendaftaran dan Pembelian Premi Asuransi</p> -->
				</div>
			</div>
		</div>
	</a>
	</div>
	<div class="col-md-6 col-lg-6 col-sm-6 hvr-wobble-horizontal">
	<a href="<?php echo base_url('pembayaran/datapembayaran') ?>">
		<div class="card p-30">
			<div class="media">
				<div class="media-left meida media-middle">
					<span><i class="fa fa-credit-card f-s-40 color-primary"></i></span>
				</div>
				<div class="media-body m-l-20">
					<h3>Pembayaran</h3>
					<p class="m-b-0">Histori pemesanan dan pembayaran Premi Asuransi</p>
				</div>
			</div>
		</div>
	</a>
	</div>

	<div class="col-md-6 col-lg-6 col-sm-6 hvr-wobble-horizontal">
	<a href="<?php echo base_url('laporan/klaim') ?>">
		<div class="card p-30">
			<div class="media">
				<div class="media-left meida media-middle">
					<span><i class="fa  fa-file-archive-o f-s-40 color-warning"></i></span>
				</div>
				<div class="media-body m-l-20">
					<h3>Klaim</h3>
					<p class="m-b-0">Pengajuan dan Progres Klaim pererta terdaftar</p>
				</div>
			</div>
		</div>
	</a>
	</div>

	</div>
<?php } ?>
