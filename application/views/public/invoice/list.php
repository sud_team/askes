<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Invoice</h4>
                <h6 class="card-subtitle">
                </h6>
                <br>
                <div class="table-responsive">
                    <form method="post" action="<?php echo base_url() ?>produksi/saveBatasPensiun">
                        <table id="DataPrapen" class="display nowrap table-hover table-bordered" cellspacing="0" width="100%">
                            <table id="upload" class="display table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-3">#</th>
                                <th>Tgl Upload</th>
                                <th class="w-6">Action</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach($data_invoice AS $v){ ?>
								<tr>
									<td class="w-3"></td>
									<td class="w-3"><?php echo $v->tgl_upload ?></td>
									<td class="w-3"><a href="<?php echo site_url("invoice/generate_pdf/".$v->tgl_upload) ?>" class="btn btn-success" >PDF</a></td>
								</tr>
							<?php } ?>
						</tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>