<!DOCTYPE html>
<html lang="en" >

<head>
<meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="djpsehat, asuransi, kesehatan, dirjen pajak" name="keywords">

    <!-- HTML Meta Tags -->
    <title>Login | DJP Sehat </title>
    <meta name="description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="DJP Sehat">
    <meta itemprop="description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">
    <meta itemprop="image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="http://djpsehat.com/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="DJP Sehat">
    <meta property="og:description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">
    <meta property="og:image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DJP Sehat">
    <meta name="twitter:description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia">
    <meta name="twitter:image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">

    <!-- Whatsapp thumb -->
    <meta property="og:site_name" content="http://djpsehat.com/">
    <meta property="og:title" content="DJP Sehat" />
    <meta property="og:description" content="Tugu Mandiri | Asuransi Kesehatan terbaik untuk Karyawan Dirjen Pajak dari Agra Indonesia" />
    <meta property="og:image" itemprop="image" content="http://djpsehat.com/<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-180x180.png">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />
    <!-- Meta Tags Generated via http://heymeta.com -->
    <!-- Favicons -->

    <!-- <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon"> -->

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/ela/icons/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url() ?>assets/ela/icons/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/ela/icons/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/ela/icons/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/ela/icons/icon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/ela/icons/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/ela/icons/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/logincias/logincias.css"> -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/animate.css">
    <link href="<?php echo base_url() ?>assets/ela/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/ela/css/helper.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/ela/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/ela/css/gradientactive.css" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar login-gradient">
    <!-- <body class="fix-header fix-sidebar gradient-active"> -->
    <!-- <span>
        <img src="<?php echo base_url() ?>assets/ela/images/logo.png" alt="homepage" class="dark-logo" />
    </span> -->
    <div id="main-wrapper">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-5 col-sm-8 col-xs-12">
                    <div class="login-content card animated fadeIn">
                        <div class="login-form">
                            <div class="animated fadeIn text-center" href="http://djpsehat.com/" target="_blank">
                                <!-- Logo icon -->
                                <img src="<?php echo base_url() ?>assets/ela/images/tugulogo.png" alt="homepage" class="login-logo " />
                                <p class="m-b-5 text-center">DJP Sehat</p> <br>
                                <h4>Login</h4>
                            </div>
                            <!-- <h4>Login</h4> -->
                            <form id="login_form" class="dialog-form text-left" action="<?php echo base_url(); ?>login/auth" method="post" name="loginform">
                                <fieldset>
                                    <div class="form-group">

                                        <?php $success_message = $this->session->flashdata('success'); ?>
                                        <?php if ($success_message) : ?>
                                            <div class="text-success text-center animated flash">
                                                <i class="fa fa-check"></i>
                                                <span><?php echo $this->session->flashdata('success'); ?></span>
                                            </div>
                                        <?php endif ?>
                                        <?php $error_message = $this->session->flashdata('warning'); ?>
                                        <?php if ($error_message) : ?>
                                            <div class="text-danger text-center">
                                                <i class="fa fa-ban"></i>
                                                <span><?php echo $this->session->flashdata('warning'); ?></span>
                                            </div>
                                        <?php endif ?>
                                    </div>
									<div class="hide-form">
										<div class="form-group animated fadeInUp delay2">
											<label for="user_name">Email</label>
											<input type="text" id="user_username" class="form-control" name="username" autocomplete="on" />
										</div>
										<div class="form-group animated fadeInUp delay4">
											<label for="user_password">Password</label>
											<input type="password" id="user_password" class="form-control" name="password" autocomplete="on">
										</div>
										<div class="checkbox  animated fadeInUp delay6">
											<label>
												<input type="checkbox"> Remember Me
											</label>
											<label class="pull-right">
											<a href="#"> Lupa Password?</a>
										</label>
										</div>
									</div>
                                    <div class="m-b-30 m-t-20 ">
                                        <div class="text-center animated zoomIn delay10 m-t-10">
											<button type="submit"  class="btn btn-primary btn-flat animated fadeInUp delay8">Login</button>
                                            <p class="m-t-5">Belum punya akun? <a href="<?php echo base_url('registration/user') ?>">Registrasi Disini</a></p>                                           

                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <script src="<?php echo base_url() ?>assets/ela/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url() ?>assets/ela/js/jquery.slimscroll.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url() ?>assets/ela/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>

    <script src="<?php echo base_url() ?>assets/ela/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>assets/ela/js/lib/owl-carousel/owl.carousel-init.js"></script>

    <script src="<?php echo base_url() ?>assets/ela/js/custom.min.js"></script>


</body>

</html>