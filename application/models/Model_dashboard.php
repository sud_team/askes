<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}
	

	public function grafik1($cabang, $capem, $tahun){
		$and = "";
		
		if($cabang==0 && $capem==0){
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				if ($kodeBank=="000") {
					$and .= "";
				} else {
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
				}
			}
		}else if($cabang!=0 && $capem==0){
			$and .= " and kodeBank = '$cabang'";		
		}else{
			$and .= " and kodeBank = '$capem'";
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(JumlahPremiTenor) plafon from t_pinjaman where convert(varchar(7),create_date,120) = '$tahun' and StatusRekonsel = 1 ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafikospremi($cabang, $capem, $tahun){
		$and = "";
		if($cabang==0 && $capem==0){
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				if ($kodeBank=="000") {
					$and .= "";
				} else {
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
				}
			}
		}else if($cabang!=0 && $capem==0){
			$and .= " and kodeBank = '$cabang'";		
		}else{
			$and .= " and kodeBank = '$capem'";
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(TotalOsPremi) ospremi from t_pinjaman where convert(varchar(7),create_date,120) = '$tahun' and StatusRekonsel = 1  ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->ospremi;
		}

	}

	public function grafikklaim($tahun){
		$and = "";
		if($this->session->userdata("roles")=="2"){
			$kodeBank = $this->session->userdata("kodeBank");
			if ($kodeBank=="000") {
				$and .= "";
			} else {
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(NominalDisetujui) plafon from t_pinjaman_klaim a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi) where convert(varchar(7),b.create_date,120) = '$tahun' and StatusPembayaran = '1' ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafikrestitusi($tahun){
		$and = "";
		if($this->session->userdata("roles")=="2"){
			$kodeBank = $this->session->userdata("kodeBank");
			if ($kodeBank=="000") {
				$and .= "";
			} else {
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(PengembalianPremi) plafon from t_pinjaman_restitusi a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi) where convert(varchar(7),b.create_date,120) = '$tahun' and StatusPembayaran = '1' ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafikpremi($tahun){
		$and = "";
		if($this->session->userdata("roles")=="2"){
			$kodeBank = $this->session->userdata("kodeBank");
			if ($kodeBank=="000") {
				$and .= "";
			} else {
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(JumlahPremiTenor) plafon from t_pinjaman where convert(varchar(7),create_date,120) = '$tahun' and StatusRekonsel = 1 ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafik1tahun($cabang, $capem, $tahun){
		$and = "";
		if($cabang==0 && $capem==0){
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				if ($kodeBank=="000") {
					$and .= "";
				} else {
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
				}
			}
		}else if($cabang!=0 && $capem==0){
			$and .= " and kodeBank = '$cabang'";		
		}else{
			$and .= " and kodeBank = '$capem'";
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(plafon) plafon from t_pinjaman where convert(varchar(4),create_date,120) = '$tahun' and StatusRekonsel = 1 ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafikklaimtahun($cabang, $capem, $tahun){
		$and = "";
		if($cabang==0 && $capem==0){
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				if ($kodeBank=="000") {
					$and .= "";
				} else {
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
				}
			}
		}else if($cabang!=0 && $capem==0){
			$and .= " and kodeBank = '$cabang'";		
		}else{
			$and .= " and kodeBank = '$capem'";
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(NominalDisetujui) plafon from t_pinjaman_klaim a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi) where convert(varchar(4),b.create_date,120) = '$tahun' and StatusPembayaran = '1' ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafikrestitusitahun($cabang, $capem, $tahun){
		$and = "";
		if($cabang==0 && $capem==0){
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				if ($kodeBank=="000") {
					$and .= "";
				} else {
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
				}
			}
		}else if($cabang!=0 && $capem==0){
			$and .= " and kodeBank = '$cabang'";		
		}else{
			$and .= " and kodeBank = '$capem'";
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(PengembalianPremi) plafon from t_pinjaman_restitusi a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi) where convert(varchar(4),b.create_date,120) = '$tahun' and StatusPembayaran = '1' ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function grafikpremitahun($cabang, $capem, $tahun){
		$and = "";
		if($cabang==0 && $capem==0){
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				if ($kodeBank=="000") {
					$and .= "";
				} else {
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
				}
			}
		}else if($cabang!=0 && $capem==0){
			$and .= " and kodeBank = '$cabang'";		
		}else{
			$and .= " and kodeBank = '$capem'";
		}

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		$query = $this->db->query("select sum(JumlahPremiTenor) plafon from t_pinjaman where convert(varchar(4),create_date,120) = '$tahun' and StatusRekonsel = 1 ".$and);
		if(empty($query)){
			return 0;
		}else{
			return $query->row()->plafon;
		}

	}

	public function JatuhTempo(){
		$and ="";
		if($this->session->userdata("roles")=="2"){
			$kodeBank = $this->session->userdata("kodeBank");
			if ($kodeBank=="000") {
				$and .= "";
			} else {
			$and .= " and kodeBank = '$kodeBank' ";
			}
		}

		$hariini = date_format(date_create(hariini()),'Y-m-d');
		$query = $this->db->query("select a.NomorPinjaman, NamaDebitur, asuransi, kodeBank, min(TahunKe) TahunKe from t_pinjaman_bj a join t_pinjaman b on a.NomorRegistrasi=b.NomorRegistrasi and a.NomorPinjaman=b.NomorPinjaman join t_debitur c on b.idDebitur=c.idDebitur and b.cif=c.cif join t_asuransi d on d.kodeAngka=b.kodeAsuransi where StatusBayar = 0 and convert(varchar(10),TglJatuhTempo,120) between '2018-01-01' and '2018-12-31' ".$and." group by a.NomorPinjaman, NamaDebitur,asuransi,kodeBank");
		// $query = $this->db->query("select a.NomorPinjaman, NamaDebitur, asuransi, min(TahunKe) TahunKe from t_pinjaman_bj a join t_pinjaman b on a.NomorRegistrasi=b.NomorRegistrasi and a.NomorPinjaman=b.NomorPinjaman join t_debitur c on b.idDebitur=c.idDebitur and b.cif=c.cif join t_asuransi d on d.kodeAngka=b.kodeAsuransi where StatusBayar = 0 and convert(varchar(10),dateadd(m,-1, TglJatuhTempo),120) <= '$hariini' and convert(varchar(10),TglJatuhTempo,120) >= '$hariini' ".$and." group by a.NomorPinjaman, NamaDebitur,asuransi ");
		return $query;
	}

	public function JatuhTempoDt($nopin,$tahunke){
		$and ="";
		if($this->session->userdata("roles")=="2"){
			$kodeBank = $this->session->userdata("kodeBank");
			if ($kodeBank=="000") {
				$and .= "";
			} else {
			$and .= " and kodeBank = '$kodeBank' ";
			}
		}

		$hariini = date_format(date_create(hariini()),'Y-m-d');
		$query = $this->db->query("select ospremi, TglJatuhTempo from t_pinjaman_bj where NomorPinjaman = '$nopin' and TahunKe = '$tahunke' ".$and);
		return $query->row();
	}
}