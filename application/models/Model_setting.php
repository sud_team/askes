<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_setting extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function t_asuransi(){
		$query = $this->db->query("select * from t_asuransi where kodeAngka != 99");
		return $query->result();
	}

	public function list_debitur_prapen(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updatePrapen(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$query = $this->db->query("update t_pinjaman set StatusPrapen = '1' where NomorPinjaman = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Status Prapen Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function list_debitur_prorate(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function list_debitur_umur(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, a.TglLahir as tgl, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen, UsiaDebitur from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function list_debitur_polis(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select a.NomorRegistrasi,NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, a.TglLahir as tgl, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur, NomorPolis, UsiaDebitur from t_pinjaman a join t_pinjaman_polis b on(a.NomorRegistrasi=b.NomorRegistrasi) join t_debitur c on(a.idDebitur=c.idDebitur and a.cif=c.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function list_debitur_mutasi(){
		$nopin = $this->input->post("nopin");
		$abc = $this->db->query("select idDebitur from t_pinjaman where NomorPinjaman = '$nopin'")->row()->idDebitur;
		$query = $this->db->query("select a.idDebitur, NomorRegistrasi, NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, a.TglLahir as tgl, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPinjaman, UsiaDebitur from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where a.idDebitur = '$abc'");
		return $query;
	}

	public function list_debitur_data_mutasi(){
		$query = $this->db->query("select * from history_delete");
		return $query;
	}

	public function updateUmur(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$usia = $this->input->post("UsiaDebitur");
		$tgllahir = date_format(date_create($this->input->post("tgllahir")),"Y-m-d");
		$query = $this->db->query("update a set a.TglLahir = '$tgllahir' from t_debitur a join t_pinjaman b on a.idDebitur=b.idDebitur where NomorPinjaman = '$nopin'");
		$query2 = $this->db->query("update t_pinjaman set TglLahir = '$tgllahir', UsiaDebitur = '$usia' where NomorPinjaman = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Usia Debitur dan Tgl Lahir Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function updatePolis(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorRegistrasi");
		$nomorpolis = $this->input->post("nomorpolis");
		$query = $this->db->query("update t_pinjaman_polis set NomorPolis = '$nomorpolis'  where NomorRegistrasi = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Polis Nomor Registrasi $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function updateMutasi(){
		$user = create_user();
		$hariini = hariini();
		$jml = substr($this->input->post("NomorPinjaman"), 0,1);
		$kode = substr($this->input->post("NomorPinjaman"), 1);
		if($jml>1){
			$abc = $this->db->query("select idDebitur from t_pinjaman where NomorPinjaman = '$kode'")->row()->idDebitur;
			$query_x = $this->db->query("select * from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where a.idDebitur = '$abc' and NomorPinjaman = '$kode'")->row();
			if(empty($query_x->Bunga)){
				$bunga = 0;
			}else{
				$bunga = $query_x->Bunga;
			}
			$insert = $this->db->query("INSERT INTO history_delete VALUES
           	('$query_x->NomorRegistrasi'
           	,'$query_x->idDebitur'
           	,'$query_x->cif'
           	,'$query_x->NamaDebitur'
           	,'$query_x->jk'
			,'$query_x->NomorKtp'
			,'$query_x->TempatLahir'
			,'$query_x->TglLahir'
			,'$query_x->alamat'
			,'$query_x->UsiaDebitur'
			,'$query_x->KodePekerjaan'
			,'$query_x->NomorPinjaman'
			,'$query_x->kodeBank'
			,'$query_x->idProduk'
			,'$query_x->NomorPK'
			,'$query_x->TglAkadKredit'
			,'$query_x->TglAkhirKredit'
			,'$query_x->TenorTahun'
			,'$query_x->TenorBulan'
			,'$query_x->UsiaDebiturKredit'
			,'$query_x->plafon'
			,'$query_x->TarifPremi'
			,'$query_x->premi'
			,'$query_x->JumlahPremiTenor'
			,'$query_x->TglByrPremi'
			,'$query_x->FeePresenBroker'
			,'$query_x->FeePremiBroker'
			,'$query_x->FeePersenBank'
			,'$query_x->FeePremiBank'
			,'$query_x->NomorReferensiPremi'
			,'$query_x->StatusPinjaman'
			,'$query_x->kodeAsuransi'
			,'$query_x->idTc'
			,'$query_x->TcKet'
			,'$query_x->TcAc'
			,'$query_x->StatusPolis'
			,'$query_x->StatusRekonsel'
			,'$query_x->StatusKlaim'
			,'$query_x->StatusRestitusi'
			,'$query_x->StatusDok'
			,'$query_x->TglRekonsel'
			,'$query_x->TglDokLengkap'
			,'$query_x->TglAlokasi'
			,'$query_x->TotalOsPremi'
			,'$query_x->NomorInvoice'
			,'$query_x->TotalSeharusnya'
			,'$query_x->StatusPrapen'
			,'$query_x->umur2'
			,'$query_x->StatusBunga'
			,'$bunga'
			,'$query_x->create_user'
			,'$query_x->create_date'
			,'$query_x->update_user'
			,'$query_x->update_date'
			,'$user'
			,'$hariini')");
			$query = $this->db->query("delete from t_pinjaman_dok where NomorRegistrasi = '$query_x->NomorRegistrasi'");
			$query = $this->db->query("delete from t_pinjaman_polis where NomorRegistrasi = '$query_x->NomorRegistrasi'");
			$query = $this->db->query("delete from t_pinjaman_bj where NomorPinjaman = '$kode'");
			$query = $this->db->query("delete from t_pinjaman where NomorPinjaman = '$kode'");
		}else{
			$query_x = $this->db->query("select * from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where a.idDebitur = '$kode'")->row();
			if(empty($query_x->Bunga)){
				$bunga = 0;
			}else{
				$bunga = $query_x->Bunga;
			}
			$insert = $this->db->query("INSERT INTO history_delete VALUES
           	('$query_x->NomorRegistrasi'
           	,'$query_x->idDebitur'
           	,'$query_x->cif'
           	,'$query_x->NamaDebitur'
           	,'$query_x->jk'
			,'$query_x->NomorKtp'
			,'$query_x->TempatLahir'
			,'$query_x->TglLahir'
			,'$query_x->alamat'
			,'$query_x->UsiaDebitur'
			,'$query_x->KodePekerjaan'
			,'$query_x->NomorPinjaman'
			,'$query_x->kodeBank'
			,'$query_x->idProduk'
			,'$query_x->NomorPK'
			,'$query_x->TglAkadKredit'
			,'$query_x->TglAkhirKredit'
			,'$query_x->TenorTahun'
			,'$query_x->TenorBulan'
			,'$query_x->UsiaDebiturKredit'
			,'$query_x->plafon'
			,'$query_x->TarifPremi'
			,'$query_x->premi'
			,'$query_x->JumlahPremiTenor'
			,'$query_x->TglByrPremi'
			,'$query_x->FeePresenBroker'
			,'$query_x->FeePremiBroker'
			,'$query_x->FeePersenBank'
			,'$query_x->FeePremiBank'
			,'$query_x->NomorReferensiPremi'
			,'$query_x->StatusPinjaman'
			,'$query_x->kodeAsuransi'
			,'$query_x->idTc'
			,'$query_x->TcKet'
			,'$query_x->TcAc'
			,'$query_x->StatusPolis'
			,'$query_x->StatusRekonsel'
			,'$query_x->StatusKlaim'
			,'$query_x->StatusRestitusi'
			,'$query_x->StatusDok'
			,'$query_x->TglRekonsel'
			,'$query_x->TglDokLengkap'
			,'$query_x->TglAlokasi'
			,'$query_x->TotalOsPremi'
			,'$query_x->NomorInvoice'
			,'$query_x->TotalSeharusnya'
			,'$query_x->StatusPrapen'
			,'$query_x->umur2'
			,'$query_x->StatusBunga'
			,'$bunga'
			,'$query_x->create_user'
			,'$query_x->create_date'
			,'$query_x->update_user'
			,'$query_x->update_date'
			,'$user'
			,'$hariini')");
			$query = $this->db->query("delete from t_pinjaman_dok where NomorRegistrasi = '$query_x->NomorRegistrasi'");
			$query = $this->db->query("delete from t_pinjaman_polis where NomorRegistrasi = '$query_x->NomorRegistrasi'");
			$query = $this->db->query("delete from t_pinjaman where idDebitur = '$kode'");
			$query2 = $this->db->query("delete from t_debitur where idDebitur = '$kode'");
		}

		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Delete Debitur Nomor Pinjaman $kode')");
			return 1;
		}else{
			return 0;
		}
	}

	public function updateProrate(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$premi = str_replace('.','',$this->input->post("JumlahPremiTenor"));
		$premi = str_replace(' ','',$premi);
		$fee = $premi*0.1;
		$query = $this->db->query("update t_pinjaman set JumlahPremiTenor = '$premi', FeePremiBroker = '$fee', FeePremiBank = '$fee', TotalSeharusnya = '$premi' where NomorPinjaman = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Premi Prorate Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function list_debitur_pindahan(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select a.NomorPinjaman, a.TglAkadKredit, a.TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur, kodeAngka, asuransi, a.kodeAsuransi from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) join t_asuransi c on c.kodeAngka=a.kodeAsuransi where a.NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updatePindahan(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$tahun = $this->input->post("TahunKe");
		$kodeAsuransiBaru = $this->input->post("kodeAsuransi");
		$tahunsebelumnya = $tahun;
		$query = $this->db->query("select NomorRegistrasi, kodeAsuransi from t_pinjaman where NomorPinjaman = '$nopin'");
		$kodeSebelumnya = $query->row()->kodeAsuransi;
		$noreg = $query->row()->NomorRegistrasi;

		$query = $this->db->query("update t_pinjaman set kodeAsuransi = '$kodeAsuransiBaru' where NomorPinjaman = '$nopin'");

		if($query){
			$insert = $this->db->query("insert into t_pinjaman_pindahan_history values ('$noreg','$nopin','$tahunsebelumnya','$kodeSebelumnya','$tahun','$kodeAsuransiBaru','$user','$hariini')");
			$log = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Perpindahan Asuransi Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function list_debitur_kode(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen, kodePekerjaan from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updateKodePekerjaan(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$KodePekerjaan = $this->input->post("KodePekerjaan");
		$query = $this->db->query("update t_pinjaman set kodePekerjaan = '$KodePekerjaan' where NomorPinjaman = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Kode Pekerjaan Debitur Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function list_debitur_premibj(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updatePremiBj(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$premi = str_replace(',','',$this->input->post("JumlahPremiTenor"));
		$fee = $premi*0.1;
		$query = $this->db->query("update t_pinjaman_bj set ospremi = '$premi' where NomorPinjaman = '$nopin' and TahunKe = '2'");
		if($query){
			$query2 = $this->db->query("update t_pinjaman set JumlahPremiTenor = '$premi', FeePremiBank = '$fee', FeePremiBroker = '$fee',TotalOsPremi = (select sum(ospremi) from t_pinjaman_bj where NomorPinjaman = '$nopin')-$premi, TotalSeharusnya = (select sum(ospremi) from t_pinjaman_bj where NomorPinjaman = '$nopin') where NomorPinjaman = '$nopin'");
			if($query2){
				$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Premi BJ Nomor Pinjaman $nopin')");
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	
}