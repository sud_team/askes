<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_master extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function KeteranganTc(){
		$query = $this->db->query("select * from t_keterangan_tc");
		return $query->result();
	}

	public function tc(){
		$query = $this->db->query("select * from t_tc");
		return $query->result();	
	}

	public function asuransi(){
		$query = $this->db->query("select * from t_asuransi");
		return $query->result();	
	}

	public function saveAsuransi(){
		$angka = str_replace(".","",$this->input->post("angka"));
		$kode = $this->input->post("kode");
		$asuransi = $this->input->post("asuransi");
		$feebroker = str_replace(",",".",$this->input->post("feebroker"));
		$feebank = str_replace(",",".",$this->input->post("feebank"));
		$id = $this->db->query("select max(idAsuransi) id from t_asuransi")->row()->id+1;
		$query = $this->db->query("insert into t_asuransi values ('$id','$kode','$asuransi','$angka','$feebroker','$feebank')");
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function produk(){
		$query = $this->db->query("select * from t_produk");
		return $query->result();	
	}

	public function saveProduk(){
		$produk = $this->input->post("produk");
		$kode = $this->input->post("kode");
		
		$id = $this->db->query("select max(idProduk) id from t_produk")->row()->id+1;
		$query = $this->db->query("insert into t_produk (namaProduk,kodeProduk) values ('$produk','$kode')");
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function rate(){
		$query = $this->db->query("select * from t_tc_premi");
		return $query->result();	
	}

	public function saveRatePremi(){
		$kode = $this->input->post("kode");
		$namaratepremi = $this->input->post("namaratepremi");
		$rate = $this->input->post("rate")/1000;
		$id = $this->db->query("select max(idTcPremi) id from t_tc_premi")->row()->id+1;
		$query = $this->db->query("insert into t_tc_premi values ('$id','$namaratepremi','$rate','','$kode')");
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function saveTc(){
		$usia_minimum = $this->input->post("usia_minimum");
		$usia_maksimal = $this->input->post("usia_maksimal");
		$uang_minimum = str_replace(".", "", $this->input->post("uang_minimum"));
		$uang_maksimal = str_replace(".", "", $this->input->post("uang_maksimal"));
		$batas_usia_maksimal = $this->input->post("batas_usia_maksimal");
		$user = $this->session->userdata("idUser");
		$hariini = date("Y-m-d H:i:s");
		$keterangan = "";
		$no = 1;
		$jml = count($this->input->post("keterangan"));
		foreach ($this->input->post("keterangan") as $key) {
			$keterangan .= $key;
			if($no>=1 && $no<$jml){
				$keterangan .= "+";
			}
			$no++;
		}

		$max = $this->db->query("select max(idTc) idTc from t_tc");
		if($max->row()->idTc==NULL){
			$idTc = 1;
		}else{
			$idTc = $max->row()->idTc+1;
		}

		$ac = 0;
		if($keterangan=="AC"){
			$ac = 1;
		}

		$query = $this->db->query("insert into t_tc values ('$idTc','$usia_minimum','$usia_maksimal','$batas_usia_maksimal','$uang_minimum','$uang_maksimal','$keterangan','$ac','000','1','$user','$hariini','','')");

		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function getBank($cabang){
		// $cabang = $this->input->post("cabang");
		if($cabang==0){
			$query = $this->db->query("select cabang, capem from t_bank_capem a join t_bank_cabang b on(a.idCabang=b.kodeBank)");
		}else{
			$query = $this->db->query("select cabang, capem from t_bank_capem a join t_bank_cabang b on(a.idCabang=b.kodeBank) 	where a.idCabang = '$cabang'");
		}
		return $query;
	}

	public function getUser(){
		$query = $this->db->query("select username, d.roles from t_user c join t_roles d on(c.roles=d.idRoles) where c.roles != 1 ");
		return $query;
	}

	public function saveUser(){
		$username = $this->input->post("username");
		$pass = pass($this->input->post("password"));
		$role = $this->input->post("role");
		
		$user = create_user();
		$hariini = hariini();

		$query = $this->db->query("insert into t_user (username,pass,hak_akses,roles,create_user,create_date,status) values ('$username','$pass','$role','$role','$user','$hariini','active')");

		return $query;
	}
	
	public function deleteUser(){
		$idUser = $this->input->post("idUser");
	
		$query = $this->db->query("DELETE FROM t_user where idUser = '$idUser' ");

		return $query;
	}

	public function bank(){
		$kodeBank = $this->session->userdata("kodeBank");
		$idUser = $this->session->userdata("idUser");
		if($idUser==1){
			$query = $this->db->query("select idUser, username, kodeBank, is_online from t_user where idUser != '$idUser'");
		}else{
			$query = $this->db->query("select idUser, username, kodeBank, is_online from t_user where kodeBank ='$kodeBank' and idUser != '$idUser'");
		}
		return $query->result();
	}
	
}