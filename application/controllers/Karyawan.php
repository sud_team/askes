<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper'); 
		$this->load->model('Model_klaim','klaim');
	} 

	
	public function karyawan_form(){
		if($this->input->post('submit')){			
			$this->session->set_flashdata('success', '<span id="simpan-berhasil"></span>');
			$data_peserta = array(
				'namaPeserta' => $this->input->post("namaPeserta"),
				'tglLahir' => dateFormatDatabase($this->input->post("tglLahir")),
				'namaBank' => $this->input->post("namaBank"),
				'alamat' => $this->input->post("alamat"),
				'tempatLahir' => $this->input->post("tempatLahir"),
				'namaPemilikBank' => $this->input->post("namaPemilikBank"),
				'noRekeningBank' => $this->input->post("noRekeningBank"),
				'jenisKelamin' => $this->input->post("jenisKelamin"),
				'statusPernikahan' => $this->input->post("statusPernikahan"),
				'noHp' => $this->input->post("noHp"),
			);
			$this->db->where('idUser',$this->session->userdata("idUser"));
			$this->db->update('t_karyawan',$data_peserta);
			redirect('karyawan/karyawan_view');
		}
		
		$member = $this->db->get_where('t_karyawan',array('idUser' => $this->session->userdata("idUser")))->row();
		
		
		$data['member'] = $member;
		$this->app->render('Order','karyawan/karyawan_form',$data);
		
	}
	
	public function karyawan_view(){
		$member = $this->db->get_where('t_karyawan',array('idUser' => $this->session->userdata("idUser")))->row();
		
		$peserta = $this->db
			->select("t_peserta_order.*,t_invoice.tglAkhirAsuransi,t_invoice.status_bayar,t_invoice.tglMulaiAsuransi,t_karyawan.tempatLahir,t_karyawan.namaPeserta AS namaKaryawan,t_karyawan.namaPeserta AS namaKaryawan")
			->join("t_invoice", "t_invoice.idInvoice = t_peserta_order.idInvoice", "inner")
			->join("t_karyawan", "t_invoice.idUser = t_karyawan.idUser", "inner")
			->where(array("t_invoice.idUser" => $this->session->userdata("idUser"), 'status_bayar' => 'sudah_lunas'))
			->group_by('noRegistrasi')
			->get('t_peserta_order')
			->result();
		
		
		$data['member'] = $member;
		$data['peserta'] = $peserta;
		$this->app->render('Order','karyawan/karyawan_view',$data);
		
	}
	
}
