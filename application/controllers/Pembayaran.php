<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{

  public function __Construct()
  {
    parent::__Construct();
    $this->app->auth();
    $this->load->model("Model_helper", "helper");
    $this->load->model("Model_pembayaran", "bayar");
  }

  public function datapembayaran()
  { 
	$data['invoices'] = $this->bayar->filter_internalmemo_klaim()->result();
    $this->app->render('Pembayaran', 'pembayaran/data_pembayaran', $data);
  }

  public function filter_internalmemo()
  {
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $jenis_im = $this->input->post("jenis_im");

    $peserta = $this->bayar->filter_internalmemo_klaim()->result();

    $data = array();
    $i = 1;

    foreach ($peserta as $r) {

      $data[] = array(
        '<a href="' . base_url('pembayaran/konfirmasi_pembayaran/' . $r->idInvoice) . '" class="text-primary"> ' . $r->noInvoice . '</a>',
        $r->jmlPeserta . ' Orang',
        date_format(date_create($r->tglMulaiAsuransi), 'd-M-Y'),
        price($r->jmlPremi),
        '<span class="badge badge-danger animated fadeIn infinite f-s-12">' . $r->status_bayar . '</span>',
        // $r->tglBayar,
        '<a href="' . base_url('pembayaran/konfirmasi_pembayaran/' . $r->idInvoice) . '" class="f-s-18"> <i class="fa fa-folder-open" aria-hidden="true">',
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $recordsTotal,
      "recordsFiltered" => $recordsTotal,
      "data" => $data
    );

    echo json_encode($output);
    exit();
  }

  public function konfirmasi_pembayaran($idInvoice = null)
  {
    $data['member']  = $this->db->get_where("t_karyawan", array("idUser" => $this->session->userdata("idUser")))->row();
    $data['order']  = $this->db->select("levelPlan,COUNT(levelPlan) AS jml_peserta")->get_where("t_peserta_order", array("idInvoice" => $idInvoice))->row();
    $data['invoice']  = $this->db->get_where("t_invoice", array("idInvoice" => $idInvoice))->row();

    if ($this->input->post('submit')) {
      $noInvoice = $this->input->post("noInvoice");
      $tglBayar = $this->input->post("tglBayar");

      $data_peserta = array(
        'tglBayar' => dateFormatDatabase($tglBayar),
        'status_bayar' => 'validasi',
        'bankTujuan' => $this->input->post("bankTujuan"),
        'jmlBayar' => $this->input->post("jmlBayar"),
		'filenameBuktiTransfer' => $this->_upload_process()
      );
      $this->db->where('noInvoice', $noInvoice);
      $this->db->update('t_invoice', $data_peserta);

      $this->session->set_flashdata('success', '<span id="konfirmasi-berhasil"></span>');
      redirect('pembayaran/datapembayaran');
    }
    $this->app->render('Pembayaran', 'pembayaran/form_konfirmasi_pembayaran', $data);
  }

	
	public function detail_pembayaran($idInvoice = null)
  {
    $data['member']  = $this->db->get_where("t_karyawan", array("idUser" => $this->session->userdata("idUser")))->row();
    $data['order']  = $this->db->select("levelPlan,COUNT(levelPlan) AS jml_peserta")->get_where("t_peserta_order", array("idInvoice" => $idInvoice))->row();
    $data['invoice']  = $this->db->get_where("t_invoice", array("idInvoice" => $idInvoice))->row();

    $this->app->render('Pembayaran', 'pembayaran/detail_pembayaran_lunas', $data);
  }
  
	private function _upload_process(){
		$config['upload_path'] = './upload/pembayaran/';
		$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|jpg|jpeg|txt';
	    $config['max_size'] = '30000';
	    $config['max_width']  = '8000'; 
	    $config['max_height']  = '7768';
		$config['file_name'] = 'BuktiTransfer'.date("YmdHis");

	    $this->load->library('upload', $config);
		$this->upload->initialize($config);
	   
		if ($this->upload->do_upload('filenameBuktiTransfer')) {
			return $this->upload->data("file_name");
		}else{
			$this->upload->display_errors();
		}
    
		return NULL;
	}

  public function invoice($filename_invoice)
  {
    $this->load->helper('download');
    force_download('assets/pdfs/' . $filename_invoice, NULL);
  }

}
