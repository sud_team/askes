<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper'); 
		$this->load->model('Model_klaim','klaim');
	} 

	public function pengajuan($idPeserta = null){ 
		
		$data['noKlaim'] =  'KLM'.mt_rand(100000, 999999);
		$data['idPeserta'] =  $idPeserta;
		$data['member']  =
			$this->db->select("t_peserta_order.*,t_karyawan.alamat AS alamat,t_karyawan.namaPeserta AS namaKaryawan,t_karyawan.tempatLahir AS tempatLahir,t_invoice.tglMulaiAsuransi AS tglMulaiAsuransi,t_invoice.tglMulaiAsuransi AS tglAkhirAsuransi")
			->join("t_invoice","t_invoice.idInvoice = t_peserta_order.idInvoice","inner")
			->join("t_karyawan","t_invoice.idUser = t_karyawan.idUser","inner")
			->where(array("t_peserta_order.idPeserta" => $idPeserta))
			->limit(1)
			->get('t_peserta_order')		
			->row();
			
		$this->app->render('Pengajuan Klaim','klaim/formKlaim',$data);
	}

	public function proses($idKlaim = null){ 
		
		$data['member']  =
			$this->db->select("t_klaim.*,t_peserta_order.noRegistrasi,t_peserta_order.namaPeserta,t_peserta_order.jenisKelamin,t_karyawan.alamat AS alamat,t_karyawan.namaPeserta AS namaKaryawan,t_karyawan.tempatLahir AS tempatLahir,t_invoice.tglMulaiAsuransi AS tglMulaiAsuransi,t_invoice.tglAkhirAsuransi AS tglAkhirAsuransi")
			->join("t_peserta_order","t_peserta_order.idPeserta = t_klaim.idPeserta","inner")			
			->join("t_invoice","t_invoice.idInvoice = t_peserta_order.idInvoice","inner")
			->join("t_karyawan","t_invoice.idUser = t_karyawan.idUser","inner")
			->where(array("t_klaim.idKlaim" => $idKlaim))
			->get('t_klaim')		
			->row();
			
		if($this->input->post("valid")){
			$idKlaim = $this->input->post("idKlaim");
			$tglTerimaDokumenAsli = $this->input->post("tglTerimaDokumenAsli");
			
			$data_verifikasi = array(
				'tglKirimKeAsuransi' => dateFormatDatabase($this->input->post("tglKirimKeAsuransi")),
				'status' => 'verified',
				'tglTerimaDokumenAsli' => dateFormatDatabase($this->input->post("tglTerimaDokumenAsli")),
				'catatanVerifikasi' => $this->input->post("catatanVerifikasi"),
			);
			$this->db->where('idKlaim',$idKlaim);
			$this->db->update('t_klaim',$data_verifikasi);
			
			$this->session->set_flashdata('success', '<span id="simpan-berhasil"></span>');
			redirect('laporan/klaim');
		}elseif($this->input->post("penilaian")){
			$idKlaim = $this->input->post("idKlaim");
			
			$data_verifikasi = array(
				'tglKeputusanKlaim' => dateFormatDatabase($this->input->post("tglKeputusanKlaim")),
				'keputusanKlaim' => $this->input->post("keputusanKlaim"),
				'status' => 'approved',
				'alasanKlaim' => $this->input->post("alasanKlaim"),
				'nominalKlaimDisetujui' => $this->input->post("nominalKlaimDisetujui"),
			);
			$this->db->where('idKlaim',$idKlaim);
			$this->db->update('t_klaim',$data_verifikasi);
			
			$this->session->set_flashdata('success', '<span id="simpan-berhasil"></span>');
			redirect('laporan/klaim');
		}
					
		$this->app->render('Verifikasi Klaim','klaim/formVerifikasi',$data);
	}

	public function saveKlaim(){
		$noKlaim = $this->input->post("noKlaim");
		$idPeserta = $this->input->post("idPeserta");
		$namaRumahSakit = $this->input->post("namaRumahSakit");
		$alamatRumahSakit = $this->input->post("alamatRumahSakit");
		$telpRumahSakit = $this->input->post("telpRumahSakit");
		$kondisiPasien = $this->input->post("kondisiPasien");
		$tglBerobat = $this->input->post("tglBerobat");
		$biayaPengobatan = $this->input->post("biayaPengobatan");
		$diagnosaSakit = $this->input->post("diagnosaSakit");
		
		
		$data_save = array(
			'noKlaim' => $noKlaim,
			'idPeserta' => $idPeserta,
			'namaRumahSakit' => $namaRumahSakit,
			'alamatRumahSakit' => $alamatRumahSakit,
			'telpRumahSakit' => $telpRumahSakit,
			'kondisiPasien' => $kondisiPasien,
			'biayaPengobatan' => $biayaPengobatan,
			'tglBerobat' => dateFormatDatabase($tglBerobat),
			'diagnosaSakit' => $diagnosaSakit,		
			'filename_resumeMedis' => $this->_upload_process('resumeMedis'),		
			'filename_kwitansiPembayaran' => $this->_upload_process('kwitansiPembayaran'),		
			'filename_rincianPengobatan' => $this->_upload_process('rincianPengobatan'),		
			'filename_dokumenTambahan' => $this->_upload_process('dokumenTambahan'),	
			'tglDibuat' => date("Y-m-d H:i:s"),
			'status' => 'pengajuan',
		);
		
		$this->db->insert('t_klaim',$data_save);
	
		$this->session->set_flashdata('success', '<span class="order-berhasil"></span>');
	
		redirect("klaim/pengajuan/".$idPeserta);
	}
	
	public function download_dokumen($folder,$filename_dokumen){
	  $this->load->helper('download');
	  
	  force_download('upload/klaim/'.$folder.'/'.$filename_dokumen, NULL);
	  
  }
	
	private function _upload_process($name){
		$config['upload_path'] = './upload/klaim/'.$name;
		$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|jpg|jpeg|txt';
	    $config['max_size'] = '30000';
	    $config['max_width']  = '8000'; 
	    $config['max_height']  = '7768';
		$config['file_name'] = $name.date("YmdHis");

	    $this->load->library('upload', $config,$name);
		 $this->$name->initialize($config);
	   
		if ($this->$name->do_upload($name)) {
			return $this->$name->data("file_name");
		}else{
			$this->$name->display_error();
		}
    
		return NULL;
	}
}
