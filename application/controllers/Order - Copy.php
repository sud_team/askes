<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->load->library('session');
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_produksi','produksi');
	}
	
	public function premi(){
		if($this->input->post('submit')){			
			$this->session->set_flashdata('success', '<span id="simpan-berhasil"></span>');
			$data_peserta = array(
				'namaPeserta' => $this->input->post("namaPeserta"),
				'tglLahir' => dateFormatDatabase($this->input->post("tglLahir")),
				'namaBank' => $this->input->post("namaBank"),
				'levelPlan' => $this->input->post("levelPlan"),
				'alamat' => $this->input->post("alamat"),
				'tempatLahir' => $this->input->post("tempatLahir"),
				'namaPemilikBank' => $this->input->post("namaPemilikBank"),
				'noRekeningBank' => $this->input->post("noRekeningBank"),
				'jenisKelamin' => $this->input->post("jenisKelamin"),
				'statusPernikahan' => $this->input->post("statusPernikahan"),
				'noHp' => $this->input->post("noHp"),
			);
			$this->db->where('idUser',$this->session->userdata("idUser"));
			$this->db->update('t_peserta',$data_peserta);
			redirect('order/akun_view');
		}
		
		$member = $this->db->get_where('t_peserta',array('idUser' => $this->session->userdata("idUser")))->row();
		
		$data['member'] = $member;
		$this->app->render('Order','produksi/order',$data);
		
	}
	
	public function akun_view(){
		if($this->input->post('submit')){			
			$this->session->set_flashdata('success', '<span id="simpan-berhasil"></span>');
			$data_peserta = array(
				'namaPeserta' => $this->input->post("namaPeserta"),
				'tglLahir' => dateFormatDatabase($this->input->post("tglLahir")),
				'namaBank' => $this->input->post("namaBank"),
				'levelPlan' => $this->input->post("levelPlan"),
				'namaPemilikBank' => $this->input->post("namaPemilikBank"),
				'noRekeningBank' => $this->input->post("noRekeningBank"),
				'jenisKelamin' => $this->input->post("jenisKelamin"),
				'statusPernikahan' => $this->input->post("statusPernikahan"),
				'noHp' => $this->input->post("noHp"),
			);
			$this->db->where('idUser',$this->session->userdata("idUser"));
			$this->db->update('t_peserta',$data_peserta);
			//redirect('login/auth');
		}
		
		$member = $this->db->get_where('t_peserta',array('idUser' => $this->session->userdata("idUser")))->row();
		
		$data['member'] = $member;
		$this->app->render('Order','produksi/akun_view',$data);
		
	}


	public function tambah_peserta(){
		
		$config = array(
			"protocol" => "smtp",
			"smtp_host" => 'smtp.gmail.com',
			"smtp_port" => 587,
			"smtp_user" => "bhayunugroho0809@gmail.com",
			"smtp_pass" => "marethatiara123",
			'crlf'      => "\r\n",
            'newline'   => "\r\n"
		);
		$member = $this->db->get_where('t_peserta',array('idUser' => $this->session->userdata("idUser")))->row();
		$this->load->library("phpmailer_lib");
		
		$mail = $this->phpmailer_lib->load();
		
		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = "bhayunugroho0809@gmail.com";
		$mail->Password = "maretha1234";
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;
		//$mail->SMTPDebug = 2;
		$mail->SetFrom('bhayunugroho0809@gmail.com', 'noreply');  //Siapa yg mengirim email
        $mail->Subject    = "Invoice tagihan";
        $mail->MsgHTML($this->load->view("public/produksi/invoice_tagihan","",TRUE));
        	
		if($this->input->post('order')){			
			$this->session->set_flashdata('success', '<span class="order-berhasil"></span>');
					
			$tglMulaiAsuransi = dateFormatDatabase($this->input->post("tglMulaiAsuransi"));
			$futureDate = strtotime('+1 Year',strtotime($tglMulaiAsuransi));
			$futureDate = date("Y-m-d",$futureDate);
			
			$data_peserta = array();
			$no_invoice =  'INV'.mt_rand(100000, 999999);
			$filename="invoice_tagihan-".date("Y-m-d").".pdf";	
			$data_invoice = array(
				'noInvoice' => $no_invoice,
				'idUser' => $this->session->userdata("idUser"),
				'tglMulaiAsuransi' => $tglMulaiAsuransi,
				'filename_invoice' => $filename
			);
		
			$this->db->insert('t_invoice',$data_invoice);
			
			$idInvoice = $this->db->insert_id();
			$jml_premi = 0;
			
			foreach($this->input->post("namaLengkapHidden") AS $k=>$v){
				$noRegister =  'REG'.mt_rand(100000, 999999).$k;		
				$age = new Datetime(date("Y-m-d",strtotime($this->input->post("tglLahirHidden")[$k])));
				$today = new DateTime();
				$diff = $today->diff($age);
				$yearage = $diff->y;
				if($this->input->post("levelPlanHidden")[$k] == '800'){
					if($yearage <= 23 ){
						$premi = 1775600;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "M" && $yearage > 23 ){
						$premi = 2509400;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "F" && $yearage > 23 ){
						$premi = 2908800;
					}
				}elseif($this->input->post("levelPlanHidden")[$k] == '1000'){
					if($yearage <= 23 ){
						$premi = 2475000;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "M" && $yearage > 23 ){
						$premi = 3501800;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "F" && $yearage > 23 ){
						$premi = 4060100;
					}
				}elseif($this->input->post("levelPlanHidden")[$k] == '1500'){
					if($yearage <= 23 ){
						$premi = 4824400;
					}elseif($row['I'] == "M" && $yearage > 23 ){
						$premi = 4824400;
					}elseif($row['I'] == "F" && $yearage > 23 ){
						$premi = 5541900;
					}
				}
				$jml_premi += $premi;
				$data_peserta[] = array(
					'idInvoice' => $idInvoice,
					'noRegistrasi' => $noRegister,
					'idUser' => $this->session->userdata("idUser"),
					'namaPeserta' => $this->input->post("namaLengkapHidden")[$k],
					'tglLahir' => dateFormatDatabase($this->input->post("tglLahirHidden")[$k]),
					'tglMulaiAsuransi' => $tglMulaiAsuransi,
					'tglAkhirAsuransi' => $futureDate,
					'premi' => $premi,
					'levelPlan' => $this->input->post("levelPlanHidden")[$k],
					'jenisKelamin' => $this->input->post("jenisKelaminHidden")[$k],
					'statusPernikahan' => $this->input->post("statusPernikahanHidden")[$k],
					'noHp' => $this->input->post("noHpHidden")[$k],
				);				
			}			
			$data_premi = array(
				'jmlPremi' => $jml_premi,				
			);
			$this->db->where('idInvoice',$idInvoice);
			$this->db->update('t_invoice',$data_premi);
						
			$toEmail = $this->session->userdata("username"); // siapa yg menerima email ini
			$mail->AddAddress($toEmail);
					
			$this->db->insert_batch('t_peserta_order',$data_peserta);
			$mail->AddAttachment($this->generate_pdf($no_invoice,$idInvoice,$filename));
			$mail->Send();
			redirect('order/tambah_peserta');
		}
		
		$member = $this->db->get_where('t_peserta',array('idUser' => $this->session->userdata("idUser")))->row();
		
		$data['member'] = $member;
		$this->app->render('Order','produksi/tambah_peserta',$data);
		$this->load->view('public/produksi/_jsproduksi',$data);
		
	}	
	
	public function generate_pdf($no_invoice = null,$idInvoice,$filename){
		$this->load->library('pdf');
		
		$html_content = '<html>';
		
		$html_content .= '<head>';
		$html_content .= '<style>
			@page { margin: 100px 25px; }
			header { position: fixed; top: -60px; left: 0px; right: 0px;padding-bottom:30px !important; }
			footer { position: fixed; bottom: -60px; left: 0px; right: 0px;  }
			
		  </style>';
		$html_content .= '</head>';
		$html_content .= '<body>';
		
		$html_content .= '<header>';
		$html_content .= '<div ><img style="float:left" src="assets/ela/images/tugulogo.png"/><br/>';
		$html_content .= '</div><div style="clear:both"></div>';
		$html_content .= '<hr/></header><br/><br/>';
		$html_content .= '<div style="background:#5D6975;margin-bottom:20px;color:#fff;padding:9px;text-align:center">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</div>';
		$html_content .= '<table width="100%"><tr><td>NO INVOICE : '.$no_invoice.'</td></tr><tr><td>Tanggal : '.date("Y F d").'</td></tr></table></div><br/>';
		$html_content .= '<table border="1" style="border-collapse:collapse"width="100%" cellspacing="5" cellpadding="5">';
		$html_content .= '<tr style="background:#eeeeee;font-weight:bold">
				<td>Nama Peserta</td>
				<td>Jenis Kelamin</td>
				<td>Level Plan</td>
				<td>Premi</td>
			</tr>';
			
		$data = $this->db->get_where("t_peserta_order",array("idInvoice" => $idInvoice))->result();
		$total = 0;
		foreach($data AS $v){
			
			$html_content .= '<tr>
				<td>'.$v->namaPeserta.'</td>
				<td>'.$v->jenisKelamin.'</td>
				<td>'.$v->levelPlan.'</td>
				<td>'.number_format((int)$v->premi).'</td>					
			</tr>';
			$total += $v->premi;
		}
		$html_content .= '<tr>
				<td colspan="3" style="text-align:left">Total</td>
				<td>'.number_format($total).'</td>				
			</tr>';
		$html_content .= '</table><br/><br/>';
		// $html_content .= '<div class="page_break" style="page-break-before: always;"></div>';
		// $html_content .= '<table><tr><td>No Rekening Mandiri</td></tr></table>';
		$html_content .= '</body>';
		$html_content .= '</html>';
		$this->pdf->loadHtml($html_content);
		
		$this->pdf->render();
		
		
		//$this->pdf->stream("invoice.pdf", array("Attachment"=>0));
		file_put_contents("assets/pdfs/".$filename, $this->pdf->output());
		
		
		return "assets/pdfs/".$filename;
	}
}
