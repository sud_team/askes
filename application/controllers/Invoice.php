<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->library('session');
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_produksi','produksi');
	}
	
	public function list_invoice(){
		
		$datax['data_invoice'] = $this->db->get('t_invoice')->result();
		$this->app->render('Upload','invoice/list',$datax);
	}	
	
	public function generate_pdf($tgl_upload = null){
		$this->load->library('pdf');
		
		$data = $this->db->get_where('t_peserta',array('tgl_upload' => $tgl_upload))->result();
		$html_content = '<html>';
		$html_content .= '<body>';
		$html_content .= '<header>';
		$html_content .= '<div><img style="float:left" src="'.base_url("assets/ela/images/logo.png").'"/>';
		$html_content .= '<h1 class"text-danger">INVOICE ASKES</h1>';
		$html_content .= '</div><div style="clear:both"></div></header>';
		$html_content .= '<hr/>';
		$html_content .= '<div style="background:#5D6975;margin-bottom:20px;color:#fff;padding:9px;text-align:center">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</div>';
		$html_content .= '<table border="1" style="border-collapse:collapse"width="100%" cellspacing="5" cellpadding="5">';
		$html_content .= '<tr style="background:#eeeeee;font-weight:bold">
				<td>Level Plan</td>
				<td>Peserta</td>
				<td>Total Premi</td>
			</tr>';
		$dataPlan = array('700','900','1400');
		
		
		foreach($dataPlan AS $v){
			 $dataPlan = $this->db->select(
				'COUNT(*) AS jml_peserta, SUM(premi) AS premi'
			)->get_where('t_peserta',array('levelPlan' => $v))->row();
			$html_content .= '<tr>
				<td>'.$v.'</td>
				<td>'.$dataPlan->jml_peserta.'</td>
				<td>'.number_format((int)$dataPlan->premi).'</td>
				
			</tr>';
		}
		$html_content .= '</table>';
		$html_content .= '</body>';
		$html_content .= '</html>';
		$this->pdf->loadHtml($html_content);
		$this->pdf->render();
		$this->pdf->stream("invoice.pdf", array("Attachment"=>0));
	}
}
