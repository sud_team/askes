<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->library('session');
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_produksi','produksi');
	}
	
	public function submitForm(){
		$data = $this->session->userdata('preview_data');
		if($this->db->insert_batch('t_peserta',$data)){
			$data_upload = array(
				'tgl_upload' => date('Y-m-d'),
				'noInvoice' => 'INV'.substr(strtotime(date('Y-m-d')),0,6)
			);
			
			$this->db->insert('t_invoice',$data_upload);
			$this->session->set_userdata('preview_data',NULL);
			
			return 'true';
		}
	}
	public function upload_process(){
			$datax["array"] = "";
			$berhasil = 0;
			$config['upload_path'] = './upload/produksi/';
			$config['allowed_types'] = 'xlsx';    
			$config['max_size']  = '2048';    
			$config['overwrite'] = true;    
			$config['file_name'] = $_FILES['file']['name'];

		    $this->load->library('upload', $config);
		    $this->upload->initialize($config);

		    $this->upload->do_upload("file");
	        $upload_data = $this->upload->data(); 
			if($upload_data["file_ext"]!=".xlsx"){
	        	echo "asdasd ";
	        }else{
	        	$data = array(); 		      
		        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		         
				$inputFileName = 'upload/produksi/'.$upload_data["file_name"];
				
				try {						
					$csvreader = new PHPExcel_Reader_Excel2007();
					$loadcsv = $csvreader->load($inputFileName);

				} catch (Exception $e) {
					die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
							. '": ' . $e->getMessage());

				}
				$this->session->set_flashdata('success', '<span id="konfirmasi-berhasil"></span>');
		  		$sheet = $loadcsv->getActiveSheet()->toArray(null,null,null,null,null,null,null,null,null,null,null,null,null,null);

			    $namafile = $upload_data["raw_name"];
		        $datax["success"] = false;
		        $datax["file03"] = $upload_data["raw_name"]."-03";
			    $numrow = 1;
			
			
			    foreach($sheet as $row){
				  	if($numrow > 1){
						if($row['1'] != ""){
							$dataP  = array(
								"noPeserta" => $row['11']
							);
							$this->db->where('noRegistrasi',$row['1']);
							$this->db->update('t_peserta_order',$dataP);
						}
			      	}
					$numrow++;
			    }
				
				$this->session->set_userdata('preview_data', $data);
			}
		
	}
	public function uploadnopeserta(){
		error_reporting(0);
		$datax["debitur"] = "";
		
		$datax['data_peserta'] = $this->session->userdata('preview_data');
		$this->app->render('Upload','produksi/uploadnopeserta',$datax);
	}

	 public function bukti_transfer($filename_buktitransfer)
	  {
		$this->load->helper('download');
		force_download('upload/pembayaran/' . $filename_buktitransfer, NULL);
	  }
	


	public function rekonsel(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["rekonsel"] = $this->produksi->rekonsel();
		$data["asuransi"] = $this->helper->asuradur();
		$this->app->render('Rekonsel','produksi/rekonsel',$data);
		$this->load->view("public/produksi/_jsproduksi",$data);
	}

	public function validasiRekonsel(){
		$validasi = $this->produksi->validasiRekonsel();
		if($validasi){
			
			$this->load->library("phpmailer_lib");
		
			$mail = $this->phpmailer_lib->load();
			
			$mail->isSMTP();
			$mail->Host = "smtp.gmail.com";
			$mail->SMTPAuth = true;
			$mail->Username = "bhayunugroho0809@gmail.com";
			$mail->Password = "maretha1234";
			$mail->SMTPSecure = "tls";
			$mail->Port = 587;
			//$mail->SMTPDebug = 2;
			$mail->SetFrom('bhayunugroho0809@gmail.com', 'noreply');  //Siapa yg mengirim email
			$mail->Subject    = "Invoice tagihan";
			$mail->Body       = "Invoice Tagihan";
			
			$toEmail = $this->session->userdata("username"); // siapa yg menerima email ini
			$mail->AddAddress($toEmail);
			
			$noInvoice = $this->input->post("noInvoice");
			$mail->AddAttachment($this->generate_pdf($noInvoice,$idInvoice));
			//$mail->Send();
		
			$this->session->set_flashdata("success","<span id='berhasil-rekonsel'></span>");
		}else{
			$this->session->set_flashdata("danger","<span id='gagal-rekonsel'></span>");		}
	}

	public function generate_pdf($noInvoice = null){
		$this->load->library('pdf');
		
		$html_content = '<html>';
		$html_content .= '<body>';
		$html_content .= '<header>';
		$html_content .= '<div ><img style="float:left" src="assets/ela/images/tugulogo.png"/><br/>';
		$html_content .= '</div><div style="clear:both"></div></header>';
		$html_content .= '<hr/>';
		$html_content .= '<div style="background:#5D6975;margin-bottom:20px;color:#fff;padding:9px;text-align:center">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</div>';
		$html_content .= '<table width="100%"><tr><td>NO INVOICE : '.$noInvoice.'</td></tr><tr><td>Tanggal : '.date("Y F d").'</td></tr></table></div><br/>';
		$html_content .= '<table border="1" style="border-collapse:collapse"width="100%" cellspacing="5" cellpadding="5">';
		$html_content .= '<tr style="background:#eeeeee;font-weight:bold">
				<td>Nama Peserta</td>
				<td>Jenis Kelamin</td>
				<td>Level Plan</td>
				<td>Premi</td>
			</tr>';
			
		$data = $this->db->get_where("t_peserta_order",array("idInvoice" => $idInvoice))->result();
		$total = 0;
		foreach($data AS $v){
			
			$html_content .= '<tr>
				<td>'.$v->namaPeserta.'</td>
				<td>'.$v->jenisKelamin.'</td>
				<td>'.$v->levelPlan.'</td>
				<td>'.number_format((int)$v->premi).'</td>					
			</tr>';
			$total += $v->premi;
		}
		$html_content .= '<tr>
				<td colspan="3" style="text-align:left">Total</td>
				<td>'.number_format($total).'</td>				
			</tr>';
		$html_content .= '</table>';
		$html_content .= '</body>';
		$html_content .= '</html>';
		$this->pdf->loadHtml($html_content);
		
		$this->pdf->render();
		
		$filename="invoice_tagihan-".date("Y-m-d").".pdf";	
		//$this->pdf->stream("invoice.pdf", array("Attachment"=>0));
		file_put_contents("assets/pdfs/".$filename, $this->pdf->output());
		
		
		return "assets/pdfs/".$filename;
	}
	
	public function filter_rekonsel(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_rekonsel();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {
          		if ($r->TenorTahun<1) {
          			$bulan = $r->TenorTahun;
          		} else {
          			$bulan = $r->TenorBulan;
          		}
          		$link = "-";
				if(!empty($r->filenameBuktiTransfer)){
					$link =  '<a class="btn btn-sm btn-outline-info" href="'.base_url("produksi/bukti_transfer/".$r->filenameBuktiTransfer).'"> <i class="fa fa-download"></i> Download </a>';
				}
                  $data[] = array(
                  
                      $r->noInvoice,
                      $r->namaPeserta,
                      date_format(date_create($r->tglMulaiAsuransi),'d-M-Y'),
					  $r->jmlPeserta,
					$link,
                      price_desimal($r->jmlPremi),	'<input type="checkbox" onClick="pickDataRekonsel(this,event)" data-premi="'.$r->jmlPremi.'"name="checkbox[]" value="\''.$r->noInvoice.'\'" class="chkDel">',
                 );


               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function prapen(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["rekonsel"] = $this->produksi->rekonsel();
		$data["asuransi"] = $this->helper->asuradur();
		$data["file02"] = "";
        $data["file03"] = "";
        $data["data_peserta"] = $this->db->get('t_peserta')->result_array();
		
		$this->app->render('Pra Pensiun','produksi/prapen',$data);
	}

	public function filter_prapen(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_prapen();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {
          			if ($r->umur2==0) {
          				$value = '<input type="text" class="col-xs-2 custom-select f-s-12" id="batas'.$i.'" name="batas"><input type="hidden" class="col-xs-2 custom-select f-s-12" id="submit'.$i.'" name="submit" value="Input">';
          				$btn = '<a class="btn btn-xs btn-warning" onclick="Input(\''.$i.'-'.$r->NomorRegistrasi.'\')" style="color: #ffffff;">Input</a>';
          			} else {
          				$value = '<input type="text" class="form-control" id="batas'.$i.'"  name="batas" value="'.$r->umur2.'"><input type="hidden" class="col-xs-2 custom-select f-s-12" id="submit'.$i.'" name="submit" value="Submit">';
          				$btn = '<a class="btn btn-xs btn-success" onclick="Input(\''.$i.'-'.$r->NomorRegistrasi.'\')" style="color: #ffffff;">Submit</a>';
          			}
          			

                  $data[] = array(
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      date_format(date_create($r->TglAkhirKredit),'d-M-Y'),
                      cekPekerjaan($r->KodePekerjaan),
                      str_replace(".", " Tahun ", $r->UsiaDebitur)." Bulan",
                      ($r->TenorBulan)." Bulan",
                      price_desimal($r->plafon),
                      price_desimal($r->JumlahPremiTenor),
                      cekNamaAsdur($r->kodeAsuransi),
                  	  $value,
                  	  $btn
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}


    public function saveBatasPensiun(){
    	if ($this->input->post("submit")=="Input") {
    		$noreg = $this->input->post("noreg");
	    	$batas = $this->input->post("batas");
	    	$query = $this->db->query("select plafon, UsiaDebitur, TenorTahun,FeePresenBroker, FeePersenBank, kodeAsuransi from t_pinjaman where NomorRegistrasi = '$noreg'");
	    	$plafon=$query->row()->plafon;
	    	$usia=ceil($query->row()->UsiaDebitur);
	    	if($usia>$batas){
	    		$usia=$batas;
	    	}

	    	if($noreg=="180809074849"){
	    		$usia = 54.5;
	    	}
	    	$tenor=$query->row()->TenorTahun;
	    	$broker=$query->row()->FeePresenBroker;
	    	$bank=$query->row()->FeePersenBank;
	    	$kodeAsuransi=$query->row()->kodeAsuransi;
	    	// if($kodeAsuransi==8){
	    	// 	$sebelumnya = $tenor*($plafon*0.004);
	    	// 	$setelah = 0;
	    	// }else{
		    	$sebelumnya = ($batas-$usia)*($plafon*0.004);
		    	$setelah = (($usia+$tenor)-$batas)*($plafon*0.005);
	    	// }
	    	
	    	$JumlahPremiTenor = $setelah+$sebelumnya;
	    	$feebroker = $JumlahPremiTenor*$broker;
	    	$feebank = $JumlahPremiTenor*$bank;
	    	$input = $this->db->query("update t_pinjaman set JumlahPremiTenor = '$JumlahPremiTenor', FeePremiBroker = '$feebroker', FeePremiBank = '$feebank', TotalSeharusnya = '$JumlahPremiTenor', umur2 = '$batas', idTc ='3', TcKet='AC', TcAc = '1' where NomorRegistrasi = '$noreg'");
	    	if($input){
	    		echo "Berhasil";
	    	}else{
	    		echo "Gagal";
	    	}
    	} else {
    		$noreg = $this->input->post("noreg");
	    	$batas = $this->input->post("batas");
	    	$query = $this->db->query("select plafon, UsiaDebitur, TenorTahun,FeePresenBroker, FeePersenBank from t_pinjaman where NomorRegistrasi = '$noreg'");
	    	$plafon=$query->row()->plafon;
	    	$usia=ceil($query->row()->UsiaDebitur);
	    	if($usia>$batas){
	    		$usia=$batas;
	    	}
	    	$tenor=$query->row()->TenorTahun;
	    	$broker=$query->row()->FeePresenBroker;
	    	$bank=$query->row()->FeePersenBank;
	    	$sebelumnya = ($batas-$usia)*($plafon*0.004);
	    	$setelah = (($usia+$tenor)-$batas)*($plafon*0.005);
	    	
	    	$JumlahPremiTenor = $setelah+$sebelumnya;
	    	$feebroker = $JumlahPremiTenor*$broker;
	    	$feebank = $JumlahPremiTenor*$bank;
	    	$input = $this->db->query("update t_pinjaman set JumlahPremiTenor = '$JumlahPremiTenor', FeePremiBroker = '$feebroker', FeePremiBank = '$feebank', TotalSeharusnya = '$JumlahPremiTenor', umur2 = '$batas', StatusPrapen = '0' where NomorRegistrasi = '$noreg'");
	    	if($input){
	    		echo "Berhasil";
	    	}else{
	    		echo "Gagal";
	    	}
    	}

    	// redirect('produksi/prapen');
    }

    public function hasil_ftp(){
		$data["file02"] = "";
        $data["file03"] = "";
		$this->app->render('Hasil FTP','produksi/hasil_ftp',$data);
	}

	public function filter_02(){
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

        $peserta = $this->produksi->filter_02();
        $data = array();
        $i = 1;
        
        foreach($peserta->result() as $r) {

            $data[] = array(
                $r->Keterangan.' '.$r->nopin,
                $r->NomorRegistrasi
            );

            $i++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $peserta->num_rows(),
            "recordsFiltered" => $peserta->num_rows(),
            "data" => $data
        );

        echo json_encode($output);
        exit();
	}

	public function filter_03(){

		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

        $peserta = $this->produksi->filter_03();
        $data = array();
        $i = 1;
        
        foreach($peserta->result() as $r) {

            $data[] = array(
                $r->Keterangan.' '.$r->nopin,
                $r->NomorRegistrasi
            );

            $i++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $peserta->num_rows(),
            "recordsFiltered" => $peserta->num_rows(),
            "data" => $data
        );

        echo json_encode($output);
        exit();
	}

	public function filter_detail(){
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

        $peserta = $this->produksi->filter_detail();
        $data = array();
        $i = 1;
        
        foreach($peserta->result() as $r) {

            $data[] = array(
		    	$i,	
		    	$r->NomorRegistrasi,	
		    	$r->cif,	
		    	$r->NomorPinjaman,	
		    	$r->kodeBank,	
		    	$r->NamaDebitur,	
		    	$r->idProduk,	
		    	$r->jk,	
		    	$r->NomorKtp,	
		    	$r->TempatLahir,	
		    	date_format(date_create($r->TglLahir),'Y-m-d'),	
		    	$r->UsiaDebitur." -> ".ceil($r->UsiaDebitur),	
		    	$r->alamat,
		    	$r->KodePekerjaan,
		    	$r->NomorPK,
		    	date_format(date_create($r->TglAkadKredit),'Y-m-d'),	
		    	date_format(date_create($r->TglAkhirKredit),'Y-m-d'),	
		    	price($r->plafon),	
		    	price($r->JumlahPremiTenor),	
		    	$r->NomorReferensiPremi,	
		    	$r->StatusPinjaman,	
		    	$r->kodeAsuransi,	
		    	'AC'
            );

            $i++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $peserta->num_rows(),
            "recordsFiltered" => $peserta->num_rows(),
            "data" => $data
        );

        echo json_encode($output);
        exit();
	}

    public function bunga(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["rekonsel"] = $this->produksi->rekonsel();
		$data["asuransi"] = $this->helper->asuradur();
		$data["file02"] = "";
        $data["file03"] = "";
		$this->app->render('Penerapan Bunga','produksi/bunga',$data);
	}

	public function form_bunga($noreg){
		$data["debitur"] = $this->produksi->debitur_bunga($noreg);
		$data["file02"] = "";
        $data["file03"] = "";
		$this->app->render('Form Penerapan Bunga','produksi/form_bunga',$data);
	}

	public function hitungBunga(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->hitungBunga();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {

                $data[] = array(
                    $r->BulanKe,
                    number_format($r->SisaPinjaman,2,',','.'),
                      number_format($r->AngsuranBunga,2,',','.'),
                      number_format($r->AngsuranPokok,2,',','.'),
                      number_format($r->TotalAngsuran,2,',','.'),
                      number_format($r->ospremi,2,',','.')
                );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function simpanHitungBunga(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->simpanHitungBunga();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {

                $data[] = array(
                    $r->BulanKe,
                    number_format($r->SisaPinjaman,2,',','.'),
                      number_format($r->AngsuranBunga,2,',','.'),
                      number_format($r->AngsuranPokok,2,',','.'),
                      number_format($r->TotalAngsuran,2,',','.'),
                      number_format($r->ospremi,2,',','.')
                );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function filter_bunga(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_bunga();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {

          		if ($r->StatusPinjaman==1) {
          			$stt = "NEW";
          		} else {
          			$stt = "TOP UP";
          		}
          		

                  $data[] = array(
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      cekNamaBank($r->kodeBank),
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      date_format(date_create($r->TglAkhirKredit),'d-M-Y'),
                      $r->TenorBulan." Bulan",
                      price($r->plafon),
                      $stt,
                      $r->Bunga,
                      "<a href='".base_url()."produksi/form_bunga/".$r->NomorRegistrasi."' class='btn btn-xs btn-primary'><i class='fa fa-folder-open' aria-hidden='true'></i></a>"
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function perhitungan_bunga($noreg){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->perhitungan_bunga($noreg);
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {

                  $data[] = array(
                      $r->BulanKe,
                      number_format($r->SisaPinjaman,2,',','.'),
                      number_format($r->AngsuranBunga,2,',','.'),
                      number_format($r->AngsuranPokok,2,',','.'),
                      number_format($r->TotalAngsuran,2,',','.'),
                      number_format($r->ospremi,2,',','.')
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function rekonsel_bj(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["rekonsel"] = $this->produksi->rekonsel();
		$data["asuransi"] = $this->helper->asuradur();
		$this->app->render('List Rekonsel BJ','produksi/rekonsel_bj',$data);
	}

	public function validasiRekonselBj(){
		$validasi = $this->produksi->validasiRekonselBj();
		if($validasi){
			echo "Data Berhasil Divalidasi";
		}else{
			echo "Data Gagal Divalidasi ";
		}
	}

	public function filter_rekonsel_bj(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_rekonsel_bj();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {
          		$bj = $this->produksi->filter_rekonsel_bj_detail($r->NomorRegistrasi,$r->tahun);
                  $data[] = array(
                  	'<input type="checkbox" name="checkbox[]" value="\''.$bj->idTbj.'\'" class="chkDel">',
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      date_format(date_create($bj->TglJatuhTempo),'d-M-Y'),
                      ($r->TenorBulan)." Bulan",
                      price($bj->plafon),
                      price_desimal($bj->SisaPinjaman),
                      price($r->tahun),
                      price_desimal($bj->ospremi),
                      cekNamaBank($r->kodeBank),
                      cekNamaAsdur($r->kodeAsuransi)
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function polis(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["polis"] = $this->produksi->polis();
		$this->app->render('Input Polis','produksi/polis',$data);
	}

	public function filter_polis(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_polis();
          $data = array();
          $i = 1;

          foreach($peserta->result() as $r) {
        		$text = '<span id="link'.$r->NomorRegistrasi.'"><input type="text" id="nopolis'.$r->NomorRegistrasi.'" name="nopolis'.$r->NomorRegistrasi.'" class="col-xs-3 custom-select f-s-12">
                                <br>-
                                <input type="date" class="col-12 custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tglpolis'.$r->NomorRegistrasi.'" id="tglpolis'.$r->NomorRegistrasi.'"></span>';
                $btn = '<span id="terbitkan'.$r->NomorRegistrasi.'" ><a class="btn btn-xs btn-warning" onclick="terbitkan(\''.$r->NomorRegistrasi.'\')" style="color: #ffffff;">Terbitkan</a></span>';
                  $data[] = array(
                      '',
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      $r->TenorBulan." Bulan",
                      price($r->plafon),
                      cekNamaBank($r->kodeBank),
                      cekNamaAsdur($r->kodeAsuransi),
                      $text,
                      $btn
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function terbitkanPolis(){
		$terbit = $this->produksi->terbitkanPolis();
		if($terbit==0){
			echo "Gagal Proses Polis";
		}else if($terbit==-1){
			echo "Gagal Input Polis";
		}else{
			echo "Polis Berhasil Terbit";
		}
	}

	public function melengkapiDokumen(){
		$noreg = $this->input->post("noreg");
		$debitur = $this->db->query("select NamaDebitur, cif from t_debitur where idDebitur = (select idDebitur from t_pinjaman where NomorRegistrasi = '$noreg') ");
		$i=0; 
		$baris = "";
		$baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-4 control-label">No. CIF</lable>';
        $baris .= '<lable class="col-sm-4 control-label" id="cif">'.$debitur->row()->cif.'</lable>';

        $baris .= '</div>';
        $baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-4 control-label">Nama</lable>';
        $baris .= '<lable class="col-sm-4 control-label" id="nama">'.$debitur->row()->NamaDebitur.'</lable>';

        $baris .= '</div>';
        $baris .= '<div class="col-xs-12">';
        $baris .= '<hr>';
        $baris .= '</div>';
		foreach (cekDokKurang($noreg)->result() as $abc) {
            $baris .= '<div class="row p-10 text-left">';
            $baris .= '<input type="hidden" name="urut[]" value="'.$i.'">';
            $baris .= '<input type="hidden" name="noreg" id="noreg" value="'.$noreg.'">';
            $baris .= '<input type="hidden" name="namaDokumen'.$i.'" value="'.$abc->dokumen.'">';
            $baris .= '<input type="file" name="dok'.$i.'" class="btn btn-outline-info btn-sm w-50"><lable class="col-sm-4 control-label">'.$abc->dokumen.'</lable>';
            $baris .= '</div>';
        $i++; 
    	}
    	echo $baris;
	}
}
