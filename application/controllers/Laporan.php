<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	
	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_laporan','laporan');
	}


	public function klaim(){
		$klaim = $this->db->select("t_klaim.*,t_peserta_order.namaPeserta, ")
			->join("t_peserta_order","t_peserta_order.idPeserta = t_klaim.idPeserta","inner");
			
		if($this->session->userdata("roles") == 5){
			$klaim = $klaim->where(array("t_peserta_order.idUser" => $this->session->userdata("idUser") ));
		}
		$klaim = $klaim->get('t_klaim')->result();
		
		$data['klaim'] = $klaim;
		if($this->session->userdata("roles") == 5){
			$this->app->render('Laporan Detail Klaim','laporan/klaim',$data);
		}else{
			$this->app->render('Laporan Detail Klaim','laporan/klaimbroker',$data);			
		}
	}
	
	public function listklaim(){
		$draw = intval($this->input->post("draw"));
      	$start = intval($this->input->post("start"));
      	$length = intval($this->input->post("length"));

      	$klaim = $this->db->select("t_klaim.*")
			->join("t_peserta_order","t_peserta_order.idPeserta = t_klaim.idPeserta","inner");
			
		if($this->session->userdata("roles") == 5){
			$klaim = $klaim->where(array("t_peserta_order.idUser" => $this->session->userdata("idUser") ));
		}
		$klaim = $klaim->get('t_klaim');
	
      	$data = array();
      	$i = 1;

		foreach($klaim->result() as $key) {
			if($this->session->userdata("roles") == 5){
				$link = '<span class="badge badge-danger animated fadeIn infinite f-s-12">'.$key->status.'</span>'; 
			}else{
				$name="";
				if($key->status == "pengajuan"){
					$name="Verifikasi";
				}elseif($key->status == "verified"){
					$name="Penilaian";
				}elseif($key->status == "approved"){
					$name="Bayar";
				}
				$link = '<a href="' . base_url('klaim/proses/' . $key->idKlaim) . '" class="btn-danger btn-small"> '.$name.' </a>';
			}
            $data[] = array(
              	$i,
				$key->noKlaim,
				$key->namaRumahSakit,
				$key->telpRumahSakit,
				$key->tglBerobat,
				$key->biayaPengobatan,
				$key->alamatRumahSakit,
				$key->kondisiPasien,
				$key->diagnosaSakit,
				$key->status,
				$link,
            );
       		$i++;
      	}
      	$output = array(
            "draw" => $draw,
            "recordsTotal" => $klaim->num_rows(),
            "recordsFiltered" => $klaim->num_rows(),
            "data" => $data
        );

      	echo json_encode($output);
      	exit();
	}

	public function invoice(){
		$this->app->render('Invoice Pembayaran','laporan/invoice/form_invoice',null);
	}	
}
