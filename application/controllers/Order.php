<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->load->library('session');
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_produksi','produksi');
	}
	
	
	public function emaildesain(){
		$this->load->view("public/produksi/invoice_tagihan");
	}
	public function generateinvoice(){
		$this->load->library('pdf');
		$data['jml_peserta'] = 3;
		$data['data_peserta'] = $this->db->get_where("t_peserta_order",array())->result_array();
		$data['invoice'] = $this->db->get_where("t_invoice",array())->row();
		$data['data_karyawan'] = $this->db->get_where("t_karyawan",array())->row_array();
			
		$html = $this->load->view("public/produksi/pdf_invoice_tagihan",$data,TRUE);
		
		$this->pdf->loadHtml($html);
		
		$this->pdf->render();
		
		
		$this->pdf->stream("invoice_example.pdf", array("Attachment"=>0));
	
	}
	
	
	public function generateinvoicelunas(){
		$this->load->library('pdf');
			
		$html = $this->load->view("public/produksi/example_invoice_lunas","",TRUE);
		
		$this->pdf->loadHtml($html);
		
		$this->pdf->render();
		
		
		$this->pdf->stream("invoicelunas_example.pdf", array("Attachment"=>0));
	}


	public function tambah_peserta(){
		
		$config = array(
			"protocol" => "smtp",
			"smtp_host" => 'smtp.gmail.com',
			"smtp_port" => 587,
			"smtp_user" => "bhayunugroho0809@gmail.com",
			"smtp_pass" => "marethatiara123",
			'crlf'      => "\r\n",
            'newline'   => "\r\n"
		);
		$this->load->library("phpmailer_lib");
		
		$mail = $this->phpmailer_lib->load();
		
		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = "bhayunugroho0809@gmail.com";
		$mail->Password = "maretha1234";
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;
		//$mail->SMTPDebug = 2;
		$mail->SetFrom('bhayunugroho0809@gmail.com', 'noreply');  //Siapa yg mengirim email
        $mail->Subject    = "Invoice Tagihan DJP Sehat";
        	
		if($this->input->post('order')){			
			$this->session->set_flashdata('success', '<span class="order-berhasil"></span>');
					
			$tglMulaiAsuransi = dateFormatDatabase($this->input->post("tglMulaiAsuransi"));
			$futureDate = strtotime('+1 Year',strtotime($tglMulaiAsuransi));
			$tglAkhirAsuransi = date("Y-m-d",$futureDate);
			
			$data_peserta = array();
			$no_invoice =  'INV'.mt_rand(100000, 999999);
			$filename="invoice_tagihan-".date("Y-m-d").".pdf";	
			$data_invoice = array(
				'noInvoice' => $no_invoice,
				'idUser' => $this->session->userdata("idUser"),
				'tglMulaiAsuransi' => $tglMulaiAsuransi,
				'tglAkhirAsuransi' => $tglAkhirAsuransi,
				'levelPlan' => $this->input->post("levelPlanLock")[0],
				'jmlPremi' => $this->input->post("jmlPremiHidden"),
				'filename_invoice' => $filename
			);
		
			$this->db->insert('t_invoice',$data_invoice);
			
			$idInvoice = $this->db->insert_id();
			$jml_premi = 0;
			
			foreach($this->input->post("namaLengkapHidden") AS $k=>$v){ 
				$noRegister =  'REG'.mt_rand(100000, 999999).$k;		
				$age = new Datetime(date("Y-m-d",strtotime($this->input->post("tglLahirHidden")[$k])));
				$today = new DateTime();
				$diff = $today->diff($age);
				$yearage = $diff->y;
				if($this->input->post("levelPlanHidden")[$k] == '800'){
					if($yearage <= 23 ){
						$premi = 1552261;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "M" && $yearage > 23 ){
						$premi = 2186368;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "F" && $yearage > 23 ){
						$premi = 2535167;
					}
				}elseif($this->input->post("levelPlanHidden")[$k] == '1000'){
					if($yearage <= 23 ){
						$premi = 2255305;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "M" && $yearage > 23 ){
						$premi = 3183090;
					}elseif($this->input->post("jenisKelaminHidden")[$k] == "F" && $yearage > 23 ){
						$premi = 3679042;
					}
				}elseif($this->input->post("levelPlanHidden")[$k] == '1500'){
					if($yearage <= 23 ){
						$premi = 3266757;
					}elseif($row['I'] == "M" && $yearage > 23 ){
						$premi = 4620180;
					}elseif($row['I'] == "F" && $yearage > 23 ){
						$premi = 5328205;
					}
				}
				$jml_premi += $premi;
				$data_peserta[] = array(
					'idInvoice' => $idInvoice,
					'noRegistrasi' => $noRegister,
					'idUser' => $this->session->userdata("idUser"),
					'namaPeserta' => $this->input->post("namaLengkapHidden")[$k],
					'hubunganKeluarga' => $this->input->post("hubunganKeluargaHidden")[$k],
					'tglLahir' => dateFormatDatabase($this->input->post("tglLahirHidden")[$k]),
					'premi' => $premi,
					'levelPlan' => $this->input->post("levelPlanLock"),
					'jenisKelamin' => $this->input->post("jenisKelaminHidden")[$k],
					'statusPernikahan' => $this->input->post("statusPernikahanHidden")[$k],
					'noHp' => $this->input->post("noHpHidden")[$k],
				);				
			}			
			
	
			$data_email['jml_peserta'] = count($data_peserta);
			$data_email['invoice'] = $this->db->get_where("t_invoice",array("idInvoice" => $idInvoice))->row();
			$data_email['data_peserta'] = $data_peserta;
			$data_email['data_karyawan'] = $this->db->get_where("t_karyawan",array("idUser" => $this->session->userdata("idUser")))->row();
			$mail->MsgHTML($this->load->view("public/produksi/invoice_tagihan",$data_email,TRUE));
        			
						
			$toEmail = $this->session->userdata("username"); // siapa yg menerima email ini
			$mail->AddAddress($toEmail);
					
			$this->db->insert_batch('t_peserta_order',$data_peserta);
			$mail->AddAttachment($this->generate_pdf($no_invoice,$idInvoice,$filename,$data_email));
			$mail->Send();
			redirect('order/tambah_peserta');
		}
		$invoice = $this->db->where(array("idUser" => $this->session->userdata("idUser")))->order_by("idInvoice","ASC")->get("t_invoice")->row();
		$member = $this->db->get_where('t_karyawan',array('idUser' => $this->session->userdata("idUser")))->row();
		
		$data['member'] = $member;
		$data['invoice'] = $invoice;
		$this->app->render('Order','produksi/tambah_peserta',$data);
		$this->load->view('public/produksi/_jsproduksi',$data);
		
	}	
	
	public function generate_pdf($no_invoice = null,$idInvoice = null,$filename = null,$data = null){
		$this->load->library('pdf');
		
		$html = $this->load->view("public/produksi/pdf_invoice_tagihan",$data,TRUE);
		$this->pdf->loadHtml($html);
		$this->pdf->render();
		//$this->pdf->stream("invoicelunas_example.pdf", array("Attachment"=>0));

		file_put_contents("assets/pdfs/".$filename, $this->pdf->output());
			
		return "assets/pdfs/".$filename;
	}
}
