<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_setting','setting');
	}

  public function setPrapen(){
    $this->app->render('List Debitur','master/prapen',null);
  }

  public function filter_setPrapen(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_prapen();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      if ($key->StatusPrapen==0){
        $btn = '<span id="link"><a onclick="updatePrapen(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Set Prapen</a></span>';
      }else{
        $btn = '-';
      }

      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        price($key->JumlahPremiTenor),
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();
  }

  public function updatePrapen(){
    $update = $this->setting->updatePrapen();
    echo $update;
  }

  public function setProrate(){
    $this->app->render('List Debitur','master/prorate',null);
  }

  public function filter_setProrate(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_prorate();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<a onclick="updateProrate(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Update Premi</a>';

      $text = '<span id="link"><input type="text" id="premi" name="JumlahPremiTenor" class="col-xs-3 custom-select f-s-12 price" value="'.price($key->JumlahPremiTenor).'"></span>';


      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $text,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updateProrate(){
    $update = $this->setting->updateProrate();
    echo $update;
  }

  public function setMutasi(){
    $this->app->render('List Debitur','master/mutasi',null);
  }

  public function DataMutasi(){
    $this->app->render('List Debitur','master/data_mutasi',null);
  }

  public function setPolis(){
    $this->app->render('List Debitur','master/polis',null);
  }

  public function filter_setMutasi(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_mutasi();

    $data = array();
    $i = 1;

    $jml = count($peserta->result());

    foreach($peserta->result() as $key) {
      if($jml>=2){
        $btn = '<a onclick="updateMutasi(\''.$jml.$key->NomorPinjaman.'\')" class="btn btn-danger btn-xs" style="color: #ffffff;">Delete</a>';
      }else{
        $btn = '<a onclick="updateMutasi(\''.$jml.$key->idDebitur.'\')" class="btn btn-danger btn-xs" style="color: #ffffff;">Delete</a>';
      }

      if($key->StatusPinjaman==1){
        $status = "NEW";
      }else{
        $status = "TOP UP";
      }

      $data[] = array(
        $key->NomorRegistrasi,
        $key->NomorPinjaman,
        $key->idDebitur,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $status,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updateMutasi(){
    $update = $this->setting->updateMutasi();
    echo $update;
  }

  public function filter_setPolis(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_polis();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<a onclick="updatePolis(\''.$key->NomorRegistrasi.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Update Polis</a>';

      $text = '<span id="link"><input type="text" id="nomorpolis" name="nomorpolis" class="col-xs-3 custom-select f-s-12" value="'.$key->NomorPolis.'"></span>';

      $data[] = array(
        $key->NomorRegistrasi,
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $text,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updatePolis(){
    $update = $this->setting->updatePolis();
    echo $update;
  }

  public function filter_DataMutasi(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_data_mutasi();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $user = $this->db->query("select username from t_user where idUser = '$key->delete_user'")->row()->username;
      if($key->StatusPinjaman==1){
        $status = "NEW";
      }else{
        $status = "TOP UP";
      }

      $data[] = array(
        '',
        $key->NomorRegistrasi,
        $key->NomorPinjaman,
        $key->idDebitur,
        $key->NamaDebitur,
        my_date($key->TglLahir),
        cekPekerjaan($key->KodePekerjaan),
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $status,
        cekNamaBank($key->kodeBank),
        $user,
        my_date($key->delete_date)
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function setUmur(){
    $this->app->render('List Debitur','master/umur',null);
  }

  public function filter_setUmur(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_umur();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<a onclick="updateUmur(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Update Usia</a>';

      $tgllahir = '<span id="link"><input type="text" id="tgllahir" name="tgllahir" class="col-xs-3 custom-select f-s-12" value="'.my_date($key->tgl).'"></span>';
      $text = '<span id="link"><input type="text" id="UsiaDebitur" name="UsiaDebitur" class="col-xs-3 custom-select f-s-12" value="'.$key->UsiaDebitur.'"></span>';


      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $tgllahir,
        $text,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updateUmur(){
    $update = $this->setting->updateUmur();
    echo $update;
  }

  public function setPindahan(){
    $this->app->render('Debitur','master/pindahan',null);
  }

  public function filter_setPindahan(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_pindahan();
    $t_asuransi = $this->setting->t_asuransi();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<span id="link"><a onclick="updatePindahan(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Pindahkan</a></span>';

        $tahun = date("Y");

      $asuransi = '<input type="hidden" id="tahun'.$key->NomorPinjaman.'" name="tahun'.$key->NomorPinjaman.'" value="'.$tahun.'"><select id="kodeAsuransi'.$key->NomorPinjaman.'" name="kodeAsuransi'.$key->NomorPinjaman.'">';
                    foreach ($t_asuransi as $value) {
                      $selected = '';
                      if($value->kodeAngka==$key->kodeAsuransi){
                        $selected = 'selected';
                      }
                      $asuransi .= '<option value="'.$value->kodeAngka.'" '.$selected.'>'.$value->asuransi.'</option>';
                    }
                  $asuransi .= '</select>';


      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        $asuransi,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();
  }

  public function updatePindahan(){
    $update = $this->setting->updatePindahan();
    echo $update;
  }

  public function setKodePekerjaan(){
    $this->app->render('Debitur','master/pekerjaan',null);
  }

  public function filter_setKodePekerjaan(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_kode();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<a onclick="updateKodePekerjaan(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Update Kode Pekerjaan</a>';

      $text = '<span id="link"><input type="text" id="KodePekerjaan" name="KodePekerjaan" class="col-xs-3 custom-select f-s-12" value="'.$key->kodePekerjaan.'"></span>';


      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $text,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updateKodePekerjaan(){
    $update = $this->setting->updateKodePekerjaan();
    echo $update;
  }

    public function setPremiBj(){
    $this->app->render('List Debitur','master/premibj',null);
  }

  public function filter_setPremiBj(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_premibj();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<a onclick="updatePremiBj(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Update Premi</a>';

      $text = '<span id="link"><input type="text" id="premi" name="JumlahPremiTenor" class="col-xs-3 custom-select f-s-12 price" value="'.price($key->JumlahPremiTenor).'"></span>';


      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $text,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updatePremiBj(){
    $update = $this->setting->updatePremiBj();
    echo $update;
  }


}
