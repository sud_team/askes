<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->load->library('session');
		$this->load->model('Model_helper','helper'); 
		$this->load->model('Model_produksi','produksi');
	}
	
	public function user(){
		if($this->input->post('submit')){
			$no_registrasi = strtotime(date('Y-m-d H:i:s'));
			
			$data_user = array(
				'username' => $this->input->post('emailPeserta'),
				'pass' => pass($this->input->post('password')),
				'hak_akses' => 5,
				'roles' => 5,
				'ganti_password' => 1,
				'emailPeserta' => $this->input->post('emailPeserta'),
				'status' =>  'active'
			);	
			
			$this->db->insert('t_user',$data_user);
			
			$data_karyawan = array(
				'idUser' => $this->db->insert_id(),
				'nip' => $this->input->post('nip'),
				'namaPeserta' => $this->input->post('namaPeserta'),
				);	
			
			$this->db->insert('t_karyawan',$data_karyawan);
			
			$this->session->set_flashdata('success', 'Registrasi berhasil, silahkan login'); 
			redirect('login/auth');
		}		
		$this->load->view('public/produksi/registration');
	}	
	
	public function generate_pdf($tgl_upload = null){
		$this->load->library('pdf');
		
		$data = $this->db->get_where('t_peserta',array('tgl_upload' => $tgl_upload))->result();
		$html_content = '<html>';
		$html_content .= '<body>';
		$html_content .= '<header>';
		$html_content .= '<div ><img style="float:left" src="'.base_url("assets/ela/images/logo.png").'"/>';
		$html_content .= '<h1>INVOICE ASKES</h1>';
		$html_content .= '</div><div style="clear:both"></div></header>';
		$html_content .= '<hr/>';
		$html_content .= '<div style="background:#5D6975;margin-bottom:20px;color:#fff;padding:9px;text-align:center">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</div>';
		$html_content .= '<table border="1" style="border-collapse:collapse"width="100%" cellspacing="5" cellpadding="5">';
		$html_content .= '<tr style="background:#eeeeee;font-weight:bold">
				<td>Level Plan</td>
				<td>Peserta</td>
				<td>Total Premi</td>
			</tr>';
		$dataPlan = array('700','900','1400');
		
		
		foreach($dataPlan AS $v){
			 $dataPlan = $this->db->select(
				'COUNT(*) AS jml_peserta, SUM(premi) AS premi'
			)->get_where('t_peserta',array('levelPlan' => $v))->row();
			$html_content .= '<tr>
				<td>'.$v.'</td>
				<td>'.$dataPlan->jml_peserta.'</td>
				<td>'.number_format((int)$dataPlan->premi).'</td>
				
			</tr>';
		}
		$html_content .= '</table>';
		$html_content .= '</body>';
		$html_content .= '</html>';
		$this->pdf->loadHtml($html_content);
		$this->pdf->render();
		$this->pdf->stream("invoice.pdf", array("Attachment"=>0));
	}
}
