<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peserta extends CI_Controller
{

	public function __Construct()
	{
		parent::__Construct();
		$this->load->library('session');
		$this->load->model('Model_helper', 'helper');
		$this->load->model('Model_produksi', 'produksi');
	}

	public function listpeserta()
	{
		
		$data['members'] = $member;
		$this->app->render('Order', 'peserta/listpeserta', $data);
		$this->load->view('public/peserta/_jspeserta', $data);
	}


	public function listpesertaklien()
	{
		$member = $this->db
			->select("t_peserta_order.*,t_invoice.tglAkhirAsuransi,t_invoice.tglMulaiAsuransi,t_karyawan.tempatLahir,t_karyawan.namaPeserta AS namaKaryawan,t_karyawan.namaPeserta AS namaKaryawan")
			->join("t_invoice", "t_invoice.idInvoice = t_peserta_order.idInvoice", "inner")
			->join("t_karyawan", "t_invoice.idUser = t_karyawan.idUser", "inner")
			->where(array("t_invoice.idUser" => $this->session->userdata("idUser"), 'status_bayar' => 'sudah_lunas'))
			->group_by('noRegistrasi')
			->get('t_peserta_order')
			->result();
			
		$data['member'] = $member;
		$this->app->render('Order', 'peserta/listpesertaklien', $data);
		$this->load->view('public/peserta/_jspeserta', $data);
	}

	public function datatables()
	{

		$role = $this->session->userdata("roles");

		$member = $this->db
			->select("t_peserta_order.*,t_invoice.tglMulaiAsuransi,t_invoice.status_bayar,t_karyawan.namaPeserta AS namaKaryawan")
			->join("t_invoice", "t_invoice.idInvoice = t_peserta_order.idInvoice", "inner")
			->join("t_karyawan", "t_invoice.idUser = t_karyawan.idUser", "inner")
			->get('t_peserta_order')
			->result();

		$data = array();
		$i = 1;

		foreach ($member as $k => $r) {

			$data[] = array(
				($k + 1),
				strtoupper($r->noRegistrasi),
				strtoupper($r->namaPeserta),
				strtoupper($r->namaKaryawan),
				strtoupper($r->hubunganKeluarga),
				strtoupper($r->jenisKelamin),
				strtoupper($r->levelPlan),
				strtoupper($r->statusPernikahan),
				strtoupper($r->tglMulaiAsuransi),
				strtoupper($r->tglLahir),
				price($r->premi),			
				strtoupper($r->noPeserta),
				strtoupper(str_replace("_"," ",$r->status_bayar)),
			);

			$i++;
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}


	public function datatablesklien()
	{

		$role = $this->session->userdata("roles");

		$member = $this->db
			->select("t_peserta_order.*,t_invoice.tglAkhirAsuransi,tglMulaiAsuransi,t_karyawan.tempatLahir,t_karyawan.namaPeserta AS namaKaryawan,t_karyawan.namaPeserta AS namaKaryawan")
			->join("t_invoice", "t_invoice.idInvoice = t_peserta_order.idInvoice", "inner")
			->join("t_karyawan", "t_invoice.idUser = t_karyawan.idUser", "inner")
			->where(array("t_invoice.idUser" => $this->session->userdata("idUser"), 'status_bayar' => 'sudah_lunas'))
			->group_by('noRegistrasi')
			->get('t_peserta_order')
			->result();

		$data = array();
		$i = 1;

		foreach ($member as $k => $r) {

			$data[] = array(
				// ($k + 1),
				$r->noRegistrasi,
				'<a href="' . base_url('peserta/listdetailpeserta/' . $r->idPeserta) . '" class="text-primary">' . $r->namaPeserta . '</a>',
				
				// $r->namaKaryawan,
				// $r->hubunganKeluarga,
				// $r->jenisKelamin,
				// $r->levelPlan,
				// $r->statusPernikahan,
				$r->tglMulaiAsuransi . ' s/d ' . 	$r->tglAkhirAsuransi,
				// $r->tglLahir,
				// price($r->premi),
				$r->noPeserta,
				// '<a href="' . base_url('peserta/listdetailpeserta/' . $r->idPeserta) . '" class="f-s-18"> <i class="fa fa-folder-open" aria-hidden="true"> </a>'
			);
			$i++;
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

	public function listdetailpeserta($idPeserta)
	{
		$member = array();
		$data['member'] = $this->db->select("t_peserta_order.*,t_karyawan.namaPeserta AS namaKaryawan,t_karyawan.alamat AS alamat, t_karyawan.noRekeningBank, t_karyawan.namaBank,t_karyawan.namaPemilikBank,t_karyawan.tempatLahir, t_invoice.noInvoice,t_invoice.tglMulaiAsuransi,t_invoice.tglAkhirAsuransi")
			->join("t_invoice", "t_invoice.idInvoice = t_peserta_order.idInvoice", "inner")
			->join("t_karyawan", "t_karyawan.idUser = t_peserta_order.idUser", "inner")
			->where(array("idPeserta" => $idPeserta))
			->get("t_peserta_order")
			->row();

		$data["idPeserta"] = $idPeserta;

		$this->app->render('Order', 'peserta/listpesertaklien_detail', $data);
		$this->load->view('public/peserta/_jspeserta', $data);
	}

	public function tambah_peserta()
	{

		$config = array(
			"protocol" => "smtp",
			"smtp_host" => 'smtp.gmail.com',
			"smtp_port" => 587,
			"smtp_user" => "bhayunugroho0809@gmail.com",
			"smtp_pass" => "marethatiara123",
			'crlf'      => "\r\n",
			'newline'   => "\r\n"
		);
		$member = $this->db->get_where('t_peserta', array('idUser' => $this->session->userdata("idUser")))->row();
		$this->load->library("phpmailer_lib");

		$mail = $this->phpmailer_lib->load();

		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = "bhayunugroho0809@gmail.com";
		$mail->Password = "maretha1234";
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;
		//$mail->SMTPDebug = 2;
		$mail->SetFrom('bhayunugroho0809@gmail.com', 'noreply');  //Siapa yg mengirim email
		$mail->Subject    = "Invoice tagihan";
		$mail->Body       = "Invoice Tagihan";

		if ($this->input->post('order')) {
			$this->session->set_flashdata('success', '<span class="order-berhasil"></span>');

			$tglMulaiAsuransi = dateFormatDatabase($this->input->post("tglMulaiAsuransi"));
			$futureDate = strtotime('+1 Year', strtotime($tglMulaiAsuransi));
			$futureDate = date("Y-m-d", $futureDate);

			$data_peserta = array();

			$no_invoice =  'INV' . mt_rand(100000, 999999);
			$data_invoice = array(
				'noInvoice' => $no_invoice,
				'idUser' => $this->session->userdata("idUser"),
				'tglMulaiAsuransi' => $tglMulaiAsuransi,
				'filename_invoice' => $filename
			);

			$this->db->insert('t_invoice', $data_invoice);

			$idInvoice = $this->db->insert_id();
			$jml_premi = 0;

			foreach ($this->input->post("namaLengkapHidden") as $k => $v) {

				$age = new Datetime(date("Y-m-d", strtotime($this->input->post("tglLahirHidden")[$k])));
				$today = new DateTime();
				$diff = $today->diff($age);
				$yearage = $diff->y;
				if ($this->input->post("levelPlanHidden")[$k] == '800') {
					if ($yearage <= 23) {
						$premi = 1552261;
					} elseif ($this->input->post("jenisKelaminHidden")[$k] == "M" && $yearage > 23) {
						$premi = 2186368;
					} elseif ($this->input->post("jenisKelaminHidden")[$k] == "F" && $yearage > 23) {
						$premi = 2535167;
					}
				} elseif ($this->input->post("levelPlanHidden")[$k] == '1000') {
					if ($yearage <= 23) {
						$premi = 2255305;
					} elseif ($this->input->post("jenisKelaminHidden")[$k] == "M" && $yearage > 23) {
						$premi = 3183090;
					} elseif ($this->input->post("jenisKelaminHidden")[$k] == "F" && $yearage > 23) {
						$premi = 3679042;
					}
				} elseif ($this->input->post("levelPlanHidden")[$k] == '1500') {
					if ($yearage <= 23) {
						$premi = 3266757;
					} elseif ($row['I'] == "M" && $yearage > 23) {
						$premi = 4620180;
					} elseif ($row['I'] == "F" && $yearage > 23) {
						$premi = 5328205;
					}
				}
				$jml_premi += $premi;
				$data_peserta[] = array(
					'idInvoice' => $idInvoice,
					'idUser' => $this->session->userdata("idUser"),
					'namaPeserta' => $this->input->post("namaLengkapHidden")[$k],
					'tglLahir' => dateFormatDatabase($this->input->post("tglLahirHidden")[$k]),
					'tglMulaiAsuransi' => $tglMulaiAsuransi,
					'tglAkhirAsuransi' => $futureDate,
					'premi' => $premi,
					'levelPlan' => $this->input->post("levelPlanHidden")[$k],
					'jenisKelamin' => $this->input->post("jenisKelaminHidden")[$k],
					'statusPernikahan' => $this->input->post("statusPernikahanHidden")[$k],
					'noHp' => $this->input->post("noHpHidden")[$k],
				);
			}

			$data_premi = array(
				'jmlPremi' => $jml_premi,
			);
			$this->db->where('idInvoice', $idInvoice);
			$this->db->update('t_invoice', $data_premi);

			$toEmail = $this->session->userdata("username"); // siapa yg menerima email ini
			$mail->AddAddress($toEmail);

			$this->db->insert_batch('t_peserta_order', $data_peserta);
			$mail->AddAttachment($this->generate_pdf($no_invoice, $idInvoice));
			$mail->Send();
			redirect('order/tambah_peserta');
		}

		$member = $this->db->get_where('t_peserta', array('idUser' => $this->session->userdata("idUser")))->row();

		$data['member'] = $member;
		$this->app->render('Order', 'produksi/tambah_peserta', $data);
		$this->load->view('public/produksi/_jsproduksi', $data);
	}

	public function generate_pdf($no_invoice = null, $idInvoice)
	{
		$this->load->library('pdf');

		$html_content = '<html>';
		$html_content .= '<body>';
		$html_content .= '<header>';
		$html_content .= '<div ><img style="float:left" src="assets/ela/images/tugulogo.png"/><br/>';
		$html_content .= '</div><div style="clear:both"></div></header>';
		$html_content .= '<hr/>';
		$html_content .= '<div style="background:#5D6975;margin-bottom:20px;color:#fff;padding:9px;text-align:center">INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</div>';
		$html_content .= '<table width="100%"><tr><td>NO INVOICE : ' . $no_invoice . '</td></tr><tr><td>Tanggal : ' . date("Y F d") . '</td></tr></table></div><br/>';
		$html_content .= '<table border="1" style="border-collapse:collapse"width="100%" cellspacing="5" cellpadding="5">';
		$html_content .= '<tr style="background:#eeeeee;font-weight:bold">
				<td>Nama Peserta</td>
				<td>Jenis Kelamin</td>
				<td>Level Plan</td>
				<td>Premi</td>
			</tr>';

		$data = $this->db->get_where("t_peserta_order", array("idInvoice" => $idInvoice))->result();
		$total = 0;
		foreach ($data as $v) {

			$html_content .= '<tr>
				<td>' . $v->namaPeserta . '</td>
				<td>' . $v->jenisKelamin . '</td>
				<td>' . $v->levelPlan . '</td>
				<td>' . number_format((int)$v->premi) . '</td>					
			</tr>';
			$total += $v->premi;
		}
		$html_content .= '<tr>
				<td colspan="3" style="text-align:left">Total</td>
				<td>' . number_format($total) . '</td>				
			</tr>';
		$html_content .= '</table>';
		$html_content .= '</body>';
		$html_content .= '</html>';
		$this->pdf->loadHtml($html_content);

		$this->pdf->render();

		$filename = "invoice_tagihan-" . date("Y-m-d") . ".pdf";
		//$this->pdf->stream("invoice.pdf", array("Attachment"=>0));
		file_put_contents("assets/pdfs/" . $filename, $this->pdf->output());


		return "assets/pdfs/" . $filename;
	}
}
