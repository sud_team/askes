$(document).ready(function() {
	// var cabang = $("#cabang").val();
 //   	$.ajax({
 //      	url: base_url+"produksi/capem/"+cabang,
 //      	type: 'POST',
 //      	success: function (data, textStatus, xhr) {
 //      		$("#capem").html(data);
 //      	}
 //    });
	
	$('.js-example-basic-cabang').select2();
});


$("#cek").click(function() {
	var cabang = $("#cabang").val();
	var capem = $("#capem").val();
	var produk = $("#produk").val();
	var nopin = $("#nopin").val();
	var nama = $("#nama").val();
	var tglawal = $("#tgl_mulai").val();
	var tglakhir = $("#tgl_akhir").val();
	$('#debitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"debitur/filter_debitur",
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
    });
});

function cekTracking(argument) {
    $.ajax({
        url: base_url+"debitur/cekTracking",
        type: 'POST',
        data: 'noreg='+argument,
        success: function (data, textStatus, xhr) {
            $('#tracking').modal();
            $('#dok').html(data);
        }
    });
}

