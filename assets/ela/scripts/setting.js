$(document).ready(function() {
    $('#DataDebitur').DataTable({
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 5,
    });


    $('#filter_DataMutasi').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/filter_DataMutasi",
            "type"  : "POST",
            "dataType" : "json"
        }
        
    });
});


function updatePrapen(argument) {
    var oke = confirm("Apakah anda sudah yakin?");
	if(oke){
        $.ajax({
          	url: base_url+"setting/updatePrapen",
          	type: 'POST',
          	data: 'NomorPinjaman='+argument,
          	success: function (data, textStatus, xhr) {
          		if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
          	}
        });
    }
}

$("#cariPrapen").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

$("#cariProrate").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

$("#cariUmur").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

$("#cariPolis").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

$("#cariMutasi").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

function updateMutasi(argument) {
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updateMutasi",
            type: 'POST',
            data: 'NomorPinjaman='+argument,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}

function updateUmur(argument) {
    var umur = $("#UsiaDebitur").val();
    var tgllahir = $("#tgllahir").val();
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updateUmur",
            type: 'POST',
            data: 'NomorPinjaman='+argument+"&UsiaDebitur="+umur+"&tgllahir="+tgllahir,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}


function updatePolis(argument) {
    var nomorpolis = $("#nomorpolis").val();
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updatePolis",
            type: 'POST',
            data: 'NomorRegistrasi='+argument+"&nomorpolis="+nomorpolis,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}


function updateProrate(argument) {
    var premi = $("#premi").val();
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updateProrate",
            type: 'POST',
            data: 'NomorPinjaman='+argument+"&JumlahPremiTenor="+premi,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}

$("#cariPindahan").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

function updatePindahan(argument) {
    var tahun = $("#tahun"+argument).val();
    var kodeAsuransi = $("#kodeAsuransi"+argument).val();
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updatePindahan",
            type: 'POST',
            data: 'NomorPinjaman='+argument+"&TahunKe="+tahun+"&kodeAsuransi="+kodeAsuransi,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}

$("#cariKodePekerjaan").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

function updateKodePekerjaan(argument) {
    var KodePekerjaan = $("#KodePekerjaan").val();
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updateKodePekerjaan",
            type: 'POST',
            data: 'NomorPinjaman='+argument+"&KodePekerjaan="+KodePekerjaan,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}

$("#cariPremiBJ").click(function(){
    var link = $("#link").val();
    $('#DataDebitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"setting/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

function updatePremiBj(argument) {
    var premi = $("#premi").val();
    var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
            url: base_url+"setting/updatePremiBj",
            type: 'POST',
            data: 'NomorPinjaman='+argument+"&JumlahPremiTenor="+premi,
            success: function (data, textStatus, xhr) {
                if(data==1){
                    alert("Berhasil");
                    $("#link").html("-");   
                }else{
                    alert("Gagal");
                }
            }
        });
    }
}





