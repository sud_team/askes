$(document).ready(function() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    today = yyyy+mm+dd;



    getAjaxData("0-0-"+yyyy);
 
    //on changing select option
    var tahunglobal = yyyy;
    $('#cek').click(function(){
        var cabang = $('#cabang').val();
        var capem = $('#capem').val();
        var tahun2 = $('#tahun').val();
        if(cabang==""){
            cabang = 0;
        }
        if(capem==""){
            capem = 0;
        }
        if(tahun2==""){
            tahun2 = tahunglobal; 
        }
        var param = cabang+"-"+capem+"-"+tahun2;
        // alert(param);
        tahunglobal = tahun2;
        getAjaxData(param);
    });

    function getAjaxData(id){

        //use getJSON to get the dynamic data via AJAX call
        $.getJSON(base_url+'dashboard/grafik/'+id, function(chartData) {
            // alert(chartData);
            $('#container1').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Grafik Premi '+tahunglobal
                },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Rupiah'
                    }
                },
                colors: [
                    '#20c997', 
                    '#dc3545'
                ],
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: chartData
            });
        });

        $.getJSON(base_url+'dashboard/piex/'+id, function(chartData2) {
            // alert(chartData);
            $('#container2').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Summary '+tahunglobal
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y:.0f} ',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: 
                        chartData2
                }]
            });
        });
    }

    $('#DataPeserta').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"dashboard/JatuhTempo",
            "type"  : "POST",
            "dataType" : "json"
        }
    });

    $(".datepicker3").datepicker({
        format: 'yyyy',
        autoclose: true,
        todayHighlight: true,
        viewMode: "years", 
        minViewMode: "years"
    });
    
});