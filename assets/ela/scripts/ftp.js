$(document).ready(function() {
    $('#link').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "url"   : base_url+"ftp/ftp_list",
            "type"  : "POST",
            "dataType" : "json"
        }
    });

    $('#history').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "url"   : base_url+"ftp/ftp_history",
            "type"  : "POST",
            "dataType" : "json"
        }
    });

    $('#put02').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "url"   : base_url+"ftp/ftp_list02",
            "type"  : "POST",
            "dataType" : "json"
        }
    });

    $('#put03').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "url"   : base_url+"ftp/ftp_list03",
            "type"  : "POST",
            "dataType" : "json"
        }
    });

    $('#put04').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "url"   : base_url+"ftp/ftp_list04",
            "type"  : "POST",
            "dataType" : "json"
        }
    });

    $('#put05').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "url"   : base_url+"ftp/ftp_list05",
            "type"  : "POST",
            "dataType" : "json"
        }
    });
});


