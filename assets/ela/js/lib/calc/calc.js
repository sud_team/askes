$(document).ready(function() {
	$("#calc").click(function() {
		var plafon = $("#plafon").val(); // plafon,
		var period = $("#period").val(); // tenor tahuna
		var ratePremi = $("#rPremi").val(); //rate premi
		var sukuBunga = $("#bunga").val() / 100;  //suku bunga

		if (plafon == 0 || period == 0 || ratePremi == 0) {
			alert("isi kolom kalkulator dengan lengkap");
		} else {
			$.ajax({
		      	url: base_url+"dashboard/kalkulator",
		      	type: 'POST',
		      	data: 'plafon='+plafon+'&period='+period+'&ratePremi='+ratePremi+'&sukuBunga='+sukuBunga,
		      	success: function (data, textStatus, xhr) {
		      		$("#premitotal").text(data);
		      	}
		    });
		}
	});
	$("#hitungprapen").click(function() {
		var plafon = $("#plafonprapen").val(); // plafon,
		var usiadebitur = $("#usiadebitur").val(); // tenor tahuna
		var usiabatas = $("#usiabatas").val(); //rate premi
		var tenor = $("#tenor").val();  //suku bunga

		if (plafon == 0 || usiadebitur == 0 || usiabatas == 0 || tenor == 0) {
			alert("isi kolom kalkulator dengan lengkap");
		}else if(usiadebitur>usiabatas){
			alert("Usia Debitur melebihi batas usia pensiun, gunakan perhitungan dengan rate premi Pensiun");
		} else {
			$.ajax({
		      	url: base_url+"dashboard/prapen",
		      	type: 'POST',
		      	data: 'plafon='+plafon+'&usiadebitur='+usiadebitur+'&usiabatas='+usiabatas+'&tenor='+tenor,
		      	success: function (data, textStatus, xhr) {
		      		$("#premiprapen").text(data);
		      	}
		    });
		}
	});
	$("#rest").click(function() {
		var plafon = $("#plafonR").val(); // plafon,
		var period = $("#periodR").val(); // tenor tahuna
		var ratePremi = $("#interest").val(); //rate premi
		var bulanBayar = $("#bulanBayar").val(); //rate premi
		// alert(ratePremi);
		if (plafon == 0 || period == 0 || ratePremi == 0 || bulanBayar == 0) {
			alert("isi kolom kalkulator dengan lengkap");
		} else {
			$.ajax({
		      	url: base_url+"dashboard/restitusi",
		      	type: 'POST',
		      	data: 'plafon='+plafon+'&period='+period+'&interest='+ratePremi+'&bulanBayar='+bulanBayar,
		      	success: function (data, textStatus, xhr) {
		      		$("#sudahbayar").text(data);
		      	}
		    });
		}
	});

});