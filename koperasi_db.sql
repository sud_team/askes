-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2019 at 07:13 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('b8342d486f92246b635a0daff89e38b1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0', 1557717191, 'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"login\";b:1;s:6:\"u_name\";s:5:\"admin\";s:5:\"level\";s:5:\"admin\";}'),
('49bd677d747f36246a2f9e0c6467f5a8', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0', 1557721188, 'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"login\";b:1;s:6:\"u_name\";s:5:\"admin\";s:5:\"level\";s:5:\"admin\";}'),
('8a9bfe4253b7b6d0f3e1d35d1ebcb405', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0', 1557721380, 'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"login\";b:1;s:6:\"u_name\";s:5:\"admin\";s:5:\"level\";s:5:\"admin\";}');

-- --------------------------------------------------------

--
-- Table structure for table `jns_akun`
--

CREATE TABLE `jns_akun` (
  `id` bigint(20) NOT NULL,
  `kd_aktiva` varchar(5) DEFAULT NULL,
  `jns_trans` varchar(50) NOT NULL,
  `akun` enum('Aktiva','Pasiva') DEFAULT NULL,
  `laba_rugi` enum('','PENDAPATAN','BIAYA') NOT NULL DEFAULT '',
  `pemasukan` enum('Y','N') DEFAULT NULL,
  `pengeluaran` enum('Y','N') DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jns_akun`
--

INSERT INTO `jns_akun` (`id`, `kd_aktiva`, `jns_trans`, `akun`, `laba_rugi`, `pemasukan`, `pengeluaran`, `aktif`) VALUES
(5, 'A4', 'Piutang Usaha', 'Aktiva', '', 'Y', 'Y', 'Y'),
(6, 'A5', 'Piutang Karyawan', 'Aktiva', '', 'N', 'Y', 'N'),
(7, 'A6', 'Pinjaman Anggota', 'Aktiva', '', NULL, NULL, 'Y'),
(8, 'A7', 'Piutang Anggota', 'Aktiva', '', 'Y', 'Y', 'N'),
(9, 'A8', 'Persediaan Barang', 'Aktiva', '', 'N', 'Y', 'Y'),
(10, 'A9', 'Biaya Dibayar Dimuka', 'Aktiva', '', 'N', 'Y', 'Y'),
(11, 'A10', 'Perlengkapan Usaha', 'Aktiva', '', 'N', 'Y', 'Y'),
(17, 'C', 'Aktiva Tetap Berwujud', 'Aktiva', '', NULL, NULL, 'Y'),
(18, 'C1', 'Peralatan Kantor', 'Aktiva', '', 'N', 'Y', 'Y'),
(19, 'C2', 'Inventaris Kendaraan', 'Aktiva', '', 'N', 'Y', 'Y'),
(20, 'C3', 'Mesin', 'Aktiva', '', 'N', 'Y', 'Y'),
(21, 'C4', 'Aktiva Tetap Lainnya', 'Aktiva', '', 'Y', 'N', 'Y'),
(26, 'E', 'Modal Pribadi', 'Aktiva', '', NULL, NULL, 'N'),
(27, 'E1', 'Prive', 'Aktiva', '', 'Y', 'Y', 'N'),
(28, 'F', 'Utang', 'Pasiva', '', NULL, NULL, 'Y'),
(29, 'F1', 'Utang Usaha', 'Pasiva', '', 'Y', 'Y', 'Y'),
(31, 'K3', 'Pengeluaran Lainnya', 'Aktiva', '', 'N', 'Y', 'N'),
(32, 'F4', 'Simpanan Sukarela', 'Pasiva', '', NULL, NULL, 'Y'),
(33, 'F5', 'Utang Pajak', 'Pasiva', '', 'Y', 'Y', 'Y'),
(36, 'H', 'Utang Jangka Panjang', 'Pasiva', '', NULL, NULL, 'Y'),
(37, 'H1', 'Utang Bank', 'Pasiva', '', 'Y', 'Y', 'Y'),
(38, 'H2', 'Obligasi', 'Pasiva', '', 'Y', 'Y', 'N'),
(39, 'I', 'Modal', 'Pasiva', '', NULL, NULL, 'Y'),
(40, 'I1', 'Simpanan Pokok', 'Pasiva', '', NULL, NULL, 'Y'),
(41, 'I2', 'Simpanan Wajib', 'Pasiva', '', NULL, NULL, 'Y'),
(42, 'I3', 'Modal Awal', 'Pasiva', '', 'Y', 'Y', 'Y'),
(43, 'I4', 'Modal Penyertaan', 'Pasiva', '', 'Y', 'Y', 'N'),
(44, 'I5', 'Modal Sumbangan', 'Pasiva', '', 'Y', 'Y', 'Y'),
(45, 'I6', 'Modal Cadangan', 'Pasiva', '', 'Y', 'Y', 'Y'),
(47, 'J', 'Pendapatan', 'Pasiva', 'PENDAPATAN', NULL, NULL, 'Y'),
(48, 'J1', 'Pembayaran Angsuran', 'Pasiva', '', NULL, NULL, 'Y'),
(49, 'J2', 'Pendapatan Lainnya', 'Pasiva', 'PENDAPATAN', 'Y', 'N', 'Y'),
(50, 'K', 'Beban', 'Aktiva', '', NULL, NULL, 'Y'),
(52, 'K2', 'Beban Gaji Karyawan', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(53, 'K3', 'Biaya Listrik dan Air', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(54, 'K4', 'Biaya Transportasi', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(60, 'K10', 'Biaya Lainnya', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(110, 'TRF', 'Transfer Antar Kas', NULL, '', NULL, NULL, 'N'),
(111, 'A11', 'Permisalan', 'Aktiva', '', 'Y', 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `jns_angsuran`
--

CREATE TABLE `jns_angsuran` (
  `id` bigint(20) NOT NULL,
  `ket` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `aktif` enum('Y','T','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jns_angsuran`
--

INSERT INTO `jns_angsuran` (`id`, `ket`, `satuan`, `aktif`) VALUES
(13, 5, '', 'Y'),
(14, 20, '', 'Y'),
(15, 10, '', 'Y'),
(16, 40, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `jns_simpan`
--

CREATE TABLE `jns_simpan` (
  `id` int(5) NOT NULL,
  `jns_simpan` varchar(30) NOT NULL,
  `jumlah` double NOT NULL,
  `tampil` enum('Y','T') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jns_simpan`
--

INSERT INTO `jns_simpan` (`id`, `jns_simpan`, `jumlah`, `tampil`) VALUES
(32, 'Simpanan Sukarela', 0, 'Y'),
(40, 'Simpanan Pokok', 20000, 'Y'),
(41, 'Simpanan Wajib', 10000, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `mst_area`
--

CREATE TABLE `mst_area` (
  `id` int(11) NOT NULL,
  `no_ordered` int(11) NOT NULL,
  `nama_area` varchar(255) NOT NULL,
  `tgl_dibuat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_area`
--

INSERT INTO `mst_area` (`id`, `no_ordered`, `nama_area`, `tgl_dibuat`) VALUES
(1, 1, 'ANYELIR MAKMUR', '2019-02-01 04:08:25'),
(2, 2, 'BITUNG', '2019-02-01 04:08:25'),
(3, 3, 'CIBUGEL', '2019-02-01 04:08:25'),
(4, 4, 'PASIR JAYA', '2019-02-01 04:08:25'),
(5, 5, 'RANCA DULANG', '2019-02-01 04:08:25'),
(6, 6, 'SOLEAR', '2019-02-01 04:08:25'),
(7, 7, 'TIGARAKSA', '2019-02-01 04:08:25'),
(8, 8, 'PERORANGAN', '2019-02-01 04:08:25'),
(9, 9, 'CANGKUDU', '2019-02-01 04:08:25'),
(10, 10, 'GEBANG', '2019-02-01 04:08:25'),
(11, 11, 'KADU JAYA', '2019-02-01 04:08:25'),
(12, 12, 'PASANGGRAHAN', '2019-04-16 02:56:46'),
(13, 13, 'CANGKUDU 1', '2019-04-16 02:56:57'),
(14, 14, 'BOJ0NG LOA', '2019-04-23 08:32:51'),
(15, 15, 'TANGERANG', '2019-05-02 08:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `mst_hubungan`
--

CREATE TABLE `mst_hubungan` (
  `id` int(11) NOT NULL,
  `no_ordered` int(11) DEFAULT NULL,
  `hubungan` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_hubungan`
--

INSERT INTO `mst_hubungan` (`id`, `no_ordered`, `hubungan`, `created_at`, `updated_at`) VALUES
(1, 1, 'SUAMI', NULL, '2019-04-22 03:42:32'),
(2, 2, 'ISTRI', NULL, '2019-04-22 03:42:41'),
(3, 3, 'ANAK', NULL, '2019-04-22 03:42:50'),
(4, 4, 'ORANG TUA', NULL, '2019-04-22 03:43:07'),
(5, 5, 'FAMILI LAIN', NULL, '2019-04-22 08:18:21');

-- --------------------------------------------------------

--
-- Table structure for table `mst_pendidikan`
--

CREATE TABLE `mst_pendidikan` (
  `id` int(11) NOT NULL,
  `no_ordered` int(11) DEFAULT NULL,
  `nama_pendidikan` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_pendidikan`
--

INSERT INTO `mst_pendidikan` (`id`, `no_ordered`, `nama_pendidikan`, `created_at`, `updated_at`) VALUES
(1, 1, 'SD', NULL, '2019-04-22 03:41:29'),
(2, 2, 'SMP', NULL, '2019-04-22 03:41:41'),
(3, 3, 'SMA', NULL, '2019-04-22 03:41:48'),
(4, 4, 'S1', NULL, '2019-04-22 03:42:08'),
(5, 5, 'S2', NULL, '2019-04-22 03:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `nama_kas_tbl`
--

CREATE TABLE `nama_kas_tbl` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(225) CHARACTER SET latin1 NOT NULL,
  `aktif` enum('Y','T') CHARACTER SET latin1 NOT NULL,
  `tmpl_simpan` enum('Y','T') CHARACTER SET latin1 NOT NULL,
  `tmpl_penarikan` enum('Y','T') CHARACTER SET latin1 NOT NULL,
  `tmpl_pinjaman` enum('Y','T') CHARACTER SET latin1 NOT NULL,
  `tmpl_bayar` enum('Y','T') CHARACTER SET latin1 NOT NULL,
  `tmpl_pemasukan` enum('Y','T') NOT NULL,
  `tmpl_pengeluaran` enum('Y','T') NOT NULL,
  `tmpl_transfer` enum('Y','T') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nama_kas_tbl`
--

INSERT INTO `nama_kas_tbl` (`id`, `nama`, `aktif`, `tmpl_simpan`, `tmpl_penarikan`, `tmpl_pinjaman`, `tmpl_bayar`, `tmpl_pemasukan`, `tmpl_pengeluaran`, `tmpl_transfer`) VALUES
(1, 'Kas Tunai', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(2, 'Kas Besar', 'Y', 'T', 'T', 'Y', 'T', 'Y', 'Y', 'Y'),
(3, 'Bank BNI', 'Y', 'T', 'T', 'T', 'T', 'Y', 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id` int(11) NOT NULL,
  `id_kerja` varchar(5) NOT NULL,
  `jenis_kerja` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `id_kerja`, `jenis_kerja`) VALUES
(1, '1', 'TNI'),
(2, '2', 'PNS'),
(3, '3', 'Karyawan Swasta'),
(4, '4', 'Guru'),
(5, '5', 'Buruh'),
(6, '6', 'Tani'),
(7, '7', 'Pedagang'),
(8, '8', 'Wiraswasta'),
(9, '9', 'Mengurus Rumah Tangga'),
(10, '99', 'Lainnya'),
(11, '98', 'Pensiunan'),
(12, '97', 'Penjahit'),
(13, '10', 'PELAUT'),
(14, '11', 'MAHASISWA'),
(15, '12', 'TIDAK BEKERJA');

-- --------------------------------------------------------

--
-- Table structure for table `suku_bunga`
--

CREATE TABLE `suku_bunga` (
  `id` int(10) NOT NULL,
  `opsi_key` varchar(20) NOT NULL,
  `opsi_val` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suku_bunga`
--

INSERT INTO `suku_bunga` (`id`, `opsi_key`, `opsi_val`) VALUES
(1, 'bg_tab', '0'),
(2, 'bg_pinjam', '9'),
(3, 'biaya_adm', '0'),
(4, 'denda', '0'),
(5, 'denda_hari', '15'),
(6, 'dana_cadangan', '40'),
(7, 'jasa_anggota', '40'),
(8, 'dana_pengurus', '5'),
(9, 'dana_karyawan', '5'),
(10, 'dana_pend', '5'),
(11, 'dana_sosial', '5'),
(12, 'jasa_usaha', '70'),
(13, 'jasa_modal', '30'),
(14, 'pjk_pph', '5'),
(15, 'pinjaman_bunga_tipe', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_anggota`
--

CREATE TABLE `tbl_anggota` (
  `id` bigint(20) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 NOT NULL,
  `identitas` varchar(255) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `tmp_lahir` varchar(225) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `status` varchar(30) NOT NULL,
  `agama` varchar(30) NOT NULL,
  `departement` varchar(255) NOT NULL,
  `pendidikan` varchar(100) DEFAULT NULL,
  `alamat_kantor_anggota` text,
  `pekerjaan` varchar(30) NOT NULL,
  `alamat` text CHARACTER SET latin1 NOT NULL,
  `kota` varchar(255) NOT NULL,
  `notelp` varchar(250) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `jabatan_id` int(10) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `pass_word` varchar(225) NOT NULL,
  `file_pic` varchar(225) NOT NULL,
  `data_penjamin` enum('anak','istri','suami') DEFAULT NULL,
  `nama_penjamin` varchar(255) DEFAULT NULL,
  `nik_penjamin` varchar(255) DEFAULT NULL,
  `agama_penjamin` varchar(255) DEFAULT NULL,
  `pendidikan_penjamin` varchar(255) DEFAULT NULL,
  `pekerjaan_penjamin` varchar(255) NOT NULL,
  `no_telpon_penjamin` varchar(100) DEFAULT NULL,
  `alamat_penjamin` text,
  `alamat_kantor_penjamin` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_anggota`
--

INSERT INTO `tbl_anggota` (`id`, `nik`, `nama`, `identitas`, `jk`, `tmp_lahir`, `tgl_lahir`, `status`, `agama`, `departement`, `pendidikan`, `alamat_kantor_anggota`, `pekerjaan`, `alamat`, `kota`, `notelp`, `tgl_daftar`, `jabatan_id`, `aktif`, `pass_word`, `file_pic`, `data_penjamin`, `nama_penjamin`, `nik_penjamin`, `agama_penjamin`, `pendidikan_penjamin`, `pekerjaan_penjamin`, `no_telpon_penjamin`, `alamat_penjamin`, `alamat_kantor_penjamin`) VALUES
(1, NULL, 'AAM AMINAH', '0001', 'P', 'JAKARTA', '1970-10-05', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BARENGKOK RT.003/003 KEL. SOLEAR KEC. SOLEAR', 'KEB.TANGERANG', '085311865503', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(2, NULL, 'AAS SETIAWATI', '0002', 'P', 'TANGERANG', '1980-08-27', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CIREUNDEU RT.003/002 KEL.CIKAREO KEC. SOLEAR', 'KAB.TANGERANG', '083894598510', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(3, NULL, 'ENUNG ROHANI', '0003', 'P', 'TANGERANG', '1976-01-14', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANGNENGAH RT.002/003 KEL. SOLEAR KEC. SOLEAR', 'KAB.TANGERANG', '082125033253', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(4, NULL, 'LASIH LESTARI', '0004', 'P', 'SURAKARTA', '1972-01-10', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN KIRANA SURYA BLOK D.005/024 RT.003/008 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB.TANGERANG', '085813590570', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(5, NULL, 'IDA RIYANI', '0005', 'P', 'LAMPUNG', '1972-06-01', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN ADIYASAH BLOK K.11/7/8 RT.001/005 KEL.CIKUYA KEC.SOLEAR', 'KAB.TANGERANG', '081283522264', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(6, NULL, 'SITI AROFAH', '0006', 'P', 'SUKOHARJO', '1969-03-11', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN KIRANA SURYA BLOK F.05/24 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB.TANGERANG', '085286766500', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(7, NULL, 'SUTIAWATI', '0007', 'P', 'TANGERANG', '1979-03-03', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA GEDE RT.003/002 KEL. MUNJUL KEC. SOLEAR', 'KAB.TANGERANG', '089527238386', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(8, NULL, 'AISAH', '0008', 'P', 'TANGERANG', '1974-05-07', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASIR KIYANG RT.001/002 KEL. SOLEAR KEC. SOLEAR', 'KAB.TANGERANG', '081319949476', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(9, NULL, 'MARTINI', '0009', 'P', 'TANGERANG', '1974-03-03', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA GEDE KALER RT.002/006 KEL. MUNJUL KEC.SOLEAR', 'KAB.TANGERANG', '085782941479', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(10, NULL, 'SUHERTI', '0010', 'P', 'TANGERANG', '1981-01-09', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. POS CIKUYA RT.002/001 KEL. CIKASUNGKA KEC. SOLEAR', 'KEB.TANGERANG', '081290399517', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(11, NULL, 'RISDAH', '0011', 'P', 'PADANG', '1965-12-05', 'Cerai Mati', 'Islam', 'SOLEAR', NULL, NULL, 'Pensiunan', 'TAMAN ADIYASAH BLOK A.11/10 RT.005/005 KEL. CIKASUNGKA KEC. SOLEAR', 'KAB.TANGERANG', '082111401600', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(12, NULL, 'ELI WARTINI', '0012', 'P', 'TANGERANG', '1969-09-04', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA MANEUH RT.001/003 KEL. MUNJUL KEC.SOLEAR', 'KAB.TANGERANG', '089677318225', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(13, NULL, 'SOFIYATUN', '0013', 'P', 'SOLO', '1970-07-02', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'MUNJUL PERMAI BLOK F.15/18 RT.001/005 KEL. MUNJUL KEC. SOLEAR', 'KEB.TANGERANG', '08121952865', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(14, NULL, 'SURYANINGSIH', '0014', 'P', 'JAKARTA', '1974-01-03', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Guru', 'PERUM MUNJUL PERMAI BLOK F.14/01 RT002/005 DESA.MUNJUL KEC.SOLEAR', 'KAB.TANGERANG', '085212190812', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(15, NULL, 'YULIA AFRIDA', '0015', 'P', 'MEDAN', '1969-07-12', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'PERUM TAMAN ADIYASA BLOK D.16/32 RT.003/007 KEL.CISASUNGKA KEC. SOLEAR', 'KAB.TANGERANG', '085715996042', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(16, NULL, 'SRI NINGSIH', '0016', 'P', 'PEMALANG', '1969-08-07', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. LEUNGSIR RT.006/001 KEL. MUNJUL KEC. SOLEAR', 'KAB.TANGERANG', '082111236220', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(17, NULL, 'YAYAH SRI HAYATI', '0017', 'P', 'BOGOR', '1966-12-04', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Lainnya', 'PERUM BUKIT CIKASUNGKA BLOK ACF.06/01 RT002/009', 'KEB TANGERANG', '081388503159', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(18, NULL, 'ERNA WARNI', '0018', 'P', 'JAKARTA', '1972-12-12', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN ADIYASA BLOK G. 15/21 RT.005/008 DESA. CIKASUNGKA KEC. SOLEAR ', 'KAB.TANGERANG', '081389321400', '2018-07-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(19, NULL, 'TUTI MARYATI', '0019', 'P', 'BANYUMAS/PERWOKERTO', '1968-09-19', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. TALAGA RT.002/001 KEL. TALAGA KEC. CIKUPA ', 'KAB.TANGERANG', '085217539944', '2018-07-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(20, NULL, 'ROHIMAH', '0020', 'P', 'TANGERANG', '1975-05-22', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SUKAMANAH RT.004/003 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '081298809248', '2018-07-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(21, NULL, 'TAUFIK WALHIDAYAT', '0021', 'L', 'TANGERANG', '1959-10-22', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085219704403', '2018-02-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(22, NULL, 'HJ.ARWIYAH', '0022', 'P', 'TANGERANG', '1943-01-01', 'Cerai Mati', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERANG RT.005/001 KEL. BUNDER KEC. CIKUPA', 'TANGERANG', '', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(23, NULL, 'SITI MARIYAM', '0023', 'P', 'TANGERANG', '1982-08-15', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP.KADU SANGERANG RT.005/OO1 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083806063312', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(24, NULL, 'ALIYAH', '0024', 'P', 'TANGERANG', '1985-12-05', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDE KEC.CIKUPA', 'TANGERANG', '081283008074', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(25, NULL, 'MUHAMAD AMSORI', '0025', 'L', 'TANGERANG', '1996-04-23', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP.KADU RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089627908687', '2018-02-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(26, NULL, 'NAPSAH', '0026', 'P', 'TANGERANG', '1966-05-19', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(27, NULL, 'LAILATUL KODARIAH', '0027', 'P', 'TANGERANG', '1991-04-13', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '087884992152', '2018-02-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(28, NULL, 'SAHATI', '0028', 'P', 'TANGERANG', '1973-07-02', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083872261933', '2018-02-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(29, NULL, 'ANAH', '0029', 'P', 'TANGERANG', '1952-08-05', 'Cerai Mati', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.POS BITUNG RT.001/003 KEL.KADUJAYA KEC.CURUG', 'TANGERANG', '083806525077', '2018-02-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(30, NULL, 'TRIE SUCI ATI', '0030', 'P', 'TANGERANG', '1987-01-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089533421016', '2018-02-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(31, NULL, 'MARYUNI', '0031', 'P', 'NGAWI', '1978-01-02', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081387002103', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(32, NULL, 'AMRIAH', '0032', 'P', 'TANGERANG', '1973-07-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(33, NULL, 'MASNUM', '0033', 'P', 'KOTABUMI', '1973-12-04', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085211286171', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(34, NULL, 'SUMIYATI', '0034', 'P', 'SERANG', '1976-05-01', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(35, NULL, 'DEDEH MULYATI', '0035', 'P', 'TANGERANG', '1965-12-10', '', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '08981789782', '2018-02-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(36, NULL, 'SRI SULASTRI S.PD', '0036', 'P', 'KARANG ANYAR', '1968-02-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'BANTEN', '082112260358', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(37, NULL, 'TUTI ALAWIYAH', '0037', 'P', 'TANGERANG', '1978-11-30', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081285842921', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(38, NULL, 'SITI HALIMAH', '0038', 'P', 'BOGOR', '1970-06-20', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083813203948', '2018-07-11', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(39, NULL, 'WETI SUSILAWATI', '0039', 'P', 'PANDEGLANG', '1971-01-26', 'Kawin', 'Islam', 'TIGARAKSA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BARU PINANG RT.001/003 KEL.TIGARAKSA KEC.TIGARAKSA', 'KAB.TANGERANG', '081388259717', '2018-02-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(40, NULL, 'ETTY LASVITAWATI', '0040', 'P', 'TANGERANG', '1968-02-27', 'Kawin', 'Islam', 'TIGARAKSA', NULL, NULL, 'Mengurus Rumah Tangga', 'PURI PERMAI 3 BLOK E 10/26 KEL.TIGARAKSA KEC.TIGARAKSA', 'KAB.TANGERANG', '085284782208', '2018-02-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(41, NULL, 'TATU MASTURAH', '0041', 'P', 'TANGERANG', '1973-01-01', 'Kawin', 'Islam', 'TIGARAKSA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADUAGUNG RT.001/002 KEL.MARGASARI KEC.TIGARAKSA', 'KAB.TANGERANG', '083871842797', '2018-02-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(42, NULL, 'SAROPAH', '0042', 'P', 'TANGERANG', '1986-11-22', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089675850084', '2018-07-11', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(43, NULL, 'YULIYANA', '0043', 'P', 'TANGERANG', '1990-07-27', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '082210803075', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(44, NULL, 'SALIYAH', '0044', 'P', 'TANGERANG', '1971-06-12', '', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085280734144', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(45, NULL, 'DESY PURWATI', '0045', 'P', 'TANGERANG', '1996-09-21', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085280734144', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(46, NULL, 'UWAT', '0046', 'P', 'TANGERANG', '1976-01-19', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '08568117479', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(47, NULL, 'IKA PURNAMA', '0047', 'P', 'TANGERANG', '1990-02-07', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '082260764549', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(48, NULL, 'SURYANAH', '0048', 'P', 'TANGERANG', '1970-05-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-02-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(49, NULL, 'ACIH', '0049', 'P', 'TANGERANG', '1970-06-18', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-02-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(50, NULL, 'SUPIATI/UPI', '0050', 'P', 'TANGERANG', '1973-01-25', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081380590154', '2018-02-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(51, NULL, 'ARNI', '0051', 'P', 'TANGERANG', '1976-07-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083891139024', '2018-02-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(52, NULL, 'MARYATI', '0052', 'P', 'TANGERANG', '1996-01-15', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089603658077', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(53, NULL, 'HALIMAH', '0053', 'P', 'TANGERANG', '1961-04-24', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089629775472', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(54, NULL, 'SUKARSIH', '0054', 'P', 'TANGERANG', '1976-01-06', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081388362198', '2018-02-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(55, NULL, 'NINING', '0055', 'P', 'TANGERANG', '1985-01-18', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083891139034', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(56, NULL, 'FITRI AFRIANI', '0056', 'P', 'TANGERANG', '1991-04-17', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.TALAGA RT.002/001 KEL.TALAGA KEC.CIKUPA', 'TANGERANG', '081280242368', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(57, NULL, 'SITI JAOJAH', '0057', 'P', 'TANGERANG', '1989-10-18', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085890139256', '2018-02-27', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(58, NULL, 'JUMRIAH', '0058', 'P', 'TANGERANG', '1989-05-25', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089609831264', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(59, NULL, 'HAERIYAH', '0059', 'P', 'BOGOR', '1981-05-02', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083873526410', '2018-02-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(60, NULL, 'MARDIYAH', '0060', 'P', 'TANGERANG', '1984-08-09', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(61, NULL, 'AISAH', '0061', 'P', 'TANGERANG', '1966-05-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-02-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(62, NULL, 'OPAH KUSMAWATI', '0062', 'P', 'TANGERANG', '1986-09-18', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '087806789159', '2018-02-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(63, NULL, 'UUN', '0063', 'P', 'TANGERANG', '1970-12-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(64, NULL, 'TITIN', '0064', 'P', 'TEGAL', '1957-11-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-02-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(65, NULL, 'NARMAH', '0065', 'P', 'TANGERANG', '1965-06-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '087774215123', '2018-02-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(66, NULL, 'SITI NUROHMAH', '0066', 'P', 'TANGERANG', '1979-08-24', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083893782779', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(67, '321303030649700', 'MASKHANAH,A.MA', '0067', 'P', 'BANJAR NEGARA', '1962-05-01', 'Kawin', 'Islam', 'TANGERANG', NULL, NULL, 'Mengurus Rumah Tangga', 'JL.IR SUTAMI GG DAHLIA NO.117 RT.004/010 KEL.SUKASARI KEC.TANGERANG', 'TANGERANG', '085714974750', '2018-03-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(68, NULL, 'SUSI', '0068', 'P', 'TANGERANG', '1982-08-28', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083892545214', '2018-03-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(69, NULL, 'YUNI AMBARINI', '0069', 'P', 'TANGERANG', '1996-06-01', '', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081314533635', '2018-02-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(70, NULL, 'ESAH', '0070', 'P', 'TANGERANG', '1984-06-17', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085213807239', '2018-03-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(71, NULL, 'HARYATI', '0071', 'P', 'TANGERANG', '1975-10-02', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-03-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(72, NULL, 'SATI', '0072', 'P', 'TANGERANG', '1942-01-17', 'Cerai Mati', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085211371304', '2018-03-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(73, NULL, 'ANWAL ROSIDIN', '0073', 'L', 'PURWODADI', '1975-10-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081284100720', '2018-03-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(74, NULL, 'MARHAMAH', '0074', 'P', 'TANGERANG', '1966-03-28', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '082114293943', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(75, NULL, 'SRI LESTARI', '0075', 'P', 'SEMARANG', '1976-05-25', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089535189696', '2018-03-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(76, NULL, 'MIMI', '0076', 'P', 'BOGOR', '1979-06-14', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081296626936', '2018-03-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(77, NULL, 'HAYATI', '0077', 'P', 'TANGERANG', '1985-06-18', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085284352088', '2018-03-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(78, NULL, 'ANAH', '0078', 'P', 'TANGERANG', '1957-10-09', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-03-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(79, NULL, 'ADANG KUSNADI', '0079', 'L', 'SUKABUMI', '1962-12-08', 'Kawin', 'Islam', 'TANGERANG', NULL, NULL, 'Wiraswasta', 'KAV.AGRARIA NO.16 RT.001/008 KEL.NUSAJAYA KEC.KARAWACI', 'TANGERANG', '085871950657', '2018-03-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(80, NULL, 'ANIH SAANIH', '0081', 'P', 'TANGERANG', '1976-05-07', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANGNENGAH RT.002/003 KEL. SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(81, NULL, 'DEDE SUHERNI', '0082', 'P', 'TANGERANG', '1979-06-06', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'KAMPUNG MALANG NENGAH RT 02/03 DS SOLEAR', 'TANGERANG', '12/03/', '2018-03-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(82, NULL, 'IDA FARIDA', '0083', 'P', 'JAKARTA', '1971-07-07', 'Kawin', 'Islam', 'SOLEAR', NULL, NULL, 'Mengurus Rumah Tangga', 'TM KIRANA SURYA BLOK K025/20', 'TANGERANG', '081388036502', '2018-03-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(83, NULL, 'SITI RARIYA', '0084', 'P', 'TANGERANG', '1990-04-26', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085213901855', '2018-03-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(84, NULL, 'IYAM', '0085', 'P', 'TANGERANG', '1971-04-04', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085285792507', '2018-03-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(85, NULL, 'ASNIYAH', '0086', 'P', 'INDRAMAYU', '1987-06-10', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG RT. 03/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '085959701941', '2018-03-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(86, NULL, 'TITIN MARYATI', '0087', 'P', 'TANGERANG', '1975-10-25', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG RT. 03/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '081286756829', '2018-03-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(87, NULL, 'HADILAH', '0088', 'P', 'INDRAMAYU', '1966-07-06', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Wiraswasta', 'KP. RANCA DULANG RT. 03/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '087784771605', '2018-03-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(88, NULL, 'FANNY NOVITA', '0089', 'P', 'LAHAT', '1982-11-03', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.005/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081282993948', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(89, NULL, 'Y.NURHAYATI', '0090', 'P', 'CIANJUR', '1967-05-28', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.003/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081293813467', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(90, '321030303049687', 'EVA SUGIARTI', '0091', 'P', 'TANGERANG', '1969-09-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.003/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089533165267', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(91, NULL, 'PADILAH', '0092', 'P', 'TANGERANG', '1970-07-04', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.002/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081382185735', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(92, NULL, 'TITIN SARTINAH', '0093', 'P', 'TANGERANG', '1972-11-02', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.003/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083897216081', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(93, NULL, 'MUSLIHA', '0094', 'P', 'TANGERANG', '1975-07-27', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINONG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '08983108559', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(94, NULL, 'SUHAYANI', '0095', 'P', 'JAKARTA', '1974-05-03', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.005/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081311236398', '2018-03-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(95, NULL, 'KUNDRIYATI', '0096', 'P', 'TANGERANG', '1972-11-30', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PEKULONAN BARAT RT.001/003 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '088213298637', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(96, NULL, 'JAMILAH', '0097', 'P', 'TANGERANG', '1986-09-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Karyawan Swasta', 'KP.CICAYUR BUBULAK RT.002/005', 'TANGERANG', '08998378249', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(97, NULL, 'MURTASIYAH', '0098', 'P', 'TANGERANG', '1983-02-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089504154106', '2018-03-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(98, NULL, 'MEISYIAH', '0099', 'P', 'PURWOREJO', '1972-07-16', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG RT. 02/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '081311347094', '2018-03-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(99, NULL, 'MARWIYAH', '0100', 'P', 'TANGERANG', '1938-01-06', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085288900419', '2018-02-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(100, NULL, 'SITI KHOFIFAH', '0101', 'P', 'TANGERANG', '1975-02-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.002/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081298211725', '2018-03-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(101, NULL, 'EEN MAESAROH', '0102', 'P', 'SUBANG', '1972-08-14', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081203008218', '2018-03-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(102, NULL, 'SITI  MUDRIKAH', '0103', 'P', 'TANGERANG', '1989-06-22', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK JINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089670753581', '2018-03-28', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(103, NULL, 'JUBAEDAH', '0104', 'P', 'TANGERANG', '1960-09-03', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADEMANGAN RT.003/005 KEL.PAKULONAN BARAT KEC.KELAPADUA', 'TANGERANG', '', '2018-03-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(104, NULL, 'YULIANA', '0105', 'P', 'TANGERANG', '1972-09-28', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADEMANGAN RT.003/005 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089534878967', '2018-03-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(105, NULL, 'YUNIARSIH CICIH', '0106', 'P', 'JAKARTA', '1973-06-27', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADEMANGAN RT.002/005 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '08811653580', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(106, NULL, 'UMIYATI', '0107', 'P', 'TANGERANG', '1989-02-05', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Lainnya', 'KP.RUMPAK SINANG RT.001/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '08777859241', '2018-03-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(107, NULL, 'YULI SUTIHAT', '0108', 'P', 'TANGERANG', '1981-11-22', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.002/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '085936867510', '2018-03-29', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(108, NULL, 'TATI HARYATI', '0109', 'P', 'TANGERANG', '1980-12-04', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'JL.KANO XX NO.9 RT.001/006 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087771192147', '2018-03-29', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(109, NULL, 'MUTOHAROH', '0110', 'P', 'TANGERANG', '1985-07-05', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089615322633', '2018-03-29', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(110, NULL, 'JUMHANA TUN HASANAH', '0111', 'P', 'TANGERANG', '1991-08-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089537587416', '2018-03-29', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(111, NULL, 'LISTIAWATI', '0112', 'P', 'TANGERANG', '1988-03-20', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081282201184', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(112, NULL, 'SRI ROYANI', '0113', 'P', 'TANGERANG', '1984-06-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089537579445', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(113, NULL, 'MARYANIH', '0114', 'P', 'TANGERANG', '1965-11-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089537579445', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(114, NULL, 'WARI\'AH', '0115', 'P', 'WARGABINANGUN', '1991-02-08', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085280581545', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(115, NULL, 'TRI RAHAYU', '0117', 'P', 'TANGERANG', '1992-10-08', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '087883543239', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(116, NULL, 'HAERIYAH', '0118', 'P', 'TANGERANG', '1983-10-05', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Buruh', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085959691990', '2018-03-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(117, NULL, 'ZANALIAH', '0119', 'P', 'SEKUNYIT', '1971-09-13', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG RT. 03/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '085283543812', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(118, NULL, 'UMAYAH ', '0120', 'P', 'TASIKMALAYA', '1994-09-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 04/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '082319647646', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(119, NULL, 'DARIAH', '0121', 'P', 'TANGERANG', '1958-05-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '082110874564', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(120, NULL, 'DEWI ', '0122', 'P', 'TANGERANG', '1969-10-28', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 04/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(121, NULL, 'IIN ROSLIANA ', '0124', 'P', 'TANGERANG', '1977-02-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085212677629', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(122, NULL, 'SAHANAH ', '0125', 'P', 'TANGERANG', '1967-07-24', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 08/02 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(123, NULL, 'ANI BIN MADUDI', '0126', 'P', 'TANGERANG', '1975-02-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '081319906897', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(124, NULL, 'ISAH', '0127', 'P', 'TANGERANG', '1980-05-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SADANG RT. 01/03 KEL. CEMPAKA KEC. CISOKA ', 'TANGERANG', '081210404185', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(125, NULL, 'UKMI', '0128', 'P', 'TANGERANG', '1964-12-28', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 02/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '089615750087', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(126, NULL, 'SURNI', '0129', 'P', 'TANGERANG', '1992-09-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASIR HUNI RT. 10/02 KEL. BOJONG LOA KEC. CISOKA ', 'TANGERANG', '085219346485', '2018-05-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(127, NULL, 'SUMYATI ', '0130', 'P', 'TANGERANG', '1968-06-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 04/05 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085256178517', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(128, NULL, 'ASMAH', '0131', 'P', 'TANGERANG', '1986-12-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASIR HUNI RT. 10/02 KEL. BOJONGLOA KEC. CISOKA ', 'TANGERANG', '082113316599', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(129, NULL, 'SITI NINGRAT ', '0132', 'P', 'TANGERANG', '1983-02-18', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASIR HUNI RT. 10/02 KEL. BOJONGLOA KEC. CISOKA ', 'TANGERANG', '', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(130, NULL, 'EMPAT ', '0133', 'P', 'TANGERANG', '1963-05-25', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL MESJID RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085215472789', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(140, NULL, 'SARMUNAH ', '0134', 'P', 'JAKARTA', '1963-06-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 04/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(141, NULL, 'ASMAH ', '0135', 'P', 'TANGERANG', '1987-01-18', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. LEUWIDAHU RT. 04/04 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(142, NULL, 'ELAH HAYATI ', '0136', 'P', 'TANGERANG', '1985-05-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. SAGA RT. 02/03 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '089628464045', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(143, NULL, 'ATI ROHAYATI', '0137', 'P', 'TANGERANG', '1970-01-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CIBUGEL RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'TANGERANG', '', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(144, NULL, 'OOY WIDOWATI', '0138', 'P', 'TANGERANG', '1984-10-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '082312716579', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(145, NULL, 'NYAI ARYANI ', '0140', 'P', 'TANGERANG', '1979-03-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085219107821', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(146, NULL, 'RUKIAH ', '0141', 'P', 'TANGERANG', '1987-05-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL MESJID RT. 02/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '089615750087', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(147, NULL, 'NURJANAH ', '0142', 'P', 'TANGERANG', '1981-05-24', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 02/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(148, NULL, 'SRI SUSANAH ', '0143', 'P', 'TANGERANG', '1981-12-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '081283137434', '2018-04-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(149, NULL, 'SARIDAH', '0144', 'P', 'TANGERANG', '1972-03-26', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '082311352803', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(150, NULL, 'YENI MARYENI', '0145', 'P', 'JAKARTA', '1984-01-23', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG RT. 02/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '083892295089', '2018-04-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(151, NULL, 'NINIEK KURNIAWATI', '0146', 'P', 'YOGYAKARTA', '1981-03-16', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG RT. 04/01 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '082211089913', '2018-04-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(152, NULL, 'WIDIANINGSIH', '0147', 'P', 'TASIKMALAYA', '1975-12-12', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Mengurus Rumah Tangga', 'RANCA DULANG RT.003/001 KEL.MARGASARI KEC.KARAWACI', 'TANGERANG', '085213318848', '2018-02-15', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(153, NULL, 'MAHDIHARIS', '0148', 'L', 'TANGERANG', '1982-04-11', 'Kawin', 'Islam', 'RANCA DULANG', NULL, NULL, 'Wiraswasta', 'JLN.DHARMA BAKTI NO:11 RT.002/004 KEL.PABUARAN KEC.KARAWACI', 'TANGERANG', '087887740194', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(154, NULL, 'TUSRINI ', '0149', 'L', 'TANGERANG', '1972-12-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. BOJONGLOA RT. 02/01 KEL. BOJONGLOA KEC. CISOKA ', 'TANGERANG', '085220521977', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(155, NULL, 'SADIAH ', '0150', 'P', 'TANGERANG', '1975-06-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. PASANGGRAHA RT. 02/01 KEL. PASANGGRAHA KEC. CISOKA ', 'TANGERANG', '085219697827', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(156, NULL, 'SITI UNANI ', '0151', 'P', 'TANGERANG', '1990-08-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONG LOA RT. 02/01 KEL. BOJONG LOA KEC. CISOKA ', 'TANGERANG', '083890593592', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(157, NULL, 'ENDAH MEGA SARI ', '0152', 'P', 'TANGERANG', '1987-06-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIGOONG RT. 09/03 KEL. BOJONG LOA KEC. CISOKA ', 'TANGERANG', '085214955802', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(158, NULL, 'NURIDAH', '0153', 'P', 'TANGERANG', '1981-11-26', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083897492604', '2018-03-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(159, NULL, 'OMAH', '0154', 'P', 'LAMPUNG', '1973-09-22', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083875262549', '2018-04-11', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(160, NULL, 'RASNAH', '0155', 'P', 'TANGERANG', '1980-03-01', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-04-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(161, NULL, 'SUKIYEM', '0156', 'P', 'T.SERDANG', '1961-06-30', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.004/004 KEL.TANJUNG DALAM KEC.ULOK KUPAI', 'TANGERANG', '', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(162, NULL, 'NURSEHA', '0157', 'P', 'TANGERANG', '1992-07-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.002/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081210093398', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(163, NULL, 'SITI MASTIAH', '0158', 'P', 'TANGERANG', '1981-03-01', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.002/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(164, NULL, 'SITI AMALIAH', '0159', 'P', 'JAKARTA', '1986-01-21', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087782115551', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(165, NULL, 'SOPIAH', '0160', 'P', 'TANGERANG', '1977-08-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081283251460', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(166, NULL, 'MAESAROH', '0161', 'P', 'TANGERANG', '1984-06-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087809842047', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(167, NULL, 'NURASIYAH', '0162', 'P', 'TANGERANG', '1981-04-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '08977746777', '2018-04-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(168, NULL, 'ROSANAH', '0163', 'P', 'TANGERANG', '1972-03-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081318682912', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(169, NULL, 'MISLAH', '0164', 'P', 'TANGERANG', '1970-10-13', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081380898670', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(170, NULL, 'RIYATI', '0165', 'P', 'TANGERANG', '1976-04-29', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083878855426', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(171, NULL, 'ASIYAH', '0166', 'P', 'TANGERANG', '1979-09-16', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.002/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083895208455', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(172, NULL, 'ROMLAH', '0167', 'P', 'TANGERANG', '1962-04-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081282384074', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(173, NULL, 'SITI JAMSIAH', '0168', 'P', 'JAKARTA', '1968-09-18', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'JL.KANO XX MO.05 RT.001/006 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081385439685', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(174, NULL, 'ANISYA SYAHREZAD', '0169', 'P', 'AMUNTAI', '1978-06-15', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'JL.KANO XX MO.05 RT.001/006 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '082114436989', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(175, NULL, 'SRI WAHYUNI', '0170', 'P', 'TANGERANG', '1985-01-20', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087881939590', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(176, NULL, 'SITI RACHMAINI', '0171', 'P', 'JAKARTA', '1983-12-03', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083893169222', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(177, NULL, 'SITI LUTFIAH', '0172', 'P', 'TANGERANG', '1990-03-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089667802025', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `tbl_anggota` (`id`, `nik`, `nama`, `identitas`, `jk`, `tmp_lahir`, `tgl_lahir`, `status`, `agama`, `departement`, `pendidikan`, `alamat_kantor_anggota`, `pekerjaan`, `alamat`, `kota`, `notelp`, `tgl_daftar`, `jabatan_id`, `aktif`, `pass_word`, `file_pic`, `data_penjamin`, `nama_penjamin`, `nik_penjamin`, `agama_penjamin`, `pendidikan_penjamin`, `pekerjaan_penjamin`, `no_telpon_penjamin`, `alamat_penjamin`, `alamat_kantor_penjamin`) VALUES
(178, NULL, 'SITI HURIAH', '0173', 'P', 'TANGERANG', '1995-09-09', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.PABUARAN RT.001/002 KEL.KARANG TENGAH KEC.PAGEDANGAN', 'TANGERANG', '089604387963', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(179, NULL, 'ANIH', '0174', 'P', 'TANGERANG', '1973-03-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.002/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081315264552', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(180, NULL, 'KOMARIYAH', '0175', 'P', 'SINAR OGAN', '1989-03-21', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081298002295', '2018-04-12', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(181, NULL, 'USNAH PUJANTI', '0176', 'P', 'TANGERANG', '1983-02-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087782115314', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(182, NULL, 'KHODIJATUL KUBRO', '0177', 'P', 'TANGERANG', '1982-06-06', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(183, NULL, 'MURINDAH', '0178', 'P', 'TEGAL', '1986-08-31', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. CIREWED RT. 02/01 KEL. SUKADAMAI KEC. CIKUPA', 'TANGERANG', '083873608387', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(184, NULL, 'EMI', '0179', 'P', 'TANGERANG', '1985-09-17', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '081291235212', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(185, NULL, 'SRI MULYANI', '0180', 'P', 'LAMPUNG', '1981-10-16', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Penjahit', 'GRIYA PASIR JAYA 2 BLOK.A1/09 RT. 043/02 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '089648342338', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(186, NULL, 'RUKIYAH', '0181', 'P', 'SRIMULYO', '2018-01-26', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. PENGKOLAN RT. 02/01 KEL. PASIR KADUNG KEC. CIKUPA', 'TANGERANG', '', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(187, NULL, 'MAIROH', '0182', 'P', 'PEMALANG', '1980-10-10', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '082312065784', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(188, NULL, 'HERAWATI', '0183', 'P', 'TANGERANG', '1979-09-25', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'TALAGASARI RT. 014/03 KEL. TALAGASARI KEC. CIKUPA', 'TANGERANG', '083877803106', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(189, NULL, 'MUNIROH', '0184', 'P', 'TANGERANG', '1972-04-30', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '081221728349', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(190, NULL, 'SUGIYANTI', '0185', 'P', 'LAMPUNG', '1979-10-10', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA II BLK. A2/11', 'TANGERANG', '08892361492', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(191, NULL, 'S.E LINDA SARI', '0186', 'P', 'BANTEN', '1981-04-30', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'PERUM GRIYA PASIR JAYA BLK. C NO. 03 RT. 044/03 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '081282127025', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(192, NULL, 'SUNARSIH', '0187', 'P', 'PANDEGLANG', '1976-07-16', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA YASA BLK.H5/08 RT. 04/06 KEL. PASIR GADUNG KEC. CIKUPA', 'TANGERANG', '085214061611', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(193, NULL, 'AISAH', '0188', 'P', 'CIANJUR', '1975-05-03', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, '', 'KP. PASIR GADUNG RT. 01/03 KEL. PASIR GADUNG KEC. CIKUPA', 'TANGERANG', '081289458126', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(194, NULL, 'INDRIANI MEDIAWATI', '0189', 'P', 'BANDUNG', '1985-11-15', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA II BLK.B/3 RT. 043/02', 'TANGERANG', '089534876355', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(195, NULL, 'NURJANNAH', '0190', 'P', 'PANDEGLANG', '1992-04-22', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Wiraswasta', 'KP. PASIR RT. 06/04 KEL. PASIR JAYA KEC. JATIUWUNG', 'TANGERANG', '083876010162', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(196, NULL, 'HUSNIAH', '0191', 'P', 'TANGERANG', '1979-09-22', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '085311176855', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(197, NULL, 'NINA ATIKAH', '0192', 'P', 'NGAWI', '1976-09-21', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Lainnya', 'GRIYA PASIR JAYA E-18 RT.044/03 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '081380464228', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(198, NULL, 'MASIAH', '0193', 'P', 'TANGERANG', '1978-08-05', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '081382419845', '2018-04-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(199, NULL, 'IPAH ', '0194', 'P', 'TANGERANG', '1990-09-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 09/04 KEL. CIBUGEL KEC. CISOKA ', 'KAB.TANGERANG', '085280372874', '2018-04-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(200, NULL, 'NUR SAMSIAH ', '0195', 'P', 'TANGERANG', '1993-02-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CISALAK RT. 03/03 KEL. CIRENDEU KEC. SOLEAR ', 'TANGERANG', '085215976684', '2018-04-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(201, NULL, 'KOMARIAH ', '0196', 'P', 'TANGERANG', '1972-01-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085216417455', '2018-04-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(202, NULL, 'ROHATI ', '0197', 'P', 'TANGERANG', '1953-11-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR ', 'TANGERANG', '085219697827', '2018-04-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(203, NULL, 'SITI ROSMANIAH', '0198', 'P', 'JAKARTA', '1981-12-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.BOJONG LOA RT.002/001 KEL.BOJONG LOA KEC.CISOKA', 'KAB.TANGERANG', '085321328294', '2018-04-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(204, NULL, 'YENI HERNAWATI', '0199', 'P', 'TANGERANG', '1981-05-22', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASANGGRAHAN RT.002/001 KEL.PASANGGRAHAN KEC.SOLEAR', 'KAB. TANGERANG', '081297229371', '2018-04-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(205, NULL, 'SAINAH', '0200', 'P', 'TANGERANG', '1972-07-06', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '085921309478', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(206, NULL, 'MARHENI', '0201', 'P', 'TANGERANG', '1977-05-07', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Pedagang', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083808866786', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(207, NULL, 'WASLIYAH', '0202', 'P', 'TANGERANG', '1965-05-11', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(208, NULL, 'FLORENTINA ISTIARYANI', '0203', 'P', 'SLEMAN', '1966-06-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'JL.KANO XXI NO.01 RT.001/006 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '082111140965', '2018-04-19', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(209, NULL, 'MAKIYATUL ARMINAH', '0204', 'P', 'TANGERANG', '1995-06-16', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.002/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089561042792', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(210, NULL, 'SULASIAH', '0205', 'P', 'TANGERANG', '1976-07-05', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083898624005', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(211, NULL, 'SITI MASITOH', '0206', 'P', 'TANGERANG', '1991-07-26', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083873169905', '2018-04-19', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(212, NULL, 'SARTINAH', '0207', 'P', 'TANGERANG', '1983-09-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Karyawan Swasta', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(213, NULL, 'NURJANAH', '0208', 'P', 'JAKARTA', '1971-07-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(214, NULL, 'LENI', '0209', 'P', 'BANDUNG', '1977-10-17', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNDER RT. 013/02 KEL. BUNDER KEC. CIKUPA', 'TANGERANG', '089533133288', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(215, NULL, 'MARHATI', '0210', 'P', 'TANGERANG', '1965-10-08', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089629775472', '2018-04-18', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(216, NULL, 'TUTIK SUNARTI', '0211', 'P', 'TANGERANG', '1976-08-14', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '081297883201', '2018-04-18', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(217, NULL, 'NARSIH', '0212', 'P', 'TANGERANG', '1968-07-03', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNUT RT. 04/02 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(218, NULL, 'INDAH PRATIWI', '0213', 'P', 'TANGERANG', '1990-03-15', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 09/04 KEL. PASIR JAYA KEC. CIKUPA ', 'TANGERANG', '087889330384', '2018-04-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(219, NULL, 'KOMSIDAH', '0214', 'P', 'TANGERANG', '1962-12-12', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 010/04 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '082111574771', '2018-04-18', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(220, NULL, 'PUDIAH', '0215', 'P', 'KAB.TANGERANG', '1982-06-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIGOONG RT.009/002 KEL.BOJONG LOA KEC.CISOKA', 'TANGERANG', '083891350113', '2018-04-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(221, NULL, 'EMUN', '0216', 'P', 'KAB.TANGERANG', '1982-06-29', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'TANGERANG', '085215395375', '2018-04-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(222, NULL, 'INAH', '0217', 'P', 'KAB.TANGERANG', '1975-09-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIGOONG RT.009/002 KEL.BOJONG LOA KEC.CISOKA', 'TANGERANG', '083812983221', '2018-04-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(223, NULL, 'SITI AISYAH', '0218', 'P', 'KAB.TANGERANG', '1962-04-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'PERUM PESONA WIBAWA PRAJA BLOK H5 NO.3 RT.003/006 KEL.JEUNGJING KEC.CISOKA', 'TANGERANG', '085311865248', '2018-04-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(224, NULL, 'MA,MUN B.BAIH', '0219', 'L', 'SUKABUMI', '1978-06-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.CIBUGEL RT.002/004 KEL.CIBUGEL KEC.CISOKA', 'TANGERANG', '', '2018-04-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(225, NULL, 'SAMAH', '0220', 'P', 'TANGERANG', '1996-01-01', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.002/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '082298860029', '2018-04-25', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(226, NULL, 'AHMAD FADILAH', '0221', 'L', 'TANGERANG', '1966-11-16', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Karyawan Swasta', 'KP.RUMPAK SINANG RT.002/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '082285155173', '2018-04-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(227, NULL, 'KURNIAWATI', '0222', 'P', 'TANGERANG', '1982-07-18', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081282795751', '2018-03-20', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(228, NULL, 'SUKRYAH', '0223', 'P', 'TANGERANG', '1975-11-01', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089561550095', '2018-04-04', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(229, NULL, 'ERNA SUSILAWATI', '0224', 'P', 'TANGERANG', '1978-01-07', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.002/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089673185416', '2018-04-24', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(230, NULL, 'NURYANAH', '0225', 'P', 'TANGERANG', '1979-07-20', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '085319789128', '2018-04-26', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(231, NULL, 'DASEM', '0226', 'P', 'SUBANG', '1979-01-16', 'Cerai Mati', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085218959484', '2018-04-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(232, NULL, 'YANTI', '0227', 'P', 'TANGERANG', '1977-10-15', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '083895420989', '2018-04-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(233, NULL, 'SITI JUMIKAH', '0228', 'P', 'TANGERANG', '1990-07-05', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '083897442311', '2018-04-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(234, NULL, 'M.YOYO SUHENDRA', '0229', 'L', 'TANGERANG', '1974-07-20', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Buruh', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '087786596683', '2018-04-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(235, NULL, 'SESTI ROSTANALA', '0230', 'P', 'TANGERANG', '1995-05-23', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '087771505685', '2018-04-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(236, NULL, 'FITRIANI', '0231', 'P', 'JAKARTA', '1990-05-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.BOJONG LOA RT.002/001 KEL.BOJONG LOA KEC.CISOKA', 'KAB.TANGERANG', '085321328294', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(237, NULL, 'TATI SUMIATI', '0232', 'P', 'TANGERANG', '1978-09-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.003/004 KEL.CARINGIN KEC.CISOKA', 'TANGERANG', '085212340024', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(238, NULL, 'BIRGITA RINI MARSINAWATI', '0233', 'P', 'WAY LIMA', '1975-11-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.CIBUGEL RT.003/005 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085211881050', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(239, NULL, 'METI HARDIYANTI', '0234', 'P', 'TANGERANG', '1993-07-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BANOGA RT.002/002 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '089553691715', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(240, NULL, 'META SARI', '0235', 'P', 'TANGERANG', '1994-08-28', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BANOGA RT.002/002 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '082312168212', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(241, NULL, 'ENENG SULASTRI', '0236', 'P', 'TANGERANG', '1978-11-28', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SUKAMULYA RT.003/002 KEL.CARINGIN KEC.CISOKA', 'TANGERANG', '', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(242, NULL, 'EKA YATI', '0237', 'P', 'TANGERANG', '1980-06-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CIBUGEL RT.003/005 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(243, NULL, 'HAERIYAH', '0238', 'P', 'TANGERANG', '1986-01-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.AMPEL MASJID RT.001/007 KEL.GEMBONG KEC. BALARAJA', 'KAB.TANGERANG', '085213359491', '2018-04-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(244, NULL, 'MARYATI', '0239', 'P', 'BANJARNEGARA', '1990-02-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081287175851', '2018-05-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(245, NULL, 'EMI NURHAYATI', '0240', 'P', 'TANGERANG', '1986-07-28', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089512708969', '2018-05-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(246, NULL, 'ATI KRISNAWATI', '0241', 'P', 'TANGERANG', '1990-08-23', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002.002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '087884566622', '2018-04-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(247, NULL, 'KUSMIYATI', '0242', 'P', 'KENDAL', '1959-05-28', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Lainnya', 'KP.RUMPAK SINANG RT.003/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081385694211', '2018-03-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(248, NULL, 'SAPURO', '0243', 'P', 'TANGERANG', '1978-05-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/0041 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(249, NULL, 'MARYOYOH', '0244', 'P', 'TANGERANG', '1975-05-04', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '085813947575', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(250, NULL, 'SITI HAYAROH', '0245', 'P', 'TANGERANG', '1987-08-02', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083893035695', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(251, NULL, 'ISMATUL KHOIRIYAH', '0246', 'P', 'TANGERANG', '1979-01-02', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089626436203', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(252, NULL, 'MARHUMAH', '0247', 'P', 'TANGERANG', '1964-05-02', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP.KALIPATEN RT.001/005 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '085711256604', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(253, NULL, 'JUMROH', '0248', 'P', 'TANGERANG', '1980-10-16', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.001/005 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(254, NULL, 'SULASTRI', '0249', 'P', 'TANGERANG', '1981-06-24', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PAKULONAN BARAT RT. 03/03 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '089533005287', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(255, NULL, 'NURLINDA', '0250', 'P', 'TANGERANG', '1986-07-20', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '083111688517', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(256, NULL, 'MAMNUAH', '0251', 'P', 'TANGERANG', '1963-03-07', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081210024969', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(257, NULL, 'HJ. KOYMAH', '0252', 'P', 'TANGERANG', '1977-10-22', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.003/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081293246773', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(258, NULL, 'IYEN SULASTRI', '0253', 'P', 'TANGERANG', '1987-08-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP.KALIPATEN RT.002/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '085945058141', '2018-04-26', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(259, NULL, 'FITRIAH', '0254', 'P', 'TANGERANG', '1984-06-30', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.005/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081289588654', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(260, NULL, 'AMENG SUAMAH', '0255', 'P', 'TANGERANG', '1982-03-23', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089532213390', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(261, NULL, 'DITHA YULIANTI', '0256', 'P', 'TANGERANG', '1988-02-26', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.004/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087776625865', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(262, NULL, 'SUNARIAH', '0257', 'P', 'TANGERANG', '1977-03-07', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNUT RT. 04/02 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '085212243747', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(263, NULL, 'UMIYATI', '0258', 'P', 'KLATEN', '1973-02-13', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. PULO RT. 012/05 KEL. BITUNG JAYA KEC. CIKUPA', 'TANGERANG', '', '2018-05-03', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(264, NULL, 'SURYANAH', '0259', 'P', 'TANGERANG', '1981-02-08', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. BUNUT RT. 01/02 KEL. PASIR GADUNG KEC. CIKUPA', 'TANGERANG', '', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(265, NULL, 'SUPARYATI', '0260', 'P', 'LAMPUNG TENGAH', '1974-10-05', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Wiraswasta', 'GRIYA PASIR JAYA 2 BLK. B/19 RT. 005/002 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '085289431455', '2018-05-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(266, NULL, 'SITI BADRIAH', '0261', 'P', 'TANGERANG', '1987-01-18', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASIR JAYA RT. 06/03 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '087782630914', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(267, NULL, 'SURYAH', '0262', 'P', 'TANGERANG', '1981-11-08', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BUNUT RT.001/002 KEL.PASIR GADUNG KEC.CIKUPA', 'TANGERANG', '081280679861', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(268, NULL, 'NURAENI ', '0263', 'P', 'TANGERANG', '1981-04-16', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BUNUT RT.003/002 KEL.PASIR JAYA KEC.CIKUPA', 'TANGERANG', '085210909593', '2018-05-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(269, NULL, 'NURHAYATI', '0264', 'P', 'TANGERANG', '1991-05-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.TOBAT RT.004/002 KEL.SENTUL JAYA KEC.BALARAJA', 'KAB.TANGERANG', '', '2018-05-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(270, NULL, 'NENG DEWI', '0265', 'P', 'TANGERANG', '1984-06-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BANOGA RT.002/002 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '085216420938', '2018-05-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(271, NULL, 'INTAN MULYASARI', '0266', 'P', 'TANGERANG', '1994-10-23', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.CIBUGEL RT.004/005 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-05-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(272, NULL, 'MATYATUN', '0267', 'P', 'PURWOREJO', '1983-02-10', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA II BLOK A 01/11 RT.005/002 KEL.PASIR JAYA KEC. CIKUPA', 'KAB.TANGERANG', '081283447176', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(273, NULL, 'EKO WAHYUNINGSIH', '0268', 'P', 'SEMARANG', '1975-04-15', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNUT RT.005/002 KEL.PASIR JAYA KEC CIKUPA', 'TANGERANG', '', '2018-05-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(274, NULL, 'RANI', '0269', 'P', 'BANDUNG', '1992-05-05', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.GEBANG DS.SUKADAMAI RT.003/006 KEC.CIKUPA', 'TANGERANG', '089631838309', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(275, NULL, 'SUMYATI', '0270', 'P', 'TANGERANG', '1980-01-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '083890600013', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(276, NULL, 'SITI NURAENI', '0271', 'P', 'TANGERANG', '1989-03-03', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089516413886', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(277, NULL, 'AMSINAH', '0272', 'P', 'TANGERANG', '1993-01-08', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089516413862', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(278, NULL, 'SUMINAH', '0273', 'P', 'TANGERANG', '1994-04-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Lainnya', 'KP.KALIPATEN RT.002/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '08994720860', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(279, NULL, 'SYUKRON BAEJURI', '0274', 'L', 'TANGERANG', '1973-11-08', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP.RUMPUKSINANG  RT.003/002 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081272015717', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(280, NULL, 'IYUN HUSNULAILY', '0275', 'P', 'TANGERANG', '1965-04-23', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP, PAKULONAN BARAT RT. 03/03 NO. 8 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '085216646964', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(281, NULL, 'SITI AHYATI', '0276', 'P', 'TANGERANG', '1981-10-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.CISOKA', 'KAB.TANGERANG', '', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(282, NULL, 'RUSNANI', '0277', 'P', 'TANGERANG', '1990-05-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '082312161728', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(283, NULL, 'JAENUDIN', '0278', 'P', 'TANGERANG', '1979-04-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.CIBUGEL RT.006/005 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085931958178', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(284, NULL, 'HJ.SAMINAH', '0279', 'P', 'TANGERANG', '1968-08-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '087887972319', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(285, NULL, 'LAHYATI', '0280', 'P', 'TANGERANG', '1968-04-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Guru', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085211728310', '2018-05-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(286, NULL, 'RUKIAH', '0281', 'P', 'JAKARTA', '1965-03-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.001/006 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(287, NULL, 'SUPRIYADI', '0282', 'L', 'TANGERANG', '1982-07-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CILABAN RT.017/04 KEL.BOJONG LOA KEC.CISOKA', 'KAB.TANGERANG', '081315692973', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(288, NULL, 'NYAI', '0283', 'P', 'TANGERANG', '1988-07-30', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASIR HUNI RT.010/002 KEL.BOJONG LOA KEC.CISOKA', 'KAB.TANGERANG', '081315692973', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(289, NULL, 'SRI NURHAYATI', '0284', 'P', 'TANGERANG', '1976-08-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASANGGRAHAN RT.006/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-05-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(290, NULL, 'UMYANAH', '0285', 'P', 'TANGERANG', '1984-08-13', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KALIPATEN RT.001/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '089533110699', '2018-05-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(291, NULL, 'MULHIMAH', '0286', 'P', 'TANGERANG', '1965-04-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.RUMPAK SINANG RT.001/001 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '087809842047', '2018-05-16', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(292, NULL, 'FAUZIAH', '0287', 'P', 'TANGERANG', '1973-01-08', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RUMPAK SINANG RT. 02/01 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '', '2018-05-16', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(293, NULL, 'MUTMAINAH', '0288', 'P', 'TANGERANG', '1988-11-07', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. KALIPATEN RT. 02/04  KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '083897391872', '2018-05-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(294, NULL, 'SITI APSAH', '0289', 'P', 'TANGERANG', '1983-07-20', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KALIPATEN RT. 02/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '', '2018-05-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(295, NULL, 'TITIN SUPRIATIN ', '0290', 'P', 'TANGERANG', '1988-03-12', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. KALIPATEN RT. 03/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '083876547047', '2018-05-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(296, NULL, 'ENEH MARLENIH', '0291', 'P', 'TANGERANG', '1981-09-07', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081211387147', '2018-05-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(297, NULL, 'NURJAMILAH', '0292', 'P', 'TANGERANG', '1990-03-13', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085280447807', '2018-05-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(298, NULL, 'UNAYAH', '0293', 'P', 'TANGERANG', '1983-10-14', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081291406241', '2018-05-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(299, NULL, 'SUSI LAWATI', '0294', 'P', 'TANGERANG', '1985-05-15', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.WARU RT.014/006 DS.PASIR JAYA KEL.CIKUPA', 'TANGERANG', '', '2018-05-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(300, NULL, 'INDAH SURANTI', '0295', 'P', 'BANYUMAS', '1990-07-27', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNDER RT.013/002 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089536947088', '2018-05-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(301, NULL, 'UMIYATI', '0296', 'P', 'TANGERANG', '1975-03-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASANGGRAHAN RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(302, NULL, 'YULININGSIH', '0297', 'P', 'TANGERANG', '1992-07-09', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SINGEUNG CADAS RT.004/001 KEL.SOLEAR KEC. SOLEAR', 'KAB.TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(303, NULL, 'ELIZIA PATRICIA KABERU', '0298', 'P', 'TANGERANG', '1999-04-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SAGA RT.003/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '089533060631', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(304, NULL, 'NENENG NURLAELAH', '0299', 'P', 'SERANG', '1975-09-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CANGKUDU RT.005/003 KEL.CANGKUDU KEC.BALARAJA', 'KAB.TANGERANG', '085211148044', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(305, NULL, 'MARTI', '0300', 'P', 'TANGERANG', '1972-02-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CANGKUDU RT.005/003 KEL.CANGKADU KEC.BALARAJA', 'KAB.TANGERANG', '081387995676', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(306, NULL, 'HJ.AMINAH', '0301', 'P', 'TANGERANG', '1971-08-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '085212533286', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(307, NULL, 'FITRIA STEPANI RONALDO', '0302', 'P', 'TANGERANG', '1989-05-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CANGKUDU RT.005/003 KEL.CANGKUDU KEC.BALARAJA', 'TANGERANG', '082141054744', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(308, NULL, 'UMAR PELANI ', '0303', 'L', 'TANGERANG', '1987-03-06', '', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. SAGA RT. 02/03 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '081288704570', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(309, NULL, 'MOHAMAD IDRIS ', '0304', 'L', 'TANGERANG', '1980-06-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. KECOK RT. 03/01 KEL. JEUNGJING KEC. CISOKA ', 'TANGERANG', '081210831628', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(310, NULL, 'RINI ARIYANTI ', '0305', 'P', 'TANGERANG', '1990-05-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA ', 'TANGERANG', '089508851145', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(311, NULL, 'MARIYAM ', '0306', 'P', 'TANGERANG', '1967-03-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(312, NULL, 'PEPI USANDI ', '0307', 'L', 'TANGERANG', '1985-05-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '081398476406', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(313, NULL, 'UDIN ', '0308', 'L', 'TANGERANG', '1980-05-18', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '085280423052', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(314, NULL, 'HENI ', '0309', 'P', 'TANGERANG', '1994-04-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. NAGREG RT. 16/03 KEL. CISOKA KEC. CISOKA ', 'TANGERANG', '083804611603', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(315, NULL, 'ENUNG ', '0310', 'P', 'TANGERANG', '1978-07-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. SIGEUNG CADAS RT. 04/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(316, NULL, 'ROMDONAH ', '0311', 'P', 'TANGERANG', '1997-01-19', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEC. BALARAJA ', 'TANGERANG', '089676745916', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(317, NULL, 'SUHAENI ', '0312', 'P', 'TANGERANG', '1975-12-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'LUKUN RT. 02/01 KEL. CISOKA KEC. CISOKA ', 'TANGERANG', '081289913920', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(318, NULL, 'EEN ', '0313', 'P', 'TANGERANG', '1981-05-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. LEUWIDAHU RT. 03/04 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(319, NULL, 'ROHANAH ', '0314', 'P', 'TANGERANG', '1968-09-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. LEUWIDAHU RT. 03/04 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(320, NULL, 'DASMI ', '0315', 'P', 'TANGERANG', '1959-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. LEWIDAHU RT. 03/04 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(321, NULL, 'LILIS ', '0316', 'P', 'TANGERANG', '1987-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. LEWI DAHU RT. 03/04 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(322, NULL, 'ROIDA ', '0317', 'P', 'TANGERANG', '1972-09-09', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'PESONA WIBAWA PREAJA B-B 3/15 RT. 05/06 KEL. JEUNGJING KEC. CISOKA ', 'TANGERANG', '085322019720', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(323, NULL, 'HENDRI ', '0318', 'L', 'CIAMIS', '1988-10-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '085211088676', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(324, NULL, 'ISMIATI ', '0319', 'P', 'TANGERANG', '1990-05-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '085211088676', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(325, NULL, 'NURHAYATI ', '0320', 'P', 'TANGERANG', '1987-08-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. SIGEUNG CADAS RT. 04/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(326, NULL, 'ADE LESTARI ', '0321', 'P', 'TANGERANG', '1991-08-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(327, NULL, 'SUPRIYATI ', '0322', 'P', 'TANGERANG', '1966-07-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(328, NULL, 'DIRAN ADI SAPUTRO', '0323', 'L', 'BOYOLALI', '1972-06-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081293548334', '2018-05-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(329, NULL, 'HELMIATI', '0324', 'P', 'BANJAR NEGERI', '1971-05-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085217361543', '2018-05-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(330, NULL, 'ERIKA SEPNI', '0325', 'P', 'PALEMBANG', '1991-10-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP.KADU SANGERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081932603534', '2018-05-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(331, NULL, 'MAEMUNAH', '0326', 'P', 'SERANG', '1973-03-19', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL..KADU JAYA KEC.CURUG', 'TANGERANG', '081297883201', '2018-05-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(332, NULL, 'MARYANI', '0327', 'P', 'TANGERANG', '1987-03-04', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.WARU RT.014/006 DS.PASIR JAYA KEC. CIKUPA', 'TANGERANG', '085215444514', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(333, NULL, 'HAMDAH', '0328', 'P', 'TANGERANG', '1974-12-04', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '08516502133', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(334, NULL, 'SITI AISYAH', '0329', 'P', 'TANGERANG', '1965-08-18', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(335, NULL, 'MUANIH', '0330', 'P', 'TANGERANG', '1975-03-04', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. WARU RT. 014/06 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(336, NULL, 'LILIS YULIATI', '0331', 'P', 'BULOK', '1981-07-15', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNUT RT. 03/02 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '082112365640', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(337, NULL, 'RATMALA', '0332', 'P', 'BOGOR', '1969-07-20', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNUT RT. 01/02 KEL. PASIR GADUNG KEC. CIKUPA', 'TANGERANG', '', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(338, NULL, 'NURAENI', '0333', 'P', 'TANGERANG', '1965-04-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RUMPAK SINANG RT. 01/01 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '', '2018-05-24', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(339, NULL, 'SUHAEMI', '0334', 'P', 'TANGERANG', '1987-04-15', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Pedagang', 'KP. KALIPATEN RT. 03/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '087778591997', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(340, NULL, 'HELMIATI BR.SITEPU', '0335', 'P', 'BAHOROK', '1975-06-07', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP.KALIPATEN RT.003/004 KEL.PAKULONAN BARAT KEC.KELAPA DUA', 'TANGERANG', '081214662209', '2018-05-24', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(341, NULL, 'RUHIYANAH', '0336', 'P', 'JAKARTA', '1966-12-26', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KALIPATEN RT. 03/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '085959908240', '2018-05-24', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(342, NULL, 'MUNIROH', '0337', 'P', 'TANGERANG', '1979-06-21', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KALIPATEN RT. 02/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '087774231250', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(343, NULL, 'HARIS FRIATNA', '0338', 'P', 'TANGERANG', '1969-08-14', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Karyawan Swasta', 'KP. PAKULONAN BARAT RT. 03/03 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(344, NULL, 'SRI RATNA DEWI', '0339', 'P', 'TANGERANG', '1989-11-28', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Pedagang', 'KP. KALIPATEN RT. 03/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '083894751145', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(345, NULL, 'MUSLIYAH', '0340', 'P', 'TANGERANG', '1988-07-17', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. KALIPATEN RT. 01/05 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '083895190863', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `tbl_anggota` (`id`, `nik`, `nama`, `identitas`, `jk`, `tmp_lahir`, `tgl_lahir`, `status`, `agama`, `departement`, `pendidikan`, `alamat_kantor_anggota`, `pekerjaan`, `alamat`, `kota`, `notelp`, `tgl_daftar`, `jabatan_id`, `aktif`, `pass_word`, `file_pic`, `data_penjamin`, `nama_penjamin`, `nik_penjamin`, `agama_penjamin`, `pendidikan_penjamin`, `pekerjaan_penjamin`, `no_telpon_penjamin`, `alamat_penjamin`, `alamat_kantor_penjamin`) VALUES
(346, NULL, 'DINA SUTRIYANA', '0341', 'P', 'TANGERANG', '1977-06-21', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. KALIPATEN RT.01/05 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '081287623321', '2018-05-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(347, NULL, 'IWAN HERMAWAN', '0342', 'L', 'TANGERANG', '1985-01-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.BANOGA RT.002/002 KEL.CARINGIN KEC.CISOKA', 'TANGERANG', '085313271807', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(348, NULL, 'RIFALDI GUSTI ', '0343', 'L', 'TANGERANG', '1997-08-23', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. SUKAMULYA RT. 03/02 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '082114801195', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(349, NULL, 'AMANAH ', '0344', 'P', 'TANGERANG', '1958-07-23', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. BANOGA RT. 02/02 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '082312168212', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(350, NULL, 'JUHEDI ', '0345', 'L', 'SERANG', '1983-06-09', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. BANOGA RT. 02/02 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '089536917154', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(351, NULL, 'TITIH SUPRIYATI ', '0346', 'P', 'JAKARTA', '1960-06-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. PAJAGAN RT. 04/04 KEL. CIKANDE KEC. JAYANTI ', 'TANGERANG', '081280421852', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(352, NULL, 'IBNU MALIK ', '0347', 'L', 'TANGERANG', '1988-01-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(353, NULL, 'UMAYAH ', '0348', 'P', 'TANGERANG', '1980-03-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 04/05 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085313518970', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(354, NULL, 'NARSIDI ', '0349', 'L', 'TANGERANG', '1969-07-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '038389582557', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(355, NULL, 'SITI MUNAWAROH ', '0350', 'P', 'TANGERANG', '1984-01-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(356, NULL, 'KOMARIAH ', '0351', 'P', 'TANGERANG', '1971-05-19', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085287388607', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(357, NULL, 'FITRI HANDAYANI', '0352', 'P', 'TANGERANG', '1991-01-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CARINGIN RT. 01/06 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(358, NULL, 'HERI BAEZURI ', '0353', 'L', 'TANGERANG', '1993-04-01', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. CIBUGEL MESJID RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '083808603454', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(359, NULL, 'SUNARIYAH ', '0355', 'P', 'TANGERANG', '1969-01-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL MESJID RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(360, NULL, 'JUMANAH ', '0356', 'P', 'TANGERANG', '1989-12-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. SIGEUNG RT. 04/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(361, NULL, 'RAMLA ', '0357', 'P', 'JAKARTA', '1973-08-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(362, NULL, 'ISMAWATI ', '0358', 'P', 'TANGERANG', '1976-12-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. SIGEUNG CADAS RT. 04/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(363, NULL, 'NINA PEBRIANA', '0359', 'P', 'TANGERANG', '1997-02-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. SECANG RT. 04/04 KEL. SUKATANI KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(364, NULL, 'ROLIASI ', '0360', 'P', 'TANGERANG', '1974-01-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. MAREME RT. 05/02 KEL. CISOKA KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(365, NULL, 'MARWINAH ', '0361', 'P', 'TANGERANG', '1974-03-18', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. SAGA RT. 03/03 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(366, NULL, 'ABSAH ', '0362', 'P', 'TANGERANG', '1982-04-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. SAGA RT. 02/03 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '085312025090', '2018-05-28', 2, 'N', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(367, NULL, 'KARTINI ', '0363', 'P', 'TANGERANG', '1964-03-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. SAGA RT. 02/03 KEL. CARINGIN KEC. CISOKA ', 'TANGERANG', '083877112547', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(368, NULL, 'EUMUT MAINAH ', '0364', 'P', 'TANGERANG', '1979-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL RT. 04/05 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085211755379', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(369, NULL, 'YANI ', '0365', 'P', 'TANGERANG', '1986-07-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. CIBUGEL ASEM RT. 01/06 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '082310508577', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(370, NULL, 'UJU ', '0366', 'P', 'TANGERANG', '1960-08-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL ASEM RT. 01/06 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '083898882142', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(371, NULL, 'SUHEMI ', '0367', 'P', 'TANGERANG', '1969-03-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. KORAL RT. 01/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(372, NULL, 'IYAN ', '0368', 'P', 'TANGERANG', '1965-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CISOKA RT. 03/01 KEL. CISOKA KEC. CISOKA ', 'TANGERANG', '085280370584', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(373, NULL, 'SANTI ', '0369', 'P', 'TANGERANG', '1988-06-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. KORAL RT. 04/01 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(374, NULL, 'NURHAPIPAH ', '0370', 'P', 'TANGERANG', '1992-04-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. KORAL RT. 01/04 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(375, NULL, 'YAYAH HULBIYAH ', '0371', 'P', 'TANGERANG', '1970-04-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 04/05 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '085280423332', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(376, NULL, 'ALDI AULADI ABDURROBIH ', '0372', 'L', 'TANGERANG', '1999-07-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. CIBUGEL RT. 04/05 KEL. CIBUGEL KEC. CISOKA ', 'TANGERANG', '089561342866', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(377, NULL, 'UMYATI ', '0373', 'P', 'TANGERANG', '1972-05-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR ', 'TANGERANG', '083877492240', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(378, NULL, 'AWANG', '0374', 'L', 'TANGERANG', '1977-07-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '085215311109', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(379, NULL, 'HAMDIAH', '0375', 'P', 'TANGERANG', '1970-04-22', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '083870375977', '2018-05-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(380, NULL, 'INTAN MARYATI', '0376', 'P', 'KARIPAN', '1978-11-30', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'TANGERANG', '085215520287', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(381, NULL, 'ASNAH', '0377', 'P', 'TANGERANG', '1957-08-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-05-27', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(382, NULL, 'MAEMUNAH', '0378', 'P', 'TANGERANG', '1982-09-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(383, NULL, 'LULI LESTARI', '0379', 'P', 'TANGERANG', '1997-02-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.CISOKA RT.003/001 KEL.CISOKA KEC.CISOKA', 'KAB.TANGERANG', '081211691704', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(384, NULL, 'SARTINAH', '0380', 'P', 'TANGERANG', '1984-09-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '082112643587', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(385, NULL, 'AWALUDIN', '0381', 'L', 'TANGERANG', '1978-08-25', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'TANGERANG', '085280423052', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(386, NULL, 'MAHMUDIN', '0382', 'L', 'TANGERANG', '1979-10-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SAGA RT.002/002 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(387, NULL, 'YANAH', '0383', 'P', 'TANGERANG', '1981-12-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP.KADU SANGERENG RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '082310690894', '2018-05-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(388, NULL, 'MUHAMAD YUSUF', '0384', 'L', 'TANGERANG', '1970-09-02', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. PAKULONAN BARAT RT. 05/03 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '081296636752', '2018-10-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(389, NULL, 'MURYANIH', '0385', 'P', 'TANGERANG', '1979-05-25', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. KALIPATEN RT. 05/04 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '085892923050', '2018-05-31', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(390, NULL, 'HERLINDA', '0386', 'P', 'TANGERANG', '1981-06-22', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KALIPATEN RT. 01/05 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '', '2018-10-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(391, NULL, 'RITA', '0387', 'P', 'TANGERANG', '1975-09-05', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KALIPATEN RT. 01/05 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '082310709003', '2018-10-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(392, NULL, 'ELIS', '0388', 'P', 'TANGERANG', '1966-08-06', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADEMANGAN RT. 01/05 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '085780309228', '2018-10-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(393, NULL, 'MULYATI', '0389', 'P', 'TANGERANG', '1980-03-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CIBUGEL RT.004/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085218154862', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(394, NULL, 'SITI MURTIAH', '0390', 'P', 'TANGERANG', '1977-09-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(395, NULL, 'SARMUNI', '0391', 'P', 'TANGERANG', '1975-02-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(396, NULL, 'SAMI', '0392', 'P', 'TANGERANG', '1965-07-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.006/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(397, NULL, 'EDIH', '0393', 'L', 'TANGERANG', '1975-12-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(398, NULL, 'LINA MARLIN', '0394', 'P', 'LEBAK', '1981-03-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(399, NULL, 'AMINNUDIN', '0395', 'L', 'LEBAK', '1979-07-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.BOJONG SAPI RT.006/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085210259960', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(400, NULL, 'ROHIMAH', '0396', 'P', 'TANGERANG', '1975-03-25', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(401, NULL, 'NAPIS', '0397', 'L', 'TANGERANG', '1972-05-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(402, NULL, 'ENENG KARTINI', '0398', 'P', 'TANGERANG', '1977-04-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(403, NULL, 'HARYATI', '0399', 'P', 'TANGERANG', '1968-09-19', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-06-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(404, NULL, 'ATIKAH', '0400', 'P', 'TANGERANG', '1981-09-09', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(405, NULL, 'DEWI', '0401', 'P', 'TANGERANG', '1978-02-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(406, NULL, 'MURDANAH', '0402', 'P', 'TANGERANG', '1981-06-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP.LEUWIDAHU RT.003/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(407, NULL, 'SARNAMAH', '0403', 'P', 'TANGERANG', '1965-09-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.002/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(408, NULL, 'SATNI', '0404', 'P', 'TANGERANG', '1977-01-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.003/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(409, NULL, 'ANING', '0405', 'P', 'TANGERANG', '1960-09-30', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.LEUWIDAHU RT.004/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '081299820701', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(410, NULL, 'OYOH', '0406', 'P', 'TANGERANG', '1977-05-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.005/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(411, NULL, 'RISMA NURUL RAHMAYANTI', '0407', 'P', 'TANGERANG', '1996-08-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, '', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(412, NULL, 'MULYATI', '0408', 'P', 'TANGERANG', '1980-08-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(413, NULL, 'SAMSUL KABERO', '0409', 'L', 'JAKARTA', '1986-11-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP.SAGA RT.003/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(414, NULL, 'MARYAM', '0410', 'P', 'TANGERANG', '1977-03-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.004/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(415, NULL, 'NURYATI', '0411', 'P', 'TANGERANG', '1986-05-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.LEUWIDAHU RT.003/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '085280728118', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(416, NULL, 'IKA', '0412', 'P', 'TANGERANG', '1996-08-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.005/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '081318398605', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(417, NULL, 'MARYATI', '0413', 'P', 'TANGERANG', '1978-11-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.SAGA RT.002/003 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(418, NULL, 'HOMSILAH', '0414', 'P', 'TANGERANG', '1990-07-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.LEUWIDAHU RT.003/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(419, NULL, 'AIDA', '0415', 'P', 'TANGERANG', '1981-04-09', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '085212309011', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(420, '3216549877894561', 'HALIMATUS SA\'DIAH', '0416', 'P', 'JAKARTA', '1989-09-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '083879457089', '2018-07-02', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(421, NULL, 'NURYATI', '0417', 'P', 'TANGERANG', '1981-06-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '085216423142', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(422, NULL, 'UDI MASHUDI', '0418', 'L', 'TANGERANG', '1979-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.CANGKUDU RT.008/004 KEL.CANGKUDU KEC.BALARAJA', 'KAB.TANGERANG', '085281919830', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(423, NULL, 'DEDE SUMANTRI', '0419', 'L', 'TANGERANG', '1994-01-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.001/006 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(424, NULL, 'ELIYANTI', '0420', 'P', 'TANGERANG', '1992-08-17', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.KOPAL RT.001/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(425, NULL, 'ENI.C', '0421', 'P', 'TANGERANG', '1973-03-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(426, NULL, 'KARTINI', '0422', 'P', 'TANGERANG', '1974-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(427, NULL, 'RAKMAH', '0423', 'P', 'TANGERANG', '1959-12-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KOLAR RT.001/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085210259960', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(428, NULL, 'KURNIASIH', '0424', 'P', 'TANGERANG', '1981-12-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '089532071473', '2018-06-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(429, NULL, 'IBROHIM', '0425', 'L', 'TANGERANG', '1979-02-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(430, NULL, 'MULYANAH', '0426', 'P', 'TANGERANG', '1981-07-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085210497186', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(431, NULL, 'RUDI PERMANA', '0427', 'L', 'TANGERANG', '1973-03-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(432, NULL, 'NOVA', '0428', 'P', 'TANGERANG', '1987-06-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '083872435224', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(433, NULL, 'M.TOPIK', '0429', 'L', 'TANGERANG', '1975-05-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASANGGRAHAN RT.005/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(434, NULL, 'MUHAMAD YUSUP', '0430', 'L', 'TANGERANG', '1994-05-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP.KADU SANGERENG RT.004/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '087885088376', '2018-06-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(435, NULL, 'SITI KHODIJAH', '0431', 'P', 'TANGERANG', '1993-12-07', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU SANGERENG RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089652141675', '2018-07-04', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(436, NULL, 'NURYANINGSICH', '0432', 'P', 'TANGERANG', '1990-05-21', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP. TAKO RT. 04/02 KEL. PASIR GADUNG KEC. CIKUPA', 'TANGERANG', '089534758646', '2018-07-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(437, NULL, 'HAIDZ', '0433', 'P', 'TANGERANG', '1969-10-10', 'Kawin', 'Islam', 'ANYELIR MAKMUR', NULL, NULL, 'Wiraswasta', 'KP. RUMPAK SINANG RT. 03/01 KEL. PAKULONAN BARAT KEC. KELAPA DUA', 'TANGERANG', '087877045232', '2018-07-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(438, NULL, 'SARIM MULYANA', '0434', 'L', 'TANGERANG', '1985-10-25', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.CISALAK RT.001/002 KEL.CIREUNDEU KEC.SOLEAR', 'KAB.TANGERANG', '081319342739', '2018-05-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(439, NULL, 'FITRIA', '0435', 'P', 'TANGERANG', '1987-07-18', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.SUKAMANTRI RT.002/009 KEL.GEMBONG KEC.BALARAJA', 'KAB.TANGERANG', '', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(440, NULL, 'SUDIR', '0436', 'L', 'TANGERANG', '1970-11-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.BOJONG SAPI RT.001/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(441, NULL, 'HASANUDIN', '0437', 'L', 'TANGERANG', '1988-08-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.CARING RT.002/001 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(442, NULL, 'MULKIYAH', '0438', 'P', 'TANGERANG', '1993-03-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Lainnya', 'KP.CARINGIN RT.002/001 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(443, NULL, 'MARNAH', '0439', 'P', 'TANGERANG', '1990-07-24', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.SIGEUNG RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '085213075038', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(444, NULL, 'SITI LARASATI', '0440', 'P', 'TANGERANG', '1998-12-28', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.CIBUGEL RT.003/005 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(445, NULL, 'MUHAMAD ANDI HERYANA', '0441', 'L', 'TANGERANG', '1992-06-30', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.BOJONG LOA RT.002/001 KEL.BOJONG LOA KEC.CISOKA', 'KAB.TANGERANG', '083870375206', '2018-07-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(446, NULL, 'ROHINAH', '0442', 'P', 'TANGERANG', '1969-07-28', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Wiraswasta', 'KP.BUNUT RT.005/002 DS.PASIR JAYA KEC.CIKUPA', 'TANGERANG', '', '2018-07-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(447, NULL, 'MARYANAH', '0443', 'P', 'JAKARTA', '1959-12-17', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Pedagang', 'KP.TAKO RT.004/002 DS.PASIR GADUNG KEC.CIKUPA', 'TANGERANG', '089534758646', '2018-07-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(448, NULL, 'MARITI', '0444', 'P', 'TEGAL', '1980-06-09', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA  2 BLOK A NO.15 RT.043/002 DS.PASIR JAYA KEC.CIKUPA', 'TANGERANG', '082123241190', '2018-07-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(449, NULL, 'SITI SUHERNA', '0446', 'P', 'TANGERANG', '1983-10-01', 'Kawin', 'Islam', 'TANGERANG', NULL, NULL, 'Lainnya', 'JL.AL MAKMUR KEBALEN NO.10 RT.004/003 KEL.PINANG KEC.PINANG', 'TANGERANG', '082249894030', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(450, NULL, 'LIA OKTIANA', '0447', 'P', 'TANGERANG', '1995-10-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.SECANG RT.004/006 KEL.CEMPAKA KEC.CISOKA', 'TANGERANG', '', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(451, NULL, 'NAWIYAH', '0448', 'P', 'TANGERANG', '1966-06-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.LEUWIDAHU RT.004/004 KEL.CARINGIN KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(452, NULL, 'ERAH', '0449', 'P', 'TANGERANG', '1952-07-24', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(453, NULL, 'AGUS DIAN', '0450', 'L', 'TANGERANG', '1974-08-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'PNS', 'KP.BOJONG SAPI RT.003/003 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '085211889726', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(454, NULL, 'MUHIDIN', '0451', 'L', 'TANGERANG', '1983-04-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Lainnya', 'KP.CIBUGEL RT.004/005 KEL.CIBUGEL KEC.CISOKA ', 'KAB.TANGERANG', '', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(455, NULL, 'IIN SUHAETI', '0452', 'P', 'TANGERANG', '1987-02-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA ', 'KAB.TANGERANG', '', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(456, NULL, 'ECIH', '0453', 'P', 'TANGERANG', '1974-04-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Penjahit', 'KP.KAPUDANG RT.004/005 KEL.SUKATANI KEC.CISOKA', 'KAB.TANGERANG', '085814134243', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(457, NULL, 'MUMUN', '0454', 'P', 'TANGERANG', '1968-08-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SECANG RT.003/006 KEL.CEMPAKA KEC.CISOKA', 'KAB.TANGERANG', '083890174244', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(458, NULL, 'ENGKOS', '0455', 'L', 'TANGERANG', '1968-08-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.KAPUDANG RT.004/005 KEL.SUKATANI KEC.CISOKA', 'KAB.TANGERANG', '085814134243', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(459, NULL, 'SINDI', '0456', 'P', 'TANGERANG', '2001-03-21', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Lainnya', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA ', 'KAB.TANGERANG', '', '2018-07-16', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(460, NULL, 'LESTARI KURNAENI HANDAYANI', '0457', 'P', 'PEMALANG', '1974-08-22', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA 1 BLOK D NO. 13 DS. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '', '2018-07-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(461, NULL, 'MAYA WARTINI', '0458', 'P', 'TANGERANG', '1987-12-08', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BUNUT RT.05/02 KEL. PASIR JAYA KEC. CIKUPA', 'TANGERANG', '', '2018-07-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(462, NULL, 'FX SARANA', '0459', 'L', 'KULON PROGO', '1972-01-24', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'BUKIT GADING CISOKA BLOK D2/29 KEL.SELAPAJANG KEC.CISOKA', 'KAB.TANGERANG', '082310496774', '2018-07-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(463, NULL, 'MUHAMAD RUIK', '0460', 'L', 'TANGERANG', '1999-07-22', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, '', 'KP.CIBUGEL RT.004/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '081318048841', '2018-07-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(464, NULL, 'ENDIN SAPRUDIN', '0461', 'L', 'TANGERANG', '1990-12-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SIGEUNG CADAS RT.004/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(465, NULL, 'ATIH', '0462', 'P', 'TANGERANG', '1979-08-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SIGEUNG CADAS RT.004/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(466, NULL, 'RUMI', '0463', 'P', 'TANGERANG', '1968-10-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'KAB.TANGERANG', '', '2018-07-25', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(467, NULL, 'NENENG', '0464', 'P', 'TANGERANG', '1963-05-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASANGGRAHAN RT.005/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(468, NULL, 'DHIDI SUMARDI', '0465', 'L', 'TANGERANG', '1969-05-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Lainnya', 'KP.PASANGGRAHAN RT.002/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(469, NULL, 'YUDIS TIRA', '0466', 'L', 'TANGERANG', '1990-01-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP.CISALAK RT.003/003 KEL.CIREUNDEU KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(470, NULL, 'ELYANAH', '0467', 'P', 'TANGERANG', '1993-10-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.PASANGGRAHAN RT.005/001 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(471, NULL, 'MAYA YUNINGSIH', '0468', 'P', 'TANGERANG', '1992-11-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.003/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '081281112728', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(472, NULL, 'TRIYONO', '0469', 'L', 'TANGERANG', '1982-03-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.CIBUGEL RT.003/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '08991716000', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(473, NULL, 'PIPIN SAPINAH', '0470', 'P', 'TANGERANG', '1977-08-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CIBUGEL RT.005/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(474, NULL, 'RUKIAH', '0471', 'P', 'TANGERANG', '1987-05-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.CIBUGEL RT.002/004 KEL.CIBUGEL KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(475, NULL, 'SUHERUM', '0472', 'P', 'TANGERANG', '1967-12-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'PERUM PESONA WIBAWA PRAJA BLOK C2/10 RT.007/006 KEL.JEUNGJING KEC.CISOKA', 'KAB.TANGERANG', '', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(476, NULL, 'EDI SUHERDI', '0473', 'L', 'TANGERANG', '1978-03-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP.MARGALUYU RT.001/001 KEL.SUKATANI KEC.CISOKA', 'KAB.TANGERANG', '081283937279', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(477, NULL, 'ENDI KURNIADI', '0474', 'L', 'TANGERANG', '1985-03-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.SIGEUNG RT.001/002 KEL.SOLEAR KEC.SOLEAR', 'KAB.TANGERANG', '083894419376', '2018-07-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(478, NULL, 'DEVI FAUZIAH', '0475', 'P', 'TANGERANG', '1992-04-16', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '087889996365', '2018-08-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(479, NULL, 'SITI SUMARNI', '0476', 'P', 'TANGERANG', '1983-03-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT 005/001 DESA SOLEAR KECAMATAN SOLEAR', 'KABUPATEN TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(480, NULL, 'HERMAN', '0478', 'L', 'TANGERANG', '1981-02-13', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. BOJONG SAPI RT 006/003 KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(481, NULL, 'AENAH', '0479', 'P', 'TANGERANG', '1959-12-13', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONGSAPI RT 003/003 KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(482, NULL, 'NURHALIMAH', '0480', 'P', 'TANGERANG', '1997-05-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONG SAPI KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(483, NULL, 'MISTA', '0481', 'L', 'TANGERANG', '1968-04-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. BOJONG SAPI RT 001/003 KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(484, NULL, 'SITI KOMALASARI', '0502', 'P', 'TANGERANG', '1989-04-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBULUH RT. 02/02 KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '085213848493', '2018-08-27', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(485, NULL, 'SITI ROSLIANI', '0482', 'P', 'TANGERANG', '1975-10-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONG SAPI RT 003/003 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(486, NULL, 'ROSITA', '0483', 'L', 'TANGERANG', '1979-07-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. PASANGGRAHAN RT 002/001 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(487, NULL, 'NENENG SUYATI', '0484', 'P', 'TANGERANG', '1977-01-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT 002/001 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(488, NULL, 'MARYATI', '0501', 'P', 'TANGERANG', '1993-04-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBULUH RT. 07/02 KEL. CIBUGEL KEC. CISOKA ', 'KABUPATEN TANGERANG', '', '2018-08-27', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(489, NULL, 'SITI KHAERANI', '0485', 'P', 'TANGERANG', '1993-09-15', 'Belum Kawin', 'Islam', 'PERORANGAN', NULL, NULL, 'Wiraswasta', 'MARGASARI RT 001/004 KEL. MARGASARI KEC. KARAWACI', 'TANGERANG', '085695860715', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(490, NULL, 'MARIAM', '0497', 'P', 'TANGERANG', '1977-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KABUPATEN TANGERANG', '083812883684', '2018-08-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(491, NULL, 'RENI ANGGRAENI', '0496', 'P', 'TANGERANG', '1977-08-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT. 02/03 KEL. SOLEAR KEC. SOLEAR', 'KABUPATEN TANGERANG', '', '2018-08-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(492, NULL, 'WESIH', '0495', 'P', 'TANGERANG', '1974-07-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. MALANG NENGAH RT. 02/3 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-08-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(493, NULL, 'UCU YUNINGSIH', '0486', 'P', 'TANGERANG', '1985-03-10', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. BOJONG SAPI RT 003/003 KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '', '2018-08-13', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(494, NULL, 'JUJU', '0487', 'P', 'TANGERANG', '1959-01-08', 'Cerai Mati', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONG SAPI RT 003/003 KEL. CIBUGEL KEC. CISOKA', 'KABUPATEN TANGERANG', '', '2018-08-13', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(495, NULL, 'EMBAY', '0488', 'P', 'TANGERANG', '1972-03-12', 'Cerai Mati', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT 001/006 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-08-13', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(496, NULL, 'INDAH SETIAWATI', '0489', 'P', 'MERAMBUNG', '1996-05-29', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT 002/001 KEL. PASANGGRAHAN KEC. SOLEAR', 'KABUPATEN TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(497, NULL, 'ASEP SOPIYAN', '0490', 'L', 'TANGERANG', '1991-07-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT 002/001 KEL. PASANGGRAHAN KEC. SOLEAR', 'KABUPATEN TANGERANG', '', '2018-09-13', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(498, NULL, 'SELLA SURIMAH', '0491', 'P', 'BANDUNG', '1982-03-29', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT 002/001 KEL. KADU KEC. CURUG', 'KABUPATEN TANGERANG', '081328003935', '2018-08-15', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(499, NULL, 'RISMA YUNITA', '0492', 'P', 'TANGERANG', '1989-02-18', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. KADU JAYA RT 002/002 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '', '2018-08-15', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(500, NULL, 'HENI BT. MARKASIM', '0493', 'P', 'TANGERANG ', '1978-07-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT 002/003 KEL. SOLEAR KEC. SOLEAR', 'KABUPATEN TANGERANG', '081213961007', '2018-08-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(501, NULL, 'MANSUR', '0494', 'L', 'TANGERANG', '1966-11-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. MALANG NENGAH RT 002/003 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '081317405583', '2018-08-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(582, NULL, 'EKA JAYA', '0498', 'P', 'TANGERANG', '1983-09-14', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU SANERENG RT.006/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '089533360129', '2018-08-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(583, NULL, 'RUSNAH', '0499', 'P', 'TANGERANG', '1971-06-10', 'Cerai Mati', 'Islam', 'BITUNG', NULL, NULL, 'Buruh', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '089536417764', '2018-08-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(584, NULL, 'DARTIM', '0503', 'L', 'INDRAMAYU', '1968-02-25', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Buruh', 'KP.KADU RT.001/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '', '2018-08-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(585, NULL, 'MAEMUNAH', '0504', 'P', 'TANGERANG', '1981-10-07', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU RT.005/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '081311860279', '2018-08-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(586, NULL, 'KHOLISAH', '0445', 'P', 'PENDEGLANG', '1977-06-02', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.MARGASARI RT.002/004 KEL.MARGASARI KEC.KARAWACI', 'TANGERANG', '081310829095', '1970-01-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(587, NULL, 'BUDI SETIAWAN', '0477', 'L', 'TANGERANG', '1978-05-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. PASANGGRAHAN RT 002/001 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-08-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(588, NULL, 'YAYAN HERYANTO', '0505', 'P', 'TANGERANG', '1977-02-16', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP.CILABAN RT.013/006 KEL.BOJONG LOA KEC.CISOKA', 'KAB.TANGERANG', '081294817005', '2018-09-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(589, NULL, 'susiah', '0506', 'P', 'SERANG', '1995-01-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '083877552399', '2018-09-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(590, NULL, 'KARTINI', '0507', 'P', 'TANGERANG', '1970-07-01', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU JAYA RT.002/002 KEL.KADU JAYA KEC.CURUG', 'TANGERANG', '081297883201', '2018-09-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(591, NULL, 'NUR DIANA', '0508', 'P', 'GRESIK', '1981-04-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP.KADU RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085311194538', '2018-09-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(592, NULL, 'SURYATI', '0509', 'P', 'TANGERANG', '1990-09-07', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Wiraswasta', 'KP.TANJUNG PASIR GADUNG RT.004/001 KEL.SUKA ASIH KEC.PASAR KEMIS', 'TANGERANG', '', '2018-09-13', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(593, NULL, 'YUYU YUHANAH', '500', 'P', 'TANGERANG', '1981-01-21', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Karyawan Swasta', 'VILA TANGERANG ELOK BLOK B-24 NO.12', 'TANGERANG', '-', '2018-08-23', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(594, NULL, 'JUISAH', '0512', 'P', 'JAKARTA', '1982-06-09', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU RT.003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085281975535', '2018-09-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(598, NULL, 'DWI MAYASARI', '0510', 'P', 'LAMPUNG', '1978-05-04', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'PERUM GRIYA PERMATA CISOKA BLOK C9/24 RT/RW 008/004 KEL. CIBUGEL KEC. CISOKA\r\n', 'KAB. TANGERANG', '08111225576', '2018-10-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(599, NULL, 'SOLEMAN', '0511', 'L', 'TANGERANG', '1959-03-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Lainnya', 'KP. CIBUGEL ASEM RT/RW 001/006 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-10-01', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(600, NULL, 'OMI ROSMI', '0513', 'P', 'TANGERANG', '1980-02-25', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.KADU RT003/001 KEL.BUNDER KEC.CIKUPA', 'TANGERANG', '085313544529', '2018-10-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(601, NULL, 'ASTIAH', '0514', 'P', 'TANGERANG', '1962-06-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CISALAK RT. 03/03 KEL. CIREUNDEU KEC. SOLEAR', 'KAB. TANGERANG', '085219697827', '2018-10-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(602, NULL, 'NINING SUNARYO', '0515', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '085219697827', '2018-10-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `tbl_anggota` (`id`, `nik`, `nama`, `identitas`, `jk`, `tmp_lahir`, `tgl_lahir`, `status`, `agama`, `departement`, `pendidikan`, `alamat_kantor_anggota`, `pekerjaan`, `alamat`, `kota`, `notelp`, `tgl_daftar`, `jabatan_id`, `aktif`, `pass_word`, `file_pic`, `data_penjamin`, `nama_penjamin`, `nik_penjamin`, `agama_penjamin`, `pendidikan_penjamin`, `pekerjaan_penjamin`, `no_telpon_penjamin`, `alamat_penjamin`, `alamat_kantor_penjamin`) VALUES
(603, NULL, 'EUIS NURLELAH', '0516', 'P', 'TANGERANG', '1977-05-05', 'Cerai Hidup', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. BOJONG SAPI RT. 03/03 KEL. CIBUGEL KEC. CISOKA ', 'KAB. TANGERANG', '', '2018-10-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(604, NULL, 'HAERIAH ADE ISTIQOMAH', '0517', 'P', 'TANGERANG', '1981-07-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADONGDONG RT. 04/03 KEL. PASIR NANGKA KEC. TIGARAKSA', 'KAB. TANGERANG', '', '2018-10-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(605, NULL, 'MARYATI', '0518', 'P', 'TANGERANG', '1994-03-25', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBULUH RT. 02/02 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-10-29', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(606, NULL, 'SRI WAHYUNI', '0519', 'P', 'WAY JEPARA', '1986-06-25', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '081388367758', '2018-10-31', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(607, NULL, 'WIWIK KARTIKA SARI', '0520', 'P', 'JAKARTA', '1971-08-29', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '0895351896965', '2018-10-31', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(608, NULL, 'UMAR', '522', 'L', 'TANGERANG', '1980-03-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. LEUWIDAHU RT. 04/04 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '085212340024', '2018-11-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(609, NULL, 'SUMANTRI', '521', 'L', 'TANGERANG', '1970-01-01', 'Cerai Mati', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '085211169703', '2018-11-05', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(610, NULL, 'LINDAWATI', '525', 'P', 'ROWO REJO', '1986-12-08', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA II RT. 43/02', 'KAB. TANGERANG', '085281311978', '2018-11-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(611, NULL, 'MELIAWATI', '523', 'P', 'TANGERANG', '1991-05-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 05/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '087855546056', '2018-11-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(612, NULL, 'ASEP FEBRI SOLIHIN', '524', 'L', 'TANGERANG', '1986-02-08', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '081220056599', '2018-11-08', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(613, NULL, 'SUMARNI', '0080', 'P', 'TANGERANG', '1993-06-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. CISEREH RT/RW 002/001 KELURAHAN CISEREH KEC.TIGARAKSA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(614, NULL, 'SUMA', '0116', 'L', 'TANGERANG', '1966-07-01', 'Cerai Mati', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. CISEREH RT/RW 002/001 KEL. CISEREH KEC. TIGARAKSA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(615, NULL, '0123', 'MULYANI', 'P', 'TANGERANG', '1984-04-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BARENGKOK RT/RW 003/003 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(616, NULL, 'NENGSIH', '0139', 'P', 'TANGERANG', '1979-06-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SUKAMANAH RT/RW 004/003 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(617, NULL, 'RONAH', '0354', 'P', 'TANGERANG', '1974-07-02', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT/RW 002/003 KEL. SOLEAR SEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(618, NULL, 'DEWI PUSPITASARI', '526', 'P', 'TANGERANG', '1985-12-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. BARENGKOK RT. 03/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(619, NULL, 'YANI', '527', 'P', 'TANGERANG', '1972-04-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. BARENGKOK RT. 03/03 KEL. SOLER KEC. SOLEAR', 'KAB. TANGERANG', '081317552846', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(620, NULL, 'SUKESIH', '528', 'P', 'TANGERANG', '1988-07-18', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. BARENGKOK RT. 03/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(621, NULL, 'BARIYAH', '529', 'P', 'TANGERANG', '1973-11-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. BARENGKOK RT. 03/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(622, NULL, '530', 'SUHAYATI', 'P', 'TANGERANG', '2018-11-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT. 02/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(623, NULL, 'SUHARSIH', '531', 'P', 'TANGERANG', '2018-11-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT. 02/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '083813786626', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(624, NULL, 'SUHARTI', '532', 'P', 'TANGERANG', '1977-01-30', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT. 02/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(625, NULL, 'PANI ALAMSAH', '533', 'P', 'TANGERANG', '1999-05-26', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Lainnya', 'KP. MALANG NENGAH RT. 02/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '083887498381', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(626, NULL, 'ETRIK HUMAEROH', '534', 'P', 'TANGERANG', '1995-02-28', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALANG NENGAH RT. 02/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '083891332066', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(627, NULL, 'OSIH', '535', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BARENGKOK RT. 03/03 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(628, NULL, 'ROHAYA', '536', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(629, NULL, 'SITI SAODAH', '537', 'P', 'TANGERANG', '1974-02-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(630, NULL, 'NURFAIZI', '538', 'L', 'TANGERANG', '1985-08-05', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. GEMBONG RT. 01/02 KEL. GEMBONG KEC. BALARAJA', 'KAB. TANGERANG', '081383469033', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(631, NULL, 'FITRIA STEPANI RONALDO', '539', 'P', 'TANGERANG', '1989-05-06', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Karyawan Swasta', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '082141054744', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(632, NULL, 'SURATNI', '540', 'P', 'TANGERANG', '1970-01-01', 'Cerai Mati', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP, CANGKUDU RT. 02/03 KEL. CANGKUDU KEL. BALARAJA', 'KAB. TANGERANG', '085218207491', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(633, NULL, 'SUHENSIH', '541', 'P', 'TANGERANG', '1975-05-15', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085312024641', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(634, NULL, 'SITI AMALLIANI', '542', 'P', 'TANGERANG', '1997-06-15', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. CANGKUDU', 'KAB. TANGERANG', '0895629369838', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(635, NULL, 'SUHETI', '543', 'P', 'TANGERANG', '1974-09-12', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(636, NULL, 'HALIMAH', '544', 'P', 'TANGERANG', '1975-10-10', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '081317555743', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(637, NULL, 'SITI MASITOH', '545', 'P', 'TANGERANG', '1977-06-22', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '081280765054', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(638, NULL, 'NANI MARDIAH', '546', 'P', 'TANGERANG', '1974-07-10', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(639, NULL, 'ALPIAH', '547', 'P', 'TANGERANG', '1978-08-14', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '081385586949', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(640, NULL, 'YUSNANI', '548', 'P', 'TANGERANG', '1976-07-06', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(641, NULL, 'RITA SUSILAWATI', '549', 'P', 'TANGERANG', '1979-04-10', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '087871773590', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(642, NULL, 'SADIAH', '550', 'P', 'TANGERANG', '2018-11-15', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MALAKA RT. 09/01 KEL. SITUTERATE KEC. CIKANDE', 'KAB. TANGERANG', '081316225661', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(643, NULL, 'SITI NURHAYATI', '551', 'P', 'TANGERANG', '1984-03-30', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(644, NULL, 'NUNUNG MULYANI', '552', 'P', 'TANGERANG', '1973-01-01', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085210934336', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(645, NULL, 'KARTINI', '553', 'P', 'TANGERANG', '1984-06-04', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(646, NULL, 'SITI RATNA SUMINAR', '554', 'P', 'TANGERANG', '1984-05-28', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2018-11-12', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(647, NULL, 'SUHAEMAH', '0555', 'P', 'TANGERANG', '1966-10-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Pedagang', 'KP. KADU BUNDER RT.003/001 KEL. BUNDER KEC. CIKUPA\r\n', 'TANGERANG', '089602186378', '2018-11-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(648, NULL, 'ARYUNAWATI', '0556', 'P', 'LEBAK', '1993-03-03', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP. SERDANG RT. 003/003 KEL. SERDANG WETAN KEC. LEGOK ', 'TANGERANG', '08380876174', '2018-11-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(653, NULL, 'ANITA ', '0557', 'P', 'JAWA BART', '1987-03-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Wiraswasta', 'KP. BOJONG SARUNG RT. 009/001 KEL. LEBAK GEDONG KEC. LEBAK GEDONG ', 'TANGERANG', '083894587749', '2018-11-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(654, NULL, 'MAESAROH', '558', 'P', 'TANGERANG', '1979-11-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '085215520287', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(655, NULL, 'H. ACHMAD', '559', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pensiunan', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '085280376841', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(656, NULL, 'SANIP', '560', 'L', 'TANGERANG', '1972-04-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. CIBULUH RT. 02/02 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(657, NULL, 'YULIANTI', '561', 'P', 'TANGERANG', '1984-03-08', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 012/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '085212528104', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(658, NULL, 'TUTI ALAWIYAH', '562', 'P', 'TANGERANG', '1983-11-23', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085289678629', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(659, NULL, 'SUKAMAH', '563', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085280183493', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(660, NULL, 'MARDIANAH', '564', 'P', 'TANGERANG', '1984-01-15', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEL. BALARAJA', 'KAB. TANGERANG', '085212529918', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(661, NULL, 'KOMARIAH', '565', 'P', 'TANGERANG', '1970-01-01', 'Cerai Hidup', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEL. BALARAJA', 'KAB. TANGERANG', '085211148044', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(662, NULL, 'YUNENGSIH', '566', 'P', 'TANGERANG', '1983-04-04', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. CANGKUDU', 'KAB. TANGERANG', '081283877680', '2018-11-19', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(663, NULL, 'TRYA OCTAVIANI', '567', 'P', 'TANGERANG', '1992-10-10', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIREWED RT. 04/01 KEL. SUKADAMAI KEC. CIKUPA', 'KAB. TANGERANG', '081996427168', '2018-11-21', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(664, NULL, 'ASIAH', '568', 'P', 'TANGERANG', '1978-02-07', 'Cerai Hidup', 'Islam', 'GEBANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. GEBANG RT. 02/05 KEL. SUKADAMAI KEC. CIKUPA', 'KAB. TANGERANG', '', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(665, NULL, 'EEN', '569', 'P', 'TANGERANG', '1986-04-17', 'Kawin', 'Islam', 'GEBANG', NULL, NULL, 'Karyawan Swasta', 'KP. GEBANG RT. 02/05 KEL. SUKADAMAI KEC. CIKUPA', 'KAB. TANGERANG', '', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(666, NULL, 'MULHAT MUPLIHA', '570', 'P', 'TANGERANG', '1993-11-07', 'Kawin', 'Islam', 'GEBANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASIR GADUNG RT. 01/04 KEL. PASIR GADUNG KEC. CIKUPA', 'KAB. TANGERANG', '083806282884', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(667, NULL, 'ELIS', '571', 'P', 'TANGERANG', '1990-08-10', 'Kawin', 'Islam', 'GEBANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.GEBANG RT.002/005 KEL.SUKADAMI KEC. CIKUPA', 'KAB. TANGERANG', '081314864835', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(668, NULL, 'MARIYAM', '572', 'P', 'TANGERANG', '1982-11-17', 'Kawin', 'Islam', 'GEBANG', NULL, NULL, 'Karyawan Swasta', 'KP.GEBANG RT.002/005 KEL.SUKADAMAI KEC.CIKUPA', 'KAB.TANGERANG', '081282741049', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(669, NULL, 'ASMINAH', '574', 'P', 'TANGERANG', '1974-10-08', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '083813202877', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(670, NULL, 'DEVI YULIANTI', '573', 'P', 'TANGERANG', '1994-07-07', 'Kawin', 'Islam', 'GEBANG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.GEBANG RT.004/005 KEL.SUKADAMI KEC.CIKUPA', 'KAB.TANGERANG', '089682218436', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(672, NULL, 'YUNIYADAH', '575', 'P', 'TANGERANG', '1979-03-26', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '083813892855', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(673, NULL, 'ASAIDAH', '576', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '087782882923', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(674, NULL, 'BAYDILAH', '577', 'P', 'TANGERANG', '2018-11-22', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CISEREH RT. 05/06 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(675, NULL, 'SOBARI', '582', 'L', 'TANGERANG', '1968-05-29', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Wiraswasta', 'KP. BITUNG RT.002/005 KEL.KADU JAYA KEC.CURUG', 'KAB.TANGERANG', '081311321459', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(676, NULL, 'ANIH', '583', 'P', 'TANGERANG', '1980-11-04', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT.001/005 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082122235260', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(677, NULL, 'NENENG HERAWATI', '584', 'P', 'TANGERANG', '1984-04-15', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP.BITUNG RT.003/005 KEL.KADU JAYA KEC. CURUG', 'KAB.TANGERANG', '', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(678, NULL, 'ROSMANIAH', '585', 'P', 'TANGERANG', '2018-11-22', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT.002/005 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '081399747763', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(679, NULL, 'ARNIH', '586', 'P', 'TANGERANG', '1976-04-09', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT.001/005 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082114013803', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(680, NULL, 'SITI NURYANI', '578', 'P', 'TANGERANG', '1996-08-26', 'Belum Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Lainnya', 'KP. ONYAM RT. 13/03 KEL. KADU KEC. CURUG', 'KAB. TANGERANG', '08567174448', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(681, NULL, 'ENIH', '587', 'P', 'TANGERANG', '1962-08-12', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT.002/005 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082297914788', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(682, NULL, 'SURIAH', '579 ', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 03/04 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '085946581453', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(683, NULL, 'SURYANI', '581', 'P', 'TANGERANG', '1982-01-26', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT.003/004 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082297914788', '2018-11-22', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(684, NULL, 'ANI SUMIYATI', '580', 'P', 'TANGERANG', '1981-12-09', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '087771802540', '2018-11-22', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(685, NULL, 'INDAH MAWARNI', '588', 'P', 'TANGERANG', '2000-07-23', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. ANGSANA RT. 02/01 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-11-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(686, NULL, 'UWEN', '589', 'P', 'TANGERANG', '1974-05-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. ANGSANA RT. 02/01 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-11-26', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(687, NULL, 'NURHAYA', '590', 'P', 'TANGERANG', '2018-11-28', 'Belum Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Lainnya', 'KP. KADU RT. 06/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '081717866847', '2018-11-28', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(688, NULL, 'SUGIAT', '591', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. BOJONG SAPI RT. 01/03 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(689, NULL, 'H. SAMIN', '592', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. CIBUGEL RT. 05/05 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '085213413609', '2018-12-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(690, NULL, 'ENDI BIN ASDA', '593', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. CIBUGEL RT. 03/05 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-03', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(691, NULL, 'MISTU', '596', 'L', 'TANGERANG', '1978-05-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. KORAL RT. 01/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(692, NULL, 'JUHENAH', '597', 'P', 'TANGERANG', '1985-12-14', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KORAL RT. 01/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(693, NULL, 'SUGENG SUPRIYADI', '598', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'TAMAN ADIYASA BLOK G. 07/31 RT. 04/08 KEL. CIKASUNGKA KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(694, NULL, 'EVA KUSTIANSARI', '599', 'P', 'TANGERANG', '1973-10-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN ADIYASA BLOK G. 07/31 RT. 04/08 KEL. CIKASUNGKA KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(695, NULL, 'INA ANASTACIA', '594', 'P', 'CIREBON', '1970-04-22', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PASIR JAYA BLK. A1/15 RT. 05/02 KEL. PASIR JAYA KEC. CIKUPA', 'KAB. TANGERANG', '081238447175', '2018-12-06', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(696, NULL, 'MUKARROMAH', '595', 'P', 'PUTIHDOH', '1993-11-20', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. TALAGA RT. 03/02 KEL. TALAGA KEC. CIKUPA', 'KAB. TANGERANG', '', '2018-12-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(697, NULL, 'KANI', '601', 'L', 'TANGERANG', '1978-04-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. CIBUGEL ASEM RT. 05/05 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(698, NULL, 'JULAEHA', '600', 'P', 'TANGERANG', '1980-01-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL ASEM RT. 05/05 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(699, NULL, 'SAHRUDIN AJI', '602', 'L', 'TANGERANG', '1971-09-18', 'Cerai Hidup', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. SIGEUNG RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '085211360525', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(700, NULL, 'DEDE MULYANAH', '603', 'P', 'TANGERANG', '1974-05-14', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Karyawan Swasta', 'CISOKA INDAH REGENSI BLOK B12 NO. 09 RT. 03/07 KEL. SUKATANI KEC. CISOKA', 'KAB. TANGERANG', '081382870351', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(701, NULL, 'ICE TRISNAWATI', '604', 'P', 'TANGERANG', '1984-10-05', 'Belum Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Karyawan Swasta', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '081212918469', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(702, NULL, 'SULASTRI', '605', 'P', 'TANGERANG', '2001-07-27', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. POS SENTUL RT. 08/04 KEL. SENTUL JAYA KEC. BALARAJA', 'KAB. TANGERANG', '088213700259', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(703, NULL, 'ELISNIA EKANANDA PUTRI', '606', 'P', 'TANGERANG', '1991-06-17', 'Belum Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-10', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(704, NULL, 'ETI SUMIATI', '608', 'P', 'TASIKMALAYA', '1978-08-08', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONG SAPI RT. 03/03 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(705, NULL, 'SUPRI', '609', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. CIBUGEL RT. 07/07 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(706, NULL, 'MINAH', '610', 'P', 'TANGERANG', '1978-04-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '085213045026', '2018-12-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(707, NULL, 'H. KASIRAN', '611', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(708, NULL, 'BAHRUDIN', '612', 'L', 'TANGERANG', '1984-12-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. BOJONG SAPI RT. 03/03 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(709, NULL, 'RATNA JUWITA', '613', 'P', 'TANGERANG', '1992-06-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-17', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(710, NULL, 'LISTIA', '614', 'P', 'TANGERANG', '1996-01-19', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '085602464342', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(711, NULL, 'SRI MARYANAH', '615', 'P', 'JAKARTA', '1980-09-23', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 03/04 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '087871788093', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(712, NULL, 'KARTILA', '616', 'P', 'GEDUNG WANI', '1972-08-01', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 02/05 KEL. KADU JAYAKEC. CURUG', 'KAB. TANGERANG', '085214960049', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(713, NULL, 'ROHAYATI', '617', 'P', 'TANGERANG', '1980-04-03', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 02/04 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '081384773970', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(714, NULL, 'ENAH SUHAENAH', '618', 'P', 'TANGERANG', '1974-10-07', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '08569955091', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(715, NULL, 'NURHAYATI', '619', 'P', 'JAKARTA', '1970-01-01', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '081297883779', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(716, NULL, 'SARTINI', '620', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITING RT. 03/04 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082113358393', '2018-12-20', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(717, NULL, 'DAHLIA', '621', 'P', 'TANGERANG', '1986-06-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBUGEL MESJID RT. 03/04 KEL. CIBUGEL KEC. CISOKA ', 'KAB. TANGERANG', '', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(718, NULL, 'SITI PAOJAH', '622', 'P', 'TANGERANG', '1978-06-03', 'Cerai Mati', 'Islam', 'CIBUGEL', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(719, NULL, 'NURJANAH', '623', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '083872244903', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(720, NULL, 'MARINI SUGIANTI', '624', 'P', 'TANGERANG', '1986-03-08', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '085287105466', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(721, NULL, 'HAERUDIN', '625', 'L', 'TANGERANG', '1991-09-19', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. CIKASUNGKA RT. 15/04 KEL. CISANGKA KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(722, NULL, 'MEGAWATI', '626', 'P', 'TANGERANG', '2018-12-24', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIKASUNGKA RT. 015/04 KEL. CIKASUNGKA KEC. SOLEAR', 'KAB. TANGERANG', '', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(723, NULL, 'AHMAD MULYADI', '627', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. BOJONG SAPI RT. 03/03 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-24', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(724, NULL, 'LALA NURMALASARI', '628', 'P', 'KUNINGAN', '2018-12-27', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. PASIR GINTUNG RT. 03/01 KEL. SUKAJAYA KEC. KERONCONG', 'KAB. TANGERANG', '02110578774', '2018-12-27', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(725, NULL, 'SANI', '629', 'P', 'PANDEGLANG', '1974-11-04', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CISEREH RT. 01/06 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '081355521453', '2018-12-27', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(726, NULL, 'ENCUM', '630', 'P', 'TANGERANG', '1977-02-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BOJONG SAPI RT. 03/03 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-31', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(727, NULL, 'NURSALIM', '631', 'L', 'TANGERANG', '1993-01-06', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. SECANG RT. 04/06 KEL. CEMPAKA  KEC. CISOKA', 'KAB. TANGERANG', '', '2018-12-31', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(728, NULL, 'SRI WAHYUNI', '632', 'P', 'TANGERANG', '1984-07-09', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '0812864367496', '2019-01-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(729, NULL, 'M. RIDWAN A', '633', 'L', 'TANGERANG', '1993-09-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. KADU RT. 05/01 KEL. BITUNG JAYA  KEC. CIKUPA', 'KAB. TANGERANG', '081382984584', '2019-01-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(730, NULL, 'DEDE', '634', 'P', 'TANGERANG', '1984-04-06', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 01/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '083874155102', '2018-01-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(731, NULL, 'NURYATI', '635', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Karyawan Swasta', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '083815736095', '2019-01-02', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(732, NULL, 'UMAH', '636', 'P', 'TANGERANG', '1985-09-10', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SAGA RT. 03/04 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '', '2018-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(733, NULL, 'RUMSANI', '637', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '089696111517', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(734, NULL, 'ASTI', '638', 'P', 'TANGERANG', '1985-08-07', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085319106530', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(735, NULL, 'AYU SEPTIANI', '639', 'P', 'TANGERANG', '1984-04-12', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Karyawan Swasta', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '081299220478', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(736, NULL, 'MARSANAH', '640', 'P', 'TANGERANG', '1977-05-04', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. NAGRAK RT. 08/01 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '085332707406', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(737, NULL, 'NINH', '641', 'P', 'TANGERANG', '1980-07-15', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MANGGU RT. 014/04  KEL. CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(738, NULL, 'SITI ANITA', '642', 'P', 'TANGERANG', '1994-09-20', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. MANGGU RT. 03/06 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-01-07', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(739, NULL, 'SADIAH', '643', 'P', 'TANGERANG', '1976-07-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MANGGU RT. 014/04 KEL. CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(740, NULL, 'MADROPI', '644', 'L', 'TANGERANG', '1978-02-17', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. MANGGU RT. 014/04 KEL. CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(741, NULL, 'ASNAWATI', '645', 'P', 'TANGERANG', '1995-06-12', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Pedagang', 'KP. CIBIRIT RT. 03/01 KEL. SUKATANI KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-07', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(742, NULL, 'BASUKI', '646', 'L', 'PEKALONGAN', '1971-01-04', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. KADU RT. 06/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '085770143747', '2019-01-09', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(743, NULL, 'JUPRAN', '647', 'L', 'PANDEGLANG', '1991-07-26', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. SUKACAI LEBAK RT. 01/01 KEL. SUKACAI KEC. JIPUT ', 'PANDEGLANG', '085921384814', '2019-01-09', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(744, NULL, 'M. SARIP H', '648', 'L', 'TANGERANG', '1992-07-17', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Buruh', 'KP. BITUNG RT. 03/01 KEL. BITUNG JAYA KEC. CIKUPA', 'KAB. TANGERANG', '081288714770', '2019-01-09', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(745, NULL, 'NURSIAH', '656', 'P', 'BUMI TINGGI', '1987-08-24', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'GRIYA PERMATA CISOKA BLOK C 11/10 RT. 08/04 KEL. CIBUGEL KEC. CISOKA ', 'KAB. TANGERANG', '', '2019-01-11', 2, 'Y', '224bec3dd08832bc6a69873f15a50df406045f40', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(746, NULL, 'ENENG', '657', 'P', 'TANGERANG', '1982-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. SAGA RT. 03/03 KEL. CARINGIN KEC. CISOKA', 'KIAB. TANGERANG', '', '2019-01-14', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(747, '3603050906700002', 'ANDA SUANDA', '658', 'L', 'TANGERANG', '1970-06-09', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. BOJONG SAPI RT. 03/03 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-14', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'istri', 'ENCUM', '3603056701770004', 'Islam', 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(748, '3603052704810001', 'MANSUR', '659', 'L', 'TANGERANG', '1981-04-27', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. CIBUGEL RT. 04/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-14', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'istri', 'DESI WULANDARI', '3603054612870002', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(749, '3603054612870002', 'DESI WULANDARI', '660', 'P', 'TANGERANG', '1987-12-06', '', '', '', NULL, NULL, 'Karyawan Swasta', 'KP. CIBUGEL RT. 04/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2019-01-14', 2, 'Y', '', '', 'suami', 'MANSUR', '3603052704810001', NULL, NULL, 'Buruh', NULL, NULL, NULL),
(750, '3603056107720004', 'KUSNIAWATI', '661', 'P', 'TANGERANG', '1972-07-21', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIGOONG RT. 09/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '085280412351', '2019-01-14', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'anak', 'DADANG HARYANTO', '3603051807900003', 'Islam', 'SMP', 'Buruh', NULL, NULL, NULL),
(751, '3603036810790005', 'EHA SUHILA ', '662', 'P', 'PANDEGLANG', '1979-10-28', 'Kawin', 'Islam', 'PASIR JAYA', NULL, NULL, 'Karyawan Swasta', 'PERUM MUSTIKA TGR BLOK C.31/34 RT. 04/07 KEL. PASIR NANGKA KEC. TIGARAKSA', 'KAB. TANGERANG', '082299059136', '2019-01-17', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'IWAN', '3603030206800004', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(752, '3671024107930016', 'YOYON PARIDA', '663', 'P', 'TANGERANG', '1992-10-13', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. DUMPIT RT. 03/05 KEL. GANDASARI KEC. JATIUWUNG', 'KAB. TANGERANG', '0835322696154', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'EDI', '3603180110900005', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(753, '3603184410780005', 'MUZRIKA SAFITRI', '664', 'P', 'PANDEGLANG', '1978-10-04', '', '', '', NULL, NULL, 'Wiraswasta', 'KP. KADU RT. 06/01 KEL. BUNDER KEC. CIKUPA ', 'KAB. TANGERANG', '', '2019-01-17', 2, 'Y', '', '', NULL, 'ADE SUGIANTO', '3603181306770001', NULL, 'SMP', '', NULL, NULL, NULL),
(754, '3604111307870005', 'ARI WIBOWO', '665', 'L', 'KARANG ANYAR', '1987-07-13', 'Belum Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'PERUM PURI PRATAMA CISAIT BLOK A2C NO. 1 RT. 05/05 KEL.CISAIT KEC.  KRAGILAN', 'SERANG', '082118112664', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'MUNANDIR ', '3604111705780001', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(755, '3603184110700005', 'YENI SRI HARTATI', '666', 'P', 'TANJUNG KARANG', '1970-10-01', 'Kawin', '', '', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '085694544825', '2019-01-17', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'anak', 'MUNAZIR RIAN SAPRIAN', '3603182304960004', NULL, NULL, 'Lainnya', NULL, NULL, NULL),
(756, '3603175806840013', 'USMAYANAH', '667', 'P', 'TANGERANG', '1984-06-18', '', '', 'KADU JAYA', NULL, NULL, '', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082211963566', '2019-01-17', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'ISMAIL FAHMI', '3603172609770007', NULL, NULL, 'Wiraswasta', NULL, NULL, NULL),
(757, '3603174108750007', 'AAT ATNAWATI', '668', 'P', 'TANGERANG', '1975-08-01', 'Kawin', '', 'KADU JAYA', NULL, NULL, '', 'KP. CISEREH HAUR RT. 04/03 KEL. CUKANGGALIH KEC. CURUG', 'KAB. TANGERANG', '', '2019-01-17', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, 'TRISILO DWIIANTO', '3603170107750010', NULL, NULL, 'Buruh', NULL, NULL, NULL),
(758, '3603174210660001', 'MULYATI', '669', 'P', 'TANGERANG', '1970-01-01', 'Kawin', '', '', NULL, NULL, 'Wiraswasta', 'KP. BITUNG RT. 03/04 KEL. KADU JAYA KEC. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '', '2019-01-17', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, 'DUL SALAM', '3603173006620001', NULL, 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(759, '3603176206790007', 'SARINAH', '670', 'P', 'MAJALENGKA', '1979-06-22', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '087889919212', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'JUMADI', '3603171310740002', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(760, '3603174711690006', 'AISAH', '671', 'P', 'JAKARTA', '1970-01-01', 'Cerai Mati', '', 'BITUNG', NULL, NULL, '', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '085921399682', '2019-01-30', 2, 'Y', '', '', 'anak', 'FAUZIAH', '3603177010000006', 'Islam', 'D1', 'Lainnya', NULL, NULL, NULL),
(761, '3603174804790003', 'SRI NURHAYATI', '672', 'P', 'TANGERANG', '1979-04-08', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'PERUM GRAHA INDAH CURUG BLOK O NO. 5 RT. 01/010 KEL. CURUG KULON KEC. CURUG', 'KAB. TANGERANG', '081281808690', '1979-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'HERMAN', '3603171010770020', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(762, '3671021202860004', 'ZAINAL AFANDI', '673', 'L', 'KEMU', '1986-02-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. CIKONENG ILIR RT. 03/06 KEL. JATAKE KEC, JATIUWUNG', 'KAB. TANGERANG', '08176703632', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'istri', 'EVI SETIYAWATI', '1804225107880001', 'Islam', 'SMP', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(763, '3603186506010008', 'DINI SETIAWATI', '674', 'P', 'TANGERANG', '2001-06-25', 'Belum Kawin', '', '', NULL, NULL, 'Lainnya', 'KP. LEDUG RT. 02/01 KEL. ALAM JAYA KEC. JATIUWUNG', 'KOTA TANGERANG', '083871754508', '2019-01-30', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(764, '3603180403670001', 'KHUMAEDI', '676', 'L', 'PATI', '1970-01-01', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Karyawan Swasta', 'KP. KADU RT. 05/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '085288323519', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'istri', 'SUGIANTI', '3603184107750008', 'Islam', 'SMA/Sederajat', 'Wiraswasta', NULL, NULL, NULL),
(765, '3603182808970003', 'RENDI SUSTIANA', '677', 'L', 'TANGERANG', '1997-08-28', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Lainnya', 'KP. KADU RT. 06/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '0895411871792', '2019-01-30', 2, 'Y', '', '', NULL, 'SUSI', '36031856005790017', NULL, 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(766, '3603185105850004', 'MURTAPIAH', '679', 'P', 'TANGERANG', '1985-05-11', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. DUKUH RT. 015/04 KEL. DUKUH KEC. CIKUPA', 'KAB. TANGERANG', '', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'KOMARUDIN', '3603180308820007', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(767, '3603185707840001', 'FARAH VIBRIYANTI', '680', 'P', 'INDRAMAYU', '1984-07-17', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '085890597713', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'OMAN', '3603182212780015', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(768, '3603035207800001', 'SUSILAWATI', '681', 'P', 'TANGERANG', '1980-07-12', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADUAGUNG RT. 01/02 KEL. MARGASARI KEC, TIGARAKSA', 'KAB. TANGERANG', '081315338408', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'MAMAN SUMANTRI', '3603033107850001', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(769, '3603033107850001', 'MAMAN SUMANTRI', '0682', 'L', 'TANGERANG', '1985-07-31', 'Kawin', '', '', NULL, NULL, 'Karyawan Swasta', 'KP. KADUAGUNG RT. 01/02 KEOL. MARGASARI KEC. TIGARAKSA ', 'KAB. TANGERANG', '089601649217', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'istri', 'SUSILAWATI', NULL, NULL, 'SMA/Sederajat', 'Wiraswasta', NULL, NULL, NULL);
INSERT INTO `tbl_anggota` (`id`, `nik`, `nama`, `identitas`, `jk`, `tmp_lahir`, `tgl_lahir`, `status`, `agama`, `departement`, `pendidikan`, `alamat_kantor_anggota`, `pekerjaan`, `alamat`, `kota`, `notelp`, `tgl_daftar`, `jabatan_id`, `aktif`, `pass_word`, `file_pic`, `data_penjamin`, `nama_penjamin`, `nik_penjamin`, `agama_penjamin`, `pendidikan_penjamin`, `pekerjaan_penjamin`, `no_telpon_penjamin`, `alamat_penjamin`, `alamat_kantor_penjamin`) VALUES
(770, '3603170505840010', 'YULIANTO', '0683', 'L', 'KLATEN', '1984-05-05', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Karyawan Swasta', 'KP. BITUNG RT 002 RW 004 DESA KADU JAYA, KECAMATAN CURUG, TANGERANG 15810', 'KAB. TANGERANG', '082112803070', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, 'YULIANTI', '3603175306910003', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(771, '3603175306910003', 'YULIANTI', '0684', 'P', 'TANGERANG', '1991-06-13', 'Kawin', 'Islam', 'KADU JAYA', NULL, NULL, 'Karyawan Swasta', 'KP. BITUNG RT 002 RW 005 DESA KADU JAYA, KECAMATAN CURUG, TANGERANG', 'KAB. TANGERANG', '082112803070', '2019-01-30', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'YULIANTO', '3603170505840010', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(772, '3603025307870002', 'LILIS SUWESTI ATAS ASIH', '0685', 'P', 'SERANG', '1987-07-13', '', '', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN CIKANDE BLOK C01/04 RT 002 RW 001 DESA CIKANDE, KECAMATAN JAYANTI', 'KAB. TANGERANG', '081387965464', '2019-02-04', 2, 'Y', '', '', 'suami', 'ARIS SANTOSA', '3309181608870003', NULL, 'SMA/Sederajat', 'Wiraswasta', NULL, NULL, NULL),
(773, '3603014710800007', 'DAMAYANTI', '0686', 'P', 'JAKARTA', '1980-10-07', 'Kawin', 'Islam', '', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT 001 RW 003', 'KAB. TANGERANG', '085351992254', '2019-02-04', 2, 'Y', '', '', 'suami', 'SURIA', '3603010707850004', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(774, '3205064707870011', 'SITI NURJAT', '0687', 'P', 'GARUT', '1987-07-07', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'CANGKUDU RT 001 RW 003', 'KAB. TANGERANG', '085289678629', '2019-02-04', 2, 'Y', '', '', 'suami', 'ACEP SUJANA', '3603010508820008', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(775, '3603015501710004', 'SANIMAH', '688', 'P', 'TANGERANG', '1971-01-15', 'Kawin', '', '', NULL, NULL, '', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2019-02-04', 2, 'Y', '', '', NULL, 'SARMADI', '3603010101650021', NULL, 'SD', 'Buruh', NULL, NULL, NULL),
(776, '3603015203750006', 'SUDARTI', '689', 'P', 'TANGERANG', '1975-03-12', 'Cerai Mati', '', '', NULL, NULL, '', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA ', 'KAB. TANGERANG', '087773536597', '2019-02-04', 2, 'Y', '', '', 'anak', 'SITI MASAMAH', '3603016309970005', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(777, '36030166112710006', 'YAYAH MULYATI', '690', 'P', 'SUKABUMI', '1971-12-26', 'Kawin', '', '', NULL, NULL, '', 'KP. CANGKUDU RT. 01/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '089643339607', '2019-02-04', 2, 'Y', '', '', 'suami', 'EDI RUSPANDI', '3603012001680005', NULL, 'SMP', 'Wiraswasta', NULL, NULL, NULL),
(778, '3603014211850001', 'IDA NURYANI', '0691', 'P', 'TANGERANG', '1986-11-02', 'Kawin', 'Islam', '', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT 009 RW 003', 'KAB. TANGERANG', '085777371705', '2019-02-04', 2, 'Y', '', '', 'suami', 'IRWAN KUSUMA ATMAJA', '3603012703830002', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(779, '3603014109740006', 'EMUN MAEMUNAH', '0692', 'P', 'TANGERANG', '1974-09-01', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT 007 RW 003', 'KAB. TANGERANG', '082310600813', '2019-02-04', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'AJUN', '3603011506750016', 'Islam', 'SD', 'Wiraswasta', NULL, NULL, NULL),
(780, '3603311110570007', 'IDRUS SUHADA', '0693', 'L', 'TANGERANG', '1957-10-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Wiraswasta', 'KP. CIKASUNGKA RT 015 RW 004 DESA CIKASUNGKA. KECAMATAN SOLEAR', 'KAB. TANGERANG', '', '2019-02-04', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'istri', 'SULASTRI', '3603334901650002', 'Islam', 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(781, '3603334901650002', 'SULASTRI', '0694', 'P', 'TANGERANG', '1965-01-09', 'Kawin', 'Islam', '', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIKASUNGKA RT 015 RW 004 DESA CIKASUNGKA, KECAMATAN SOLEAR\r\n', 'KAB. TANGERANG', '', '2019-02-04', 2, 'Y', '', '', 'suami', 'IDRUS SUHADA', '3603311110570007', 'Islam', 'SD', 'Wiraswasta', NULL, NULL, NULL),
(782, '3603310405760001', 'YUSMAN', '0695', 'L', 'PANDEGLANG', '1976-05-04', '', 'Islam', '', NULL, NULL, 'Karyawan Swasta', 'KP. CIKASUNGKA RT 015 RW 004 DESA CIKASUNGKA, KECAMATAN SOLEAR', 'KAB. TANGERANG', '', '2019-02-04', 2, 'Y', '', '', 'istri', 'ISNAWATI', '3603316610850001', 'Islam', 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(783, '3603316610850001', 'ISNAWATI', '0696', 'P', 'TANGERANG', '1985-10-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CIKASUNGKA RT 015 RW 004 DESA CIKASUNGKA. KECAMATAN SOLEAR', 'KAB. TANGERANG', '', '2019-02-04', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'YUSMAN', '3603310405760001', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(784, '3603314305900009', 'RATNA PUSPITA AYU', '0697', 'P', 'TANGERANG', '1993-05-23', 'Kawin', '', 'CIBUGEL', NULL, NULL, '', 'KP. SIGEUNG RT. 04/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '083625724618', '2019-02-04', 2, 'Y', '', '', NULL, 'SUHENDAR', '3603311402870004', NULL, 'SD', 'Buruh', NULL, NULL, NULL),
(785, '3603016507800011', 'SUSILAWATI', '698', 'P', 'TANGERANG', '1980-07-25', 'Kawin', '', 'CANGKUDU', NULL, NULL, '', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2019-02-04', 2, 'Y', '', '', NULL, 'ADIH SUPRIYADIH', '3603011506760008', 'Islam', 'SD', '', NULL, NULL, NULL),
(786, '3603185902790002', 'IDAH', '700', 'P', 'TANGERANG', '1979-02-19', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 05/01 KEL. BUNDER KEC. CIKUPA ', 'KAB. TANGERANG', '085719907506', '2019-02-06', 2, 'Y', '', '', 'suami', 'SUPRIANTO', '3603180808790012', NULL, 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(787, '3603174311630002', 'SARGATI', '701', 'P', 'TANGERANG', '1970-01-01', 'Kawin', '', 'KADU JAYA', NULL, NULL, '', 'KP. BITUNG RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '', '2019-02-06', 2, 'Y', '', '', NULL, 'ARSYAD', '3603172706580003', NULL, 'SD', '', NULL, NULL, NULL),
(788, '3273185508550003', 'RD. AGOESTIEN', '702', 'P', 'BANDUNG', '1970-01-01', 'Kawin', '', 'BITUNG', NULL, NULL, '', 'KOMP. BUMI LANGGENG CINUNUK BLOK 45 RT. 04/02 KEL. CINUNUK KEC. CILEUNYI', 'BANDUNG', '081318003935', '2019-02-06', 2, 'Y', '', '', NULL, 'YANTO SUPRIYANTO', '3273181505530001', 'Islam', 'SMA/Sederajat', '', NULL, NULL, NULL),
(789, '3603196609790003', 'YUYUN YULIANINGSIH', '0703', 'P', 'TANGERANG', '1979-09-26', '', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA DULANG DESA RANCA IYUH RT 007 RW 002, KECAMATAN PANONGAN, TANGERANG', 'KAB. TANGERANG', '089513092421', '2019-02-06', 2, 'Y', '', '', NULL, 'SITI MUNIROH', '3603195503500004', 'Islam', 'SMP', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(790, '3603055112500001', 'JANAH', '0704', 'P', 'TANGERANG', '1950-12-11', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, '', 'KP. SAGA RT 002 RW 003 DESA CARINGIN. KECAMATAN CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', 'anak', 'IWAN HERIAWAN', '3603051506890010', NULL, NULL, '', NULL, NULL, NULL),
(791, '3603015106590003', 'ASIAH', '0705', 'P', 'TANGERANG', '1959-06-11', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT 009 RW 003', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', 'anak', 'HAMDANI', '3603010703800008', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(792, '3603072105860002', 'SURFITRI', '706', 'P', 'TANGERANG', '1994-03-03', 'Kawin', '', '', NULL, NULL, 'Karyawan Swasta', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '08129090951641', '2019-02-11', 2, 'Y', '', '', 'suami', 'DARYANTO', '3603072105860002', NULL, 'SD', 'Wiraswasta', NULL, NULL, NULL),
(793, '3603015203770005', 'MARSANI', '707', 'P', 'TANGERANG', '1977-03-12', 'Kawin', '', '', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT. 07/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '089671382765', '2019-02-11', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, 'NURYADI', '3603012312680001', NULL, NULL, 'Buruh', NULL, NULL, NULL),
(794, '3603014706800006', 'SAODAH', '708', 'P', 'TANGERANG', '1980-06-07', 'Kawin', '', 'CANGKUDU', NULL, NULL, '', 'KP. CANGKUDU RT. 07/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '087773536597', '2019-02-11', 2, 'Y', '', '', NULL, 'ROSIYANTO', '3603010309800005', NULL, NULL, 'Karyawan Swasta', NULL, NULL, NULL),
(795, '3603014101960004', 'FITRI YANI', '709', 'P', 'TANGERANG', '2016-04-01', '', '', '', NULL, NULL, '', 'KP. CANGKUDU RT. 07/03 KEL. CANGKUDU KEC, BALARAJA', 'KAB. TANGERANG', '0852100951610', '2019-02-11', 2, 'Y', '', '', 'suami', 'JUMAN', '36063011606880002', NULL, 'SMA/Sederajat', 'PNS', NULL, NULL, NULL),
(796, '3603021107540004', 'JALENUL', '722', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Buruh', 'KP. DANGDEUR RT. 01/01 KEL. DANGDEUR KEC. JAYANTI', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', 'anak', 'AHMAD MURSID', '3603021702990004', NULL, 'SMP', 'MAHASISWA', NULL, NULL, NULL),
(797, '3603315506850008', 'SITI SYAMSIAH', '723', 'P', 'TANGERANG', '1985-06-15', 'Kawin', '', '', NULL, NULL, 'Wiraswasta', 'KP. MANGGU RT. 03/06 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', 'suami', 'ADIH', '3603310505800011', 'Islam', 'SD', 'Buruh', NULL, NULL, NULL),
(798, '3603314308400001', 'MUNAYATI', '724', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MANGGU RT. 03/06 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'AHMAD ', '3603311101450001', 'Islam', 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(799, '3603055302650001', 'ENUNG', '725', 'P', 'TANGERANG', '1970-01-01', 'Kawin', '', '', NULL, NULL, '', 'KP. MANGGU RT. 014/04 KEL CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', NULL, 'DURAHMAN', '3603051402590001', NULL, NULL, 'Wiraswasta', NULL, NULL, NULL),
(800, '3603054204680003', 'TIKAH', '726', 'P', 'TANGERANG', '1970-01-01', 'Kawin', '', '', NULL, NULL, '', 'KP. CIBIRAT RT. 03/01 KEL. SUKATANI KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', 'suami', 'SAMSUDIN', '3603050402670003', NULL, NULL, 'Buruh', NULL, NULL, NULL),
(801, '3603054608880013', 'SITI PAHERA', '727', 'P', 'TANGERANG', '2019-02-13', '', '', '', NULL, NULL, '', 'KP. MANGGU RT. 14/04 KEL. CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', NULL, 'ANDI SUHANDI', '36030507109220006', NULL, 'SMP', '', NULL, NULL, NULL),
(802, '3603055503940001', 'NENENG KOMALASARI', '728', 'P', 'TANGERANG', '1996-10-05', 'Kawin', '', '', NULL, NULL, '', 'KP. CIBIRIT RT. 03/01 KEL. SUKATANI KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', NULL, 'RUDI', '3603050107910030', NULL, 'SD', '', NULL, NULL, NULL),
(803, '3603055411770001', 'HAER ', '729', 'P', 'TANGERANG', '1977-11-14', 'Kawin', '', '', NULL, NULL, '', 'KP. MANGGU RT. 014/04 KEL. CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', NULL, 'JAHIDI', '3603050301720005', NULL, NULL, '', NULL, NULL, NULL),
(804, '3603054305730002', 'HALIMAH', '730', 'P', 'TANGERANG', '1973-05-03', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MANGGU RT 014 RW 004 DESA CISOKA. KECAMATAN CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'YAYAN MUHTIAR', '3603052008710001', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(805, '3603034704920002', 'SITI HALIMATUSA\'DIAH', '731', 'P', 'TANGERANG', '1992-04-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, '', 'KP. MANGGU RT 014 RW 004 DESA CISOKA, KECAMATAN CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', 'suami', 'HADI YATMA', '3603052001920003', 'Islam', 'SMA/Sederajat', 'Karyawan Swasta', NULL, NULL, NULL),
(806, '3603316712870002', 'SARMIAH', '732', 'P', 'TANGERANG', '1989-12-27', 'Kawin', '', '', NULL, NULL, '', 'KP. MANGGU RT 003 RW 008 DESA PASANGGRAHAN, KECAMATAN SOLEAR', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
(807, '3603054703750002', 'BAYANAH', '733', 'P', 'TANGERANG', '1975-08-07', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MANGGU RT 014 RW 004 DESA CISOKA, KECAMATAN CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '', '', 'suami', 'AYADI', '3603050105640002', 'Islam', 'SD', 'Pedagang', NULL, NULL, NULL),
(808, '3603054207690002', 'AKOM', '734', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. MANGGU RT. 14/04 KEL. CISOKA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-13', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'DALI', '3603051012660001', 'Islam', 'SD', 'Buruh', NULL, NULL, NULL),
(809, '3603174406730008', 'AMEH', '735', 'P', 'TANGERANG', '1973-06-04', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU JAYA RT 002 RW 002 DESA KADU JAYA, KECAMATAN CURUG', 'KAB. TANGERANG', '081384158113', '2019-02-14', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'SADUN', '3603172110640001', 'Islam', 'SD', 'Wiraswasta', NULL, NULL, NULL),
(810, '3603184407870014', 'NASIAH', '736', 'P', 'TANGERANG', '1987-07-04', 'Kawin', 'Islam', 'BITUNG', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT 003 RW 001 KELURAHAN BUNDER, KECAMATAN CIKUPA', 'KAB. TANGERANG', '085894860305', '2019-02-14', 2, 'Y', '', '', 'suami', 'ASPIDIN', '3603181703870005', 'Islam', 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(811, '3603015708790014', 'SITI MARIYAM', '737', 'P', 'TANGERANG', '1979-08-17', 'Kawin', 'Islam', 'CANGKUDU', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. CANGKUDU RT 002 RW 003 DESA CANGKUDU, KECAMATAN BALARAJA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'RUDI HARTONO', '3603180602760012', 'Islam', NULL, '', NULL, NULL, NULL),
(812, '3603056101930003', 'SISKA VERA', '738', 'P', 'TANGERANG', '1993-01-21', '', '', 'CIBUGEL', NULL, NULL, '', 'KP. BANOGA RT. 02/02 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, 'MUHAMAD FADLI', '3603311805930002', NULL, 'SMP', 'Karyawan Swasta', NULL, NULL, NULL),
(813, '3603311101450001', 'AHMAD', '739', 'L', 'TANGERANG', '1970-01-01', 'Kawin', '', '', NULL, NULL, 'Buruh', 'KP. MANGGU RT. 03/06 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '', '', 'istri', 'MUNAYATI', '3603314308400001', NULL, 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(814, '3201201109780004', 'SAPRUDIN', '740', 'L', 'TANGERANG', '1978-09-11', '', '', '', NULL, NULL, '', 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA\r\n\r\n\r\n\r\n', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', NULL, 'AYAMI', '3603055901820001', NULL, NULL, 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(815, '3603054105850001', 'SUHAYAH', '741', 'P', 'TANGERANG', '1985-05-01', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BANOGA RT. 02/02 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'SUMAN', '3603051708880006', 'Islam', 'SMP', 'Wiraswasta', NULL, NULL, NULL),
(816, '3603051708880006', 'SUMAN', '742', 'L', 'TANGERANG', '1988-08-17', '', '', '', NULL, NULL, 'Wiraswasta', 'KP. BANOGA RT. 02/02 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '', '', 'istri', 'SUHAYAH', '3603054105850001', NULL, NULL, 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(817, '3603055901820001', 'AYAMI', '743', 'P', 'TANGERANG', '1982-03-01', '', '', '', NULL, NULL, 'Karyawan Swasta', 'KP. RANCA MANGGU RT, 02/04 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '', '', 'suami', 'SAPRUDIN', '3201201109780004', NULL, NULL, 'Buruh', NULL, NULL, NULL),
(818, '3603056606760002', 'DAHLIA', '744', 'P', 'TANGERANG', '1976-02-26', 'Kawin', 'Islam', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. BANOGA RT. 02/02 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'WAWAN SETIAWAN', '3603051207750001', 'Islam', 'SD', 'Wiraswasta', NULL, NULL, NULL),
(819, '3603055208920006', 'DEWI SUSANTI', '746', 'P', 'TANGERANG', '1992-08-12', 'Kawin', 'Islam', 'CIBUGEL', 'SMP', NULL, 'Mengurus Rumah Tangga', 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'SUMARDI', '3603051501850004', 'Islam', 'SMP', 'Karyawan Swasta', NULL, 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', NULL),
(820, '360305150471003', 'SARIP MS', '747', 'L', 'TANGERANG', '1971-04-15', '', '', '', 'SD', NULL, 'Buruh', 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '', '', 'istri', 'MISNI', '3603055505700001', NULL, 'SD', 'Mengurus Rumah Tangga', NULL, 'KP. RANCA MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', NULL),
(821, '3603055505700001', 'MISNI', '748', 'P', 'TANGERANG', '1970-05-15', 'Kawin', '', 'CIBUGEL', NULL, NULL, 'Mengurus Rumah Tangga', 'KP. RANCA MANGGU RT. 02/04 KEL. SEPANJANG KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '', '', 'suami', 'SARIP MS', '3603051504710003', NULL, NULL, 'Buruh', NULL, 'KP. RANCA MANGGU RT. 02/04 KEL. SEPANJANG KEC. CISOKA', NULL),
(822, '1509015101940001', 'JUMI DARIYA', '749', 'P', 'MANGUN JAYO', '1994-01-11', 'Kawin', 'Islam', 'BITUNG', 'SD', NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '', '2019-02-20', 2, 'Y', '', '', 'suami', 'FITRA ANDI', '1508032506820001', 'Islam', 'SMA', 'Karyawan Swasta', NULL, 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', NULL),
(823, '3603184502780008', 'SULASTRI', '750', 'P', 'SUBANG', '1978-02-05', '', '', 'BITUNG', 'SMP', NULL, '', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '089652141641', '2019-02-20', 2, 'Y', '', '', NULL, 'ENDANG KURNIAWAN', '3603180503750002', NULL, NULL, 'Pedagang', NULL, 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', NULL),
(824, '3603054701990003', 'YULIYANTI', '751', 'P', 'TANGERANG', '1998-08-10', 'Belum Kawin', 'Islam', 'CIBUGEL', 'SMA', NULL, 'Karyawan Swasta', 'KP. SAGA RT. 02/03 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '085215838172', '2019-02-25', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', '', 'JANAH', '3603055112500001', 'Islam', 'SD', 'Mengurus Rumah Tangga', NULL, NULL, NULL),
(825, '3603054701800002', 'HERAWATI', '752', 'P', 'PALEMBANG', '1980-01-07', 'Kawin', 'Islam', 'CIBUGEL', 'SMA', NULL, 'Mengurus Rumah Tangga', 'PERUM GRIYA PERMATA BLOK C. 10/20 RT. 06/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'SUTARNO', '3603050106730002', 'Islam', 'SMP', 'Karyawan Swasta', NULL, 'PERUM GRIYA PERMATA BLOK C. 10/20 RT. 06/04 KEL. CIBUGEL KEC. CISOKA', NULL),
(826, '3603050102670004', 'MASRONI', '753', 'L', 'TANGERANG', '1970-01-01', '', 'Islam', '', NULL, NULL, 'Buruh', 'KP. CARINGIN RT. 03/01 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '085280728580', '2019-02-25', 2, 'Y', '', '', 'istri', 'MAESAROH', '3603056310800004', NULL, 'SD', 'Buruh', '085214041677', 'KP. CARINGIN RT. 03/01 KEL. CARINGIN KEC. CISOKA', NULL),
(827, '3603056310800004', 'MAESAROH', '754', 'P', 'PANDEGLANG', '1980-10-23', 'Kawin', 'Islam', 'CIBUGEL', 'SMA', NULL, 'Buruh', 'KP. CARINGIN RT. 03/01 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '085280728580', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'MASRONI', '3603050102670004', 'Islam', 'SD', 'Buruh', '085214041677', 'KP. CARINGIN RT. 03/01 KEL. CARINGIN KEC. CISOKA', NULL),
(828, '3603016903640001', 'SARIAH', '755', 'P', 'TANGERANG', '1970-01-01', '', '', 'CANGKUDU', 'SD', NULL, '', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '', '2019-02-25', 2, 'Y', '', '', NULL, 'DJAMSURI', '3603012012450001', NULL, NULL, '', '085210934336', 'KP. CANGKUDU RT. 02/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(829, '3603185007710016', 'PURWANTI', '756', 'P', 'JAKARTA', '1971-07-10', '', '', 'PASIR JAYA', 'SMP', NULL, 'Karyawan Swasta', 'BUTIK TIARA BLK. O.3/13 RT. 40/06 KEL. PASIR JAYA KEC. CIKUPA', 'KAB. TANGERANG', '', '2019-03-06', 2, 'Y', '', '', 'istri', 'SUPRIYADI', '3603182907700001', NULL, 'SMA', 'Karyawan Swasta', '085217724082', 'BUTIK TIARA BLK. O.3/13 RT. 40/06 KEL. PASIR JAYA KEC. CIKUPA', NULL),
(830, '3603181011760023', 'AMSONI', '757', 'L', 'TANGERANG', '1976-11-10', '', '', 'BITUNG', NULL, NULL, '', 'KP. KADU RT. 04/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '', '2019-03-13', 2, 'Y', '', '', 'anak', 'FIKKA AISYAH PUTRI', '3603186205000006', NULL, 'SMP', 'MAHASISWA', '083875262549', 'KP. KADU RT. 04/01 KEL. BUNDER KEC. CIKUPA', NULL),
(831, '3603175608720004', 'ARIYAH', '758', 'P', 'TANGERANG', '1972-08-16', 'Kawin', 'Islam', 'BITUNG', 'SD', NULL, 'Mengurus Rumah Tangga', 'KP. KADU JAYA RT. 02/02 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '', '2019-03-13', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', '', 'RESIYATI', '36031745058500014', 'Islam', 'SMP', 'Mengurus Rumah Tangga', '083877552399', 'KP. KADU JAYA RT. 02/02 KEL. KADU JAYA KEC. CURUG', NULL),
(832, '3201354808800004', 'ANAH', '759', 'P', 'BOGOR', '1980-08-08', '', '', '', 'SMP', NULL, '', 'KP. KADU BUNDER RT. 03/01 KEL. BUNDER KEC, CIKUPA', 'KAB. TANGERANG', '', '2019-03-13', 2, 'Y', '', '', 'suami', 'SARNATA', '3201351508700002', NULL, 'SD', 'Mengurus Rumah Tangga', NULL, 'KP. KADU BUNDER RT. 03/01 KEL. BUNDER KEC, CIKUPA', NULL),
(833, '3601175901960003', 'SITI ROHANIYAH', '01/06/96', 'P', 'PANDEGLANG', '1996-06-01', '', '', '', 'SMA', NULL, '', 'KP. SITUGARU RT. 02/02 KEL, PANJANGJAYA KEC. MANDALAWANGI', 'PANDEGLANG', '083841318758', '2019-03-13', 2, 'Y', '', '', NULL, 'IYAN SOPIAN', '3601172502940004', NULL, 'SMA', 'Karyawan Swasta', '083877065663', 'KP. SITUGARU RT. 02/02 KEL, PANJANGJAYA KEC. MANDALAWANGI', NULL),
(834, '3603172608640002', 'H. MAD SARIP', '761', 'L', 'TANGERANG', '1970-01-01', '', '', 'KADU JAYA', 'SMP', NULL, 'Buruh', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '', '2019-03-13', 2, 'Y', '', '', 'istri', 'ROSMINAH', '3603174308650004', NULL, 'SD', 'Mengurus Rumah Tangga', '081399747763', 'KP. BITUNG RT. 02/05 KEL. KADU JAYA KEC. CURUG', NULL),
(835, '3603170712760002', 'DARMAWAN', '762', 'L', 'TANGERANG', '1976-12-07', '', '', '', 'SMA', NULL, 'Karyawan Swasta', 'KP. BITUNG RT. 03/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '085602464314', '2019-03-13', 2, 'Y', '', '', NULL, 'NENENG HERAWATI', '3603175504840005', NULL, 'SMP', '', NULL, 'KP. BITUNG RT. 03/05 KEL. KADU JAYA KEC. CURUG', NULL),
(836, '3603185708940020', 'USWATUN HASANAH', '763', 'P', 'SERANG', '1994-08-17', 'Kawin', 'Islam', 'BITUNG', 'SMP', NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 04/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '083811726045', '2019-03-19', 2, 'Y', '', '', 'suami', 'ALAN SAPUTRA', '3603180610920006', 'Islam', 'SMP', 'Karyawan Swasta', '083811726045', 'KP. KADU RT. 04/01 KEL. BUNDER KEC. CIKUPA', NULL),
(837, '3603185708940020', 'USWATUN HASANAH', '763', 'P', 'SERANG', '1994-08-17', 'Kawin', 'Islam', 'BITUNG', 'SMP', NULL, 'Mengurus Rumah Tangga', 'KP. KADU RT. 04/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '083811726045', '2019-03-19', 2, 'Y', '', '', 'suami', 'ALAN SAPUTRA', '3603180610920006', 'Islam', 'SMP', 'Karyawan Swasta', '083811726045', 'KP. KADU RT. 04/01 KEL. BUNDER KEC. CIKUPA', NULL),
(838, '3603181012790027', 'ALWANI GARDAN', '764', 'L', 'TANGERANG', '1979-12-10', '', '', '', NULL, NULL, 'Wiraswasta', 'KP. KADU RT. 01/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '081290451834', '2019-02-19', 2, 'Y', '', '', 'istri', 'HADIJAH', '3603185504780010', NULL, 'SD', 'Mengurus Rumah Tangga', '081290451834', 'KP. KADU RT. 01/01 KEL. BUNDER KEC. CIKUPA', NULL),
(839, '3603186009870013', 'AMNAH', '765', 'P', 'TANGERANG', '1987-09-20', '', '', '', 'SMA', NULL, 'Karyawan Swasta', 'KP. KADU RT. 05/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '081382800073', '2019-03-20', 2, 'Y', '', '', 'suami', 'SANUKRI YANTO', '3603180405870010', NULL, 'SMA', 'Karyawan Swasta', '089516413862', 'KP. KADU RT. 05/01 KEL. BUNDER KEC. CIKUPA', NULL),
(840, '3603056206860006', 'MARYATI', '766', 'P', 'TANGERANG', '1986-06-22', 'Kawin', 'Islam', 'BOJ0NG LOA', 'SMP', 'BALARAJA', 'Karyawan Swasta', 'KP. NAGROG RT. 03/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-03-25', 2, 'Y', '', '', 'suami', 'DUDI', '3603052802860007', 'Islam', 'SMP', 'Karyawan Swasta', NULL, 'KP. NAGROG RT. 03/02 KEL. BOJONG LOA KEC. CISOKA', NULL),
(841, '3603054906900004', 'LINDA WATI', '767', 'P', 'TANGERANG', '1990-06-09', 'Kawin', 'Islam', 'BOJ0NG LOA', 'SMP', 'BALARAJA', 'Karyawan Swasta', 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-03-23', 2, 'Y', '', '', 'suami', 'MUHAMMMAD SAWIRI', '3603052104860005', 'Islam', 'SMA', 'Karyawan Swasta', NULL, 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'BALARAJA'),
(842, '3603056702900001', 'YUNISTA', '768', 'P', 'TANGERANG', '1990-02-27', '', '', '', 'SMA', NULL, 'Mengurus Rumah Tangga', 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-03-25', 2, 'Y', '', '', NULL, 'MUMUS ALKOM', '3603052103890003', NULL, NULL, '', NULL, 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'BALARAJA'),
(843, '3603056808820004', 'TUTI MARYATI', '769', 'P', 'TANGERANG', '1982-08-28', '', '', '', 'SMP', NULL, '', 'KP. PASIRHUNI RT. 010/05 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-03-25', 2, 'Y', '', '', NULL, 'ARIS', '3603051403890004', NULL, 'SMP', 'Buruh', NULL, 'KP. PASIRHUNI RT. 010/05 KEL. BOJONG LOA KEC. CISOKA', 'BALARAJA'),
(844, '3603010408820007', 'SUHADI', '770', 'L', 'TANGERANG', '1982-08-04', '', '', 'CIBUGEL', 'SMA', 'BALARAJA', 'Karyawan Swasta', 'KP. HAUAN TEGAL RT. 03/05 KEL. TOBAT KEC. BALARAJA', 'KAB. TANGERANG', '', '2019-03-25', 2, 'Y', '', '', 'istri', 'MARIA ULFAH', '3603014601870006', NULL, NULL, 'Mengurus Rumah Tangga', NULL, 'KP. HAUAN TEGAL RT. 03/05 KEL. TOBAT KEC. BALARAJA', NULL),
(845, '1871122608860007', 'SELAMET RIADI', '771', 'L', 'SUKABUMI', '1986-08-26', '', '', '', 'SMP', NULL, 'Buruh', 'GRIYA PERMATA CISOKA BLOK C 11/10 RT. 06/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2019-03-25', 2, 'Y', '', '', NULL, 'NURSIAH', '1871126406870003', NULL, NULL, '', NULL, 'GRIYA PERMATA CISOKA BLOK C 11/10 RT. 06/04 KEL. CIBUGEL KEC. CISOKA', NULL),
(846, '3206176510690001', 'MARYATI', '772', 'P', 'TASIKMALAYA', '1970-01-01', '', '', 'BITUNG', NULL, NULL, 'Pedagang', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '085311194538', '2019-03-29', 2, 'Y', '', '', 'suami', 'UUM', '3206171702640001', NULL, NULL, 'Pedagang', '085311194538', 'KP. KADU RT. 03/01 KEL. BUNDER KEC. CIKUPA', NULL),
(847, '3172015404750005', 'RENI LINDAWATI', '773', 'P', 'SUKAMARA', '1975-04-14', 'Kawin', 'Islam', 'BITUNG', 'SMA', NULL, 'Mengurus Rumah Tangga', 'JL. JEMBATAN II GG. LONTAR RT. 15/03 KEL. PEJAGALAN KEC. PENJARINGAN', 'JAKARTA UTARA', '087776496575', '2019-03-29', 2, 'Y', '', '', 'suami', 'PRABOWO', '3172010110660006', 'Islam', 'SMA', 'Karyawan Swasta', NULL, 'JL. JEMBATAN II GG. LONTAR RT. 15/03 KEL. PEJAGALAN KEC. PENJARINGAN', 'PULO GADUNG'),
(848, '3603054504630004', 'LILIS LISNAWATI', '774', 'P', 'TANGERANG', '1970-01-01', '', '', 'CANGKUDU', 'SMP', NULL, '', 'GRIYA PERMATA CISOKA BLOK A2/12 RT. 02/07 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '083872244903', '2019-04-05', 2, 'Y', '', '', 'anak', 'SRI RATNA RAHAYU', '3603064903800007', NULL, 'SMP', '', NULL, 'GRIYA PERMATA CISOKA BLOK A2/12 RT. 02/07 KEL. CIBUGEL KEC. CISOKA', NULL),
(849, '3603014304680003', 'HJ. MARNAH', '775', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CANGKUDU', 'SD', NULL, 'Pedagang', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083872244903', '2019-04-05', 2, 'Y', '', '', 'suami', 'AHMAD CHOIRUL ANWARUDIN', '3503610102980003', 'Islam', 'SMA', 'Karyawan Swasta', '085214979677', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(850, '3603014102560002', 'DUNYATI', '776', 'P', 'TANGERANG', '1970-01-01', '', '', '', NULL, NULL, '', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083815762529', '2019-04-05', 2, 'Y', '', '', 'anak', 'RAENAH', '3603014110980004', NULL, 'SMP', '', NULL, 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(851, '3603016005770007', 'YENI HIDAYATI', '777', 'P', 'TANGERANG', '1977-05-20', 'Kawin', 'Islam', 'CANGKUDU', 'SMA', NULL, 'Karyawan Swasta', 'KP. TALAGA RT. 01/02 KEL. TALAGASARI KEC. BALARAJA', 'KAB. TANGERANG', '085289678629', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'suami', 'MULYANTO', '3603011208720006', 'Islam', 'SMP', 'Buruh', NULL, 'KP. TALAGA RT. 01/02 KEL. TALAGASARI KEC. BALARAJA', NULL),
(852, '3603035510910010', 'SANGKUT MARLENA', '778', 'P', 'TANJUNG RUKU GIHAM', '1991-10-15', '', '', '', NULL, NULL, 'Mengurus Rumah Tangga', 'BUKIT GADING CISOKA BLOK B1/22 RT. 05/05 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '087872244903', '2019-04-05', 2, 'Y', '', '', NULL, 'EDI RISTANTO', '3603031001880004', NULL, 'SMA', 'Karyawan Swasta', NULL, 'BUKIT GADING CISOKA BLOK B1/22 RT. 05/05 KEL. SELAPANJANG KEC. CISOKA', NULL),
(853, '3328015006850009', 'SRI NURANTI', '779', 'P', 'TEGAL', '1985-06-10', '', '', '', NULL, NULL, '', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085214979677', '2019-04-05', 2, 'Y', '', '', NULL, 'WAWAN SETIAWAN', '3603010303880008', NULL, 'SMP', 'Wiraswasta', NULL, 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(854, '3603014209640001', 'ARIAH', '780', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CANGKUDU', 'SD', NULL, 'Pedagang', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083872244903', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', 'anak', 'HOLIFATUS SADIAH', '3603014306960002', 'Islam', 'SMA', 'Karyawan Swasta', NULL, 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(855, '3603015502830009', 'AMSINAH', '781', 'P', 'TANGERANG', '1983-02-15', '', '', 'CANGKUDU 1', NULL, NULL, '', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085214979677', '2019-04-05', 2, 'Y', '', '', 'suami', 'SUKAMAD', '3603012103760005', NULL, 'SMP', '', NULL, 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(856, '3603015207690003', 'ANAH', '782', 'P', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CANGKUDU 1', 'SD', NULL, 'Pedagang', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083815768529', '2019-04-05', 2, 'Y', '', '', 'suami', 'ABUN', '3603010405620005', 'Islam', 'SD', 'Buruh', NULL, 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(857, '3603014304910003', 'SITI UNAENAH', '783', 'P', 'TANGERANG', '1991-04-03', '', '', '', 'SMP', NULL, '', 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085289678629', '2019-04-05', 2, 'Y', '', '', NULL, 'MOHAMAD SARIF TIRTA HIDAYAT', '3603181811890003', NULL, NULL, 'Karyawan Swasta', NULL, 'KP. CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(858, '3603055705570001', 'MURYATI', '784', 'P', 'TANGERANG', '2019-04-05', '', '', 'BITUNG', 'SD', NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '', '2019-04-05', 2, 'Y', '', '', NULL, 'H. ACHMAD', '3603051701510003', NULL, 'SD', 'Pensiunan', '085280376841', 'KP. CIBUGEL RT. 05/04 KEL. CIBUGEL KEC. CISOKA', NULL),
(859, '3603176511930006', 'SARI PUTRI MUHARANI', '785', 'P', 'PONTIANAK', '1993-11-25', '', '', '', 'SMA', NULL, '', 'KP. SUKABAKTI RT. 03/07 KEL. SUKABAKTI KEC. CURUG', 'KAB. TANGERANG', '089652141641', '2019-04-05', 2, 'Y', '', '', NULL, 'WAWAN RUSWANDI', '3208140107850098', NULL, 'SMA', 'Karyawan Swasta', '089652141641', 'KP. SUKABAKTI RT. 03/07 KEL. SUKABAKTI KEC. CURUG', NULL),
(860, '3603181510750018', 'AZIZ SARHAPI', '786', 'L', 'TANGERANG', '1975-10-15', '', '', '', 'SD', NULL, 'Buruh', 'KP. KADU RT. 04/01 KEC. BUNDER KEC. CIKUPA', 'KAB. TANGERANG', '085311259236', '2019-04-05', 2, 'Y', '', '', 'istri', 'EKA LIAWATI', '3603185901840006', NULL, 'SD', 'Mengurus Rumah Tangga', NULL, 'KP. KADU RT. 04/01 KEC. BUNDER KEC. CIKUPA', NULL),
(861, '3603176004760001', 'MASITOH', '787', 'P', 'TANGERANG', '1976-04-20', 'Kawin', 'Islam', 'BITUNG', 'SD', NULL, 'Mengurus Rumah Tangga', 'KP. KADU JAYA RT. 02/02 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '081287146310', '2019-04-05', 2, 'Y', '', '', 'anak', 'EMAY PUSPITA SARI', '3603177105950006', 'Islam', 'SMA', 'Karyawan Swasta', '083899241522', 'KP. KADU JAYA RT. 02/02 KEL. KADU JAYA KEC. CURUG', NULL),
(862, '3603054609780006', 'NURHAEMI', '788', 'P', 'TANGERANG', '1978-09-06', '', '', 'CIBUGEL', 'SMP', NULL, '', 'KP. CILABAN RT. 013/06 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-04-09', 2, 'Y', '', '', 'suami', 'DULMAJID', '3603051407710007', NULL, 'SD', 'Buruh', NULL, 'KP. CILABAN RT. 013/06 KEL. BOJONG LOA KEC. CISOKA', NULL),
(863, '3603051407710004', 'DULMAJID', '789', 'L', 'TANGERANG', '1971-07-14', '', '', '', 'SD', NULL, 'Buruh', 'KP. CILABAN RT. 013/06 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-04-09', 2, 'Y', '', '', 'istri', 'NURHAEMI', '3603054609780006', NULL, 'SMP', 'Mengurus Rumah Tangga', NULL, 'KP. CILABAN RT. 013/06 KEL. BOJONG LOA KEC. CISOKA', NULL),
(864, '3603175812780001', 'NENENG', '790', 'P', 'TANGERANG', '1978-12-18', 'Kawin', 'Islam', 'KADU JAYA', 'SMP', NULL, 'Mengurus Rumah Tangga', 'KP. BITUNG RT. 01/05 RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '081308573662', '2019-04-10', 2, 'Y', '', '', 'suami', 'SUGANDA', '3603172612730005', 'Islam', 'SMA', 'Karyawan Swasta', '081308573662', 'KP. BITUNG RT. 01/05 RT. 01/05 KEL. KADU JAYA KEC. CURUG', NULL),
(865, '3602256106950002', 'RATNASARI', '791', 'P', 'LEBAK', '1995-06-21', 'Kawin', 'Islam', 'BITUNG', 'SMP', NULL, 'Wiraswasta', 'KP. CIRAHONG RT. 06/01 KEL. CILADAEUN KEC. LEBAKGEDONG', 'KAB. TANGERANG', '', '2019-04-10', 2, 'Y', '', '', 'suami', 'JAJANG SUHERMAN', '3602252411900001', 'Islam', 'SMP', 'Wiraswasta', NULL, 'KP. CIRAHONG RT. 06/01 KEL. CILADAEUN KEC. LEBAKGEDONG', NULL),
(866, '3603051710680001', 'SURYANI', '792', 'L', 'TANGERANG', '1970-01-01', 'Kawin', 'Islam', 'CIBUGEL', 'SD', NULL, 'Buruh', 'KP. CILISUNG RT. 02/02 KEL. JEUNGJING KEC. CISOKA', 'KAB. TANGERANG', '', '2019-04-12', 2, 'Y', '', '', 'istri', 'YUYUM', '3603055712770003', 'Islam', 'SD', 'Mengurus Rumah Tangga', NULL, 'KP. CILISUNG RT. 02/02 KEL. JEUNGJING KEC. CISOKA', NULL),
(867, '3603051501850004', 'SUMARDI', '745', 'L', 'TANGERANG', '1985-01-15', '', '', '', 'SMP', NULL, 'Karyawan Swasta', 'KP. MANGGU RT. 02/04 KEL. SELAPANJANG KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-18', 2, 'Y', '', '', NULL, 'DEWI SUSANTI', '3603055208920006', NULL, 'SMP', '', NULL, NULL, NULL),
(868, '3603055712770003', 'YUYUM', '793', 'P', 'TANGERANG', '1977-12-17', '', '', '', 'SD', NULL, 'Mengurus Rumah Tangga', 'KP. CILISUNG RT. 02/02 KEL. JEUNGJING KEC. CISOKA', 'KAB. TANGERANG', '', '2019-04-12', 2, 'Y', '', '', 'suami', 'SURYANI', '3603051710680001', 'Islam', 'SD', 'Buruh', NULL, 'KP. CILISUNG RT. 02/02 KEL. JEUNGJING KEC. CISOKA', 'BALARAJA'),
(869, '3603015508750005', 'RINA INDRIYANI', '804', 'P', 'TANGERANG', '1975-08-15', '', '', 'CANGKUDU 1', NULL, NULL, 'Pedagang', 'KP. CANGKUDU RT. 08/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085214979677', '2019-04-22', 2, 'Y', '', '', NULL, 'ASNAWI', '3603012207690005', NULL, NULL, '', NULL, 'KP. CANGKUDU RT. 08/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(870, '3603014307780004', 'ENCUM SUMIYATI', '805', 'P', 'TANGERANG', '1978-07-03', '', '', '', NULL, NULL, 'Wiraswasta', 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083872244903', '2019-04-22', 2, 'Y', '', '', 'suami', 'DIDIH SUHARDI', '3603010506770015', NULL, NULL, 'Wiraswasta', NULL, 'KP. CANGKUDU RT. 05/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(871, '3603014101650002', 'PATIMAH', '806', 'P', 'TANGERANG', '1970-01-01', '', '', '', NULL, NULL, 'Pedagang', 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '085214979677', '2019-04-22', 2, 'Y', '', '', NULL, 'SUBUR ', '3603010502540002', NULL, NULL, 'Buruh', NULL, 'CANGKUDU RT. 09/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(872, '3173046807721001', 'ENI KUSMAWATI', '807', 'P', 'TANGERANG', '1972-07-28', 'Kawin', 'Islam', 'CANGKUDU 1', 'SMP', NULL, 'Pedagang', 'KP. CANGKUDU RT. 03/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083815762529', '2019-04-22', 2, 'Y', '', '', 'suami', 'MUHAMAD ROFIK', '3173043006700017', 'Islam', 'SMA', 'Wiraswasta', NULL, 'KP. CANGKUDU RT. 03/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(873, '3603014709870004', 'ROSTIANI', '808', 'P', 'TANGERANG', '1987-09-07', 'Kawin', 'Islam', 'CANGKUDU 1', 'SMA', NULL, 'Karyawan Swasta', 'KP. CANGKUDU RT. 04/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083872244903', '2019-04-22', 2, 'Y', '', '', 'suami', 'ADE HASAN BASRI', '3603010308820007', 'Islam', 'SMA', 'Wiraswasta', NULL, 'KP. CANGKUDU RT. 04/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(874, '3603015008910004', 'SITI ELISA', '809', 'P', 'TANGERANG', '1991-08-10', '', '', '', 'SMP', NULL, 'Wiraswasta', 'KP. CANGKUDU RT. 08/03 KEL. CANGKUDU KEC. BALARAJA', 'KAB. TANGERANG', '083872244903', '2019-04-22', 2, 'Y', '', '', NULL, 'AJIS SABRONI', '3603011204820006', NULL, NULL, 'Karyawan Swasta', NULL, 'KP. CANGKUDU RT. 08/03 KEL. CANGKUDU KEC. BALARAJA', NULL),
(875, '3603174903880006', 'SAEPITRI', '810', 'P', 'TANGERANG', '2009-09-19', '', '', 'KADU JAYA', 'SMA', NULL, 'Mengurus Rumah Tangga', 'KP. KADU JAYA RT. 01/05 KEL. KADU JAYA KEC. CURUG', 'KAB. TANGERANG', '082121277609', '2019-04-25', 2, 'Y', '', '', NULL, 'ACHAMD FAUZI', '3603173011790013', NULL, NULL, 'Wiraswasta', NULL, 'KP. KADU JAYA RT. 01/05 KEL. KADU JAYA KEC. CURUG', NULL),
(876, '3603312411780002', 'DASEP HAERUL', '803', 'L', 'LEBAK', '1978-11-24', 'Kawin', '', 'PASANGGRAHAN', 'SMA', NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'istri', 'HOLILAH', '3603316608770002', NULL, 'SMP', 'Pedagang', NULL, 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', NULL),
(877, '360112490720001', 'YENI NURAENI', '802', 'P', 'TANGERANG', '1972-10-09', '', '', '', NULL, NULL, 'Mengurus Rumah Tangga', 'TAMAN KIRANA SURYA BLOK P.03/33 RT. 06/08 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'suami', 'E. HASANUDIN', '3603310102730004', NULL, NULL, 'Karyawan Swasta', NULL, 'TAMAN KIRANA SURYA BLOK P.03/33 RT. 06/08 KEL. PASANGGRAHAN KEC. SOLEAR', NULL),
(878, '3603311607900005', 'REO SWASTIKA', '801', 'L', 'TANGERANG', '1990-07-16', '', '', '', NULL, NULL, 'Karyawan Swasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'istri', 'RATNA JUWITA', '3603056006920009', NULL, 'SMA', '', NULL, 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', NULL),
(879, '3603316608770002', 'HOLILAH', '800', 'P', 'TANGERANG', '1977-08-26', '', '', '', 'SMP', NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 02/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'suami', 'DASEP HAERUL', '3603312411780002', NULL, NULL, 'Pedagang', NULL, 'KP. PASANGGRAHAN RT. 02/01 KEL. SOLEAR KEC. SOLEAR', NULL),
(880, '3202111103790008', 'DEDE RUSLI', '799', 'L', 'JAKARTA', '1979-03-11', '', '', '', 'SMA', NULL, 'Wiraswasta', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'istri', 'TATI HARTATI', '3202117112800003', NULL, NULL, '', NULL, 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', NULL),
(881, '3202117112800003', 'TATI HARTATI', '798', 'P', 'JAKARTA', '1980-12-31', '', '', '', NULL, NULL, 'Pedagang', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'suami', 'DEDE RUSLI', '3202111103790008', NULL, NULL, 'Wiraswasta', NULL, 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', NULL),
(882, '3603310102730004', 'E. HASANUDIN', '797', 'L', 'TANGERANG', '1973-02-01', '', '', '', NULL, NULL, 'Karyawan Swasta', 'TAMAN KIRANA SURYA BLOK F.03/33 RT. 06/09 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', NULL, 'YENI NURAENI', '3601124910720001', NULL, NULL, 'Karyawan Swasta', NULL, 'TAMAN KIRANA SURYA BLOK F.03/33 RT. 06/09 KEL. PASANGGRAHAN KEC. SOLEAR', NULL),
(883, '3603315706770006', 'ROHAYATI', '796', 'P', 'TANGERANG', '1977-06-17', '', '', '', NULL, NULL, '', 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', NULL, 'SYARIP', '3603310505660004', NULL, 'SD', 'Pedagang', NULL, 'KP. PASANGGRAHAN RT. 05/01 KEL. SOLEAR KEC. SOLEAR', NULL),
(884, '3603316606970001', 'BELA PUTRIYANI', '795', 'P', 'TANGERANG', '1997-06-25', 'Kawin', 'Islam', 'CIBUGEL', 'SMA', NULL, 'MAHASISWA', 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', '', 'DASEP HAERUL', '3603316606970001', 'Islam', 'SMA', 'Wiraswasta', NULL, 'KP. PASANGGRAHAN RT. 02/01 KEL. PASANGGRAHAN KEC. SOLEAR', NULL),
(885, '3603054506660003', 'NAWIYAH', '794', 'P', 'TANGERANG', '1970-01-01', '', '', '', 'SD', NULL, 'Pedagang', 'KP. LEUWIDAHU RT. 04/04 KEL. CARINGIN KEC. CISOKA', 'KAB. TANGERANG', '', '2019-04-15', 2, 'Y', '', '', 'suami', 'ESDI', '3603050607600005', NULL, 'SD', 'Karyawan Swasta', NULL, 'KP. LEUWIDAHU RT. 04/04 KEL. CARINGIN KEC. CISOKA', NULL),
(886, '3603056405630001', 'SUKARTINI', '713', 'P', 'TANGERANG', '1970-01-01', '', '', 'BOJ0NG LOA', NULL, NULL, '', 'KP. NAGROG RT. 04/02 RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', NULL, 'ASDARI', '3603051006770003', NULL, 'SMP', '', NULL, 'KP. NAGROG RT. 04/02 RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', NULL),
(887, '36030563003850003', 'MULYANAH', '714', 'P', 'TANGERANG', '1985-03-23', '', '', '', NULL, NULL, '', 'KP. CIGOONG RT. 09/03 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', NULL, 'MUHAMAD NUR', '3603051810960007', NULL, 'SD', '', NULL, 'KP. CIGOONG RT. 09/03 KEL. BOJONG LOA KEC. CISOKA', NULL),
(888, '3603055102810008', 'SITI ASIAH', '715', 'P', 'JAKARTA', '1981-02-11', 'Kawin', 'Islam', 'BOJ0NG LOA', 'SMA', NULL, 'Mengurus Rumah Tangga', 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', '', 'SITI ALFIAH', '3603055711870001', 'Islam', 'SMP', 'Mengurus Rumah Tangga', NULL, 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', NULL),
(889, '3603055706750007', 'JASMI', '716', 'P', 'TANGERANG', '1975-06-17', 'Kawin', 'Islam', 'BOJ0NG LOA', 'SD', NULL, 'Mengurus Rumah Tangga', 'KP. CIGOONG RT. 09/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', 'anak', 'M. HELMI', '3603050707960004', 'Islam', 'SMP', 'TIDAK BEKERJA', '083892370107', 'KP. CIGOONG RT. 09/02 KEL. BOJONG LOA KEC. CISOKA', NULL),
(890, '3603054902770002', 'USMIYATI', '717', 'P', 'TANGERANG', '1977-02-09', 'Kawin', 'Islam', 'BOJ0NG LOA', 'SMP', NULL, 'Mengurus Rumah Tangga', 'KP. CIBUGEL ASEM RT. 03/05 KEL. CIBUGEL KEC. CISOKA', 'KAB. TANGERANG', '085217405779', '1970-01-01', 2, 'Y', '1e157dd5081c6699192c94068932336f5c00ebf5', '', '', 'ASNA WIDODO', '3603031006950003', 'Islam', 'SMA', 'Karyawan Swasta', '0895610440724', 'KP. CIBUGEL ASEM RT. 03/05 KEL. CIBUGEL KEC. CISOKA', NULL),
(891, '3603054807970004', 'MARSITI', '718', 'P', 'TANGERANG', '1997-07-08', '', '', '', NULL, NULL, '', 'KP. NAGROG RT. 03/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', 'suami', 'FERDI SETIAWAN', '3603050402980002', NULL, 'SMP', '', NULL, 'KP. NAGROG RT. 03/02 KEL. BOJONG LOA KEC. CISOKA', NULL),
(892, '3603054311710003', 'EMUN', '719', 'P', 'TANGERANG', '1971-11-03', '', '', '', 'SD', NULL, 'Pedagang', 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', 'KAB. TANGERANG', '', '2019-02-11', 2, 'Y', '', '', NULL, 'AHMAD', '3603050709640005', NULL, 'SD', 'Buruh', NULL, 'KP. NAGROG RT. 04/02 KEL. BOJONG LOA KEC. CISOKA', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id` bigint(20) NOT NULL,
  `nm_barang` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `harga` double NOT NULL,
  `jml_brg` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id`, `nm_barang`, `type`, `merk`, `harga`, `jml_brg`, `ket`) VALUES
(1, 'Lemari Es', 'Elektronik', 'Toshiba', 500000, 4, ''),
(2, 'Komputer', 'K300 Corei3', 'Asus', 5000000, 4, ''),
(3, 'Kompor Gas', 'Tr675000', 'Rinai', 100000, 7, ''),
(4, 'Pinjaman Uang', 'Uang', '-', 0, 70, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengajuan`
--

CREATE TABLE `tbl_pengajuan` (
  `id` bigint(20) NOT NULL,
  `no_ajuan` int(11) NOT NULL,
  `ajuan_id` varchar(255) NOT NULL,
  `anggota_id` bigint(20) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `nominal` bigint(20) NOT NULL,
  `lama_ags` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `tgl_cair` date NOT NULL,
  `tgl_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pinjaman_d`
--

CREATE TABLE `tbl_pinjaman_d` (
  `id` bigint(20) NOT NULL,
  `tgl_bayar` datetime NOT NULL,
  `pinjam_id` bigint(20) NOT NULL,
  `angsuran_ke` bigint(20) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `denda_rp` int(11) NOT NULL,
  `terlambat` int(11) NOT NULL,
  `ket_bayar` enum('Angsuran','Pelunasan','Bayar Denda') NOT NULL,
  `dk` enum('D','K') NOT NULL,
  `kas_id` bigint(20) NOT NULL,
  `jns_trans` bigint(20) NOT NULL,
  `update_data` datetime NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `jml_pokok_angsuran` decimal(29,12) NOT NULL,
  `jml_jasa_angsuran` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_pinjaman_d`
--

INSERT INTO `tbl_pinjaman_d` (`id`, `tgl_bayar`, `pinjam_id`, `angsuran_ke`, `jumlah_bayar`, `denda_rp`, `terlambat`, `ket_bayar`, `dk`, `kas_id`, `jns_trans`, `update_data`, `user_name`, `keterangan`, `jml_pokok_angsuran`, `jml_jasa_angsuran`) VALUES
(1, '2018-03-08 15:30:00', 22, 1, 108750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'ADMIN', '', '100000.000000000000', 8750),
(3, '2018-03-08 15:35:00', 24, 1, 543750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'ADMIN', '', '0.000000000000', 0),
(4, '2018-03-08 15:40:00', 25, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'ADMIN', '', '0.000000000000', 0),
(5, '2018-03-08 15:50:00', 26, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'ADMIN', '', '0.000000000000', 0),
(6, '2018-03-08 10:15:00', 27, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(7, '2018-03-08 10:55:00', 28, 1, 81563, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(8, '2018-03-08 11:50:00', 30, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(11, '2018-03-08 12:00:00', 31, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(12, '2018-03-08 12:05:00', 32, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(13, '2018-03-08 12:05:00', 33, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(17, '2018-03-14 00:00:00', 36, 1, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(19, '2018-03-14 00:00:00', 24, 2, 293750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(20, '2018-03-14 00:00:00', 25, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(21, '2018-03-14 00:00:00', 26, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2250),
(22, '2018-03-14 00:00:00', 27, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '100000.000000000000', 8750),
(23, '2018-03-14 00:00:00', 28, 2, 81563, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(24, '2018-03-14 00:00:00', 30, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '250000.000000000000', 43750),
(25, '2018-03-14 00:00:00', 37, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(26, '2018-03-14 00:00:00', 31, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(27, '2018-03-14 00:00:00', 32, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(28, '2018-03-14 00:00:00', 33, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '75000.000000000000', 6562.5),
(29, '2018-03-14 00:00:00', 38, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(30, '2018-03-14 00:00:00', 40, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(31, '2018-03-14 00:00:00', 41, 1, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(32, '2018-03-14 00:00:00', 42, 1, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(35, '2018-03-21 00:00:00', 44, 1, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '50000.000000000000', 4375),
(38, '2018-03-21 00:00:00', 24, 3, 293750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(39, '2018-03-21 00:00:00', 25, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '50000.000000000000', 4375),
(40, '2018-03-21 00:00:00', 26, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(41, '2018-03-21 00:00:00', 27, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '2019-05-08 09:50:00', 'admin', '', '50000.000000000000', 4375),
(42, '2018-03-21 00:00:00', 28, 3, 81563, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '50000.000000000000', 4375),
(43, '2018-03-21 00:00:00', 30, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(44, '2018-03-21 00:00:00', 30, 4, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '50000.000000000000', 4375),
(45, '2018-03-21 00:00:00', 37, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(46, '2018-03-21 00:00:00', 47, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '150000.000000000000', 13125),
(47, '2018-03-21 00:00:00', 31, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '2019-05-08 09:51:00', 'admin', '', '25000.000000000000', 2187.5),
(48, '2018-07-02 00:00:00', 43, 1, 108750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '-', '100000.000000000000', 8750),
(49, '2018-07-02 00:00:00', 43, 2, 108750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '-', '75000.000000000000', 6562.5),
(50, '2018-03-21 00:00:00', 32, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(51, '2018-03-21 00:00:00', 40, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(52, '2018-03-21 00:00:00', 41, 2, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2187.5),
(54, '2018-03-21 00:00:00', 45, 1, 27187, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(55, '2018-03-21 00:00:00', 46, 1, 163125, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(56, '2018-03-28 00:00:00', 44, 2, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(58, '2018-03-28 00:00:00', 24, 4, 293750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(59, '2018-03-28 00:00:00', 25, 4, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(60, '2018-03-28 00:00:00', 26, 4, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(61, '2018-03-28 00:00:00', 27, 4, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(63, '2018-03-28 00:00:00', 50, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(66, '2018-03-28 00:00:00', 37, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(67, '2018-03-28 00:00:00', 47, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(68, '2018-03-28 00:00:00', 51, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(69, '2018-03-28 00:00:00', 31, 4, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(70, '2018-03-28 00:00:00', 32, 4, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(71, '2018-03-28 00:00:00', 33, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(72, '2018-03-28 00:00:00', 38, 2, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(73, '2018-03-28 00:00:00', 52, 1, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(74, '2018-03-28 00:00:00', 40, 3, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(75, '2018-03-28 00:00:00', 41, 3, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(76, '2018-03-28 00:00:00', 42, 3, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(77, '2018-03-28 00:00:00', 45, 2, 27187, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(78, '2018-03-28 00:00:00', 46, 2, 163125, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '0.000000000000', 0),
(105, '2018-03-21 00:00:00', 38, 3, 85000, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '85000.000000000000', 0),
(106, '2018-03-21 00:00:00', 42, 3, 54375, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '50000.000000000000', 4375),
(107, '2018-03-21 00:00:00', 22, 2, 108750, 0, 0, 'Angsuran', 'D', 1, 48, '2019-05-13 10:56:00', 'admin', '', '100000.000000000000', 8750),
(108, '2018-03-21 00:00:00', 22, 3, 108750, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '100000.000000000000', 8750),
(109, '2018-03-08 00:00:00', 23, 5, 27188, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '25000.000000000000', 2188),
(110, '2018-03-28 00:00:00', 28, 4, 81562, 0, 0, 'Angsuran', 'D', 1, 48, '0000-00-00 00:00:00', 'admin', '', '75000.000000000000', 6562);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pinjaman_h`
--

CREATE TABLE `tbl_pinjaman_h` (
  `id` bigint(20) NOT NULL,
  `tgl_pinjam` datetime NOT NULL,
  `anggota_id` bigint(20) NOT NULL,
  `barang_id` bigint(20) NOT NULL,
  `lama_angsuran` bigint(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bunga` float(10,2) NOT NULL,
  `biaya_adm` int(11) NOT NULL,
  `lunas` enum('Belum','Lunas') NOT NULL,
  `dk` enum('D','K') NOT NULL,
  `kas_id` bigint(20) NOT NULL,
  `jns_trans` bigint(20) NOT NULL,
  `update_data` datetime NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `contoh` int(23) NOT NULL,
  `jasa` int(11) DEFAULT NULL,
  `pokok` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_pinjaman_h`
--

INSERT INTO `tbl_pinjaman_h` (`id`, `tgl_pinjam`, `anggota_id`, `barang_id`, `lama_angsuran`, `jumlah`, `bunga`, `biaya_adm`, `lunas`, `dk`, `kas_id`, `jns_trans`, `update_data`, `user_name`, `keterangan`, `contoh`, `jasa`, `pokok`) VALUES
(22, '2018-03-01 15:25:00', 21, 4, 20, 2000000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'ADMIN', '', 0, 0, 0),
(23, '2018-03-01 15:30:00', 30, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'ADMIN', '', 0, 0, 0),
(24, '2018-03-01 15:35:00', 31, 4, 40, 10000000, 17.50, 0, 'Belum', 'K', 1, 7, '2019-05-02 12:20:00', 'admin', '', 0, 0, 0),
(25, '2018-03-01 15:35:00', 32, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'ADMIN', '', 0, 0, 0),
(26, '2018-03-01 15:40:00', 33, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'ADMIN', '', 0, 0, 0),
(27, '2018-03-01 10:10:00', 34, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(28, '2018-03-01 10:55:00', 35, 4, 20, 1500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(30, '2018-03-01 11:50:00', 36, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(31, '2018-03-01 11:50:00', 58, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(32, '2018-03-01 12:00:00', 60, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(33, '2018-03-01 12:05:00', 62, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(34, '2018-03-01 00:00:00', 48, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '-', 0, 0, 0),
(35, '2018-03-01 00:00:00', 56, 4, 20, 1000000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '-', 0, 0, 0),
(36, '2018-03-08 00:00:00', 29, 4, 20, 1000000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:42:00', 'admin', '', 0, 0, 0),
(37, '2018-03-08 00:00:00', 49, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:43:00', 'admin', '', 0, 0, 0),
(38, '2018-03-08 00:00:00', 65, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:46:00', 'admin', '', 0, 0, 0),
(39, '2018-03-08 00:00:00', 69, 4, 20, 1000000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:46:00', 'admin', '', 0, 0, 0),
(40, '2018-03-08 00:00:00', 70, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:47:00', 'admin', '', 0, 0, 0),
(41, '2018-03-08 00:00:00', 71, 4, 20, 1000000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:47:00', 'admin', '', 0, 0, 0),
(42, '2018-03-08 00:00:00', 73, 4, 20, 1000000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:48:00', 'admin', '', 0, 0, 0),
(43, '2018-03-08 00:00:00', 79, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 10:49:00', 'admin', 'pinjaman 1', 0, 0, 0),
(44, '2018-03-14 00:00:00', 28, 4, 20, 1000000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(45, '2018-03-14 00:00:00', 75, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '2019-05-06 14:04:00', 'admin', '', 0, 0, 0),
(46, '2018-03-14 00:00:00', 76, 4, 20, 3000000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(47, '2018-03-14 00:00:00', 53, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(48, '2018-03-28 00:00:00', 66, 4, 20, 2000000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(49, '2018-03-28 00:00:00', 101, 4, 20, 1500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(50, '2018-03-21 00:00:00', 45, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(51, '2018-03-21 00:00:00', 54, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0),
(52, '2018-03-21 00:00:00', 64, 4, 20, 500000, 8.75, 0, 'Belum', 'K', 1, 7, '0000-00-00 00:00:00', 'admin', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE `tbl_setting` (
  `id` bigint(20) NOT NULL,
  `opsi_key` varchar(255) NOT NULL,
  `opsi_val` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `opsi_key`, `opsi_val`) VALUES
(1, 'nama_lembaga', 'KOPERASI DWP BINA SEJAHTERA'),
(2, 'nama_ketua', ''),
(3, 'hp_ketua', ''),
(4, 'alamat', 'JL. KISAMAUN NO.1 KEL.SUKARASA'),
(5, 'telepon', '021-55795402'),
(6, 'kota', 'TANGERANG'),
(7, 'email', ''),
(8, 'web', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_kas`
--

CREATE TABLE `tbl_trans_kas` (
  `id` bigint(20) NOT NULL,
  `tgl_catat` datetime NOT NULL,
  `jumlah` double NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `akun` enum('Pemasukan','Pengeluaran','Transfer') NOT NULL,
  `dari_kas_id` bigint(20) DEFAULT NULL,
  `untuk_kas_id` bigint(20) DEFAULT NULL,
  `jns_trans` bigint(20) DEFAULT NULL,
  `dk` enum('D','K') DEFAULT NULL,
  `update_data` datetime NOT NULL,
  `user_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trans_sp`
--

CREATE TABLE `tbl_trans_sp` (
  `id` bigint(20) NOT NULL,
  `tgl_transaksi` datetime NOT NULL,
  `anggota_id` bigint(20) NOT NULL,
  `jenis_id` int(5) NOT NULL,
  `jumlah` double NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `akun` enum('Setoran','Penarikan') NOT NULL,
  `dk` enum('D','K') NOT NULL,
  `kas_id` bigint(20) NOT NULL,
  `update_data` datetime NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `nama_penyetor` varchar(255) NOT NULL,
  `no_identitas` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_trans_sp`
--

INSERT INTO `tbl_trans_sp` (`id`, `tgl_transaksi`, `anggota_id`, `jenis_id`, `jumlah`, `keterangan`, `akun`, `dk`, `kas_id`, `update_data`, `user_name`, `nama_penyetor`, `no_identitas`, `alamat`) VALUES
(3, '2018-02-12 12:35:00', 1, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(4, '2018-02-12 12:40:00', 2, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(5, '2018-02-12 12:40:00', 2, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(7, '2018-02-12 12:40:00', 3, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(9, '2018-02-12 12:40:00', 3, 32, 70000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(10, '2018-02-12 12:45:00', 4, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(11, '2018-02-12 12:45:00', 4, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(12, '2018-02-12 12:45:00', 4, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(13, '2018-02-12 12:55:00', 5, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(15, '2018-02-12 12:55:00', 5, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(16, '2018-02-12 13:00:00', 6, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(17, '2018-02-12 12:00:00', 6, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(18, '2018-02-12 13:00:00', 6, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(19, '2018-02-12 13:00:00', 7, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(20, '2018-02-12 13:00:00', 7, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(21, '2018-02-12 13:00:00', 7, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(22, '2018-02-12 13:00:00', 8, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(23, '2018-02-12 13:00:00', 8, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(24, '2018-02-12 13:00:00', 8, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(27, '2018-02-12 13:55:00', 9, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(28, '2018-02-12 13:55:00', 9, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(29, '2018-02-12 14:00:00', 10, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(30, '2018-02-12 14:00:00', 10, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(31, '2018-02-12 14:00:00', 10, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(32, '2018-02-12 14:00:00', 11, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(33, '2018-02-12 14:00:00', 11, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(34, '2018-02-12 14:00:00', 11, 32, 70000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(35, '2018-02-12 14:00:00', 12, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(36, '2018-02-12 14:00:00', 12, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(37, '2018-02-12 14:00:00', 12, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(38, '2018-02-12 14:05:00', 13, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(39, '2018-02-12 14:05:00', 13, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(40, '2018-02-12 14:05:00', 13, 32, 70000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(41, '2018-02-12 14:05:00', 14, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(42, '2018-02-12 14:05:00', 14, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(44, '2018-02-12 14:05:00', 14, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(46, '2018-02-12 14:15:00', 15, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(47, '2018-02-12 14:15:00', 16, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(48, '2018-02-12 14:15:00', 16, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(49, '2018-02-12 14:15:00', 17, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(50, '2018-02-12 14:15:00', 17, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(51, '2018-02-12 14:20:00', 18, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(52, '2018-02-12 14:20:00', 18, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(53, '2018-02-12 14:20:00', 18, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(54, '2018-02-12 14:20:00', 17, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(55, '2018-02-12 14:20:00', 19, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(56, '2018-02-12 14:20:00', 19, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(57, '2018-02-12 14:20:00', 19, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(58, '2018-02-12 14:20:00', 20, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(59, '2018-02-12 14:20:00', 20, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(60, '2018-02-12 14:20:00', 20, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(61, '2018-02-21 14:25:00', 39, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(62, '2018-02-21 14:25:00', 39, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(63, '2018-02-21 14:25:00', 39, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(64, '2018-02-21 14:25:00', 40, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(65, '2018-02-21 14:25:00', 40, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(66, '2018-02-21 14:25:00', 41, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(67, '2018-02-21 14:25:00', 41, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(68, '2018-02-26 14:25:00', 21, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(69, '2018-02-26 14:25:00', 21, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(70, '2018-02-26 14:25:00', 21, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(71, '2018-02-26 14:30:00', 22, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(72, '2018-02-26 14:30:00', 22, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(73, '2018-02-26 14:30:00', 23, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(74, '2018-02-26 14:30:00', 23, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(75, '2018-02-26 14:30:00', 24, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(76, '2018-02-26 14:30:00', 24, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(77, '2018-02-26 14:30:00', 25, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(78, '2018-02-26 14:30:00', 25, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(79, '2018-02-26 14:35:00', 26, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(81, '2018-02-26 14:35:00', 35, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(82, '2018-02-26 14:35:00', 35, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(84, '2018-03-01 13:30:00', 22, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(85, '2018-03-01 13:30:00', 22, 32, 90000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(86, '2018-03-01 13:35:00', 23, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(87, '2018-03-01 13:35:00', 23, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(88, '2018-03-01 13:40:00', 24, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(89, '2018-03-01 14:00:00', 24, 32, 40000, '', 'Setoran', 'D', 1, '2019-04-23 14:00:00', 'admin', '', '', ''),
(90, '2018-03-01 13:45:00', 25, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(91, '2018-03-01 13:45:00', 25, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(93, '2018-03-01 13:45:00', 26, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(94, '2018-03-01 13:50:00', 27, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(95, '2018-03-01 13:50:00', 27, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(96, '2018-03-01 13:50:00', 27, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(97, '2018-03-01 13:55:00', 28, 40, 20000, '', 'Setoran', 'D', 1, '2019-04-13 10:00:00', 'admin', '', '', ''),
(98, '2018-03-01 13:55:00', 28, 41, 10000, '', 'Setoran', 'D', 1, '2019-04-13 10:00:00', 'admin', '', '', ''),
(99, '2018-03-01 13:55:00', 28, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(100, '2018-03-01 14:05:00', 29, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(101, '2018-03-01 14:05:00', 29, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(102, '2018-03-01 14:05:00', 30, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(103, '2018-03-01 14:05:00', 30, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(104, '2018-03-01 14:10:00', 31, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(105, '2018-03-01 14:10:00', 31, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(106, '2018-03-01 14:10:00', 31, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(107, '2018-03-01 14:15:00', 32, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(108, '2018-03-01 14:15:00', 32, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(109, '2018-03-01 14:15:00', 33, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(110, '2018-03-01 14:15:00', 33, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(111, '2018-03-01 14:15:00', 34, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(112, '2018-03-01 14:15:00', 34, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(113, '2018-03-01 14:20:00', 36, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(114, '2018-03-01 14:20:00', 36, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(115, '2018-03-01 14:25:00', 38, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(116, '2018-03-01 14:25:00', 38, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(117, '2018-03-01 14:25:00', 42, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(118, '2018-03-01 14:25:00', 42, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(119, '2018-03-01 14:30:00', 43, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(120, '2018-03-01 14:30:00', 43, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(121, '2018-03-01 14:35:00', 44, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(122, '2018-03-01 14:35:00', 44, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(123, '2018-03-01 14:40:00', 45, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(125, '2018-03-01 14:40:00', 45, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(126, '2018-03-01 14:50:00', 46, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(127, '2018-03-01 14:50:00', 46, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(128, '2018-03-01 14:50:00', 46, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(129, '2018-03-01 14:50:00', 47, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(130, '2018-03-01 14:50:00', 47, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(131, '2018-03-01 14:50:00', 47, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(132, '2018-03-01 14:55:00', 48, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(133, '2018-03-01 14:55:00', 48, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(134, '2018-03-01 15:00:00', 49, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(135, '2018-03-01 15:00:00', 49, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(136, '2018-03-01 15:00:00', 49, 32, 15000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(137, '2018-03-01 15:00:00', 50, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(138, '2018-03-01 15:00:00', 50, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(139, '2018-03-01 15:00:00', 51, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(140, '2018-03-01 15:00:00', 51, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(141, '2018-03-01 15:00:00', 51, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(142, '2018-03-01 15:00:00', 52, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(143, '2018-03-01 15:00:00', 52, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(144, '2018-03-01 15:00:00', 53, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(145, '2018-03-01 15:00:00', 53, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(146, '2018-03-01 15:00:00', 53, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(147, '2018-03-01 15:00:00', 54, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(148, '2018-03-01 15:00:00', 54, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(149, '2018-03-01 15:00:00', 54, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(150, '2018-03-01 15:00:00', 55, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(151, '2018-03-01 15:00:00', 55, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(152, '2018-03-01 15:00:00', 56, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(153, '2018-03-01 15:00:00', 56, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(154, '2018-03-01 15:00:00', 57, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(155, '2018-03-01 15:00:00', 57, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(156, '2018-03-01 15:00:00', 57, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(157, '2018-03-01 15:00:00', 58, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(158, '2018-03-01 15:00:00', 58, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(159, '2018-03-01 15:00:00', 59, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(160, '2018-03-01 15:00:00', 59, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(161, '2018-03-01 15:00:00', 60, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(162, '2018-03-01 15:00:00', 60, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(163, '2018-03-01 11:00:00', 61, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(164, '2018-03-01 11:00:00', 61, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(165, '2018-03-01 11:00:00', 62, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(166, '2018-03-01 11:00:00', 62, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(167, '2018-03-01 11:00:00', 63, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(168, '2018-03-01 11:00:00', 63, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(169, '2018-03-01 11:00:00', 64, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(170, '2018-03-01 11:00:00', 64, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(171, '2018-03-01 11:00:00', 65, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(172, '2018-03-01 11:00:00', 65, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(173, '2018-03-01 11:00:00', 66, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(174, '2018-03-01 11:00:00', 66, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(175, '2018-02-12 13:45:00', 3, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(176, '2018-02-12 13:45:00', 5, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(177, '2018-02-12 13:45:00', 9, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(178, '2018-02-12 13:50:00', 15, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(179, '2018-02-26 14:25:00', 26, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(180, '2018-03-01 14:00:00', 26, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(181, '2018-03-01 14:00:00', 66, 32, 70000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(182, '2018-03-08 09:30:00', 60, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(183, '2018-03-01 09:45:00', 62, 32, 5000, '', 'Setoran', 'D', 1, '2019-04-25 10:22:00', 'admin', '', '', ''),
(184, '2018-03-08 10:00:00', 63, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(185, '2018-03-08 10:05:00', 65, 32, 25000, '', 'Setoran', 'D', 1, '2019-04-30 13:59:00', 'ADMIN', '', '', ''),
(186, '2018-03-08 10:10:00', 66, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(187, '2018-03-08 10:10:00', 68, 32, 55000, '', 'Setoran', 'D', 1, '2019-04-24 10:15:00', 'admin', '', '', ''),
(188, '2018-03-08 10:10:00', 68, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(189, '2018-03-08 10:10:00', 68, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(190, '2018-03-08 10:20:00', 69, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(191, '2018-03-08 10:20:00', 69, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(192, '2018-03-08 10:20:00', 53, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(193, '2018-03-08 10:25:00', 54, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(194, '2018-03-08 10:30:00', 55, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(195, '2018-03-08 10:30:00', 57, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(196, '2018-03-08 10:35:00', 58, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(197, '2018-03-08 10:40:00', 59, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(198, '2018-03-08 10:45:00', 43, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(199, '2018-03-08 10:45:00', 45, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(200, '2018-03-08 10:50:00', 49, 32, 30000, '', 'Setoran', 'D', 1, '2019-04-30 13:58:00', 'ADMIN', '', '', ''),
(201, '2018-03-08 10:50:00', 38, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(202, '2018-03-08 11:05:00', 37, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(203, '2018-03-08 11:05:00', 37, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(204, '2018-03-08 11:05:00', 37, 32, 40000, '', 'Setoran', 'D', 1, '2019-04-24 17:19:00', 'admin', '', '', ''),
(205, '2018-03-01 09:55:00', 30, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(206, '2018-03-01 09:55:00', 31, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(207, '2018-03-01 09:55:00', 32, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(208, '2018-03-01 10:15:00', 62, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(209, '2018-03-01 10:30:00', 58, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(210, '2018-03-01 10:30:00', 36, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(211, '2018-03-01 10:30:00', 48, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(212, '2018-03-01 10:35:00', 60, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(213, '2018-03-01 10:35:00', 33, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(214, '2018-03-01 10:35:00', 56, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(215, '2018-03-01 10:40:00', 34, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(216, '2018-03-01 10:40:00', 35, 32, 15000, '', 'Setoran', 'D', 1, '2019-04-25 10:59:00', 'admin', '', '', ''),
(217, '2018-03-01 10:55:00', 21, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(218, '2018-03-01 11:15:00', 32, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(219, '2018-03-01 11:20:00', 58, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(220, '2018-03-08 13:35:00', 69, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(221, '2018-03-08 13:40:00', 70, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(222, '2018-03-08 13:40:00', 70, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(223, '2018-03-08 13:40:00', 70, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(224, '2018-03-08 13:40:00', 71, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(226, '2018-03-08 13:40:00', 71, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(227, '2018-03-08 13:40:00', 71, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(228, '2018-03-08 13:45:00', 72, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(229, '2018-03-08 13:45:00', 72, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(230, '2018-03-08 13:50:00', 73, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(231, '2018-03-08 13:50:00', 73, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(232, '2018-03-08 13:50:00', 73, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(233, '2018-03-08 13:50:00', 75, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(234, '2018-03-08 13:50:00', 75, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(235, '2018-03-08 13:55:00', 76, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(236, '2018-03-08 13:55:00', 76, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(237, '2018-03-08 13:55:00', 76, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(238, '2018-03-08 14:00:00', 74, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(239, '2018-03-08 14:00:00', 74, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(240, '2018-03-08 14:00:00', 74, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(241, '2018-03-08 14:00:00', 67, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(242, '2018-03-08 14:00:00', 67, 32, 120000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(243, '2018-03-08 14:00:00', 67, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(244, '2018-04-02 11:00:00', 128, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(245, '2018-03-08 14:05:00', 79, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(246, '2018-03-08 14:05:00', 79, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(247, '2018-03-08 14:05:00', 79, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(248, '2018-03-08 16:05:00', 30, 32, 22812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(249, '2018-03-08 16:05:00', 25, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(250, '2018-03-08 16:15:00', 26, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(251, '2018-03-08 16:20:00', 44, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(252, '2018-03-08 16:20:00', 27, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(253, '2018-03-08 16:30:00', 23, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(254, '2018-03-08 16:30:00', 22, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(255, '2018-03-08 16:30:00', 31, 32, 6250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(256, '2018-03-08 16:30:00', 24, 32, 70000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(257, '2018-03-08 16:35:00', 29, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(260, '2018-02-12 18:25:00', 2, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(261, '2018-03-08 13:10:00', 62, 32, 12812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(262, '2018-03-08 13:50:00', 32, 32, 22812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(263, '2018-03-08 14:00:00', 33, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(264, '2018-03-08 14:05:00', 34, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(266, '2018-03-08 14:55:00', 35, 32, 18437, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(267, '2018-03-08 14:55:00', 21, 32, 26250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(268, '2018-03-08 15:20:00', 36, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'ADMIN', '', '', ''),
(269, '2018-03-12 12:00:00', 1, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(270, '2018-03-12 13:00:00', 1, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(271, '2018-03-12 08:10:00', 2, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(272, '2018-03-12 13:10:00', 2, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(273, '2018-03-12 13:10:00', 3, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(274, '2018-03-12 13:10:00', 3, 32, 40000, '', 'Setoran', 'D', 1, '2019-05-02 14:04:00', 'admin', '', '', ''),
(275, '2018-03-12 14:05:00', 4, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(276, '2018-03-12 14:05:00', 4, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(277, '2018-03-12 14:05:00', 6, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(278, '2018-03-12 14:05:00', 6, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(279, '2018-03-12 14:05:00', 7, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(280, '2018-03-12 14:05:00', 7, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(281, '2018-03-12 14:10:00', 8, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(282, '2018-03-12 14:10:00', 8, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(283, '2018-03-12 14:10:00', 9, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(284, '2018-03-12 14:10:00', 9, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(285, '2018-03-12 14:10:00', 10, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(286, '2018-03-12 14:15:00', 10, 32, 50000, '', 'Setoran', 'D', 1, '2019-05-02 14:20:00', 'admin', '', '', ''),
(287, '2018-03-12 14:20:00', 11, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(288, '2018-03-12 14:20:00', 11, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(289, '2018-03-12 14:25:00', 12, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(290, '2018-03-12 14:25:00', 12, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(291, '2018-03-12 14:35:00', 13, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(292, '2018-03-12 14:35:00', 13, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(293, '2018-03-12 15:05:00', 15, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(294, '2018-03-12 15:05:00', 15, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(295, '2018-03-12 15:05:00', 16, 32, 10000, '', 'Setoran', 'D', 1, '2019-05-02 15:10:00', 'admin', '', '', ''),
(296, '2018-03-12 15:05:00', 16, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(297, '2018-03-12 15:10:00', 17, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(298, '2018-03-12 15:10:00', 17, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(299, '2018-03-12 15:10:00', 18, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(300, '2018-03-12 15:10:00', 18, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(301, '2018-03-12 15:15:00', 19, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(302, '2018-03-12 15:15:00', 19, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(303, '2018-03-12 15:15:00', 80, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(304, '2018-03-12 15:15:00', 80, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(305, '2018-03-12 15:15:00', 80, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(306, '2018-03-12 15:20:00', 81, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(307, '2018-03-12 15:20:00', 81, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(308, '2018-03-12 15:20:00', 81, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(309, '2018-03-12 15:25:00', 82, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(310, '2018-03-12 15:25:00', 82, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(311, '2018-03-12 15:25:00', 82, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(312, '2018-03-12 15:30:00', 67, 32, 150000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(313, '2018-03-14 00:00:00', 29, 32, 162292, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(314, '2018-03-14 00:00:00', 30, 32, 15624, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(315, '2018-03-14 00:00:00', 23, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(316, '2018-03-14 00:00:00', 24, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(317, '2018-03-14 00:00:00', 25, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(318, '2018-03-14 00:00:00', 26, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(319, '2018-03-14 00:00:00', 27, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(320, '2018-03-14 00:00:00', 28, 32, 40000, '', 'Setoran', 'D', 1, '2019-05-06 13:48:00', 'admin', '', '', ''),
(321, '2018-03-14 00:00:00', 31, 32, 6250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(322, '2018-03-14 00:00:00', 32, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(323, '2018-03-14 00:00:00', 33, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(324, '2018-03-14 00:00:00', 34, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(325, '2018-03-14 00:00:00', 35, 32, 18437, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(326, '2018-03-14 00:00:00', 36, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(327, '2018-03-14 00:00:00', 38, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(328, '2018-03-14 00:00:00', 43, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(329, '2018-03-14 00:00:00', 44, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(330, '2018-03-14 00:00:00', 45, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(331, '2018-03-14 00:00:00', 49, 32, 22812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(332, '2018-03-14 00:00:00', 51, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(333, '2018-03-14 00:00:00', 53, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(334, '2018-03-14 00:00:00', 54, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(335, '2018-03-14 00:00:00', 58, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(336, '2018-03-14 00:00:00', 59, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(337, '2018-03-14 00:00:00', 60, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(338, '2018-03-14 00:00:00', 62, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(339, '2018-03-14 00:00:00', 63, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(340, '2018-03-14 00:00:00', 65, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(341, '2018-03-14 00:00:00', 69, 32, 5000, '', 'Setoran', 'D', 1, '2019-05-06 13:50:00', 'admin', '', '', ''),
(342, '0000-00-00 00:00:00', 70, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(343, '2018-03-14 00:00:00', 70, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(344, '2018-03-14 00:00:00', 71, 32, 45625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(345, '2018-03-14 00:00:00', 72, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(346, '2018-03-14 00:00:00', 73, 32, 15625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(347, '2018-03-14 00:00:00', 75, 32, 15000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(348, '2018-03-14 00:00:00', 76, 32, 130000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(349, '2018-03-14 00:00:00', 77, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(350, '2018-03-14 00:00:00', 78, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(351, '2018-03-14 00:00:00', 83, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(352, '2018-03-14 00:00:00', 83, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(353, '2018-03-14 00:00:00', 83, 32, 18000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(354, '2018-03-14 00:00:00', 84, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(355, '2018-03-14 00:00:00', 84, 41, 10000, '', 'Setoran', 'D', 1, '2019-05-06 13:11:00', 'admin', '', '', ''),
(356, '2018-03-14 00:00:00', 84, 32, 68000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(357, '2018-03-19 00:00:00', 85, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(358, '2018-03-19 00:00:00', 85, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(359, '2018-03-19 00:00:00', 86, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(360, '2018-03-19 00:00:00', 86, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(361, '2018-03-19 00:00:00', 87, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(362, '2018-03-19 00:00:00', 87, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(363, '2018-03-19 00:00:00', 87, 32, 18000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(364, '2018-03-20 00:00:00', 67, 32, 150000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(365, '2018-03-20 00:00:00', 85, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(366, '2018-03-20 00:00:00', 98, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(367, '2018-03-20 00:00:00', 98, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(368, '2018-03-20 00:00:00', 88, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(369, '2018-03-20 00:00:00', 88, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(370, '2018-03-20 00:00:00', 89, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(371, '2018-03-20 00:00:00', 89, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(372, '2018-03-20 00:00:00', 90, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(373, '2018-03-20 00:00:00', 90, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(374, '2018-03-20 00:00:00', 91, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(375, '2018-03-20 00:00:00', 91, 41, 10000, '', 'Setoran', 'D', 1, '2019-05-06 14:28:00', 'admin', '', '', ''),
(376, '2018-03-20 00:00:00', 92, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(377, '2018-03-20 00:00:00', 92, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(378, '2018-03-20 00:00:00', 93, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(379, '2018-03-20 00:00:00', 93, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(380, '2018-03-20 00:00:00', 94, 32, 18000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(381, '2018-03-20 00:00:00', 94, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(382, '2018-03-20 00:00:00', 94, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(383, '2018-03-20 00:00:00', 95, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(384, '2018-03-20 00:00:00', 95, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(385, '2018-03-20 00:00:00', 96, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(386, '2018-03-20 00:00:00', 96, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(387, '2018-03-20 00:00:00', 97, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(388, '2018-03-20 00:00:00', 97, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(389, '2018-03-27 00:00:00', 85, 32, 50000, '', 'Setoran', 'D', 1, '2019-05-06 14:45:00', 'admin', '', '', ''),
(390, '2018-03-29 00:00:00', 89, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(391, '2018-03-29 00:00:00', 93, 32, 50000, '', 'Setoran', 'D', 1, '2019-05-07 10:17:00', 'admin', '', '', ''),
(392, '2018-03-29 00:00:00', 95, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(393, '2018-03-29 00:00:00', 102, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(394, '2018-03-29 00:00:00', 102, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(395, '2018-03-29 00:00:00', 103, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(396, '2018-03-29 00:00:00', 103, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(397, '2018-03-29 00:00:00', 104, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(398, '2018-03-29 00:00:00', 104, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(399, '2018-03-29 00:00:00', 105, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(400, '2018-03-29 00:00:00', 105, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(401, '2018-03-29 00:00:00', 106, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(402, '2018-03-29 00:00:00', 106, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(403, '2018-03-29 00:00:00', 107, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(404, '2018-03-29 00:00:00', 107, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(405, '2018-03-29 00:00:00', 108, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(406, '2018-03-29 00:00:00', 108, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(407, '2018-03-29 00:00:00', 109, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(408, '2018-03-29 00:00:00', 109, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(409, '0000-00-00 00:00:00', 110, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(410, '2018-03-29 00:00:00', 110, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(411, '2018-03-29 00:00:00', 105, 32, 18000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(412, '2018-03-21 00:00:00', 21, 32, 52500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(413, '2018-03-21 00:00:00', 22, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(414, '2018-03-21 00:00:00', 23, 32, 30000, '', 'Setoran', 'D', 1, '2019-05-07 14:32:00', 'admin', '', '', ''),
(415, '2018-03-21 00:00:00', 24, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(416, '2018-03-21 00:00:00', 25, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(417, '2018-03-21 00:00:00', 26, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(418, '2018-03-21 00:00:00', 28, 32, 15625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(419, '2018-03-21 00:00:00', 30, 32, 25624, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(420, '2018-03-21 00:00:00', 31, 32, 56250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(421, '2018-03-21 00:00:00', 32, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(422, '2018-03-21 00:00:00', 33, 32, 2812, '', 'Setoran', 'D', 1, '2019-05-07 13:59:00', 'admin', '', '', ''),
(423, '2018-03-21 00:00:00', 34, 32, 2812, '', 'Setoran', 'D', 1, '2019-05-07 14:36:00', 'admin', '', '', ''),
(424, '2018-03-21 00:00:00', 35, 32, 28437, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(425, '2018-03-21 00:00:00', 36, 32, 5624, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(426, '2018-06-04 00:00:00', 119, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', 'DARIYAH', '0121', ''),
(427, '2018-06-04 00:00:00', 123, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(428, '2018-05-02 00:00:00', 125, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 14:47:00', 'admin', '', '', ''),
(429, '2018-03-21 00:00:00', 37, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(430, '2018-06-04 00:00:00', 127, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 14:24:00', 'admin', '', '', ''),
(431, '2018-06-04 00:00:00', 129, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 14:21:00', 'admin', '', '', ''),
(432, '0000-00-00 00:00:00', 130, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(433, '2018-03-21 00:00:00', 38, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(434, '2018-03-21 00:00:00', 43, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(435, '2018-03-21 00:00:00', 45, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(436, '2018-03-21 00:00:00', 46, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(437, '2018-03-21 00:00:00', 47, 32, 8000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(438, '2018-06-04 00:00:00', 140, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(439, '2018-03-21 00:00:00', 49, 32, 25312, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(440, '2018-06-04 00:00:00', 141, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(441, '0000-00-00 00:00:00', 142, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(442, '2018-06-04 00:00:00', 143, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(443, '2018-03-21 00:00:00', 50, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(444, '2018-06-04 00:00:00', 144, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 14:10:00', 'admin', '', '', ''),
(445, '2018-03-21 00:00:00', 51, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(446, '2018-06-04 00:00:00', 145, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 12:28:00', 'admin', '', '', ''),
(447, '2018-06-04 00:00:00', 154, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(448, '2018-03-21 00:00:00', 54, 32, 5000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(449, '2018-06-04 00:00:00', 155, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(450, '2018-06-04 00:00:00', 156, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(451, '2018-03-21 00:00:00', 53, 32, 22813, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(452, '2018-06-04 00:00:00', 157, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(453, '2018-06-04 00:00:00', 199, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(454, '2018-03-21 00:00:00', 57, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(455, '2018-03-21 00:00:00', 58, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(456, '2018-06-04 10:00:00', 200, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 12:51:00', 'admin', '', '', ''),
(457, '2018-07-02 00:00:00', 438, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(458, '2018-07-02 00:00:00', 438, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(459, '2018-03-21 00:00:00', 99, 32, 20000, '', 'Setoran', 'D', 1, '2019-05-07 14:16:00', 'admin', '', '', ''),
(460, '2018-07-02 00:00:00', 404, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(461, '2018-07-02 00:00:00', 404, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(462, '2018-07-02 00:00:00', 405, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(463, '2018-07-02 00:00:00', 405, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(464, '2018-07-02 00:00:00', 406, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(465, '2018-07-02 00:00:00', 406, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(467, '2018-07-02 00:00:00', 407, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(468, '2018-07-02 00:00:00', 407, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(469, '2018-07-02 00:00:00', 408, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(470, '2018-07-02 00:00:00', 408, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(471, '2018-06-04 10:00:00', 201, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 12:54:00', 'admin', '', '', '');
INSERT INTO `tbl_trans_sp` (`id`, `tgl_transaksi`, `anggota_id`, `jenis_id`, `jumlah`, `keterangan`, `akun`, `dk`, `kas_id`, `update_data`, `user_name`, `nama_penyetor`, `no_identitas`, `alamat`) VALUES
(472, '2018-07-02 00:00:00', 409, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(473, '2018-07-02 00:00:00', 409, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(474, '2018-07-02 00:00:00', 410, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(475, '2018-07-02 00:00:00', 410, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(476, '2018-06-04 00:00:00', 202, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(477, '2018-07-02 00:00:00', 411, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(478, '2018-07-02 00:00:00', 411, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(479, '2018-07-02 00:00:00', 412, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(480, '2018-07-02 00:00:00', 412, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(481, '2018-06-04 00:00:00', 203, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(483, '2018-07-02 00:00:00', 413, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(484, '2018-05-02 00:00:00', 37, 32, 20000, '', 'Setoran', 'D', 1, '2019-05-07 12:59:00', 'admin', '', '', ''),
(485, '2018-07-02 00:00:00', 413, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(486, '2018-07-02 00:00:00', 414, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(487, '2018-07-02 00:00:00', 414, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(488, '2018-06-04 00:00:00', 204, 32, 5625, '', 'Setoran', 'D', 1, '2019-05-07 13:00:00', 'admin', '', '', ''),
(489, '2018-05-02 00:00:00', 51, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(490, '2018-07-02 00:00:00', 415, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(491, '2018-03-21 00:00:00', 59, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(492, '2018-07-02 00:00:00', 415, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(493, '2018-06-04 00:00:00', 220, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(494, '2018-07-02 00:00:00', 416, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(495, '2018-03-21 00:00:00', 60, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(496, '2018-07-02 00:00:00', 416, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(497, '2018-05-02 00:00:00', 116, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(498, '2018-07-02 00:00:00', 417, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(499, '2018-07-02 00:00:00', 417, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(500, '2018-06-04 00:00:00', 221, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(501, '2018-07-02 00:00:00', 418, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(502, '2018-05-02 00:00:00', 244, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(503, '2018-07-02 00:00:00', 418, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(504, '2018-06-04 00:00:00', 222, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(505, '2018-05-02 00:00:00', 245, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(506, '2018-07-02 00:00:00', 419, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(507, '2018-07-02 00:00:00', 419, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(508, '2018-07-02 00:00:00', 419, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(509, '2018-06-04 00:00:00', 223, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(510, '2018-05-02 00:00:00', 246, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(511, '2018-06-04 00:00:00', 224, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(512, '2018-05-02 00:00:00', 243, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(513, '2018-05-02 00:00:00', 242, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(514, '2018-06-04 00:00:00', 236, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(515, '2018-05-02 00:00:00', 240, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(516, '2018-06-04 00:00:00', 237, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(517, '2018-05-02 00:00:00', 236, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(518, '2018-05-02 00:00:00', 237, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(519, '2018-05-02 00:00:00', 239, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(520, '2018-05-02 00:00:00', 241, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(521, '2018-06-04 00:00:00', 239, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(522, '2018-05-02 00:00:00', 238, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(523, '2018-06-04 00:00:00', 241, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(524, '2018-03-21 00:00:00', 101, 41, 10000, '', 'Setoran', 'D', 1, '2019-05-07 14:18:00', 'admin', '', '', ''),
(525, '2018-06-04 00:00:00', 242, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(526, '2018-05-02 00:00:00', 65, 32, 30000, '', 'Setoran', 'D', 1, '2019-05-07 13:19:00', 'admin', '', '', ''),
(527, '2018-05-02 00:00:00', 28, 32, 10625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(528, '2018-05-02 00:00:00', 66, 32, 105000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(529, '2018-05-02 00:00:00', 149, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(531, '2018-06-04 00:00:00', 269, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(532, '2018-05-02 00:00:00', 159, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(533, '2018-03-21 00:00:00', 63, 32, 50000, '', 'Setoran', 'D', 1, '2019-05-07 13:45:00', 'admin', '', '', ''),
(534, '2018-06-04 00:00:00', 271, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(535, '2018-05-02 00:00:00', 38, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(536, '2018-06-04 00:00:00', 281, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(537, '2018-06-04 00:00:00', 282, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(538, '2018-05-02 00:00:00', 42, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(539, '2018-06-04 00:00:00', 283, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(540, '2018-06-04 00:00:00', 284, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(541, '2018-06-04 00:00:00', 285, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(542, '2018-03-21 00:00:00', 64, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(543, '2018-06-04 00:00:00', 286, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(544, '2018-03-21 00:00:00', 66, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(545, '2018-06-04 00:00:00', 287, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(546, '2018-03-21 00:00:00', 68, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(547, '2018-03-21 00:00:00', 69, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(548, '2018-06-04 00:00:00', 288, 32, 125, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(549, '2018-03-21 00:00:00', 70, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(550, '2018-06-04 00:00:00', 289, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(551, '2018-03-21 00:00:00', 71, 32, 45625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(552, '2018-06-04 00:00:00', 301, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(553, '2018-03-21 00:00:00', 72, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(554, '2018-06-04 00:00:00', 302, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(555, '2018-06-04 00:00:00', 303, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(556, '2018-03-21 00:00:00', 73, 32, 15625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(557, '2018-06-04 00:00:00', 304, 32, 11000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(558, '2018-03-21 00:00:00', 74, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(559, '2018-05-02 00:00:00', 68, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(560, '2018-06-04 00:00:00', 305, 32, 11000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(561, '2018-03-21 00:00:00', 75, 32, 2813, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(562, '2018-05-02 00:00:00', 70, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(563, '2018-06-04 00:00:00', 306, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(564, '2018-05-02 00:00:00', 153, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(565, '2018-05-02 00:00:00', 215, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(566, '2018-06-04 00:00:00', 307, 32, 11000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(567, '2018-05-02 00:00:00', 71, 32, 35625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(568, '2018-05-02 00:00:00', 43, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(569, '2018-05-02 00:00:00', 86, 32, 80000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(570, '2018-05-02 00:00:00', 44, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(571, '2018-06-04 00:00:00', 308, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(572, '2018-05-02 00:00:00', 232, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(573, '2018-06-04 00:00:00', 309, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(574, '2018-05-02 00:00:00', 233, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(575, '2018-05-02 00:00:00', 73, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(576, '2018-05-02 00:00:00', 234, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(577, '2018-06-04 00:00:00', 310, 32, 11000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(578, '2018-05-02 00:00:00', 235, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(579, '2018-06-04 00:00:00', 311, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(580, '2018-05-02 00:00:00', 33, 32, 2812, '', 'Setoran', 'D', 1, '2019-05-11 15:14:00', 'admin', '', '', ''),
(581, '2018-05-02 00:00:00', 45, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(582, '2018-06-04 00:00:00', 312, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(583, '2018-05-02 00:00:00', 78, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(584, '2018-05-02 00:00:00', 74, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(585, '2018-06-04 00:00:00', 313, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(586, '2018-05-02 00:00:00', 54, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(587, '2018-05-02 00:00:00', 21, 32, 2500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(588, '2018-06-04 00:00:00', 314, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(589, '2018-05-02 00:00:00', 75, 32, 2813, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(590, '2018-06-04 00:00:00', 315, 32, 1000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(591, '2018-05-02 00:00:00', 22, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(592, '2018-05-02 00:00:00', 46, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(593, '2018-06-04 00:00:00', 316, 32, 11000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(594, '2018-05-02 00:00:00', 76, 32, 26875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(595, '2018-06-04 00:00:00', 317, 32, 2000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(596, '2018-05-02 00:00:00', 31, 32, 6250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(597, '2018-05-02 00:00:00', 24, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(598, '2018-05-02 00:00:00', 144, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(599, '2018-06-04 00:00:00', 318, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(600, '2018-05-02 00:00:00', 155, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(601, '2018-06-04 00:00:00', 319, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(602, '2018-05-02 00:00:00', 156, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(603, '2018-03-21 00:00:00', 76, 32, 36875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(604, '2018-06-04 00:00:00', 320, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(605, '2018-05-02 00:00:00', 157, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(606, '2018-06-04 00:00:00', 321, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(607, '2018-05-02 00:00:00', 199, 32, 10625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(609, '2018-06-04 00:00:00', 322, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(610, '2018-03-21 00:00:00', 83, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(611, '2018-06-04 00:00:00', 323, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(612, '2018-03-21 00:00:00', 84, 32, 112000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(613, '2018-06-04 00:00:00', 324, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(614, '2018-03-21 00:00:00', 99, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(615, '2018-03-21 00:00:00', 99, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(616, '2018-03-21 00:00:00', 100, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(617, '2018-03-21 00:00:00', 100, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(618, '2018-05-02 00:00:00', 141, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(619, '2018-03-21 00:00:00', 101, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(620, '2018-03-21 00:00:00', 101, 32, 18000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(621, '2018-05-02 00:00:00', 200, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(622, '2018-05-02 00:00:00', 140, 32, 10625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(623, '2018-05-02 00:00:00', 201, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(624, '2018-06-04 00:00:00', 325, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(625, '2018-06-04 00:00:00', 326, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(626, '2018-05-02 00:00:00', 129, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(627, '2018-05-02 00:00:00', 127, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(628, '2018-05-02 00:00:00', 147, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(629, '2018-05-02 00:00:00', 145, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(630, '2018-05-02 00:00:00', 154, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(631, '2018-05-02 00:00:00', 121, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(632, '2018-05-02 00:00:00', 202, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(633, '2018-06-04 00:00:00', 327, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(634, '2018-05-02 00:00:00', 119, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(635, '2018-06-04 00:00:00', 402, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(636, '2018-05-02 00:00:00', 203, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(637, '2018-06-04 00:00:00', 401, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(638, '2018-06-04 00:00:00', 400, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(639, '2018-06-04 00:00:00', 399, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(640, '2018-06-04 00:00:00', 398, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(642, '2018-05-02 00:00:00', 118, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(643, '2018-05-02 00:00:00', 220, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(644, '2018-06-04 00:00:00', 397, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(645, '2018-05-02 00:00:00', 222, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(646, '2018-06-04 00:00:00', 396, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(647, '2018-05-02 00:00:00', 223, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(648, '2018-05-02 00:00:00', 126, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(649, '2018-06-04 00:00:00', 395, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(650, '2018-06-04 00:00:00', 125, 32, 625, '', 'Setoran', 'D', 1, '2019-05-07 14:47:00', 'admin', '', '', ''),
(651, '2018-05-02 00:00:00', 120, 32, 5625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(652, '2018-06-04 00:00:00', 394, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(653, '2018-05-02 00:00:00', 123, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(654, '2018-05-02 00:00:00', 243, 32, 3000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(655, '2018-05-02 00:00:00', 236, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(656, '2018-06-04 00:00:00', 85, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(657, '2018-05-02 00:00:00', 85, 32, 10625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(658, '2018-06-04 00:00:00', 86, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(659, '2018-05-02 00:00:00', 32, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(660, '2018-05-02 00:00:00', 49, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(661, '2018-06-04 00:00:00', 87, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(662, '2018-05-02 00:00:00', 57, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(663, '2018-06-04 00:00:00', 98, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(664, '2018-05-02 00:00:00', 50, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(665, '2018-05-02 00:00:00', 62, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(666, '2018-06-04 00:00:00', 117, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(667, '2018-05-02 00:00:00', 216, 32, 15625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(668, '2018-06-04 00:00:00', 151, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(669, '2018-05-02 00:00:00', 83, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(670, '2018-06-04 00:00:00', 152, 32, 15000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(671, '2018-05-02 00:00:00', 63, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(672, '2018-06-04 00:00:00', 153, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(673, '2018-05-02 00:00:00', 35, 32, 8437, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(674, '2018-05-02 00:00:00', 64, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(675, '2018-05-02 00:00:00', 36, 32, 1875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(676, '2018-05-02 00:00:00', 99, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(677, '2018-05-02 00:00:00', 37, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(678, '2018-05-02 00:00:00', 51, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(680, '2018-05-02 00:00:00', 116, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(681, '2018-05-02 00:00:00', 101, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(682, '2018-06-06 00:00:00', 99, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(683, '2018-05-02 00:00:00', 28, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(684, '2018-06-06 00:00:00', 37, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(685, '2018-05-02 00:00:00', 66, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(686, '2018-05-02 00:00:00', 149, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(687, '2018-06-06 00:00:00', 116, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(688, '2018-05-02 00:00:00', 158, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(689, '2018-06-06 00:00:00', 28, 32, 31250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(690, '2018-06-06 00:00:00', 149, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(691, '2018-05-02 00:00:00', 159, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(692, '2018-06-06 00:00:00', 101, 32, 22000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(693, '2018-05-02 00:00:00', 38, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(694, '2018-05-02 00:00:00', 151, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(695, '2018-06-06 00:00:00', 66, 32, 90000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(696, '2018-05-02 00:00:00', 68, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(697, '2018-05-02 00:00:00', 70, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(698, '2018-06-06 00:00:00', 158, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(699, '2018-05-02 00:00:00', 215, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(700, '2018-06-06 00:00:00', 29, 32, 35000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(701, '2018-05-02 00:00:00', 71, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(702, '2018-06-06 00:00:00', 52, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(703, '2018-05-02 00:00:00', 43, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(704, '2018-06-06 00:00:00', 113, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(705, '2018-05-02 00:00:00', 216, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(706, '2018-06-06 00:00:00', 38, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(707, '2018-05-02 00:00:00', 86, 41, 20000, '', 'Setoran', 'D', 1, '2019-05-08 09:39:00', 'admin', '', '', ''),
(708, '2018-05-02 00:00:00', 244, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(709, '2018-05-02 00:00:00', 245, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(710, '2018-05-02 00:00:00', 246, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(711, '2018-06-06 00:00:00', 68, 32, 38250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(712, '2018-05-02 00:00:00', 27, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(715, '2018-06-06 00:00:00', 70, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(716, '2018-05-02 00:00:00', 78, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(717, '2018-06-06 00:00:00', 42, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(718, '2018-05-02 00:00:00', 74, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(719, '2018-06-06 00:00:00', 25, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(720, '2018-05-02 00:00:00', 21, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(721, '2018-05-02 00:00:00', 55, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(722, '2018-06-06 00:00:00', 159, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(723, '2018-05-02 00:00:00', 75, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(724, '2018-06-06 00:00:00', 215, 32, 31250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(725, '2018-06-06 00:00:00', 43, 32, 15000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(726, '2018-05-02 00:00:00', 22, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(727, '2018-05-02 00:00:00', 46, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(728, '2018-06-06 00:00:00', 72, 32, 90000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(729, '2018-06-06 00:00:00', 26, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(730, '2018-05-02 00:00:00', 76, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(731, '2018-06-06 00:00:00', 216, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(732, '2018-05-02 00:00:00', 24, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(733, '2018-05-02 00:00:00', 31, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(734, '2018-06-06 00:00:00', 231, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(735, '2018-05-02 00:00:00', 238, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(736, '2018-05-02 00:00:00', 236, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(737, '2018-06-06 00:00:00', 232, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(738, '2018-06-06 00:00:00', 233, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(739, '2018-05-02 00:00:00', 237, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(740, '2018-06-06 00:00:00', 73, 32, 25000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(741, '2018-05-02 00:00:00', 239, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(742, '2018-06-06 00:00:00', 235, 32, 26875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(743, '2018-05-02 00:00:00', 240, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(744, '2018-05-02 00:00:00', 241, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(745, '2018-06-06 00:00:00', 44, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(746, '2018-05-02 00:00:00', 242, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(748, '2018-05-02 00:00:00', 243, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(749, '2018-06-06 00:00:00', 53, 32, 11250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(750, '2018-05-02 00:00:00', 48, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(751, '2018-06-06 00:00:00', 246, 32, 1875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(752, '2018-05-02 00:00:00', 85, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(753, '2018-06-06 00:00:00', 275, 32, 18125, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(754, '2018-05-02 00:00:00', 57, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(755, '2018-05-02 00:00:00', 50, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(756, '2018-06-06 00:00:00', 276, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(757, '2018-05-02 00:00:00', 83, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(758, '2018-06-06 00:00:00', 45, 32, 5624, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(759, '2018-05-02 00:00:00', 63, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(760, '2018-06-06 00:00:00', 277, 32, 2500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(761, '2018-05-02 00:00:00', 35, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(762, '2018-06-06 00:00:00', 74, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(763, '2018-05-02 00:00:00', 64, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(765, '2018-05-02 00:00:00', 36, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(766, '2018-06-06 00:00:00', 21, 32, 2500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(767, '2018-06-06 00:00:00', 75, 32, 6250, '', 'Setoran', 'D', 1, '2019-05-10 14:47:00', 'admin', '', '', ''),
(768, '2018-06-06 00:00:00', 297, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(769, '2018-03-28 00:00:00', 22, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(770, '2018-06-06 00:00:00', 22, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(771, '2018-03-28 00:00:00', 23, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(772, '2018-06-06 00:00:00', 298, 32, 500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(773, '2018-03-28 00:00:00', 24, 32, 150000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(774, '2018-03-28 00:00:00', 25, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(775, '2018-03-28 00:00:00', 26, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(776, '2018-06-06 00:00:00', 35, 32, 12440, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(777, '2018-03-28 00:00:00', 28, 32, 20625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(778, '2018-03-28 00:00:00', 31, 32, 6250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(779, '2018-03-28 00:00:00', 32, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(780, '2018-03-28 00:00:00', 33, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(781, '2018-03-28 00:00:00', 34, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(782, '2018-03-28 00:00:00', 35, 32, 438, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(783, '2018-06-06 00:00:00', 51, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(784, '2018-06-06 00:00:00', 64, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(785, '2018-06-06 00:00:00', 328, 32, 42000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(786, '2018-06-06 00:00:00', 23, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(787, '2018-06-06 00:00:00', 329, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(788, '2018-06-06 00:00:00', 330, 32, 5500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(789, '2018-06-06 00:00:00', 1, 32, 70000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(790, '2018-06-06 00:00:00', 81, 32, 22500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(791, '2018-06-06 00:00:00', 80, 32, 22500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(792, '2018-06-06 00:00:00', 47, 32, 5311, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(793, '2018-06-06 00:00:00', 331, 32, 2000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(794, '2018-06-06 00:00:00', 76, 32, 36875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(795, '2018-06-06 00:00:00', 387, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(796, '2018-06-06 00:00:00', 24, 32, 1875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(797, '2018-06-06 00:00:00', 31, 32, 6250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(798, '2018-06-06 00:00:00', 48, 32, 25000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(799, '2018-06-06 00:00:00', 54, 32, 2000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(800, '2018-06-06 00:00:00', 49, 32, 12812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(801, '2018-06-06 00:00:00', 57, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(802, '2018-06-06 00:00:00', 78, 32, 90000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(803, '2018-06-06 00:00:00', 63, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(804, '2018-06-06 00:00:00', 83, 32, 22500, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(805, '2018-05-02 00:00:00', 101, 32, 8438, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(806, '2018-06-06 00:00:00', 84, 32, 90000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(807, '2018-05-02 00:00:00', 99, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(808, '2018-05-02 00:00:00', 143, 32, 625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(809, '2018-06-06 00:00:00', 99, 41, 10000, '', 'Setoran', 'D', 1, '2019-05-09 10:09:00', 'admin', '', '', ''),
(810, '2018-06-06 00:00:00', 37, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(811, '2018-06-06 00:00:00', 116, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(812, '2018-06-06 00:00:00', 28, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(813, '2018-03-28 00:00:00', 37, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(814, '2018-03-28 00:00:00', 38, 32, 15000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(815, '2018-06-06 00:00:00', 149, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(816, '2018-06-06 00:00:00', 101, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(817, '2018-03-28 00:00:00', 43, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(818, '2018-06-06 00:00:00', 66, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(819, '2018-06-06 00:00:00', 31, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(820, '2018-03-28 00:00:00', 45, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(824, '2018-06-06 00:00:00', 158, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(825, '2018-06-06 00:00:00', 29, 41, 20000, '', 'Setoran', 'D', 1, '2019-05-09 10:25:00', 'admin', '', '', ''),
(826, '2018-06-06 00:00:00', 52, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(827, '2018-06-06 00:00:00', 113, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(828, '2018-06-06 00:00:00', 38, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(829, '2018-06-06 00:00:00', 68, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(830, '2018-06-06 00:00:00', 25, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(831, '2018-06-06 00:00:00', 159, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(832, '2018-06-06 00:00:00', 215, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(833, '2018-06-06 00:00:00', 43, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(834, '2018-06-06 00:00:00', 72, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(835, '2018-06-06 00:00:00', 26, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(836, '2018-06-06 00:00:00', 216, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(837, '2018-06-06 00:00:00', 231, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(838, '2018-06-06 00:00:00', 73, 41, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(839, '2018-06-06 00:00:00', 235, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(845, '2018-03-28 00:00:00', 49, 32, 25312, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(846, '2018-03-28 00:00:00', 50, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(849, '2018-06-06 00:00:00', 44, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(850, '2018-03-28 00:00:00', 53, 32, 22813, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(851, '2018-06-06 00:00:00', 53, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(852, '2018-03-28 00:00:00', 54, 32, 2812, '', 'Setoran', 'D', 1, '2019-05-09 11:37:00', 'admin', '', '', ''),
(853, '2018-06-06 00:00:00', 275, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(854, '2018-06-06 00:00:00', 45, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(855, '2018-06-06 00:00:00', 277, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(856, '2018-06-06 00:00:00', 75, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(857, '2018-03-28 00:00:00', 57, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(858, '2018-03-28 00:00:00', 58, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(859, '2018-03-28 00:00:00', 59, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(860, '2018-06-06 00:00:00', 22, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(861, '2018-06-06 00:00:00', 298, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(862, '2018-06-06 00:00:00', 51, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(863, '2018-06-06 00:00:00', 328, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(864, '2018-03-28 00:00:00', 60, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(865, '2018-03-28 00:00:00', 62, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(866, '2018-06-06 00:00:00', 23, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(867, '2018-06-06 00:00:00', 81, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(868, '2018-06-06 00:00:00', 1, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(869, '2018-06-06 00:00:00', 80, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(870, '2018-06-06 00:00:00', 47, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(871, '2018-06-06 00:00:00', 76, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(872, '2018-06-06 00:00:00', 387, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(873, '2018-06-06 00:00:00', 24, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(874, '2018-06-06 00:00:00', 48, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(875, '2018-06-06 00:00:00', 49, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(876, '2018-06-06 00:00:00', 57, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(877, '2018-06-06 00:00:00', 78, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(878, '2018-06-06 00:00:00', 63, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(879, '2018-06-06 00:00:00', 83, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(880, '2018-06-06 00:00:00', 35, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(881, '2018-06-06 00:00:00', 84, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(882, '2018-03-28 00:00:00', 64, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(883, '2018-03-28 00:00:00', 65, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(884, '2018-03-28 00:00:00', 66, 32, 40000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(885, '2018-03-28 00:00:00', 68, 32, 100000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(886, '2018-03-28 00:00:00', 70, 32, 2812, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(887, '2018-03-28 00:00:00', 71, 32, 45625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(888, '2018-03-28 00:00:00', 72, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(889, '2018-03-28 00:00:00', 73, 32, 15625, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(890, '2018-03-28 00:00:00', 74, 32, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(891, '2018-03-28 00:00:00', 75, 32, 2813, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(892, '2018-03-28 00:00:00', 76, 32, 86875, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(893, '2018-03-28 00:00:00', 78, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(894, '2018-03-28 00:00:00', 83, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(895, '2018-03-28 00:00:00', 84, 32, 300000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(896, '2018-03-28 00:00:00', 99, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(897, '2018-03-28 00:00:00', 100, 32, 50000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(898, '2018-03-28 00:00:00', 101, 32, 45000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(899, '2018-03-28 00:00:00', 111, 32, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(900, '2018-03-28 00:00:00', 111, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(901, '2018-03-28 00:00:00', 111, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(902, '2018-03-28 00:00:00', 112, 32, 30000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(903, '2018-03-28 00:00:00', 112, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(904, '2018-03-28 00:00:00', 112, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(905, '2018-03-28 00:00:00', 113, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(906, '2018-03-18 00:00:00', 113, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(907, '2018-06-06 00:00:00', 75, 32, 10130, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(908, '2018-03-28 00:00:00', 115, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(909, '2018-03-28 00:00:00', 115, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(910, '2018-03-28 00:00:00', 116, 40, 20000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(911, '2018-03-28 00:00:00', 116, 41, 10000, '', 'Setoran', 'D', 1, '2019-05-13 09:52:00', 'admin', '', '', ''),
(912, '2018-06-06 00:00:00', 296, 32, 1250, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(913, '2018-03-28 00:00:00', 113, 41, 10000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', ''),
(914, '2018-03-28 00:00:00', 116, 32, 18000, '', 'Setoran', 'D', 1, '0000-00-00 00:00:00', 'admin', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` bigint(20) NOT NULL,
  `u_name` varchar(255) NOT NULL,
  `pass_word` varchar(255) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `level` enum('admin','operator','pinjaman') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `u_name`, `pass_word`, `aktif`, `level`) VALUES
(1, 'admin', '123456', 'Y', 'admin'),
(4, 'user', 'e22b7d59cb35d199ab7e54ed0f2ef58f5da5347b', 'Y', 'operator'),
(5, 'pinjaman', 'efd2770f6782f7218be595baf2fc16bc7cf45143', 'Y', 'pinjaman'),
(6, 'user2', '123456', 'Y', 'operator');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_hitung_pinjaman`
-- (See below for the actual view)
--
CREATE TABLE `v_hitung_pinjaman` (
`id` bigint(20)
,`tgl_pinjam` datetime
,`anggota_id` bigint(20)
,`lama_angsuran` bigint(20)
,`jumlah` int(11)
,`bunga` float(10,2)
,`biaya_adm` int(11)
,`lunas` enum('Belum','Lunas')
,`dk` enum('D','K')
,`kas_id` bigint(20)
,`user_name` varchar(255)
,`pokok_angsuran` decimal(14,4)
,`bunga_pinjaman` double
,`ags_per_bulan` double(29,12)
,`tempo` datetime
,`tagihan` double(29,12)
,`keterangan` varchar(255)
,`barang_id` bigint(20)
,`bln_sudah_angsur` bigint(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_transaksi`
-- (See below for the actual view)
--
CREATE TABLE `v_transaksi` (
`tbl` varchar(1)
,`id` bigint(20)
,`tgl` datetime
,`kredit` double
,`debet` double
,`dari_kas` bigint(20)
,`untuk_kas` bigint(20)
,`transaksi` bigint(20)
,`ket` varchar(255)
,`user` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `v_hitung_pinjaman`
--
DROP TABLE IF EXISTS `v_hitung_pinjaman`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_hitung_pinjaman`  AS  select `tbl_pinjaman_h`.`id` AS `id`,`tbl_pinjaman_h`.`tgl_pinjam` AS `tgl_pinjam`,`tbl_pinjaman_h`.`anggota_id` AS `anggota_id`,`tbl_pinjaman_h`.`lama_angsuran` AS `lama_angsuran`,`tbl_pinjaman_h`.`jumlah` AS `jumlah`,`tbl_pinjaman_h`.`bunga` AS `bunga`,`tbl_pinjaman_h`.`biaya_adm` AS `biaya_adm`,`tbl_pinjaman_h`.`lunas` AS `lunas`,`tbl_pinjaman_h`.`dk` AS `dk`,`tbl_pinjaman_h`.`kas_id` AS `kas_id`,`tbl_pinjaman_h`.`user_name` AS `user_name`,(`tbl_pinjaman_h`.`jumlah` / `tbl_pinjaman_h`.`lama_angsuran`) AS `pokok_angsuran`,(case when (`tbl_pinjaman_d`.`jml_jasa_angsuran` > 0) then `tbl_pinjaman_d`.`jml_jasa_angsuran` else (((`tbl_pinjaman_h`.`jumlah` / `tbl_pinjaman_h`.`lama_angsuran`) * `tbl_pinjaman_h`.`bunga`) / 100) end) AS `bunga_pinjaman`,(((((((`tbl_pinjaman_h`.`jumlah` / `tbl_pinjaman_h`.`lama_angsuran`) * `tbl_pinjaman_h`.`bunga`) / 100) + (`tbl_pinjaman_h`.`jumlah` / `tbl_pinjaman_h`.`lama_angsuran`)) + `tbl_pinjaman_h`.`biaya_adm`) * 100) / 100) AS `ags_per_bulan`,(`tbl_pinjaman_h`.`tgl_pinjam` + interval `tbl_pinjaman_h`.`lama_angsuran` month) AS `tempo`,((((((((`tbl_pinjaman_h`.`jumlah` / `tbl_pinjaman_h`.`lama_angsuran`) * `tbl_pinjaman_h`.`bunga`) / 100) + (`tbl_pinjaman_h`.`jumlah` / `tbl_pinjaman_h`.`lama_angsuran`)) + `tbl_pinjaman_h`.`biaya_adm`) * 100) / 100) * `tbl_pinjaman_h`.`lama_angsuran`) AS `tagihan`,`tbl_pinjaman_h`.`keterangan` AS `keterangan`,`tbl_pinjaman_h`.`barang_id` AS `barang_id`,ifnull(max(`tbl_pinjaman_d`.`angsuran_ke`),0) AS `bln_sudah_angsur` from (`tbl_pinjaman_h` left join `tbl_pinjaman_d` on((`tbl_pinjaman_h`.`id` = `tbl_pinjaman_d`.`pinjam_id`))) group by `tbl_pinjaman_h`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_transaksi`
--
DROP TABLE IF EXISTS `v_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_transaksi`  AS  select 'A' AS `tbl`,`tbl_pinjaman_h`.`id` AS `id`,`tbl_pinjaman_h`.`tgl_pinjam` AS `tgl`,`tbl_pinjaman_h`.`jumlah` AS `kredit`,0 AS `debet`,`tbl_pinjaman_h`.`kas_id` AS `dari_kas`,NULL AS `untuk_kas`,`tbl_pinjaman_h`.`jns_trans` AS `transaksi`,`tbl_pinjaman_h`.`keterangan` AS `ket`,`tbl_pinjaman_h`.`user_name` AS `user` from `tbl_pinjaman_h` union select 'B' AS `tbl`,`tbl_pinjaman_d`.`id` AS `id`,`tbl_pinjaman_d`.`tgl_bayar` AS `tgl`,0 AS `kredit`,`tbl_pinjaman_d`.`jumlah_bayar` AS `debet`,NULL AS `dari_kas`,`tbl_pinjaman_d`.`kas_id` AS `untuk_kas`,`tbl_pinjaman_d`.`jns_trans` AS `transaksi`,`tbl_pinjaman_d`.`keterangan` AS `ket`,`tbl_pinjaman_d`.`user_name` AS `user` from `tbl_pinjaman_d` union select 'C' AS `tbl`,`tbl_trans_sp`.`id` AS `id`,`tbl_trans_sp`.`tgl_transaksi` AS `tgl`,if((`tbl_trans_sp`.`dk` = 'K'),`tbl_trans_sp`.`jumlah`,0) AS `kredit`,if((`tbl_trans_sp`.`dk` = 'D'),`tbl_trans_sp`.`jumlah`,0) AS `debet`,if((`tbl_trans_sp`.`dk` = 'K'),`tbl_trans_sp`.`kas_id`,NULL) AS `dari_kas`,if((`tbl_trans_sp`.`dk` = 'D'),`tbl_trans_sp`.`kas_id`,NULL) AS `untuk_kas`,`tbl_trans_sp`.`jenis_id` AS `transaksi`,`tbl_trans_sp`.`keterangan` AS `ket`,`tbl_trans_sp`.`user_name` AS `user` from `tbl_trans_sp` union select 'D' AS `tbl`,`tbl_trans_kas`.`id` AS `id`,`tbl_trans_kas`.`tgl_catat` AS `tgl`,if((`tbl_trans_kas`.`dk` = 'K'),`tbl_trans_kas`.`jumlah`,if(isnull(`tbl_trans_kas`.`dk`),`tbl_trans_kas`.`jumlah`,0)) AS `kredit`,if((`tbl_trans_kas`.`dk` = 'D'),`tbl_trans_kas`.`jumlah`,if(isnull(`tbl_trans_kas`.`dk`),`tbl_trans_kas`.`jumlah`,0)) AS `debet`,`tbl_trans_kas`.`dari_kas_id` AS `dari_kas`,`tbl_trans_kas`.`untuk_kas_id` AS `untuk_kas`,`tbl_trans_kas`.`jns_trans` AS `transaksi`,`tbl_trans_kas`.`keterangan` AS `ket`,`tbl_trans_kas`.`user_name` AS `user` from `tbl_trans_kas` order by `tgl` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `jns_akun`
--
ALTER TABLE `jns_akun`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_aktiva` (`kd_aktiva`);

--
-- Indexes for table `jns_angsuran`
--
ALTER TABLE `jns_angsuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jns_simpan`
--
ALTER TABLE `jns_simpan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_area`
--
ALTER TABLE `mst_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_hubungan`
--
ALTER TABLE `mst_hubungan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_pendidikan`
--
ALTER TABLE `mst_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nama_kas_tbl`
--
ALTER TABLE `nama_kas_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suku_bunga`
--
ALTER TABLE `suku_bunga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pengajuan`
--
ALTER TABLE `tbl_pengajuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_pengajuan_ibfk_1` (`anggota_id`);

--
-- Indexes for table `tbl_pinjaman_d`
--
ALTER TABLE `tbl_pinjaman_d`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_pinjaman_d_ibfk_1` (`pinjam_id`),
  ADD KEY `tbl_pinjaman_d_ibfk_2` (`kas_id`),
  ADD KEY `tbl_pinjaman_d_ibfk_3` (`user_name`),
  ADD KEY `tbl_pinjaman_d_ibfk_4` (`jns_trans`);

--
-- Indexes for table `tbl_pinjaman_h`
--
ALTER TABLE `tbl_pinjaman_h`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kas_id` (`kas_id`),
  ADD KEY `tbl_pinjaman_h_ibfk_1` (`anggota_id`),
  ADD KEY `tbl_pinjaman_h_ibfk_3` (`user_name`),
  ADD KEY `tbl_pinjaman_h_ibfk_4` (`jns_trans`),
  ADD KEY `tbl_pinjaman_h_ibfk_5` (`barang_id`);

--
-- Indexes for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_trans_kas`
--
ALTER TABLE `tbl_trans_kas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_name` (`user_name`),
  ADD KEY `dari_kas_id` (`dari_kas_id`,`untuk_kas_id`),
  ADD KEY `untuk_kas_id` (`untuk_kas_id`),
  ADD KEY `jns_trans` (`jns_trans`);

--
-- Indexes for table `tbl_trans_sp`
--
ALTER TABLE `tbl_trans_sp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `anggota_id` (`anggota_id`),
  ADD KEY `jenis_id` (`jenis_id`),
  ADD KEY `kas_id` (`kas_id`),
  ADD KEY `user_name` (`user_name`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_name` (`u_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jns_akun`
--
ALTER TABLE `jns_akun`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `jns_angsuran`
--
ALTER TABLE `jns_angsuran`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `jns_simpan`
--
ALTER TABLE `jns_simpan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `mst_area`
--
ALTER TABLE `mst_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `mst_hubungan`
--
ALTER TABLE `mst_hubungan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mst_pendidikan`
--
ALTER TABLE `mst_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nama_kas_tbl`
--
ALTER TABLE `nama_kas_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `suku_bunga`
--
ALTER TABLE `suku_bunga`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=893;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_pengajuan`
--
ALTER TABLE `tbl_pengajuan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pinjaman_d`
--
ALTER TABLE `tbl_pinjaman_d`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `tbl_pinjaman_h`
--
ALTER TABLE `tbl_pinjaman_h`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_trans_kas`
--
ALTER TABLE `tbl_trans_kas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_trans_sp`
--
ALTER TABLE `tbl_trans_sp`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=915;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_pengajuan`
--
ALTER TABLE `tbl_pengajuan`
  ADD CONSTRAINT `tbl_pengajuan_ibfk_1` FOREIGN KEY (`anggota_id`) REFERENCES `tbl_anggota` (`id`);

--
-- Constraints for table `tbl_pinjaman_d`
--
ALTER TABLE `tbl_pinjaman_d`
  ADD CONSTRAINT `tbl_pinjaman_d_ibfk_1` FOREIGN KEY (`pinjam_id`) REFERENCES `tbl_pinjaman_h` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_d_ibfk_2` FOREIGN KEY (`kas_id`) REFERENCES `nama_kas_tbl` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_d_ibfk_3` FOREIGN KEY (`user_name`) REFERENCES `tbl_user` (`u_name`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_d_ibfk_4` FOREIGN KEY (`jns_trans`) REFERENCES `jns_akun` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pinjaman_h`
--
ALTER TABLE `tbl_pinjaman_h`
  ADD CONSTRAINT `tbl_pinjaman_h_ibfk_1` FOREIGN KEY (`anggota_id`) REFERENCES `tbl_anggota` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_h_ibfk_2` FOREIGN KEY (`kas_id`) REFERENCES `nama_kas_tbl` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_h_ibfk_3` FOREIGN KEY (`user_name`) REFERENCES `tbl_user` (`u_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_h_ibfk_4` FOREIGN KEY (`jns_trans`) REFERENCES `jns_akun` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pinjaman_h_ibfk_5` FOREIGN KEY (`barang_id`) REFERENCES `tbl_barang` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_trans_kas`
--
ALTER TABLE `tbl_trans_kas`
  ADD CONSTRAINT `tbl_trans_kas_ibfk_2` FOREIGN KEY (`user_name`) REFERENCES `tbl_user` (`u_name`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_trans_kas_ibfk_3` FOREIGN KEY (`dari_kas_id`) REFERENCES `nama_kas_tbl` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_trans_kas_ibfk_4` FOREIGN KEY (`untuk_kas_id`) REFERENCES `nama_kas_tbl` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_trans_kas_ibfk_5` FOREIGN KEY (`jns_trans`) REFERENCES `jns_akun` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_trans_sp`
--
ALTER TABLE `tbl_trans_sp`
  ADD CONSTRAINT `tbl_trans_sp_ibfk_1` FOREIGN KEY (`anggota_id`) REFERENCES `tbl_anggota` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_trans_sp_ibfk_2` FOREIGN KEY (`kas_id`) REFERENCES `nama_kas_tbl` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_trans_sp_ibfk_4` FOREIGN KEY (`jenis_id`) REFERENCES `jns_simpan` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_trans_sp_ibfk_5` FOREIGN KEY (`user_name`) REFERENCES `tbl_user` (`u_name`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
