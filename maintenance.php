<link href="http://dev.siapikagraindonesia.com/assets/ela/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="http://dev.siapikagraindonesia.com/assets/ela/css/helper.css" rel="stylesheet">
    <link href="http://dev.siapikagraindonesia.com/assets/ela/css/style.css" rel="stylesheet">
    <!-- Main wrapper  -->
    <div class="error-page" id="wrapper">
        <div class="error-box">
        

            <div class="error-body text-center">
           
                <h1>400</h1>
                <h3 class="text-uppercase">Server under maintenance </h3>
                <p class="text-muted m-t-30 m-b-30">Cobalah beberapa saat lagi</p>
                <a class="btn btn-info btn-rounded waves-effect waves-light m-b-40" href="https://www.siapikagraindonesia.com/">Back to home</a>
<br>
                <span>
                    <img src="https://www.siapikagraindonesia.com/assets/ela/images/logo.png" alt="homepage" class="dark-logo" />

                    <img src="https://www.siapikagraindonesia.com/assets/ela/images/logo-text.png" alt="homepage" class="dark-logo" />
                </span>
            </div>
        </div>
    </div>
    <footer class="footer fixed-bottom text-center no-border"> © 2018 All rights reserved. <font color="red">C</font>redit <font color="red">I</font>nsurance
        <font color="red">A</font>pplication <font color="red">S</font>ystem by <a href="http://agraindonesia.com/"><font class="text-info">Agra
            Indonesia</font></a>

    </footer> <!-- End footer -->


